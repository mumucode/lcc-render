仓库地址:[**lcc-render**](https://gitee.com/nomat/lcc-render "lcc-render")

# lcc-render
Cocos Creator自定义渲染框架，更便捷，更自由的构建渲染效果。

## 前言
最近在Cocos论坛上看到很多人发的Shader效果，自己也想使用这些效果。但是发现大家发的Shader在Creator里面并不是太好使用，大家的Shader并没有考虑到Creator自己的合图与渲染合批功能，使用限制比较大。于是就有了这个项目。</br>
本人最早是做cocos2dx的，接触creator没有多久，基本算是直接用吧，可能下面的代码会有更好的实现方式。

## 安装
安装十分简单，只要把这个项目作为Creator插件放到插件目录就可以了,具体请查看[**Creator插件包**](https://docs.cocos.com/creator/manual/zh/extension/your-first-extension.html "Creator插件包")。</br>

## 使用
如果你想使用插件中内置的效果，你可以直接在节点上添加效果组件，如图：
</br>![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/QQ截图20201111114527.jpg)</br>

你可以直接添加一个以`Effect*`开始的效果组件，当然添加完后，一般需要再把渲染的图片拖动到自动添加的`lcc$render.ShaderSpriteFrame`组件里面。如图:
</br>![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/QQ截图20201021212711.png)</br>

好了，渲染效果应该已经出来了，你可以通过修改`Effect*`组件上的参数改变渲染效果。当然，如果你想要定制你的渲染，那么就需要看看下面的文档了。</br>
其实渲染效果的基本参数都可以在`Effect*`里面设置，比如这个马赛克效果组件:
</br>![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/QQ截图20201022095118.jpg)</br>
* 当然上面只是框架上附带的一些懒人包效果，多谢Cocos论坛上提供效果的大佬们。
* 再看框架设计前，你应该具备一些基本的shader知识。

# 框架设计
框架并不是单个渲染组件，而是把节点作为渲染的容器。各种渲染参数通过组件的方式加入渲染系统中。渲染组件主要分为:
* 渲染系统`RenderSystem` </br>这类组件继承了`RenderComponent`组件，主要负责渲染组织和刷新流程等操作。具体比如更新材质，属性，形状，顶点等。
* 着色器组件`ShaderComponent` </br>着色器组件就是具体的渲染数据或则控制数据，比如 SpriteFrame, Color, Float, Vec2, Vec3, Vec4, Macro等。这类组件与向着色器传递的数据相关。

## 1、传递什么数据到Shader中?
框架通过`着色器组件`组织需要向Shader传递的数据。比如我们需要渲染一张图片,那么`ShaderSpriteFrame`组件应该是需要加入到渲染系统的，这个组件会负责把图片的数据传递到Shader里面；但是如果我们需要做`Shader遮罩`那么我们可能需要再添加一个 `ShaderSpriteFrame` 来把遮罩图片的数据传递进去；嗯，如果再想自定义下颜色，我们就再加一个 `ShaderColor` 组件。传递数据的大小是可控的，可以只传递需要的数据。

## 2、如何传递数据到Shader中？
一般我们向Shader中传递数据的方式有两种:
* `uniform` 以常量的方式传递，在Creator中我们可以通过获取渲染组件的材质直接设置属性的值。
优势是简单；缺点是不利于渲染合批。
* `attribute` 顶点数据的方式传递，在现在的Creator中我们不自定义渲染的类是办不到的。优势是可以合批渲染；缺点是现在Creator中不好实现，会消耗更多的内存。

但是特别是在需要大量渲染的地方，渲染合批是很重要的。所以框架使用了`自定义数据传递方式`，我们可以在着色器组件中选择每个数据传递的方式。当然有些数据是应该用指定的方式传递的，这类数据不能切换方式。</br>
我们可以根据一个效果图对比:<br>
* 这是使用`uniform`方式传递数据的效果:
</br>![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/QQ截图20201022093602.jpg)</br>
* 这是使用`attribute`方式传递数据的效果:
</br>![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/QQ截图20201022093525.jpg)</br>

可以看出使用attribute方式`Draw Call` 数量少得多。</br>
`
可能人看到上面的两张对比图片有问题，FrameTime在attribute方式下还要大一些。主要是因为统计的时间太小了，cocos统计的波动时间也差不多这个精度。上面只做了10张图片时候的对比，如果把图片对比数量提高到50张；那么就会明显发现attribute方式的FrameTime差不多还是上面的时间，但是uniform方式的FrameTime会大4倍左右。
`

## 3、如何自定义数据传递方式？
得益于Creator的Shader定义可以使用宏,比如:
```
#if USE_TEXTURE
uniform sampler2D texture;
#endif
```
通过宏定义我们可以动态裁剪Shader代码。</br>比如我们在Shader的顶点着色器写入下面的代码[1]:
```
#if ATTR_COLOR
in vec4 a_color;
out vec4 color;
#elif UNIF_COLOR
uniform UNIF_COLOR {
	vec4 u_color;
};
out vec4 color;
#else
vec4 D_color = vec4(1,1,1,1);
out vec4 color;
#endif
```
如果我们想使用顶点传递颜色(顶点可以使用不同的颜色)，那么我们可以在材质上定义`ATTR_COLOR`为`true`, `UNIF_COLOR`为`false`，然后通过顶点传递数据。如果我们想使用常量，可以定义`ATTR_COLOR`为`false`, `UNIF_COLOR`为`true`，然后定义常量。当然还有个默认的值，如果我们都定义为`false`的话。</br>
在使用数据的时候我们也可以通过宏判断操作，如下；
```
if ATTR_COLOR || UNIF_COLOR
	// ... ... 这是如果传递了数据到着色器的操作
#endif
```
那么顶点着色器该怎么传递这些数据呢，很简单:
```
// 定义颜色
// 上面的代码段[1]

...

void main(){
	...
#if ATTR_COLOR
	color = a_color;    // 顶点颜色传递过去了
#elif UNIF_COLOR
    color = u_color;    // 常量颜色传递过去了
#else
    color = D_color;    // 默认颜色传递过去了
#endif
	...
    // 这里也可以使用color变量
    ...
}
```
然后就是片段着色器如何使用了，更简单:
```
...
in vec4 color;
...

void main(){
    ...
    gl_FragColor = color; // 使用color颜色
    ...
}
```
哈哈，看着是不是感觉还是要写很多东西，但是我们有宏，可以简化这些操作。
```
############## 顶点着色器
// 定义颜色
LCC_VERT_VALUE_DEFINE(COLOR, vec4, color, vec4(1.0,1.0,1.0,1.0))

...

void main(){
	...
// 传递颜色
LCC_VERT_VALUE_PASS(COLOR, color)
	...
    // 这里也可以使用 color
}

############## 片段着色器

// 导入颜色
LCC_FRAG_VALUE_IMPORT(vec4, color)

...

void main(){
	...
	gl_FragColor = color; // 这里color就随便用了
	...
}
```
* 由于还没有怎么搞清楚插件怎么定义shader的include文件。所以现在框架都是手动添加的，具体的defines文件路径 `lcc-render/framework/src/render/build/lcc-defines.inc` 

## 4、着色器组件上如何选择传递的方式？
在着色器组件上的变量基本上都定义了2种方式，如图:
* `UNIFORM` 常量格式
![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/QQ截图20201021230131.png)</br>
  `Unif Macro`表示这个常量使用的宏，`Name`是宏名称，`Check Only`表示这个常量的宏只作为检测使用。
  * `所谓检测使用 表示只有前面定义了这个宏为true的时候，这个常量才启用，传递其值` 这个用在多个数据绑定上，比如：</br>
  	```
	#if USE_TEXTURE
		in vec2 a_uv;
		out vec2 uv;
	#endif
 	```
	只有在定义了`USE_TEXTURE`才会传递`a_uv`, 没有纹理，他的UV也没有作用。

  `Unif Name` 表示这个常量在Shader中的名称。

* `ATTRIBUTE` 顶点数据
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/QQ截图20201021230223.png)</br>
  顶点数据的`Attr Macro` 与 `Name` 和上面常量意义相同， `特别注意的是如果是通过顶点传递数据，可以给每个顶点分配不一样的数据，比如给图片的四个点设置不同的颜色`<br>

# 着色器组件介绍

## ShaderSpriteFrame

这个组件和图片相关，如果你的渲染效果需要图片的话需要添加这个组件，需要多少张图片就可以添加多少个。
![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/QQ截图20201021232912.png)</br>
* `Apply Shape` 是否应用当前图片的尺寸，如果选择，那么渲染的尺寸以下面的设置为准(`如果整个渲染系统中没有组件应用形状，则会用节点矩形的形状`)。
* `Shape Type` 形状的类型，可以直接使用节点的形状也可以自定义。
* `Trim` 和`Sprite`组件一致，配合上面的形状可以实现`Sprite`中的Simple模式。
* `Apply UV` 应用这张图片的UV到Shader中。
* `Custom UV` 可以定义UV顶点的顺序。
* `Apply UVRect` 应用UV在合图后的rect矩形到Shader中，`再也不用关闭合图了，但是你可能要计算真实UV与图片UV的关系，宏定义提供了宏函数`。
* `Apply Frame Size` 这个可以算是传递图片的像素尺寸到Shader中，一些效果比如高斯模糊需要。

## ShaderColor

这个组件和颜色有关，如果你的渲染效果需要传递颜色的话，需要添加这个组件。
![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/QQ截图20201021233033.png)</br>
* `Apply Node Color` 直接应用节点的颜色。
* `Color Type` 可以定义单色，也可以定义每个顶点颜色。
* `Vertex Colors` 每个顶点的颜色， 顺序是 左下， 右下， 左上， 右上。可以在修改的时候直接看变化。

## ShaderFloat/ShaderVec2/ShaderVec3/ShaderVec4
这一些数值的着色器组件只有数据类型不一样，具体用法包含在`ShaderColor`里面。

## ShaderMacro

这个着色器组件主要是可以自由定义Shader中的宏。
![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/QQ截图20201021234849.png)</br>
* `Macro Name` 定义的宏的名称。
* `Check Macro` 如果不为空，则会检测这个宏是否为`true`, 如果为`true`才会定义这个组件的宏。
* `Value Type` 宏的值类型，有`Value`数值和`Bool`布尔。
  * 注意Shader中的宏，定义值的方式。
	```
	#pragma define LAYERS range([4, 5])
	#pragma define METALLIC_SOURCE options([r, g, b, a]) 
	```
	[**Effect 语法**](https://docs.cocos.com/creator3d/manual/zh/material-system/effect-syntax.html "Effect 语法")
* `Bool` 布尔值。
* `Value` 数值。

## Effect*系列具体效果组件
这些组件基本上就是在组装多个着色器组件，并且统一控制他们，具体效果都是使用这类组件。可以看看框架的Effect*组件类源码。</br>
当前框架中内置的效果组件:
* `EffectFlashLight` 扫光效果，可以配置扫光速度，角度，数量，间距，宽度等参数。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectFlashLight.gif)</br>
* `EffectFluxaySuper` 流光效果，可以配置速度。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectFluxaySuper.gif)</br>
* `EffectGaussianBlur` 高斯模糊，因为如果太大会有性能问题，所以没有开放配置接口。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectGaussianBlur.jpg)</br>
* `EffectGlowInner` 内发光效果，可以配置颜色，宽度，阈值和闪烁等。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectGlowInner.jpg)</br>
* `EffectGlowOutter` 外发光效果，和内发光效果差不多。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectGlowOutter.jpg)</br>
* `EffectMosaic` 马赛克效果，可以配置程度(0-1)。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectMosaic.jpg)</br>
* `EffectOutline` 外轮廓线效果，可以配置颜色，宽度，闪烁等。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectOutline.gif)</br>
* `EffectPhoto` 照片效果，可以配置老照片/变灰/反相/冰冻/卡通，程度等。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectPhoto.jpg)</br>
* `EffectRoundCornerCrop` 圆角裁剪效果，可以配置程度等。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectRoundCornerCrop.jpg)</br>
* `EffectSpriteMask` Shader遮罩效果，配置原图和遮罩图等，`为了达到最佳的效果，最好关闭图片的自动裁剪并且原图和遮罩图的大小一样`。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectSpriteMask.jpg)</br>
* `EffectDissolve` 溶解效果，配置噪音图，程度等。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectDissolve.jpg)</br>
* `FffectSmoothTransform` 平滑过渡效果，配置过渡宽度，过渡方向，程度等。
* `EffectRainDropRipples` 下雨波纹效果，配置波纹的密度，动态改变等。
  ![avatar](https://gitee.com/nomat/lcc-render/raw/master/docs/EffectRainDropRipples.gif)</br>

其实，在添加了这些内置效果组件后，你同样可以再添加其他的着色器组件，只要shader里面支持对应的数据。比如内置的马赛克组件是没有设置颜色的，你完全可以自己在节点上添加一个ShaderColor组件以控制颜色。

# 结束语
这个框架简化并放开了Creator向Shader中传递数据的方式。我们可以把更多精力放在写好Shader上面，然后通过这个框架根据实际情况调整数据传递方式，以达到满意效果。框架内现在内置了部分效果，也会不定期更新从其他地方(比如shadertoy)移植过来的效果。

