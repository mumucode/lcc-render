
module lcc$render {

/**
 * 框架使用的UUID表
 */
export let UUID = {
	/**
	 * 渲染程序
	 */
	effects : {
		["lcc-2d-flash_light"] : "9350cd53-72d3-445e-9b64-6025f9b033f3",
		["lcc-2d-fluxay_super"] : "e0032378-75a3-404a-ae4e-fe63bd91c875",
		["lcc-2d-sprite_mask"] : "6f8dd8eb-f50d-4d02-8ae7-46d4bb3f73fa",
		["lcc-2d_glow_inner"] : "8bf46498-6576-462b-8168-7640ce52198c",
		["lcc-2d_glow_outter"] : "5b49d2f4-9343-47a6-84f4-2bfeeaf0eb63",
		["lcc-2d_mosaic"] : "df7ce505-6676-4f9d-8bae-5f87bd53f1c2",
		["lcc-2d_photo"] : "d3414b31-e561-40e8-b78e-0ad38f8bc7c9",
		["lcc-2d_outline"] : "e0be8740-5375-4ae9-b246-784c147bbfb9",
		["lcc-2d_gaussian_blur"] : "41f4d474-d707-45bb-af93-637573f92d54",
		["lcc-2d_round_corner_crop"] : "7c24b57e-e819-4fc9-a8d2-b06cf61b782d",
		["lcc-2d_dissolve"] : "ab297d98-4c09-4ca1-945c-a7fa8e290bca",
		["lcc-2d_transform"] : "1bef22fb-8433-4049-ad35-1fbf9e7348ad",
		["lcc-2d_rain_drop_ripples"] : "f345e656-06d2-42b3-89ec-938182b32113",
	},

	/**
	 * 材质
	 */
	materials : {
		["lcc-2d-flash_light"] : "58ce2696-4b7b-480e-af98-53b5bae48be3",
		["lcc-2d-fluxay_super"] : "46b159f9-9673-4c36-9826-b98e5af85f19",
		["lcc-2d-sprite_mask"] : "5832437a-f25e-4094-8c11-d14e5be5f4f9",
		["lcc-2d_glow_inner"] : "417f75cf-08d7-4d0f-82f7-299c79529eaa",
		["lcc-2d_glow_outter"] : "2380adcd-9331-4eb8-9b73-5c9bb2d72d8d",
		["lcc-2d_mosaic"] : "4b6195fb-f172-4a97-b618-18aa51ebf3aa",
		["lcc-2d_photo"] : "06e41864-4dc6-489a-8867-95e829db0e5b",
		["lcc-2d_outline"] : "e24b3cff-b2e0-49aa-b115-1370964437dc",
		["lcc-2d_gaussian_blur"] : "dd3d8f78-9b79-4ca7-9bf7-7a09f7b34108",
		["lcc-2d_round_corner_crop"] : "a86e8864-5390-443f-b41b-b38e9d584c43",
		["lcc-2d_dissolve"] : "4659d83b-f274-42c4-874e-6d5f3e220adc",
		["lcc-2d_transform"] : "445e85ce-defc-47c6-821f-eaaf831533fb",
		["lcc-2d_rain_drop_ripples"] : "67bace4b-a406-4378-98a9-319ea18e9c6d",
	}
}

}
