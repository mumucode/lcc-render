
//----------------------------------------------------------------- defines
	
// 属性宏
#define LCC_MACRO_ATTRIBUTE(macro)	    ATTR_##macro

// 常量宏
#define LCC_MACRO_UNIFORM(macro)		UNIF_##macro

//---------------- 变量操作

// 顶点着色器 - 变量定义
#define LCC_VALUE_VERT_DEFINE(macro, type, name, defval)	\
	#if ATTR_##macro	\
	in type a_##name;	\
	out type name;	\
    #elif UNIF_##macro	\
	uniform UNIF_##macro {	\
		type u_##name;	\
	};	\
	out type name;	\
	#else	\
	type D_##name = defval;	\
	out type name;	\
	#endif

// 顶点着色器 - 变量传递
#define LCC_VALUE_VERT_PASS(macro, name)	\
	#if ATTR_##macro	\
	name = a_##name;	\
    #elif UNIF_##macro	\
    name = u_##name;	\
    #else	\
    name = D_##name;	\
	#endif

// 片段着色器 - 变量定义
#define LCC_VALUE_FRAG_IMPORT(type, name)	\
	in type name;

//---------------- 独立属性操作

// 顶点着色器 - 如果定义了宏，属性定义
#define LCC_ATTR_VERT_DEFINE_IF(fmacro, type, name)	\
	#if fmacro	\
	in type a_##name;	\
	out type name;	\
	#endif

// 顶点着色器 - 如果定义了宏，属性定传递
#define LCC_ATTR_VERT_PASS_IF(fmacro, name)	\
	#if fmacro	\
	name = a_##name;	\
	#endif

// 片段着色器 - 如果定义了宏，属性导入
#define LCC_ATTR_FRAG_IMPORT_IF(fmacro, type, name)	\
	#if fmacro	\
	in type name;	\
	#endif

//---------------- 数组操作

// 数组宏
#define LCC_MACRO_ARRAY(macro)		UNIF_##macro

// 数组定义
#define LCC_ARRAY_DEFINE(macro, type, name, count)
    #if UNIF_##macro	\
	uniform UNIF_##macro {	\
		type u_##name[count];	\
	};	\
	#endif

// 数组
#define LCC_ARRAY(name)             u_##name

//---------------- UV 转换

// UV 转换到本地UV
#define LCC_UV_TO_LOCALUV(uv, uvrect) \
	vec2((uv.x - uvrect[0]) / uvrect[2], (uv.y - uvrect[1]) / uvrect[3])

// 本地UV 转换到UV
#define LCC_LOCALUV_TO_UV(luv, uvrect)	\
	vec2(uvrect[0] + luv.x * uvrect[2], uvrect[1] + luv.y * uvrect[3])
//----------------------------------------------------------------- 
