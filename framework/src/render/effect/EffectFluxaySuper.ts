/// <reference path = "../shader/ShaderComponent.ts" />

module lcc$render {

const {ccclass, property, menu} = cc._decorator;

@ccclass("lcc$render.EffectFluxaySuper")
@menu("i18n:lcc-render.menu_component/EffectFluxaySuper")
export class EffectFluxaySuper extends ShaderComponent {

    @property(cc.Material)
	_material: cc.Material = null;
	@property({
		type : cc.Material,
		tooltip : "效果材质"
	})
	get material(){
		return this._material;
	}
	set material(value:cc.Material){
		if(this._material != value){
			this._material = value;
			this.renderSystem.setMaterial(0, value);
		}
	}

    @property(cc.SpriteFrame)
	_spriteFrame: cc.SpriteFrame = null;
	@property({
		type : cc.SpriteFrame,
		tooltip : "效果精灵帧"
	})
	get spriteFrame(){
		return this._spriteFrame;
	}
	set spriteFrame(value:cc.SpriteFrame){
		if(this._spriteFrame != value){
			this._spriteFrame = value;
			this.shaderSpriteFrame.spriteFrame = value;
		}
	}

    @property()
	_speed: number = 1;
	@property({
		tooltip : "效果速度"
	})
	get speed(){
		return this._speed;
	}
	set speed(value:number){
		if(this._speed != value){
			this._speed = value;
			this.shaderSpeed.value = value;
		}
	}
	
	renderSystem:RenderSystem = null;
	shaderSpriteFrame:ShaderSpriteFrame = null;
	shaderSpeed:ShaderFloat = null;

    // LIFE-CYCLE CALLBACKS:

    onEnable () {
		if(!this.shaderSpriteFrame){
			this.shaderSpriteFrame = this.getShaderComponent(ShaderSpriteFrame, "spriteframe");
			if(!this.shaderSpriteFrame){
				let shader = this.addComponent(ShaderSpriteFrame);
				shader.tag = "spriteframe";
				shader.useUV = true;
				shader.useShape = true;
				this.shaderSpriteFrame = shader;
			}
		}
		if(!this.shaderSpeed){
			this.shaderSpeed = this.getShaderComponent(ShaderFloat, "speed");
			if(!this.shaderSpeed){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "speed";
				shader._valueVar.attrMacro.name = "ATTR_SPEED";
				shader._valueVar.attrName = "a_speed";
				shader._valueVar.unifMacro.name = "UNIF_SPEED";
				shader._valueVar.unifName = "u_speed";
				shader.value = 1;
				this.shaderSpeed = shader;
			}
		}
		if(!this.renderSystem){
			this.renderSystem = this.getComponent(RenderSystem);
			if(!this.renderSystem){
				this.renderSystem = this.addComponent(RenderSystem);
			}
		}
		if(CC_EDITOR){
			this.checkMaterial();
		}
	}

	/**
	 * 检查材质
	 */
	async checkMaterial(){
		if(CC_EDITOR){
			if(!this._material){
				this.material = await Utils.getAssetByUUID<cc.Material>(UUID.materials["lcc-2d-fluxay_super"]);
			}
		}
	}
}

}
