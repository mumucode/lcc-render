/// <reference path = "../shader/ShaderComponent.ts" />

module lcc$render {

const {ccclass, property, menu} = cc._decorator;

@ccclass("lcc$render.EffectRainDropRipples")
@menu("i18n:lcc-render.menu_component/EffectRainDropRipples")
export class EffectRainDropRipples extends ShaderComponent {

    @property(cc.Material)
	_material: cc.Material = null;
	@property({
		type : cc.Material,
		tooltip : "效果材质"
	})
	get material(){
		return this._material;
	}
	set material(value:cc.Material){
		if(this._material != value){
			this._material = value;
			this.renderSystem.setMaterial(0, value);
		}
	}

    @property(cc.SpriteFrame)
	_spriteFrame: cc.SpriteFrame = null;
	@property({
		type : cc.SpriteFrame,
		tooltip : "效果精灵帧"
	})
	get spriteFrame(){
		return this._spriteFrame;
	}
	set spriteFrame(value:cc.SpriteFrame){
		if(this._spriteFrame != value){
			this._spriteFrame = value;
			this.shaderSpriteFrame.spriteFrame = value;
		}
	}

    @property()
	_density: number = 15.0;
	@property({
		tooltip : "密度",
	})
	get density(){
		return this._density;
	}
	set density(value:number){
		if(this._density != value){
			this._density = value;
			this.shaderDensity.value = value;
		}
	}

    @property()
	_change: boolean = true;
	@property({
		tooltip : "动态改变",
	})
	get change(){
		return this._change;
	}
	set change(value:boolean){
		if(this._change != value){
			this._change = value;
			this.shaderChange.value = value ? 1 : 0;
		}
	}

	renderSystem:RenderSystem = null;
	shaderSpriteFrame:ShaderSpriteFrame = null;
	shaderDensity:ShaderFloat = null;
	shaderChange:ShaderFloat = null;

    // LIFE-CYCLE CALLBACKS:

    onEnable () {
		if(!this.shaderSpriteFrame){
			this.shaderSpriteFrame = this.getShaderComponent(ShaderSpriteFrame, "spriteframe");
			if(!this.shaderSpriteFrame){
				let shader = this.addComponent(ShaderSpriteFrame);
				shader.tag = "spriteframe";
				shader.useShape = true;
				shader.useUV = true;
				shader.useUVRect = true;
				shader.useFrameSize = true;
				this.shaderSpriteFrame = shader;
			}
        }
		if(!this.shaderDensity){
			this.shaderDensity = this.getShaderComponent(ShaderFloat, "density");
			if(!this.shaderDensity){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "density";
				shader._valueVar.attrMacro.name = "ATTR_DENSITY";
				shader._valueVar.attrName = "a_density";
				shader._valueVar.unifMacro.name = "UNIF_DENSITY";
				shader._valueVar.unifName = "u_density";
				this.shaderDensity = shader;
				shader.value = this._density;
			}
        }
		if(!this.shaderChange){
			this.shaderChange = this.getShaderComponent(ShaderFloat, "change");
			if(!this.shaderChange){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "change";
				shader._valueVar.attrMacro.name = "ATTR_CHANGE";
				shader._valueVar.attrName = "a_change";
				shader._valueVar.unifMacro.name = "UNIF_CHANGE";
				shader._valueVar.unifName = "u_change";
				this.shaderChange = shader;
				shader.value = this._change ? 1 : 0;
			}
        }
		if(!this.renderSystem){
			this.renderSystem = this.getComponent(RenderSystem);
			if(!this.renderSystem){
				this.renderSystem = this.addComponent(RenderSystem);
			}
		}
		if(CC_EDITOR){
			this.checkMaterial();
		}
	}
    
	/**
	 * 检查材质
	 */
	async checkMaterial(){
		if(CC_EDITOR){
			if(!this._material){
				this.material = await Utils.getAssetByUUID<cc.Material>(UUID.materials["lcc-2d_rain_drop_ripples"]);
			}
		}
	}
}

}
