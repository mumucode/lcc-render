/// <reference path = "../shader/ShaderComponent.ts" />

module lcc$render {

const {ccclass, property, menu} = cc._decorator;

@ccclass("lcc$render.EffectGlowInner")
@menu("i18n:lcc-render.menu_component/EffectGlowInner")
export class EffectGlowInner extends ShaderComponent {

    @property(cc.Material)
	_material: cc.Material = null;
	@property({
		type : cc.Material,
		tooltip : "效果材质"
	})
	get material(){
		return this._material;
	}
	set material(value:cc.Material){
		if(this._material != value){
			this._material = value;
			this.renderSystem.setMaterial(0, value);
		}
	}

    @property(cc.SpriteFrame)
	_spriteFrame: cc.SpriteFrame = null;
	@property({
		type : cc.SpriteFrame,
		tooltip : "效果精灵帧"
	})
	get spriteFrame(){
		return this._spriteFrame;
	}
	set spriteFrame(value:cc.SpriteFrame){
		if(this._spriteFrame != value){
			this._spriteFrame = value;
			this.shaderSpriteFrame.spriteFrame = value;
		}
	}

    @property(cc.Color)
	_glowColor: cc.Color = cc.Color.YELLOW;
	@property({
		type : cc.Color,
		tooltip : "发光颜色"
	})
	get glowColor(){
		return this._glowColor;
	}
	set glowColor(value:cc.Color){
		this._glowColor = value;
		this.shaderGlowColor.color = value;
	}

    @property()
	_glowColorSize: number = 0.2;
	@property({
		tooltip : "发光宽度",
		range : [0, 1, 0.1],
	})
	get glowColorSize(){
		return this._glowColorSize;
	}
	set glowColorSize(value:number){
		if(this._glowColorSize != value){
			this._glowColorSize = value;
			this.shaderGlowColorSize.value = value;
		}
	}

    @property()
	_glowThreshold: number = 0.1;
	@property({
		tooltip : "发光阈值",
		range : [0, 1, 0.1],
	})
	get glowThreshold(){
		return this._glowThreshold;
	}
	set glowThreshold(value:number){
		if(this._glowThreshold != value){
			this._glowThreshold = value;
			this.shaderGlowThreshold.value = value;
		}
	}

    @property()
	_glowFlash: number = 0.0;
	@property({
		tooltip : "发光闪烁速度",
	})
	get glowFlash(){
		return this._glowFlash;
	}
	set glowFlash(value:number){
		if(this._glowFlash != value){
			this._glowFlash = value;
			this.shaderGlowFlash.value = value;
		}
	}

	renderSystem:RenderSystem = null;
	shaderSpriteFrame:ShaderSpriteFrame = null;
	shaderGlowColor:ShaderColor = null;
	shaderGlowColorSize:ShaderFloat = null;
	shaderGlowThreshold:ShaderFloat = null;
	shaderGlowFlash:ShaderFloat = null;

    // LIFE-CYCLE CALLBACKS:

    onEnable () {
		if(!this.shaderSpriteFrame){
			this.shaderSpriteFrame = this.getShaderComponent(ShaderSpriteFrame, "spriteframe");
			if(!this.shaderSpriteFrame){
				let shader = this.addComponent(ShaderSpriteFrame);
				shader.tag = "spriteframe";
				shader.useShape = true;
				shader.useUV = true;
				shader.useUVRect = true;
				this.shaderSpriteFrame = shader;
			}
		}
		if(!this.shaderGlowColor){
			this.shaderGlowColor = this.getShaderComponent(ShaderColor, "glowColor");
			if(!this.shaderGlowColor){
				let shader = this.addComponent(ShaderColor);
				shader.tag = "glowColor";
				shader.useNodeColor = false;
				shader._colorVar.attrMacro.name = "ATTR_GLOWCOLOR";
				shader._colorVar.attrName = "a_glowColor";
				shader._colorVar.unifMacro.name = "UNIF_GLOWCOLOR";
				shader._colorVar.unifName = "u_glowColor";
				this.shaderGlowColor = shader;
				shader.color = this._glowColor;
			}
		}
		if(!this.shaderGlowColorSize){
			this.shaderGlowColorSize = this.getShaderComponent(ShaderFloat, "glowColorSize");
			if(!this.shaderGlowColorSize){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "glowColorSize";
				shader._valueVar.attrMacro.name = "ATTR_GLOWCOLORSIZE";
				shader._valueVar.attrName = "a_glowColorSize";
				shader._valueVar.unifMacro.name = "UNIF_GLOWCOLORSIZE";
				shader._valueVar.unifName = "u_glowColorSize";
				this.shaderGlowColorSize = shader;
				shader.value = this._glowColorSize;
			}
		}
		if(!this.shaderGlowThreshold){
			this.shaderGlowThreshold = this.getShaderComponent(ShaderFloat, "glowThreshold");
			if(!this.shaderGlowThreshold){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "glowThreshold";
				shader._valueVar.attrMacro.name = "ATTR_GLOWTHRESHOLD";
				shader._valueVar.attrName = "a_glowThreshold";
				shader._valueVar.unifMacro.name = "UNIF_GLOWTHRESHOLD";
				shader._valueVar.unifName = "u_glowThreshold";
				this.shaderGlowThreshold = shader;
				shader.value = this._glowThreshold;
			}
		}
		if(!this.shaderGlowFlash){
			this.shaderGlowFlash = this.getShaderComponent(ShaderFloat, "glowFlash");
			if(!this.shaderGlowFlash){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "glowFlash";
				shader._valueVar.attrMacro.name = "ATTR_GLOWFLASH";
				shader._valueVar.attrName = "a_glowFlash";
				shader._valueVar.unifMacro.name = "UNIF_GLOWFLASH";
				shader._valueVar.unifName = "u_glowFlash";
				this.shaderGlowFlash = shader;
				shader.value = this._glowFlash;
			}
		}
		if(!this.renderSystem){
			this.renderSystem = this.getComponent(RenderSystem);
			if(!this.renderSystem){
				this.renderSystem = this.addComponent(RenderSystem);
			}
		}
		if(CC_EDITOR){
			this.checkMaterial();
		}
	}

	/**
	 * 检查材质
	 */
	async checkMaterial(){
		if(CC_EDITOR){
			if(!this._material){
				this.material = await Utils.getAssetByUUID<cc.Material>(UUID.materials["lcc-2d_glow_inner"]);
			}
		}
	}
}

}
