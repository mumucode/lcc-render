/// <reference path = "../shader/ShaderComponent.ts" />

module lcc$render {

const {ccclass, property, menu} = cc._decorator;

/**
 * 过渡方向
 */
enum FadeDirect {
	LEFT_TO_RIGHT = 0,	// 左到右
	RIGHT_TO_LEFT,		// 右到左
	TOP_TO_BOTTOM,		// 上到下
	BOTTOM_TO_TOP,		// 下到上
}

/**
 * 平滑过渡效果
 */
@ccclass("lcc$render.EffectSmoothTransform")
@menu("i18n:lcc-render.menu_component/EffectSmoothTransform")
export class EffectSmoothTransform extends ShaderComponent {

	@property(cc.Material)
	_material: cc.Material = null;
	@property({
		type : cc.Material,
		tooltip : "效果材质"
	})
	get material(){
		return this._material;
	}
	set material(value:cc.Material){
		if(this._material != value){
			this._material = value;
			this.renderSystem.setMaterial(0, value);
		}
	}

	@property(cc.SpriteFrame)
	_spriteFrame: cc.SpriteFrame = null;
	@property({
		type : cc.SpriteFrame,
		tooltip : "效果精灵帧"
	})
	get spriteFrame(){
		return this._spriteFrame;
	}
	set spriteFrame(value:cc.SpriteFrame){
		if(this._spriteFrame != value){
			this._spriteFrame = value;
			this.shaderSpriteFrame.spriteFrame = value;
		}
	}
	
	@property()
	_fadeWidth: number = 0.2;
	@property({
		tooltip : "过渡宽度",
		range : [0, 1, 0.01],
	})
	get fadeWidth(){
		return this._fadeWidth;
	}
	set fadeWidth(value:number){
		if(this._fadeWidth != value){
			this._fadeWidth = value;
			this.shaderFadeWidth.value = value;
		}
	}

    @property({
		type : cc.Enum(FadeDirect)
	})
	_fadeDirect: FadeDirect = FadeDirect.LEFT_TO_RIGHT;
	@property({
		tooltip : "过渡方向",
		type : cc.Enum(FadeDirect)
	})
	get fadeDirect(){
		return this._fadeDirect;
	}
	set fadeDirect(value:FadeDirect){
		if(this._fadeDirect != value){
			this._fadeDirect = value;
			this.shaderFadeDirect.value = value;
		}
	}

	@property()
	_degree: number = 0.5;
	@property({
		tooltip : "程度",
		range : [0, 1, 0.01],
	})
	get degree(){
		return this._degree;
	}
	set degree(value:number){
		if(this._degree != value){
			this._degree = value;
			this.shaderDegree.value = value;
		}
	}

	renderSystem:RenderSystem = null;
	shaderSpriteFrame:ShaderSpriteFrame = null;
	shaderFadeWidth:ShaderFloat = null;
	shaderFadeDirect:ShaderFloat = null;
	shaderDegree:ShaderFloat = null;

	// LIFE-CYCLE CALLBACKS:

	onEnable () {
		if(!this.shaderSpriteFrame){
			this.shaderSpriteFrame = this.getShaderComponent(ShaderSpriteFrame, "spriteframe");
			if(!this.shaderSpriteFrame){
				let shader = this.addComponent(ShaderSpriteFrame);
				shader.tag = "spriteframe";
				shader.useShape = true;
				shader.useUV = true;
				this.shaderSpriteFrame = shader;
			}
		}
		if(!this.shaderFadeWidth){
			this.shaderFadeWidth = this.getShaderComponent(ShaderFloat, "fadeWidth");
			if(!this.shaderFadeWidth){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "fadeWidth";
				shader._valueVar.attrMacro.name = "ATTR_FADEWIDTH";
				shader._valueVar.attrName = "a_fadeWidth";
				shader._valueVar.unifMacro.name = "UNIF_FADEWIDTH";
				shader._valueVar.unifName = "u_fadeWidth";
				this.shaderFadeWidth = shader;
				shader.value = this._fadeWidth;
			}
		}
		if(!this.shaderFadeDirect){
			this.shaderFadeDirect = this.getShaderComponent(ShaderFloat, "fadeDirect");
			if(!this.shaderFadeDirect){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "fadeDirect";
				shader._valueVar.attrMacro.name = "ATTR_FADEDIRECT";
				shader._valueVar.attrName = "a_fadeDirect";
				shader._valueVar.unifMacro.name = "UNIF_FADEDIRECT";
				shader._valueVar.unifName = "u_fadeDirect";
				this.shaderFadeDirect = shader;
				shader.value = this._fadeDirect;
			}
		}
		if(!this.shaderDegree){
			this.shaderDegree = this.getShaderComponent(ShaderFloat, "degree");
			if(!this.shaderDegree){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "degree";
				shader._valueVar.attrMacro.name = "ATTR_DEGREE";
				shader._valueVar.attrName = "a_degree";
				shader._valueVar.unifMacro.name = "UNIF_DEGREE";
				shader._valueVar.unifName = "u_degree";
				this.shaderDegree = shader;
				shader.value = this._degree;
			}
		}
		if(!this.renderSystem){
			this.renderSystem = this.getComponent(RenderSystem);
			if(!this.renderSystem){
				this.renderSystem = this.addComponent(RenderSystem);
			}
		}
		this.checkMaterial();
	}

	/**
	 * 检查材质
	 */
	async checkMaterial(){
		if(CC_EDITOR){
			if(!this._material){
				this.material = await Utils.getAssetByUUID<cc.Material>(UUID.materials["lcc-2d_transform"]);
			}
		}
	}
}

}
	