/// <reference path = "../shader/ShaderComponent.ts" />

module lcc$render {

const {ccclass, property, menu} = cc._decorator;

@ccclass("lcc$render.EffectOutline")
@menu("i18n:lcc-render.menu_component/EffectOutline")
export class EffectOutline extends ShaderComponent {

	@property(cc.Material)
	_material: cc.Material = null;
	@property({
		type : cc.Material,
		tooltip : "效果材质"
	})
	get material(){
		return this._material;
	}
	set material(value:cc.Material){
		if(this._material != value){
			this._material = value;
			this.renderSystem.setMaterial(0, value);
		}
	}

	@property(cc.SpriteFrame)
	_spriteFrame: cc.SpriteFrame = null;
	@property({
		type : cc.SpriteFrame,
		tooltip : "效果精灵帧"
	})
	get spriteFrame(){
		return this._spriteFrame;
	}
	set spriteFrame(value:cc.SpriteFrame){
		if(this._spriteFrame != value){
			this._spriteFrame = value;
			this.shaderSpriteFrame.spriteFrame = value;
		}
	}

	@property(cc.Color)
	_outlineColor: cc.Color = cc.Color.RED;
	@property({
		type : cc.Color,
		tooltip : "描边颜色"
	})
	get outlineColor(){
		return this._outlineColor;
	}
	set outlineColor(value:cc.Color){
		this._outlineColor = value;
		this.shaderOutlineColor.color = value;
	}

	@property()
	_outlineWidth: number = 0.002;
	@property({
		tooltip : "描边宽度",
		range : [0, 1, 0.01],
	})
	get outlineWidth(){
		return this._outlineWidth;
	}
	set outlineWidth(value:number){
		if(this._outlineWidth != value){
			this._outlineWidth = value;
			this.shaderOutlineWidth.value = value;
		}
	}

	@property()
	_glowFlash: number = 0.0;
	@property({
		tooltip : "闪烁速度",
	})
	get glowFlash(){
		return this._glowFlash;
	}
	set glowFlash(value:number){
		if(this._glowFlash != value){
			this._glowFlash = value;
			this.shaderGlowFlash.value = value;
		}
	}

	renderSystem:RenderSystem = null;
	shaderSpriteFrame:ShaderSpriteFrame = null;
	shaderOutlineColor:ShaderColor = null;
	shaderOutlineWidth:ShaderFloat = null;
	shaderGlowFlash:ShaderFloat = null;

	// LIFE-CYCLE CALLBACKS:

	onEnable () {
		if(!this.shaderSpriteFrame){
			this.shaderSpriteFrame = this.getShaderComponent(ShaderSpriteFrame, "spriteframe");
			if(!this.shaderSpriteFrame){
				let shader = this.addComponent(ShaderSpriteFrame);
				shader.tag = "spriteframe";
				shader.useShape = true;
				shader.useUV = true;
				shader.useUVRect = true;
				this.shaderSpriteFrame = shader;
			}
		}
		if(!this.shaderOutlineColor){
			this.shaderOutlineColor = this.getShaderComponent(ShaderColor, "outlineColor");
			if(!this.shaderOutlineColor){
				let shader = this.addComponent(ShaderColor);
				shader.tag = "outlineColor";
				shader.useNodeColor = false;
				shader._colorVar.attrMacro.name = "ATTR_OUTLINECOLOR";
				shader._colorVar.attrName = "a_outlineColor";
				shader._colorVar.unifMacro.name = "UNIF_OUTLINECOLOR";
				shader._colorVar.unifName = "u_outlineColor";
				this.shaderOutlineColor = shader;
				shader.color = this._outlineColor;
			}
		}
		if(!this.shaderOutlineWidth){
			this.shaderOutlineWidth = this.getShaderComponent(ShaderFloat, "outlineWidth");
			if(!this.shaderOutlineWidth){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "outlineWidth";
				shader._valueVar.attrMacro.name = "ATTR_OUTLINEWIDTH";
				shader._valueVar.attrName = "a_outlineWidth";
				shader._valueVar.unifMacro.name = "UNIF_OUTLINEWIDTH";
				shader._valueVar.unifName = "u_outlineWidth";
				this.shaderOutlineWidth = shader;
				shader.value = this._outlineWidth;
			}
		}
		if(!this.shaderGlowFlash){
			this.shaderGlowFlash = this.getShaderComponent(ShaderFloat, "glowFlash");
			if(!this.shaderGlowFlash){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "glowFlash";
				shader._valueVar.attrMacro.name = "ATTR_GLOWFLASH";
				shader._valueVar.attrName = "a_glowFlash";
				shader._valueVar.unifMacro.name = "UNIF_GLOWFLASH";
				shader._valueVar.unifName = "u_glowFlash";
				this.shaderGlowFlash = shader;
				shader.value = this._glowFlash;
			}
		}
		if(!this.renderSystem){
			this.renderSystem = this.getComponent(RenderSystem);
			if(!this.renderSystem){
				this.renderSystem = this.addComponent(RenderSystem);
			}
		}
		if(CC_EDITOR){
			this.checkMaterial();
		}
	}

	/**
	 * 检查材质
	 */
	async checkMaterial(){
		if(CC_EDITOR){
			if(!this._material){
				this.material = await Utils.getAssetByUUID<cc.Material>(UUID.materials["lcc-2d_outline"]);
			}
		}
	}
}

}
