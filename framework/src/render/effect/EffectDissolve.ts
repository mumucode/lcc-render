/// <reference path = "../shader/ShaderComponent.ts" />

module lcc$render {

const {ccclass, property, menu} = cc._decorator;

/**
 * 溶解效果
 */
@ccclass("lcc$render.EffectDissolve")
@menu("i18n:lcc-render.menu_component/EffectDissolve")
export class EffectDissolve extends ShaderComponent {

	@property(cc.Material)
	_material: cc.Material = null;
	@property({
		type : cc.Material,
		tooltip : "效果材质"
	})
	get material(){
		return this._material;
	}
	set material(value:cc.Material){
		if(this._material != value){
			this._material = value;
			this.renderSystem.setMaterial(0, value);
		}
	}

	@property(cc.SpriteFrame)
	_spriteFrame: cc.SpriteFrame = null;
	@property({
		type : cc.SpriteFrame,
		tooltip : "效果精灵帧"
	})
	get spriteFrame(){
		return this._spriteFrame;
	}
	set spriteFrame(value:cc.SpriteFrame){
		if(this._spriteFrame != value){
			this._spriteFrame = value;
			this.shaderSpriteFrame.spriteFrame = value;
			if(!this._noiseFrame){
				this.noiseFrame = value;
			}else{
                this.checkNoiseFrame();
            }
		}
	}
	
	@property(cc.SpriteFrame)
	_noiseFrame: cc.SpriteFrame = null;
	@property({
		type : cc.SpriteFrame,
		tooltip : "效果噪声帧"
	})
	get noiseFrame(){
		return this._noiseFrame;
	}
	set noiseFrame(value:cc.SpriteFrame){
		if(this._noiseFrame != value){
			this._noiseFrame = value;
			this.shaderNoiseFrame.spriteFrame = value;
            this.checkNoiseFrame();
		}
	}
	
	@property()
	_degree: number = 0.7;
	@property({
		tooltip : "程度",
		range : [0, 1, 0.01],
	})
	get degree(){
		return this._degree;
	}
	set degree(value:number){
		if(this._degree != value){
			this._degree = value;
			this.shaderDegree.value = value;
		}
	}

	renderSystem:RenderSystem = null;
	shaderSpriteFrame:ShaderSpriteFrame = null;
	shaderNoiseFrame:ShaderSpriteFrame = null;
	shaderDegree:ShaderFloat = null;
    
	// LIFE-CYCLE CALLBACKS:

	onEnable () {
		if(!this.shaderSpriteFrame){
			this.shaderSpriteFrame = this.getShaderComponent(ShaderSpriteFrame, "spriteframe");
			if(!this.shaderSpriteFrame){
				let shader = this.addComponent(ShaderSpriteFrame);
				shader.tag = "spriteframe";
				shader.useShape = true;
				shader.useUV = true;
				this.shaderSpriteFrame = shader;
			}
		}
		if(!this.shaderNoiseFrame){
			this.shaderNoiseFrame = this.getShaderComponent(ShaderSpriteFrame, "noiseframe");
			if(!this.shaderNoiseFrame){
				let shader = this.addComponent(ShaderSpriteFrame);
				shader.tag = "noiseframe";
				shader._textureVar.unifMacro.name = "USE_NOISE";
                shader._textureVar.unifName = "noise";
                shader._uvVar.attrMacro.checkOnly = false;
                shader._uvVar.attrMacro.name = "NOISEOTHER";
                shader._uvVar.attrName = "a_uv1";
                
				this.shaderNoiseFrame = shader;
			}
		}
		if(!this.shaderDegree){
			this.shaderDegree = this.getShaderComponent(ShaderFloat, "degree");
			if(!this.shaderDegree){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "degree";
				shader._valueVar.attrMacro.name = "ATTR_DEGREE";
				shader._valueVar.attrName = "a_degree";
				shader._valueVar.unifMacro.name = "UNIF_DEGREE";
				shader._valueVar.unifName = "u_degree";
				this.shaderDegree = shader;
				shader.value = this._degree;
			}
		}
		if(!this.renderSystem){
			this.renderSystem = this.getComponent(RenderSystem);
			if(!this.renderSystem){
				this.renderSystem = this.addComponent(RenderSystem);
			}
        }
		this.checkMaterial();
	}

    /**
     * 检查噪声帧
     */
    private checkNoiseFrame(){
        if(this._noiseFrame && this._spriteFrame){
            // @ts-ignore
            if((this._noiseFrame == this._spriteFrame) || (this._noiseFrame._uuid == this._spriteFrame._uuid)){
                this.shaderNoiseFrame.useUV = false;
            }else{
                this.shaderNoiseFrame.useUV = true;
            }
            this.renderSystem.setDirty();
        }
    }
    
	/**
	 * 检查材质
	 */
	async checkMaterial(){
		if(CC_EDITOR){
			if(!this._material){
				this.material = await Utils.getAssetByUUID<cc.Material>(UUID.materials["lcc-2d_dissolve"]);
			}
		}
	}
}

}
	