/// <reference path = "../shader/ShaderComponent.ts" />

module lcc$render {

const {ccclass, property, menu} = cc._decorator;

@ccclass("lcc$render.EffectFlashLight")
@menu("i18n:lcc-render.menu_component/EffectFlashLight")
export class EffectFlashLight extends ShaderComponent {

    @property(cc.Material)
	_material: cc.Material = null;
	@property({
		type : cc.Material,
		tooltip : "效果材质"
	})
	get material(){
		return this._material;
	}
	set material(value:cc.Material){
		if(this._material != value){
			this._material = value;
			this.renderSystem.setMaterial(0, value);
		}
	}

    @property(cc.SpriteFrame)
	_spriteFrame: cc.SpriteFrame = null;
	@property({
		type : cc.SpriteFrame,
		tooltip : "效果精灵帧"
	})
	get spriteFrame(){
		return this._spriteFrame;
	}
	set spriteFrame(value:cc.SpriteFrame){
		if(this._spriteFrame != value){
			this._spriteFrame = value;
			this.shaderSpriteFrame.spriteFrame = value;
		}
	}

    @property(cc.Color)
	_lightColor: cc.Color = cc.Color.WHITE;
	@property({
		type : cc.Color,
		tooltip : "光线颜色"
	})
	get lightColor(){
		return this._lightColor;
	}
	set lightColor(value:cc.Color){
		this._lightColor = value;
		this.shaderLightColor.color = value;
	}

    @property()
	_lightAngle: number = 135;
	@property({
		tooltip : "光束倾斜角度",
		range : [0, 360, 1],
	})
	get lightAngle(){
		return this._lightAngle;
	}
	set lightAngle(value:number){
		if(this._lightAngle != value){
			this._lightAngle = value;
			this.shaderLightAngle.value = value;
		}
	}

    @property()
	_lightWidth: number = 0.2;
	@property({
		tooltip : "光束宽度"
	})
	get lightWidth(){
		return this._lightWidth;
	}
	set lightWidth(value:number){
		if(this._lightWidth != value){
			this._lightWidth = value;
			this.shaderLightWidth.value = value;
		}
	}

    @property()
	_enableGradient: boolean = true;
	@property({
		tooltip : "启用光束渐变"
	})
	get enableGradient(){
		return this._enableGradient;
	}
	set enableGradient(value:boolean){
		if(this._enableGradient != value){
			this._enableGradient = value;
			this.shaderEnableGradient.value = value ? 1 : 0;
		}
	}

    @property()
	_cropAlpha: boolean = true;
	@property({
		tooltip : "裁剪掉透明区域上的光"
	})
	get cropAlpha(){
		return this._cropAlpha;
	}
	set cropAlpha(value:boolean){
		if(this._cropAlpha != value){
			this._cropAlpha = value;
			this.shaderCropAlpha.value = value ? 1 : 0;
		}
	}

    @property()
	_moveSpeed: number = -1;
	@property({
		tooltip : "移动速度"
	})
	get moveSpeed(){
		return this._moveSpeed;
	}
	set moveSpeed(value:number){
		if(this._moveSpeed != value){
			this._moveSpeed = value;
			this.shaderMoveSpeed.value = value;
		}
	}

    @property()
	_moveWidth: number = 2;
	@property({
		tooltip : "移动宽度"
	})
	get moveWidth(){
		return this._moveWidth;
	}
	set moveWidth(value:number){
		if(this._moveWidth != value){
			this._moveWidth = value;
			this.shaderMoveWidth.value = value;
		}
	}

    @property()
	_lightLines: number = 1;
	@property({
		tooltip : "光束数量",
		range:[1, 20, 1]
	})
	get lightLines(){
		return this._lightLines;
	}
	set lightLines(value:number){
		if(this._lightLines != value){
			this._lightLines = value;
			this.shaderLightLines.value = value;
		}
	}

    @property()
	_lightSpace: number = 1;
	@property({
		tooltip : "光束间距"
	})
	get lightSpace(){
		return this._lightSpace;
	}
	set lightSpace(value:number){
		if(this._lightSpace != value){
			this._lightSpace = value;
			this.shaderLightSpace.value = value;
		}
	}

	renderSystem:RenderSystem = null;
	shaderSpriteFrame:ShaderSpriteFrame = null;
	shaderLightColor:ShaderColor = null;
	shaderLightAngle:ShaderFloat = null;
	shaderLightWidth:ShaderFloat = null;
	shaderEnableGradient:ShaderFloat = null;
	shaderCropAlpha:ShaderFloat = null;
	shaderMoveSpeed:ShaderFloat = null;
	shaderMoveWidth:ShaderFloat = null;
	shaderLightLines:ShaderFloat = null;
	shaderLightSpace:ShaderFloat = null;

    // LIFE-CYCLE CALLBACKS:

    onEnable () {
		if(!this.shaderSpriteFrame){
			this.shaderSpriteFrame = this.getShaderComponent(ShaderSpriteFrame, "spriteframe");
			if(!this.shaderSpriteFrame){
				let shader = this.addComponent(ShaderSpriteFrame);
				shader.tag = "spriteframe";
				shader.useShape = true;
				shader.useUV = true;
				shader.useUVRect = true;
				shader._uvRectVar.type = VariableType.ATTRIBUTE;
				this.shaderSpriteFrame = shader;
			}
		}
		if(!this.shaderLightColor){
			this.shaderLightColor = this.getShaderComponent(ShaderColor, "lightcolor");
			if(!this.shaderLightColor){
				let shader = this.addComponent(ShaderColor);
				shader.tag = "lightcolor";
				shader.useNodeColor = false;
				shader._colorVar.attrMacro.name = "ATTR_LIGHTCOLOR";
				shader._colorVar.attrName = "a_lightColor";
				shader._colorVar.unifMacro.name = "UNIF_LIGHTCOLOR";
				shader._colorVar.unifName = "u_lightColor";
				this.shaderLightColor = shader;
				shader.color = this._lightColor;
			}
		}
		if(!this.shaderLightAngle){
			this.shaderLightAngle = this.getShaderComponent(ShaderFloat, "lightangle");
			if(!this.shaderLightAngle){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "lightangle";
				shader._valueVar.attrMacro.name = "ATTR_LIGHTANGLE";
				shader._valueVar.attrName = "a_lightAngle";
				shader._valueVar.unifMacro.name = "UNIF_LIGHTANGLE";
				shader._valueVar.unifName = "u_lightAngle";
				this.shaderLightAngle = shader;
				shader.value = this._lightAngle;
			}
		}
		if(!this.shaderLightWidth){
			this.shaderLightWidth = this.getShaderComponent(ShaderFloat, "lightwidth");
			if(!this.shaderLightWidth){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "lightwidth";
				shader._valueVar.attrMacro.name = "ATTR_LIGHTWIDTH";
				shader._valueVar.attrName = "a_lightWidth";
				shader._valueVar.unifMacro.name = "UNIF_LIGHTWIDTH";
				shader._valueVar.unifName = "u_lightWidth";
				this.shaderLightWidth = shader;
				shader.value = this._lightWidth;
			}
		}
		if(!this.shaderEnableGradient){
			this.shaderEnableGradient = this.getShaderComponent(ShaderFloat, "enablegradient");
			if(!this.shaderEnableGradient){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "enablegradient";
				shader._valueVar.attrMacro.name = "ATTR_ENABLEGRADIENT";
				shader._valueVar.attrName = "a_enableGradient";
				shader._valueVar.unifMacro.name = "UNIF_ENABLEGRADIENT";
				shader._valueVar.unifName = "u_enableGradient";
				this.shaderEnableGradient = shader;
				shader.value = this._enableGradient ? 1 : 0;
			}
		}
		if(!this.shaderCropAlpha){
			this.shaderCropAlpha = this.getShaderComponent(ShaderFloat, "cropalpha");
			if(!this.shaderCropAlpha){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "cropalpha";
				shader._valueVar.attrMacro.name = "ATTR_CROPALPHA";
				shader._valueVar.attrName = "a_cropAlpha";
				shader._valueVar.unifMacro.name = "UNIF_CROPALPHA";
				shader._valueVar.unifName = "u_cropAlpha";
				this.shaderCropAlpha = shader;
				shader.value = this._cropAlpha ? 1 : 0;
			}
		}
		if(!this.shaderMoveSpeed){
			this.shaderMoveSpeed = this.getShaderComponent(ShaderFloat, "movespeed");
			if(!this.shaderMoveSpeed){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "movespeed";
				shader._valueVar.attrMacro.name = "ATTR_MOVESPEED";
				shader._valueVar.attrName = "a_moveSpeed";
				shader._valueVar.unifMacro.name = "UNIF_MOVESPEED";
				shader._valueVar.unifName = "u_moveSpeed";
				this.shaderMoveSpeed = shader;
				shader.value = this._moveSpeed;
			}
		}
		if(!this.shaderMoveWidth){
			this.shaderMoveWidth = this.getShaderComponent(ShaderFloat, "movewidth");
			if(!this.shaderMoveWidth){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "movewidth";
				shader._valueVar.attrMacro.name = "ATTR_MOVEWIDTH";
				shader._valueVar.attrName = "a_moveWidth";
				shader._valueVar.unifMacro.name = "UNIF_MOVEWIDTH";
				shader._valueVar.unifName = "u_moveWidth";
				this.shaderMoveWidth = shader;
				shader.value = this._moveWidth;
			}
		}
		if(!this.shaderLightLines){
			this.shaderLightLines = this.getShaderComponent(ShaderFloat, "lightlines");
			if(!this.shaderLightLines){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "lightlines";
				shader._valueVar.attrMacro.name = "ATTR_LIGHTLINES";
				shader._valueVar.attrName = "a_lightLines";
				shader._valueVar.unifMacro.name = "UNIF_LIGHTLINES";
				shader._valueVar.unifName = "u_lightLines";
				this.shaderLightLines = shader;
				shader.value = this._lightLines;
			}
		}
		if(!this.shaderLightSpace){
			this.shaderLightSpace = this.getShaderComponent(ShaderFloat, "lightspace");
			if(!this.shaderLightSpace){
				let shader = this.addComponent(ShaderFloat);
				shader.tag = "lightspace";
				shader._valueVar.attrMacro.name = "ATTR_LIGHTSPACE";
				shader._valueVar.attrName = "a_lightSpace";
				shader._valueVar.unifMacro.name = "UNIF_LIGHTSPACE";
				shader._valueVar.unifName = "u_lightSpace";
				this.shaderLightSpace = shader;
				shader.value = this._lightSpace;
			}
		}
		if(!this.renderSystem){
			this.renderSystem = this.getComponent(RenderSystem);
			if(!this.renderSystem){
				this.renderSystem = this.addComponent(RenderSystem);
			}
		}
		if(CC_EDITOR){
			this.checkMaterial();
		}
	}

	/**
	 * 检查材质
	 */
	async checkMaterial(){
		if(CC_EDITOR){
			if(!this._material){
				this.material = await Utils.getAssetByUUID<cc.Material>(UUID.materials["lcc-2d-flash_light"]);
			}
		}
	}
}

}
