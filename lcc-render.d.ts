declare module lcc$render {
    class MacroConfig {
        name: string;
        checkOnly: boolean;
        constructor(...params: any);
    }
    enum VariableType {
        ATTRIBUTE = 1,
        UNIFORM = 2
    }
    enum ValueType {
        SINGLE = 0,
        ARRAY = 1
    }
    class VariableConfig {
        _typesel: boolean;
        type: VariableType;
        attrMacro: MacroConfig;
        attrName: string;
        unifMacro: MacroConfig;
        unifName: string;
        constructor(...params: any);
    }
    class ShaderComponent extends cc.Component {
        _tag: string;
        get tag(): string;
        set tag(value: string);
        protected checkMaterialMacro(material: cc.Material, macro: MacroConfig): boolean;
        protected defineMaterialMacro(material: cc.Material, macro: MacroConfig, value: number | boolean): void;
        protected getShaderComponent<T extends ShaderComponent>(type: {
            prototype: T;
        }, tag?: string): T;
        protected getShaderComponents<T extends ShaderComponent>(type: {
            prototype: T;
        }, tag?: string): T[];
    }
}
declare module lcc$render {
    export interface VertexAttribute {
        name: string;
        type: number;
        num: number;
        normalize?: boolean;
    }
    export enum ShaderVariableType {
        ATTRIBUTE = 0,
        UNIFORM = 1
    }
    enum ShapeType {
        SHADER = 0,
        NODE = 1
    }
    export class RenderSystem extends cc.RenderComponent {
        attrsSize: number;
        _matsDirty: boolean;
        _shapeDirty: boolean;
        _attrsDirty: boolean;
        _vFormat: any;
        _vAttributes: VertexAttribute[];
        _shapeType: ShapeType;
        _verticesCount: number;
        _indicesCount: number;
        _localRect: number[];
        onEnable(): void;
        onDisable(): void;
        setMatsDirty(): void;
        setAttrsDirty(): void;
        setShapeDirty(): void;
        private checkRenderShape;
        setShaderShape(verticesCount: number, indicesCount: number): void;
        private onNodeSizeChanged;
        setVertsDirty(): void;
        setDirty(): void;
        protected updateToNodeShape(): void;
        protected updateNodeVertex(): void;
        updateRenderData(): void;
        protected prepareRender(): void;
        protected updateMaterial(): void;
        checkVertexAttributeName(name: string): boolean;
        addVertexAttribute(vattr: VertexAttribute, floatsize: number): number;
        getVfmt(): any;
        protected updateAttribute(): void;
        protected updateShape(): void;
        protected updateVertex(): void;
        updateWorldVerts(): void;
        updateNodeWorldVertsWebGL(): void;
        updateNodeWorldVertsNative(): void;
        protected getShaderComponent<T extends ShaderComponent>(type: {
            prototype: T;
        }, tag?: string): T;
        protected getShaderComponents<T extends ShaderComponent>(type: {
            prototype: T;
        }, tag?: string): T[];
        _activateMaterial(): void;
        _updateMaterial(): void;
        _validateRender(): any;
        _srcBlendFactor: cc.macro.BlendFactor;
        get srcBlendFactor(): cc.macro.BlendFactor;
        set srcBlendFactor(value: cc.macro.BlendFactor);
        _dstBlendFactor: cc.macro.BlendFactor;
        get dstBlendFactor(): cc.macro.BlendFactor;
        set dstBlendFactor(value: cc.macro.BlendFactor);
        setMaterial(index: any, material: any): cc.Material;
        _updateBlendFunc(force: any): void;
        _updateMaterialBlendFunc(material: any): void;
    }
    export {};
}
declare module lcc$render {
    class Utils {
        static getAssetByUUID<T extends cc.Asset>(uuid: string): Promise<T>;
    }
}
declare module lcc$render {
    let UUID: {
        effects: {
            "lcc-2d-flash_light": string;
            "lcc-2d-fluxay_super": string;
            "lcc-2d-sprite_mask": string;
            "lcc-2d_glow_inner": string;
            "lcc-2d_glow_outter": string;
            "lcc-2d_mosaic": string;
            "lcc-2d_photo": string;
            "lcc-2d_outline": string;
            "lcc-2d_gaussian_blur": string;
            "lcc-2d_round_corner_crop": string;
            "lcc-2d_dissolve": string;
            "lcc-2d_transform": string;
            "lcc-2d_rain_drop_ripples": string;
        };
        materials: {
            "lcc-2d-flash_light": string;
            "lcc-2d-fluxay_super": string;
            "lcc-2d-sprite_mask": string;
            "lcc-2d_glow_inner": string;
            "lcc-2d_glow_outter": string;
            "lcc-2d_mosaic": string;
            "lcc-2d_photo": string;
            "lcc-2d_outline": string;
            "lcc-2d_gaussian_blur": string;
            "lcc-2d_round_corner_crop": string;
            "lcc-2d_dissolve": string;
            "lcc-2d_transform": string;
            "lcc-2d_rain_drop_ripples": string;
        };
    };
}
declare module lcc$render {
    class ShaderColor extends ShaderComponent {
        _tag: string;
        _colorType: ValueType;
        get colorType(): ValueType;
        set colorType(value: ValueType);
        _colorVar: VariableConfig;
        get colorVar(): VariableConfig;
        set colorVar(value: VariableConfig);
        _colors: cc.Color[];
        _useNodeColor: boolean;
        get useNodeColor(): boolean;
        set useNodeColor(value: boolean);
        get color(): cc.Color;
        set color(value: cc.Color);
        get colors(): cc.Color[];
        set colors(value: cc.Color[]);
        private _colorOffset;
        onLoad(): void;
        onDestroy(): void;
        onEnable(): void;
        onDisable(): void;
        private onStateUpdate;
        private onNodeColorChanged;
        private onRenderShapeChecked;
        private onUpdateColors;
        private premultiplyAlpha;
        private getAttributeColors;
        private getUniformColors;
        private onRenderUpdateMaterial;
        private onRenderUpdateAttribute;
        private onRenderUpdateRenderData;
    }
}
declare module lcc$render {
    const VC: {
        tag: string;
        varSelect: boolean;
        varDeftype: VariableType;
        varAttrMacro: string;
        varAttrMacroCheckOnly: boolean;
        varAttrName: string;
        varUnifMacro: string;
        varUnifMacroCheckOnly: boolean;
        varUnifName: string;
        valDefault: number;
        valNew: () => number;
        valType: string;
        typeSize: number;
        toArray: (arr: number[], value: number, offest: number) => void;
    };
    export class ShaderFloat extends ShaderComponent {
        _tag: string;
        _valueType: ValueType;
        get valueType(): ValueType;
        set valueType(value: ValueType);
        _valueVar: VariableConfig;
        get valueVar(): VariableConfig;
        set valueVar(value: VariableConfig);
        _values: typeof VC.valDefault[];
        get value(): typeof VC.valDefault;
        set value(value_: typeof VC.valDefault);
        get values(): typeof VC.valDefault[];
        set values(value: typeof VC.valDefault[]);
        private _valueOffset;
        onLoad(): void;
        onDestroy(): void;
        onEnable(): void;
        onDisable(): void;
        private onStateUpdate;
        private onRenderShapeChecked;
        private onUpdateValues;
        private onRenderUpdateMaterial;
        private getAttributeValues;
        private onRenderUpdateAttribute;
        private onRenderUpdateRenderData;
    }
    export {};
}
declare module lcc$render {
    enum MacroValueType {
        BOOL = 0,
        VALUE = 1
    }
    export class ShaderMacro extends ShaderComponent {
        _tag: string;
        _checkMacro: string;
        get checkMacro(): string;
        set checkMacro(value: string);
        _macroName: string;
        get macroName(): string;
        set macroName(value: string);
        _valueType: MacroValueType;
        get valueType(): MacroValueType;
        set valueType(value_: MacroValueType);
        _bool: boolean;
        get bool(): boolean;
        set bool(value: boolean);
        _value: number;
        get value(): number;
        set value(value: number);
        onLoad(): void;
        onDestroy(): void;
        onEnable(): void;
        onDisable(): void;
        private onStateUpdate;
        private onRenderUpdateMaterial;
    }
    export {};
}
declare module lcc$render {
    enum ShapeType {
        NODE = 0,
        CUSTOM = 1
    }
    enum SizeMode {
        CUSTOM = 0,
        TRIMMED = 1,
        RAW = 2
    }
    export class ShaderSpriteFrame extends ShaderComponent {
        _tag: string;
        _textureVar: VariableConfig;
        get textureVar(): VariableConfig;
        set textureVar(value: VariableConfig);
        _atlas: cc.SpriteAtlas;
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _useShape: boolean;
        get useShape(): boolean;
        set useShape(value: boolean);
        _shapeType: ShapeType;
        get shapeType(): ShapeType;
        set shapeType(value: ShapeType);
        _size: cc.Size;
        get size(): cc.Size;
        set size(value: cc.Size);
        _anchor: cc.Vec2;
        get anchor(): cc.Vec2;
        set anchor(value: cc.Vec2);
        _syncSizeToNode: boolean;
        get syncSizeToNode(): boolean;
        set syncSizeToNode(value: boolean);
        _sizeMode: SizeMode;
        get sizeMode(): SizeMode;
        set sizeMode(value: SizeMode);
        _isTrimmedMode: boolean;
        get trim(): boolean;
        set trim(value: boolean);
        _useUV: boolean;
        get useUV(): boolean;
        set useUV(value: boolean);
        _uvVar: VariableConfig;
        get uvVar(): VariableConfig;
        set uvVar(value: VariableConfig);
        _customUV: boolean;
        get customUV(): boolean;
        set customUV(value: boolean);
        _uvIndexes: number[];
        get uvIndexes(): number[];
        set uvIndexes(value: number[]);
        _useUVRect: boolean;
        get useUVRect(): boolean;
        set useUVRect(value: boolean);
        _uvRectVar: VariableConfig;
        get uvRectVar(): VariableConfig;
        set uvRectVar(value: VariableConfig);
        _useFrameSize: boolean;
        get useFrameSize(): boolean;
        set useFrameSize(value: boolean);
        _frameSizeVar: VariableConfig;
        get frameSizeVar(): VariableConfig;
        set frameSizeVar(value: VariableConfig);
        _shapeRect: number[];
        private _uvOffest;
        private _uvrOffset;
        private _fsOffset;
        onLoad(): void;
        onDestroy(): void;
        onEnable(): void;
        onDisable(): void;
        private onStateUpdate;
        private checkUVIndexes;
        private onShaderuseShape;
        private onRenderCheckShape;
        private onRenderPrepare;
        private getUVRect;
        private onRenderUpdateMaterial;
        private onRenderUpdateAttribute;
        private onRenderUpdateShape;
        private onRenderUpdateRenderData;
        updateWorldVerts(comp: RenderSystem): void;
        updateWorldVertsWebGL(comp: RenderSystem): void;
        updateWorldVertsNative(comp: RenderSystem): void;
        private onRenderValidateRender;
        packToDynamicAtlas(comp: any, frame: any): void;
        private _useSpriteSize;
        private _useSpriteFrame;
        private _useAtlas;
    }
    export {};
}
declare module lcc$render {
    const VC: {
        tag: string;
        varSelect: boolean;
        varDeftype: VariableType;
        varAttrMacro: string;
        varAttrMacroCheckOnly: boolean;
        varAttrName: string;
        varUnifMacro: string;
        varUnifMacroCheckOnly: boolean;
        varUnifName: string;
        valDefault: cc.Vec2;
        valNew: () => cc.Vec2;
        valType: typeof cc.Vec2;
        typeSize: number;
        toArray: (arr: number[], value: cc.Vec2, offest: number) => void;
    };
    export class ShaderVec2 extends ShaderComponent {
        _tag: string;
        _valueType: ValueType;
        get valueType(): ValueType;
        set valueType(value: ValueType);
        _valueVar: VariableConfig;
        get valueVar(): VariableConfig;
        set valueVar(value: VariableConfig);
        _values: typeof VC.valDefault[];
        get value(): typeof VC.valDefault;
        set value(value_: typeof VC.valDefault);
        get values(): typeof VC.valDefault[];
        set values(value: typeof VC.valDefault[]);
        private _valueOffset;
        onLoad(): void;
        onDestroy(): void;
        onEnable(): void;
        onDisable(): void;
        private onStateUpdate;
        private onRenderShapeChecked;
        private onUpdateValues;
        private onRenderUpdateMaterial;
        private getAttributeValues;
        private onRenderUpdateAttribute;
        private onRenderUpdateRenderData;
    }
    export {};
}
declare module lcc$render {
    const VC: {
        tag: string;
        varSelect: boolean;
        varDeftype: VariableType;
        varAttrMacro: string;
        varAttrMacroCheckOnly: boolean;
        varAttrName: string;
        varUnifMacro: string;
        varUnifMacroCheckOnly: boolean;
        varUnifName: string;
        valDefault: cc.Vec3;
        valNew: () => cc.Vec3;
        valType: typeof cc.Vec3;
        typeSize: number;
        toArray: (arr: number[], value: cc.Vec3, offest: number) => void;
    };
    export class ShaderVec3 extends ShaderComponent {
        _tag: string;
        _valueType: ValueType;
        get valueType(): ValueType;
        set valueType(value: ValueType);
        _valueVar: VariableConfig;
        get valueVar(): VariableConfig;
        set valueVar(value: VariableConfig);
        _values: typeof VC.valDefault[];
        get value(): typeof VC.valDefault;
        set value(value_: typeof VC.valDefault);
        get values(): typeof VC.valDefault[];
        set values(value: typeof VC.valDefault[]);
        private _valueOffset;
        onLoad(): void;
        onDestroy(): void;
        onEnable(): void;
        onDisable(): void;
        private onStateUpdate;
        private onRenderShapeChecked;
        private onUpdateValues;
        private onRenderUpdateMaterial;
        private getAttributeValues;
        private onRenderUpdateAttribute;
        private onRenderUpdateRenderData;
    }
    export {};
}
declare module lcc$render {
    const VC: {
        tag: string;
        varSelect: boolean;
        varDeftype: VariableType;
        varAttrMacro: string;
        varAttrMacroCheckOnly: boolean;
        varAttrName: string;
        varUnifMacro: string;
        varUnifMacroCheckOnly: boolean;
        varUnifName: string;
        valDefault: cc.Vec4;
        valNew: () => cc.Vec4;
        valType: typeof cc.Vec4;
        typeSize: number;
        toArray: (arr: number[], value: cc.Vec4, offest: number) => void;
    };
    export class ShaderVec4 extends ShaderComponent {
        _tag: string;
        _valueType: ValueType;
        get valueType(): ValueType;
        set valueType(value: ValueType);
        _valueVar: VariableConfig;
        get valueVar(): VariableConfig;
        set valueVar(value: VariableConfig);
        _values: typeof VC.valDefault[];
        get value(): typeof VC.valDefault;
        set value(value_: typeof VC.valDefault);
        get values(): typeof VC.valDefault[];
        set values(value: typeof VC.valDefault[]);
        private _valueOffset;
        onLoad(): void;
        onDestroy(): void;
        onEnable(): void;
        onDisable(): void;
        private onStateUpdate;
        private onRenderShapeChecked;
        private onUpdateValues;
        private onRenderUpdateMaterial;
        private getAttributeValues;
        private onRenderUpdateAttribute;
        private onRenderUpdateRenderData;
    }
    export {};
}
declare module lcc$render {
    class EffectDissolve extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _noiseFrame: cc.SpriteFrame;
        get noiseFrame(): cc.SpriteFrame;
        set noiseFrame(value: cc.SpriteFrame);
        _degree: number;
        get degree(): number;
        set degree(value: number);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderNoiseFrame: ShaderSpriteFrame;
        shaderDegree: ShaderFloat;
        onEnable(): void;
        private checkNoiseFrame;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
    class EffectFlashLight extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _lightColor: cc.Color;
        get lightColor(): cc.Color;
        set lightColor(value: cc.Color);
        _lightAngle: number;
        get lightAngle(): number;
        set lightAngle(value: number);
        _lightWidth: number;
        get lightWidth(): number;
        set lightWidth(value: number);
        _enableGradient: boolean;
        get enableGradient(): boolean;
        set enableGradient(value: boolean);
        _cropAlpha: boolean;
        get cropAlpha(): boolean;
        set cropAlpha(value: boolean);
        _moveSpeed: number;
        get moveSpeed(): number;
        set moveSpeed(value: number);
        _moveWidth: number;
        get moveWidth(): number;
        set moveWidth(value: number);
        _lightLines: number;
        get lightLines(): number;
        set lightLines(value: number);
        _lightSpace: number;
        get lightSpace(): number;
        set lightSpace(value: number);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderLightColor: ShaderColor;
        shaderLightAngle: ShaderFloat;
        shaderLightWidth: ShaderFloat;
        shaderEnableGradient: ShaderFloat;
        shaderCropAlpha: ShaderFloat;
        shaderMoveSpeed: ShaderFloat;
        shaderMoveWidth: ShaderFloat;
        shaderLightLines: ShaderFloat;
        shaderLightSpace: ShaderFloat;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
    class EffectFluxaySuper extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _speed: number;
        get speed(): number;
        set speed(value: number);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderSpeed: ShaderFloat;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
    class EffectGaussianBlur extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
    class EffectGlowInner extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _glowColor: cc.Color;
        get glowColor(): cc.Color;
        set glowColor(value: cc.Color);
        _glowColorSize: number;
        get glowColorSize(): number;
        set glowColorSize(value: number);
        _glowThreshold: number;
        get glowThreshold(): number;
        set glowThreshold(value: number);
        _glowFlash: number;
        get glowFlash(): number;
        set glowFlash(value: number);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderGlowColor: ShaderColor;
        shaderGlowColorSize: ShaderFloat;
        shaderGlowThreshold: ShaderFloat;
        shaderGlowFlash: ShaderFloat;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
    class EffectGlowOutter extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _glowColor: cc.Color;
        get glowColor(): cc.Color;
        set glowColor(value: cc.Color);
        _glowColorSize: number;
        get glowColorSize(): number;
        set glowColorSize(value: number);
        _glowThreshold: number;
        get glowThreshold(): number;
        set glowThreshold(value: number);
        _glowFlash: number;
        get glowFlash(): number;
        set glowFlash(value: number);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderGlowColor: ShaderColor;
        shaderGlowColorSize: ShaderFloat;
        shaderGlowThreshold: ShaderFloat;
        shaderGlowFlash: ShaderFloat;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
    class EffectMosaic extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _degree: number;
        get degree(): number;
        set degree(value: number);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderDegree: ShaderFloat;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
    class EffectOutline extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _outlineColor: cc.Color;
        get outlineColor(): cc.Color;
        set outlineColor(value: cc.Color);
        _outlineWidth: number;
        get outlineWidth(): number;
        set outlineWidth(value: number);
        _glowFlash: number;
        get glowFlash(): number;
        set glowFlash(value: number);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderOutlineColor: ShaderColor;
        shaderOutlineWidth: ShaderFloat;
        shaderGlowFlash: ShaderFloat;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
    enum ColorType {
        OLD = 0,
        GRAY = 1,
        REVERSAL = 2,
        FROZEN = 3,
        CARTOON = 4
    }
    export class EffectPhoto extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _colorType: ColorType;
        get colorType(): ColorType;
        set colorType(value: ColorType);
        _degree: number;
        get degree(): number;
        set degree(value: number);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderColorType: ShaderFloat;
        shaderDegree: ShaderFloat;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
    export {};
}
declare module lcc$render {
    class EffectRainDropRipples extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _density: number;
        get density(): number;
        set density(value: number);
        _change: boolean;
        get change(): boolean;
        set change(value: boolean);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderDensity: ShaderFloat;
        shaderChange: ShaderFloat;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
    class EffectRoundCornerCrop extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _degree: number;
        get degree(): number;
        set degree(value: number);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderDegree: ShaderFloat;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
    enum FadeDirect {
        LEFT_TO_RIGHT = 0,
        RIGHT_TO_LEFT = 1,
        TOP_TO_BOTTOM = 2,
        BOTTOM_TO_TOP = 3
    }
    export class EffectSmoothTransform extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _fadeWidth: number;
        get fadeWidth(): number;
        set fadeWidth(value: number);
        _fadeDirect: FadeDirect;
        get fadeDirect(): FadeDirect;
        set fadeDirect(value: FadeDirect);
        _degree: number;
        get degree(): number;
        set degree(value: number);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderFadeWidth: ShaderFloat;
        shaderFadeDirect: ShaderFloat;
        shaderDegree: ShaderFloat;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
    export {};
}
declare module lcc$render {
    class EffectSpriteMask extends ShaderComponent {
        _material: cc.Material;
        get material(): cc.Material;
        set material(value: cc.Material);
        _spriteFrame: cc.SpriteFrame;
        get spriteFrame(): cc.SpriteFrame;
        set spriteFrame(value: cc.SpriteFrame);
        _maskFrame: cc.SpriteFrame;
        get maskFrame(): cc.SpriteFrame;
        set maskFrame(value: cc.SpriteFrame);
        renderSystem: RenderSystem;
        shaderSpriteFrame: ShaderSpriteFrame;
        shaderMaskFrame: ShaderSpriteFrame;
        onEnable(): void;
        checkMaterial(): Promise<void>;
    }
}
declare module lcc$render {
}
