// Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.  
//
// 圆角裁剪（支持任意宽高纹理）
//
// 原理：
//  1. 正方形纹理的圆角原理参考 https://www.cnblogs.com/jqm304775992/p/4987793.html 
//  2. 正方形纹理的圆角代码参考 yanjifa/shaderDemor 的 https://github.com/yanjifa/shaderDemo/blob/master/assets/Effect/CircleAvatar.effect
//  3. 上述皆为只针对正方形纹理做的操作，如果是长方形的纹理，那么圆角就会有拉伸后的效果，最后变成看起来就不是圆角了，本特效支持任意长方形做圆角

CCEffect %{
  techniques:
  - passes:
    - vert: vs
      frag: fs
      blendState:
        targets:
        - blend: true
      rasterizerState:
        cullMode: none
      properties:
        texture: { value: white }
        alphaThreshold: { value: 0.5 }
}%

CCProgram vs %{
  precision highp float;

  #include <cc-global>
  #include <cc-local>

//---------------- defines
	
// 属性宏
#define LCC_MACRO_ATTRIBUTE(macro)	ATTR_##macro

// 常量宏
#define LCC_MACRO_UNIFORM(macro)		UNIF_##macro

// 检查宏定义
#define LCC_MACRO_DEFINED(macro) LCC_MACRO_ATTRIBUTE(macro)||LCC_MACRO_UNIFORM(macro)

// 顶点着色器 - 变量定义
#define LCC_VALUE_VERT_DEFINE(macro, type, name, defval)	\
	#if LCC_MACRO_ATTRIBUTE(macro)	\
	in type a_##name;	\
	out type name;	\
    #elif LCC_MACRO_UNIFORM(macro)	\
	uniform LCC_MACRO_UNIFORM(macro) {	\
		type u_##name;	\
	};	\
	out type name;	\
	#else	\
	type D_##name = defval;	\
	out type name;	\
	#endif

// 顶点着色器 - 变量传递
#define LCC_VALUE_VERT_PASS(macro, name)	\
	#if LCC_MACRO_ATTRIBUTE(macro)	\
	name = a_##name;	\
    #elif LCC_MACRO_UNIFORM(macro)	\
    name = u_##name;	\
    #else	\
    name = D_##name;	\
	#endif

// 片段着色器 - 变量定义
#define LCC_VALUE_FRAG_IMPORT(type, name)	\
	in type name;

// 顶点着色器 - 如果定义了宏，属性定义
#define LCC_ATTR_VERT_DEFINE_IF(fmacro, type, name)	\
	#if fmacro	\
	in type a_##name;	\
	out type name;	\
	#endif

// 顶点着色器 - 如果定义了宏，属性定传递
#define LCC_ATTR_VERT_PASS_IF(fmacro, name)	\
	#if fmacro	\
	name = a_##name;	\
	#endif

// 片段着色器 - 如果定义了宏，属性导入
#define LCC_ATTR_FRAG_IMPORT_IF(fmacro, type, name)	\
	#if fmacro	\
	in type name;	\
	#endif

// UV 转换到本地UV
#define LCC_UV_TO_LOCALUV(uv, uvrect) \
	vec2((uv.x - uvrect[0]) / uvrect[2], (uv.y - uvrect[1]) / uvrect[3])

// 本地UV 转换到UV
#define LCC_LOCALUV_TO_UV(luv, uvrect)	\
	vec2(uvrect[0] + luv.x * uvrect[2], uvrect[1] + luv.y * uvrect[3])
//---------------- 

  in vec3 a_position;
  
LCC_ATTR_VERT_DEFINE_IF(USE_TEXTURE, vec2, uv0)
LCC_VALUE_VERT_DEFINE(UVRECT, vec4, uvrect, vec4(0.0,0.0,1.0,1.0))
LCC_VALUE_VERT_DEFINE(FRAMESIZE, vec2, framesize, vec2(1.0,1.0))
LCC_VALUE_VERT_DEFINE(COLOR, vec4, color, vec4(1.0,1.0,1.0,1.0))
LCC_VALUE_VERT_DEFINE(DEGREE, float, degree, 0.5)

  void main () {
    vec4 pos = vec4(a_position, 1);

    #if CC_USE_MODEL
    pos = cc_matViewProj * cc_matWorld * pos;
    #else
    pos = cc_matViewProj * pos;
    #endif

LCC_ATTR_VERT_PASS_IF(USE_TEXTURE, uv0)
LCC_VALUE_VERT_PASS(UVRECT, uvrect)
LCC_VALUE_VERT_PASS(FRAMESIZE, framesize)
LCC_VALUE_VERT_PASS(COLOR, color)
LCC_VALUE_VERT_PASS(DEGREE, degree)

    gl_Position = pos;
  }
}%


CCProgram fs %{
  precision highp float;
  
  #include <alpha-test>

//---------------- defines
	
// 属性宏
#define LCC_MACRO_ATTRIBUTE(macro)	ATTR_##macro

// 常量宏
#define LCC_MACRO_UNIFORM(macro)		UNIF_##macro

// 检查宏定义
#define LCC_MACRO_DEFINED(macro) LCC_MACRO_ATTRIBUTE(macro)||LCC_MACRO_UNIFORM(macro)

// 顶点着色器 - 变量定义
#define LCC_VALUE_VERT_DEFINE(macro, type, name, defval)	\
	#if LCC_MACRO_ATTRIBUTE(macro)	\
	in type a_##name;	\
	out type name;	\
    #elif LCC_MACRO_UNIFORM(macro)	\
	uniform LCC_MACRO_UNIFORM(macro) {	\
		type u_##name;	\
	};	\
	out type name;	\
	#else	\
	type D_##name = defval;	\
	out type name;	\
	#endif

// 顶点着色器 - 变量传递
#define LCC_VALUE_VERT_PASS(macro, name)	\
	#if LCC_MACRO_ATTRIBUTE(macro)	\
	name = a_##name;	\
    #elif LCC_MACRO_UNIFORM(macro)	\
    name = u_##name;	\
    #else	\
    name = D_##name;	\
	#endif

// 片段着色器 - 变量定义
#define LCC_VALUE_FRAG_IMPORT(type, name)	\
	in type name;

// 顶点着色器 - 如果定义了宏，属性定义
#define LCC_ATTR_VERT_DEFINE_IF(fmacro, type, name)	\
	#if fmacro	\
	in type a_##name;	\
	out type name;	\
	#endif

// 顶点着色器 - 如果定义了宏，属性定传递
#define LCC_ATTR_VERT_PASS_IF(fmacro, name)	\
	#if fmacro	\
	name = a_##name;	\
	#endif

// 片段着色器 - 如果定义了宏，属性导入
#define LCC_ATTR_FRAG_IMPORT_IF(fmacro, type, name)	\
	#if fmacro	\
	in type name;	\
	#endif

// UV 转换到本地UV
#define LCC_UV_TO_LOCALUV(uv, uvrect) \
	vec2((uv.x - uvrect[0]) / uvrect[2], (uv.y - uvrect[1]) / uvrect[3])

// 本地UV 转换到UV
#define LCC_LOCALUV_TO_UV(luv, uvrect)	\
	vec2(uvrect[0] + luv.x * uvrect[2], uvrect[1] + luv.y * uvrect[3])
//---------------- 

  #if USE_TEXTURE
  uniform sampler2D texture;
  #endif

LCC_ATTR_FRAG_IMPORT_IF(USE_TEXTURE, vec2, uv0)
LCC_VALUE_FRAG_IMPORT(vec4, uvrect)
LCC_VALUE_FRAG_IMPORT(vec2, framesize)
LCC_VALUE_FRAG_IMPORT(vec4, color)
LCC_VALUE_FRAG_IMPORT(float, degree)

  void main () {
    vec4 o = vec4(1, 1, 1, 1);

    #if USE_TEXTURE
    o *= texture(texture, uv0);
      #if CC_USE_ALPHA_ATLAS_TEXTURE
      o.a *= texture2D(texture, uv0 + vec2(0, 0.5)).r;
      #endif
    #endif

    o *= color;

    ALPHA_TEST(o);

	float ratio = framesize.x / framesize.y;
	float xRadius = 0.5;
	float yRadius = 0.5;
	if(framesize.x < framesize.y){
		xRadius = 0.5 * degree;
		yRadius = xRadius * ratio;
	}else{
		yRadius = 0.5 * degree;
		xRadius = yRadius / ratio;
	}

    // 约束圆角半径范围在 [0.0, 0.5]
    // 
    // 请注意这里我是用椭圆前缀去命名的半径
    // 
    // 为什么是椭圆？
    // 
    // 因为圆角，相对于长方形的纹理的宽高来说，归一化后值并不一样，不是圆，而是一个椭圆
    //
    // 比如：
    //
    // 纹理是 200 x 100 的像素，圆角半径是20像素，那么归一化后
    //    X轴上的半径就是 20 / 200 = 0.1 
    //    Y轴上的半径就是 20 / 100 = 0.2
    //
    // 这就会变成是椭圆，而不是圆
    float ellipseXRadius = clamp(0.0, 0.5, xRadius);
    float ellipseYRadius = clamp(0.0, 0.5, yRadius);

    // 将纹理uv往左上偏移，实现偏移后的坐标系原点在纹理中心
	vec2 ruv0 = LCC_UV_TO_LOCALUV(uv0, uvrect);
    vec2 uv = ruv0.xy - vec2(0.5, 0.5);

    // uv.x , uv.y :              为偏移后的的uv
    // abs(uv.x) , abs(uv.y) :    将第二、三、四象限的点都投影到第一象限上，这样子只需要处理第一象限的情况就可以，简化判断
    // 0.5 - radius :             计算出第一象限的圆角所在圆的圆心坐标
    // (rx, ry) :                 偏移映射后的 新的uv 坐标，相对于 第一象限圆角坐在圆心坐标 的相对坐标
    float rx = abs(uv.x) - (0.5 - ellipseXRadius);
    float ry = abs(uv.y) - (0.5 - ellipseYRadius);

    // 区分 以第一象限圆角所在圆心坐标为原点的坐标的四个象限
    //
    // 第一象限 mx = 1, my = 1
    // 第二象限 mx = 0, my = 1
    // 第三象限 mx = 0, my = 0
    // 第四象限 mx = 1, my = 0
    // 
    // 当 mx * my 时，只要等于1，那就是标识第一象限（实际对应圆角区域所在矩形），否则就是第二、三、四象限
    float mx = step(0.5 - ellipseXRadius, abs(uv.x));
    float my = step(0.5 - ellipseYRadius, abs(uv.y));

    // 判断点(rx, ry)是否在椭圆外部（应用椭圆公式）
    float isOutOfEllipse = step(1.0, pow(rx, 2.0) / pow(xRadius, 2.0) + pow(ry, 2.0) / pow(yRadius, 2.0));

    ///////////////////////////////////////////////////////////////////////////////////////////
    // 抗锯齿
    // 1. 先计算当前点到椭圆中心的角度
    float angleInRadian = atan(ry / rx);

    // 2. 计算这个角度下，对于对应圆角（椭圆）上的点 
    vec2 pointInEllipse = vec2(xRadius * cos(angleInRadian), yRadius * sin(angleInRadian));

    // 3. 计算这个角度下，比当前圆角大一点椭圆上的点
    vec2 pointInBigEllipse = vec2((xRadius * 1.01) * cos(angleInRadian), (yRadius * 1.01)* sin(angleInRadian));

    // 4. 计算最远点到当前椭圆的距离
    float maxDis = distance(pointInBigEllipse, pointInEllipse);

    // 5. 计算当前点到当前椭圆的距离
    float curDis = distance(vec2(rx, ry), pointInEllipse);

    // 6. 生成插值
    float smo = smoothstep(0.0, maxDis, curDis);
    ///////////////////////////////////////////////////////////////////////////////////////////

    // mx * my = 0 时，代表非椭圆角区域，alpha 值为1，代表完全采用原始纹理的透明度
    // mx * my = 1 时，代表椭圆角所在矩形区域
    //  isOutOfEllipse: 
    //    当点在椭圆外部时，此值为1，导致 alpha 最终值为0.0，即表示不显示椭圆外部的像素
    //    当点在椭圆内部时，此值为0，导致 alpha 最终值为1.0，即表示显示椭圆内部的像素
    //  smo : 抗锯齿实现
    // float alpha = 1.0 - mx * my * isOutOfEllipse;
    float alpha = 1.0 - mx * my * isOutOfEllipse * smo;

    o = vec4(o.rgb, o.a * alpha);

    gl_FragColor = o;
  }
}%
