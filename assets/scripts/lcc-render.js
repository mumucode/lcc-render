var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode;
    var MacroConfig = (function () {
        function MacroConfig() {
            var params = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                params[_i] = arguments[_i];
            }
            this.name = "";
            this.checkOnly = false;
            this.name = params[0] || "";
            this.checkOnly = params[1] || false;
        }
        __decorate([
            property({
                tooltip: "宏名称(为空表示不启用)"
            })
        ], MacroConfig.prototype, "name", void 0);
        __decorate([
            property({
                tooltip: "只用于检查"
            })
        ], MacroConfig.prototype, "checkOnly", void 0);
        MacroConfig = __decorate([
            ccclass("lcc$render.MacroConfig")
        ], MacroConfig);
        return MacroConfig;
    }());
    lcc$render.MacroConfig = MacroConfig;
    var VariableType;
    (function (VariableType) {
        VariableType[VariableType["ATTRIBUTE"] = 1] = "ATTRIBUTE";
        VariableType[VariableType["UNIFORM"] = 2] = "UNIFORM";
    })(VariableType = lcc$render.VariableType || (lcc$render.VariableType = {}));
    ;
    var ValueType;
    (function (ValueType) {
        ValueType[ValueType["SINGLE"] = 0] = "SINGLE";
        ValueType[ValueType["ARRAY"] = 1] = "ARRAY";
    })(ValueType = lcc$render.ValueType || (lcc$render.ValueType = {}));
    ;
    var VariableConfig = (function () {
        function VariableConfig() {
            var params = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                params[_i] = arguments[_i];
            }
            this._typesel = true;
            this.type = VariableType.UNIFORM;
            this.attrMacro = new MacroConfig();
            this.attrName = "";
            this.unifMacro = new MacroConfig();
            this.unifName = "";
            this._typesel = params[0];
            this.type = params[1] || VariableType.UNIFORM;
            this.attrMacro = new MacroConfig(params[2] || "", params[3] || false);
            this.attrName = params[4] || "";
            this.unifMacro = new MacroConfig(params[5] || "", params[6] || false);
            this.unifName = params[7] || "";
        }
        __decorate([
            property()
        ], VariableConfig.prototype, "_typesel", void 0);
        __decorate([
            property({
                type: cc.Enum(VariableType),
                tooltip: "变量类型",
                visible: function () {
                    return this._typesel;
                }
            })
        ], VariableConfig.prototype, "type", void 0);
        __decorate([
            property({
                type: MacroConfig,
                tooltip: "属性变量宏定义",
                visible: function () {
                    return this.type == VariableType.ATTRIBUTE;
                }
            })
        ], VariableConfig.prototype, "attrMacro", void 0);
        __decorate([
            property({
                tooltip: "属性变量名",
                visible: function () {
                    return this.type == VariableType.ATTRIBUTE;
                }
            })
        ], VariableConfig.prototype, "attrName", void 0);
        __decorate([
            property({
                type: MacroConfig,
                tooltip: "一般变量宏定义",
                visible: function () {
                    return this.type == VariableType.UNIFORM;
                }
            })
        ], VariableConfig.prototype, "unifMacro", void 0);
        __decorate([
            property({
                tooltip: "一般变量名",
                visible: function () {
                    return this.type == VariableType.UNIFORM;
                }
            })
        ], VariableConfig.prototype, "unifName", void 0);
        VariableConfig = __decorate([
            ccclass("lcc$render.VariableConfig")
        ], VariableConfig);
        return VariableConfig;
    }());
    lcc$render.VariableConfig = VariableConfig;
    var ShaderComponent = (function (_super) {
        __extends(ShaderComponent, _super);
        function ShaderComponent() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._tag = "";
            return _this;
        }
        Object.defineProperty(ShaderComponent.prototype, "tag", {
            get: function () {
                return this._tag;
            },
            set: function (value) {
                if (this._tag != value) {
                    this._tag = value;
                    this.node.emit("shader_update_tag");
                }
            },
            enumerable: false,
            configurable: true
        });
        ShaderComponent.prototype.checkMaterialMacro = function (material, macro) {
            return !macro.name || (macro.checkOnly ? material.getDefine(macro.name) :
                (material.getDefine(macro.name) !== undefined));
        };
        ShaderComponent.prototype.defineMaterialMacro = function (material, macro, value) {
            if (macro.name && !macro.checkOnly) {
                material.define(macro.name, value);
            }
        };
        ShaderComponent.prototype.getShaderComponent = function (type, tag) {
            if (tag === undefined) {
                return this.getComponent(type);
            }
            else {
                for (var _i = 0, _a = this.getComponents(type); _i < _a.length; _i++) {
                    var comp = _a[_i];
                    if (comp._tag == tag) {
                        return comp;
                    }
                }
            }
        };
        ShaderComponent.prototype.getShaderComponents = function (type, tag) {
            if (tag === undefined) {
                return this.getComponents(type);
            }
            else {
                var comps = this.getComponents(type);
                return comps.filter(function (a) { a._tag == tag; });
            }
        };
        __decorate([
            property()
        ], ShaderComponent.prototype, "_tag", void 0);
        __decorate([
            property({
                tooltip: "组件标签"
            })
        ], ShaderComponent.prototype, "tag", null);
        ShaderComponent = __decorate([
            ccclass("lcc$render.ShaderComponent"),
            executeInEditMode()
        ], ShaderComponent);
        return ShaderComponent;
    }(cc.Component));
    lcc$render.ShaderComponent = ShaderComponent;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var gfx = cc.gfx;
    ;
    var ShaderVariableType;
    (function (ShaderVariableType) {
        ShaderVariableType[ShaderVariableType["ATTRIBUTE"] = 0] = "ATTRIBUTE";
        ShaderVariableType[ShaderVariableType["UNIFORM"] = 1] = "UNIFORM";
    })(ShaderVariableType = lcc$render.ShaderVariableType || (lcc$render.ShaderVariableType = {}));
    ;
    var ATTR_POSITION = { name: gfx.ATTR_POSITION, type: gfx.ATTR_TYPE_FLOAT32, num: 2 };
    var ShapeType;
    (function (ShapeType) {
        ShapeType[ShapeType["SHADER"] = 0] = "SHADER";
        ShapeType[ShapeType["NODE"] = 1] = "NODE";
    })(ShapeType || (ShapeType = {}));
    var RenderSystem = (function (_super) {
        __extends(RenderSystem, _super);
        function RenderSystem() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.attrsSize = 0;
            _this._matsDirty = true;
            _this._shapeDirty = true;
            _this._attrsDirty = true;
            _this._vFormat = null;
            _this._vAttributes = null;
            _this._shapeType = ShapeType.NODE;
            _this._verticesCount = 0;
            _this._indicesCount = 0;
            _this._localRect = null;
            _this._srcBlendFactor = cc.macro.BlendFactor.SRC_ALPHA;
            _this._dstBlendFactor = cc.macro.BlendFactor.ONE_MINUS_SRC_ALPHA;
            return _this;
        }
        RenderSystem.prototype.onEnable = function () {
            _super.prototype.onEnable.call(this);
            this.node.on(cc.Node.EventType.SIZE_CHANGED, this.onNodeSizeChanged, this);
            this.node.on("shader_update_material", this.setMatsDirty, this);
            this.node.on("shader_update_attribute", this.setAttrsDirty, this);
            this.node.on("shader_update_shape", this.checkRenderShape, this);
            this.node.on("shader_update_vertex", this.setVertsDirty, this);
            this.setDirty();
        };
        RenderSystem.prototype.onDisable = function () {
            _super.prototype.onDisable.call(this);
            this.node.targetOff(this);
        };
        RenderSystem.prototype.setMatsDirty = function () {
            this._matsDirty = true;
            this.setVertsDirty();
        };
        RenderSystem.prototype.setAttrsDirty = function () {
            this._attrsDirty = true;
            this.setShapeDirty();
        };
        RenderSystem.prototype.setShapeDirty = function () {
            this._shapeDirty = true;
            this.setVertsDirty();
        };
        RenderSystem.prototype.checkRenderShape = function () {
            this.setShapeDirty();
            this._shapeType = ShapeType.NODE;
            this.node.emit("render_check_shape", this);
            if (this._shapeType == ShapeType.NODE) {
                this._verticesCount = 4;
                this._indicesCount = 6;
            }
            this.node.emit("render_shape_checked", this);
        };
        RenderSystem.prototype.setShaderShape = function (verticesCount, indicesCount) {
            this._shapeType = ShapeType.SHADER;
            this._verticesCount = verticesCount;
            this._indicesCount = indicesCount;
        };
        RenderSystem.prototype.onNodeSizeChanged = function () {
            if (this._shapeType === ShapeType.NODE) {
                this.setVertsDirty();
            }
        };
        RenderSystem.prototype.setVertsDirty = function () {
            _super.prototype.setVertsDirty.call(this);
        };
        RenderSystem.prototype.setDirty = function () {
            this.checkRenderShape();
            this._matsDirty = true;
            this._attrsDirty = true;
            this._shapeDirty = true;
            this.setVertsDirty();
        };
        RenderSystem.prototype.updateToNodeShape = function () {
            var rdata = this._assembler.renderData;
            rdata.initQuadIndices(rdata.iDatas[0]);
            this._localRect = [0, 0, 0, 0];
        };
        RenderSystem.prototype.updateNodeVertex = function () {
            var node = this.node, cw = node.width, ch = node.height, appx = node.anchorX * cw, appy = node.anchorY * ch, l, b, r, t;
            l = -appx;
            b = -appy;
            r = cw - appx;
            t = ch - appy;
            var local = this._localRect;
            local[0] = l;
            local[1] = b;
            local[2] = r;
            local[3] = t;
            this.updateWorldVerts();
        };
        RenderSystem.prototype.updateRenderData = function () {
            this.prepareRender();
            this.updateMaterial();
            this.updateAttribute();
            this.updateShape();
            this.updateVertex();
        };
        RenderSystem.prototype.prepareRender = function () {
            this.node.emit("render_prepare", this);
        };
        RenderSystem.prototype.updateMaterial = function () {
            if (this._matsDirty) {
                this.node.emit("render_update_material", this);
                this._updateBlendFunc(false);
                this._matsDirty = false;
            }
        };
        RenderSystem.prototype.checkVertexAttributeName = function (name) {
            for (var _i = 0, _a = this._vAttributes; _i < _a.length; _i++) {
                var va = _a[_i];
                if (va.name == name) {
                    return false;
                }
            }
            return true;
        };
        RenderSystem.prototype.addVertexAttribute = function (vattr, floatsize) {
            var offest = -1;
            var assembler = this._assembler;
            if (this.checkVertexAttributeName(vattr.name)) {
                offest = assembler.floatsPerVert;
                this._vAttributes.push(vattr);
                assembler.floatsPerVert += floatsize;
                if (CC_EDITOR) {
                    this.attrsSize = assembler.floatsPerVert * this._verticesCount;
                }
            }
            else {
                if (CC_EDITOR) {
                    Editor.error("attribute name conflict " + vattr.name);
                }
                else {
                    cc.error("attribute name conflict " + vattr.name);
                }
            }
            return offest;
        };
        RenderSystem.prototype.getVfmt = function () {
            if (!this._vFormat) {
                this._vFormat = new gfx.VertexFormat(this._vAttributes);
            }
            return this._vFormat;
        };
        RenderSystem.prototype.updateAttribute = function () {
            if (this._attrsDirty) {
                var assembler = this._assembler;
                this._vFormat = null;
                this._vAttributes = [ATTR_POSITION];
                assembler.floatsPerVert = 2;
                if (CC_EDITOR) {
                    this.attrsSize = assembler.floatsPerVert * this._verticesCount;
                }
                this.node.emit("render_update_attribute", this);
                this._attrsDirty = false;
            }
        };
        RenderSystem.prototype.updateShape = function () {
            if (this._shapeDirty) {
                var assembler = this._assembler;
                assembler.verticesCount = this._verticesCount;
                assembler.indicesCount = this._indicesCount;
                assembler.resetRenderData();
                if (this._shapeType === ShapeType.NODE) {
                    this.updateToNodeShape();
                }
                else {
                    this.node.emit("render_update_shape", this);
                }
                this._shapeDirty = false;
            }
        };
        RenderSystem.prototype.updateVertex = function () {
            if (this._vertsDirty) {
                if (this._shapeType === ShapeType.NODE) {
                    this.updateNodeVertex();
                }
                this.node.emit("render_update_vertex", this);
                this._vertsDirty = false;
            }
        };
        RenderSystem.prototype.updateWorldVerts = function () {
            if (this._shapeType === ShapeType.NODE) {
                if (CC_NATIVERENDERER) {
                    this.updateNodeWorldVertsNative();
                }
                else {
                    this.updateNodeWorldVertsWebGL();
                }
            }
            else {
                this.node.emit("render_update_worldvertex", this);
            }
        };
        RenderSystem.prototype.updateNodeWorldVertsWebGL = function () {
            var assembler = this._assembler;
            var local = this._localRect;
            var verts = assembler.renderData.vDatas[0];
            var matrix = this.node._worldMatrix;
            var matrixm = matrix.m, a = matrixm[0], b = matrixm[1], c = matrixm[4], d = matrixm[5], tx = matrixm[12], ty = matrixm[13];
            var vl = local[0], vr = local[2], vb = local[1], vt = local[3];
            var justTranslate = a === 1 && b === 0 && c === 0 && d === 1;
            var index = 0;
            var floatsPerVert = assembler.floatsPerVert;
            if (justTranslate) {
                verts[index] = vl + tx;
                verts[index + 1] = vb + ty;
                index += floatsPerVert;
                verts[index] = vr + tx;
                verts[index + 1] = vb + ty;
                index += floatsPerVert;
                verts[index] = vl + tx;
                verts[index + 1] = vt + ty;
                index += floatsPerVert;
                verts[index] = vr + tx;
                verts[index + 1] = vt + ty;
            }
            else {
                var al = a * vl, ar = a * vr, bl = b * vl, br = b * vr, cb = c * vb, ct = c * vt, db = d * vb, dt = d * vt;
                verts[index] = al + cb + tx;
                verts[index + 1] = bl + db + ty;
                index += floatsPerVert;
                verts[index] = ar + cb + tx;
                verts[index + 1] = br + db + ty;
                index += floatsPerVert;
                verts[index] = al + ct + tx;
                verts[index + 1] = bl + dt + ty;
                index += floatsPerVert;
                verts[index] = ar + ct + tx;
                verts[index + 1] = br + dt + ty;
            }
        };
        RenderSystem.prototype.updateNodeWorldVertsNative = function () {
            var assembler = this._assembler;
            var local = this._localRect;
            var verts = assembler.renderData.vDatas[0];
            var floatsPerVert = assembler.floatsPerVert;
            var vl = local[0], vr = local[2], vb = local[1], vt = local[3];
            var index = 0;
            verts[index] = vl;
            verts[index + 1] = vb;
            index += floatsPerVert;
            verts[index] = vr;
            verts[index + 1] = vb;
            index += floatsPerVert;
            verts[index] = vl;
            verts[index + 1] = vt;
            index += floatsPerVert;
            verts[index] = vr;
            verts[index + 1] = vt;
        };
        RenderSystem.prototype.getShaderComponent = function (type, tag) {
            if (tag === undefined) {
                return this.getComponent(type);
            }
            else {
                for (var _i = 0, _a = this.getComponents(type); _i < _a.length; _i++) {
                    var comp = _a[_i];
                    if (comp._tag == tag) {
                        return comp;
                    }
                }
            }
        };
        RenderSystem.prototype.getShaderComponents = function (type, tag) {
            if (tag === undefined) {
                return this.getComponents(type);
            }
            else {
                var comps = this.getComponents(type);
                return comps.filter(function (a) { a._tag == tag; });
            }
        };
        RenderSystem.prototype._activateMaterial = function () {
            _super.prototype._activateMaterial.call(this);
            this.setDirty();
        };
        RenderSystem.prototype._updateMaterial = function () {
            this.updateMaterial();
        };
        RenderSystem.prototype._validateRender = function () {
            if (!this._materials[0]) {
                return this.disableRender();
            }
            this.node.emit("render_validate_render", this);
        };
        Object.defineProperty(RenderSystem.prototype, "srcBlendFactor", {
            get: function () {
                return this._srcBlendFactor;
            },
            set: function (value) {
                if (this._srcBlendFactor === value)
                    return;
                this._srcBlendFactor = value;
                this._updateBlendFunc(true);
                this._onBlendChanged && this._onBlendChanged();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(RenderSystem.prototype, "dstBlendFactor", {
            get: function () {
                return this._dstBlendFactor;
            },
            set: function (value) {
                if (this._dstBlendFactor === value)
                    return;
                this._dstBlendFactor = value;
                this._updateBlendFunc(true);
            },
            enumerable: false,
            configurable: true
        });
        RenderSystem.prototype.setMaterial = function (index, material) {
            var materialVar = _super.prototype.setMaterial.call(this, index, material);
            if (this._srcBlendFactor !== cc.macro.BlendFactor.SRC_ALPHA ||
                this._dstBlendFactor !== cc.macro.BlendFactor.ONE_MINUS_SRC_ALPHA) {
                this._updateMaterialBlendFunc(materialVar);
            }
            this.setMatsDirty();
            return materialVar;
        };
        RenderSystem.prototype._updateBlendFunc = function (force) {
            if (!force) {
                if (this._srcBlendFactor === cc.macro.BlendFactor.SRC_ALPHA &&
                    this._dstBlendFactor === cc.macro.BlendFactor.ONE_MINUS_SRC_ALPHA) {
                    return;
                }
            }
            var materials = this.getMaterials();
            for (var i = 0; i < materials.length; i++) {
                var material = materials[i];
                this._updateMaterialBlendFunc(material);
            }
        };
        RenderSystem.prototype._updateMaterialBlendFunc = function (material) {
            material.setBlend(true, gfx.BLEND_FUNC_ADD, this._srcBlendFactor, this._dstBlendFactor, gfx.BLEND_FUNC_ADD, this._srcBlendFactor, this._dstBlendFactor);
        };
        __decorate([
            property({
                editorOnly: true,
                serializable: false,
                readonly: true,
                tooltip: "统计属性所使用的空间大小"
            })
        ], RenderSystem.prototype, "attrsSize", void 0);
        __decorate([
            property({
                animatable: false,
                type: cc.macro.BlendFactor,
                tooltip: CC_DEV && 'i18n:COMPONENT.sprite.src_blend_factor',
            })
        ], RenderSystem.prototype, "srcBlendFactor", null);
        __decorate([
            property({
                animatable: false,
                type: cc.macro.BlendFactor,
                tooltip: CC_DEV && 'i18n:COMPONENT.sprite.dst_blend_factor',
            })
        ], RenderSystem.prototype, "dstBlendFactor", null);
        RenderSystem = __decorate([
            ccclass("lcc$render.RenderSystem"),
            menu("i18n:lcc-render.menu_component/RenderSystem")
        ], RenderSystem);
        return RenderSystem;
    }(cc.RenderComponent));
    lcc$render.RenderSystem = RenderSystem;
})(lcc$render || (lcc$render = {}));
window.lcc$render = lcc$render;
var lcc$render;
(function (lcc$render) {
    var Utils = (function () {
        function Utils() {
        }
        Utils.getAssetByUUID = function (uuid) {
            return new Promise(function (resolve) {
                cc.assetManager.loadAny([uuid], function (err, asset) {
                    if (!err && asset) {
                        resolve(asset);
                    }
                    else {
                        cc.warn("not found asset : %s", uuid);
                        resolve(null);
                    }
                });
            });
        };
        return Utils;
    }());
    lcc$render.Utils = Utils;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a, _b;
    lcc$render.UUID = {
        effects: (_a = {},
            _a["lcc-2d-flash_light"] = "9350cd53-72d3-445e-9b64-6025f9b033f3",
            _a["lcc-2d-fluxay_super"] = "e0032378-75a3-404a-ae4e-fe63bd91c875",
            _a["lcc-2d-sprite_mask"] = "6f8dd8eb-f50d-4d02-8ae7-46d4bb3f73fa",
            _a["lcc-2d_glow_inner"] = "8bf46498-6576-462b-8168-7640ce52198c",
            _a["lcc-2d_glow_outter"] = "5b49d2f4-9343-47a6-84f4-2bfeeaf0eb63",
            _a["lcc-2d_mosaic"] = "df7ce505-6676-4f9d-8bae-5f87bd53f1c2",
            _a["lcc-2d_photo"] = "d3414b31-e561-40e8-b78e-0ad38f8bc7c9",
            _a["lcc-2d_outline"] = "e0be8740-5375-4ae9-b246-784c147bbfb9",
            _a["lcc-2d_gaussian_blur"] = "41f4d474-d707-45bb-af93-637573f92d54",
            _a["lcc-2d_round_corner_crop"] = "7c24b57e-e819-4fc9-a8d2-b06cf61b782d",
            _a["lcc-2d_dissolve"] = "ab297d98-4c09-4ca1-945c-a7fa8e290bca",
            _a["lcc-2d_transform"] = "1bef22fb-8433-4049-ad35-1fbf9e7348ad",
            _a["lcc-2d_rain_drop_ripples"] = "f345e656-06d2-42b3-89ec-938182b32113",
            _a),
        materials: (_b = {},
            _b["lcc-2d-flash_light"] = "58ce2696-4b7b-480e-af98-53b5bae48be3",
            _b["lcc-2d-fluxay_super"] = "46b159f9-9673-4c36-9826-b98e5af85f19",
            _b["lcc-2d-sprite_mask"] = "5832437a-f25e-4094-8c11-d14e5be5f4f9",
            _b["lcc-2d_glow_inner"] = "417f75cf-08d7-4d0f-82f7-299c79529eaa",
            _b["lcc-2d_glow_outter"] = "2380adcd-9331-4eb8-9b73-5c9bb2d72d8d",
            _b["lcc-2d_mosaic"] = "4b6195fb-f172-4a97-b618-18aa51ebf3aa",
            _b["lcc-2d_photo"] = "06e41864-4dc6-489a-8867-95e829db0e5b",
            _b["lcc-2d_outline"] = "e24b3cff-b2e0-49aa-b115-1370964437dc",
            _b["lcc-2d_gaussian_blur"] = "dd3d8f78-9b79-4ca7-9bf7-7a09f7b34108",
            _b["lcc-2d_round_corner_crop"] = "a86e8864-5390-443f-b41b-b38e9d584c43",
            _b["lcc-2d_dissolve"] = "4659d83b-f274-42c4-874e-6d5f3e220adc",
            _b["lcc-2d_transform"] = "445e85ce-defc-47c6-821f-eaaf831533fb",
            _b["lcc-2d_rain_drop_ripples"] = "67bace4b-a406-4378-98a9-319ea18e9c6d",
            _b)
    };
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var gfx = cc.gfx;
    var _temp_color = cc.color();
    var ShaderColor = (function (_super) {
        __extends(ShaderColor, _super);
        function ShaderColor() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._tag = "color";
            _this._colorType = lcc$render.ValueType.SINGLE;
            _this._colorVar = new lcc$render.VariableConfig(true, lcc$render.VariableType.UNIFORM, "ATTR_COLOR", false, "a_color", "UNIF_COLOR", false, "u_color");
            _this._colors = [cc.Color.WHITE];
            _this._useNodeColor = false;
            _this._colorOffset = 0;
            return _this;
        }
        Object.defineProperty(ShaderColor.prototype, "colorType", {
            get: function () {
                return this._colorType;
            },
            set: function (value) {
                if (this._colorType != value) {
                    this._colorType = value;
                    if (value == lcc$render.ValueType.ARRAY) {
                        this._colorVar.type == lcc$render.VariableType.UNIFORM;
                        this._colorVar._typesel = false;
                    }
                    else {
                        this._colorVar._typesel = true;
                    }
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderColor.prototype, "colorVar", {
            get: function () {
                return this._colorVar;
            },
            set: function (value) {
                this._colorVar = value;
                this.onUpdateColors();
                this.onRenderUpdateMaterial();
                this.node.emit("shader_update_attribute");
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderColor.prototype, "useNodeColor", {
            get: function () {
                return this._useNodeColor;
            },
            set: function (value) {
                if (this._useNodeColor != value) {
                    this._useNodeColor = value;
                    if (value) {
                        this.onNodeColorChanged();
                    }
                    else {
                        this.onUpdateColors();
                    }
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderColor.prototype, "color", {
            get: function () {
                return this._colors[0];
            },
            set: function (value) {
                this._useNodeColor = false;
                this._colors[0] = value;
                this.onUpdateColors();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderColor.prototype, "colors", {
            get: function () {
                return this._colors;
            },
            set: function (value) {
                this._useNodeColor = false;
                this._colors = value;
                this.onUpdateColors();
            },
            enumerable: false,
            configurable: true
        });
        ShaderColor.prototype.onLoad = function () {
            this.node.on("render_shape_checked", this.onRenderShapeChecked, this);
            this.node.on("render_update_material", this.onRenderUpdateMaterial, this);
            this.node.on("render_update_attribute", this.onRenderUpdateAttribute, this);
            this.node.on("render_update_vertex", this.onRenderUpdateRenderData, this);
            this.node.on(cc.Node.EventType.COLOR_CHANGED, this.onNodeColorChanged, this);
        };
        ShaderColor.prototype.onDestroy = function () {
            this.node.targetOff(this);
        };
        ShaderColor.prototype.onEnable = function () {
            this.onNodeColorChanged();
            this.onStateUpdate(true);
        };
        ShaderColor.prototype.onDisable = function () {
            this.onStateUpdate(false);
        };
        ShaderColor.prototype.onStateUpdate = function (enable) {
            this.node.emit("shader_update_tag");
            if (this._useNodeColor) {
                this.onNodeColorChanged();
            }
            else {
                this.onUpdateColors();
            }
        };
        ShaderColor.prototype.onNodeColorChanged = function () {
            if (this._useNodeColor) {
                this.onUpdateColors();
            }
        };
        ShaderColor.prototype.onRenderShapeChecked = function () {
            if (this._colorVar.type == lcc$render.VariableType.ATTRIBUTE) {
                this.onUpdateColors();
            }
        };
        ShaderColor.prototype.onUpdateColors = function () {
            if (this.enabled) {
                var dcolor = cc.Color.WHITE;
                if (this._colors.length <= 0) {
                    this._colors = [dcolor.clone()];
                }
                if (this._colorVar.type == lcc$render.VariableType.ATTRIBUTE) {
                    var rsys = this.getComponent(lcc$render.RenderSystem);
                    if (rsys) {
                        var vc = rsys._verticesCount;
                        if (this._colors.length < vc) {
                            var colors = this._colors;
                            for (var i = 0; i < vc; i++) {
                                var color = colors[i];
                                if (color == null) {
                                    colors[i] = dcolor.clone();
                                }
                            }
                        }
                        this._colors.length = vc;
                    }
                }
                else if (this._colorType == lcc$render.ValueType.SINGLE) {
                    this._colors.length = 1;
                }
                if (this._useNodeColor) {
                    var ncolor = this.node.color;
                    for (var i = 0, l = this._colors.length; i < l; i++) {
                        this._colors[i] = ncolor.clone();
                    }
                }
                if (this._colorVar.type == lcc$render.VariableType.ATTRIBUTE) {
                    this.node.emit("shader_update_attribute");
                }
                else {
                    this.onRenderUpdateMaterial();
                }
            }
        };
        ShaderColor.prototype.premultiplyAlpha = function (color) {
            cc.Color.premultiplyAlpha(_temp_color, color);
            return _temp_color._val;
        };
        ShaderColor.prototype.getAttributeColors = function (comp) {
            var premultiply = comp._srcBlendFactor === cc.macro.BlendFactor.ONE;
            var colors = [];
            if (this._colorVar.type === lcc$render.VariableType.ATTRIBUTE) {
                for (var i = 0, l = this._colors.length; i < l; i++) {
                    var c = this._colors[i];
                    colors.push(premultiply ? this.premultiplyAlpha(c) : c._val);
                }
            }
            return colors;
        };
        ShaderColor.prototype.getUniformColors = function (comp) {
            var premultiply = comp._srcBlendFactor === cc.macro.BlendFactor.ONE;
            var colors = [];
            if (this._colorVar.type === lcc$render.VariableType.UNIFORM) {
                for (var i = 0, l = this._colors.length; i < l; i++) {
                    var c = this._colors[i];
                    if (premultiply) {
                        cc.Color.premultiplyAlpha(_temp_color, c);
                        c = _temp_color;
                    }
                    colors.push(new cc.Vec4(c.r / 255, c.g / 255, c.b / 255, c.a / 255));
                }
            }
            return colors;
        };
        ShaderColor.prototype.onRenderUpdateMaterial = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._colorVar.unifMacro)) {
                        if (this.enabled && this._colorVar.type === lcc$render.VariableType.UNIFORM) {
                            this.defineMaterialMacro(material, this._colorVar.unifMacro, true);
                            var colors = this.getUniformColors(comp);
                            if (this._colorType == lcc$render.ValueType.SINGLE) {
                                material.setProperty(this._colorVar.unifName, colors[0]);
                            }
                            else {
                                var _colors = [];
                                for (var i = 0, l = colors.length; i < l; i++) {
                                    var c = colors[i];
                                    _colors.push(c.x, c.y, c.z, c.w);
                                }
                                material.setProperty(this._colorVar.unifName, new Float32Array(colors.length * 4));
                                material.setProperty(this._colorVar.unifName, _colors);
                            }
                        }
                        else {
                            this.defineMaterialMacro(material, this._colorVar.unifMacro, false);
                        }
                    }
                }
            }
        };
        ShaderColor.prototype.onRenderUpdateAttribute = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._colorVar.attrMacro)) {
                        if (this.enabled && this._colorVar.type === lcc$render.VariableType.ATTRIBUTE) {
                            this.defineMaterialMacro(material, this._colorVar.attrMacro, true);
                            this._colorOffset = comp.addVertexAttribute({
                                name: this._colorVar.attrName,
                                type: gfx.ATTR_TYPE_UINT8,
                                num: 4,
                                normalize: true
                            }, 1);
                        }
                        else {
                            this.defineMaterialMacro(material, this._colorVar.attrMacro, false);
                            this._colorOffset = 0;
                        }
                    }
                }
            }
        };
        ShaderColor.prototype.onRenderUpdateRenderData = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp && this.enabled) {
                if (this._colorOffset > 0) {
                    var assembler = comp._assembler;
                    var uintVerts = assembler.renderData.uintVDatas[0];
                    if (uintVerts) {
                        var colors = this.getAttributeColors(comp);
                        var clen = colors.length;
                        var colorOffset = this._colorOffset;
                        var floatsPerVert = assembler.floatsPerVert;
                        for (var i = colorOffset, l = uintVerts.length, j = 0; i < l; i += floatsPerVert, j++) {
                            uintVerts[i] = j < clen ? colors[j] : colors[0];
                        }
                    }
                }
            }
        };
        __decorate([
            property({
                type: cc.Enum(lcc$render.ValueType)
            })
        ], ShaderColor.prototype, "_colorType", void 0);
        __decorate([
            property({
                type: cc.Enum(lcc$render.ValueType),
                tooltip: "颜色类型"
            })
        ], ShaderColor.prototype, "colorType", null);
        __decorate([
            property(lcc$render.VariableConfig)
        ], ShaderColor.prototype, "_colorVar", void 0);
        __decorate([
            property({
                type: lcc$render.VariableConfig,
                tooltip: "颜色变量"
            })
        ], ShaderColor.prototype, "colorVar", null);
        __decorate([
            property([cc.Color])
        ], ShaderColor.prototype, "_colors", void 0);
        __decorate([
            property()
        ], ShaderColor.prototype, "_useNodeColor", void 0);
        __decorate([
            property({
                tooltip: "使用节点的颜色"
            })
        ], ShaderColor.prototype, "useNodeColor", null);
        __decorate([
            property({
                visible: function () {
                    return this._colorType == lcc$render.ValueType.SINGLE && this._colorVar.type == lcc$render.VariableType.UNIFORM;
                },
                type: cc.Color,
                tooltip: "颜色值"
            })
        ], ShaderColor.prototype, "color", null);
        __decorate([
            property({
                visible: function () {
                    return this._colorType == lcc$render.ValueType.ARRAY || this._colorVar.type == lcc$render.VariableType.ATTRIBUTE;
                },
                type: [cc.Color],
                tooltip: "颜色数组/颜色顶点数组"
            })
        ], ShaderColor.prototype, "colors", null);
        ShaderColor = __decorate([
            ccclass("lcc$render.ShaderColor"),
            menu("i18n:lcc-render.menu_component/ShaderColor")
        ], ShaderColor);
        return ShaderColor;
    }(lcc$render.ShaderComponent));
    lcc$render.ShaderColor = ShaderColor;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var gfx = cc.gfx;
    var VC = {
        tag: "value",
        varSelect: true,
        varDeftype: lcc$render.VariableType.UNIFORM,
        varAttrMacro: "ATTR_FLOAT",
        varAttrMacroCheckOnly: false,
        varAttrName: "a_floatv",
        varUnifMacro: "UNIF_FLOAT",
        varUnifMacroCheckOnly: false,
        varUnifName: "u_floatv",
        valDefault: 0,
        valNew: function () { return 0; },
        valType: cc.Float,
        typeSize: 1,
        toArray: function (arr, value, offest) {
            arr[offest] = value;
        },
    };
    var ShaderFloat = (function (_super) {
        __extends(ShaderFloat, _super);
        function ShaderFloat() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._tag = VC.tag;
            _this._valueType = lcc$render.ValueType.SINGLE;
            _this._valueVar = new lcc$render.VariableConfig(VC.varSelect, VC.varDeftype, VC.varAttrMacro, VC.varAttrMacroCheckOnly, VC.varAttrName, VC.varUnifMacro, VC.varUnifMacroCheckOnly, VC.varUnifName);
            _this._values = [VC.valNew()];
            _this._valueOffset = 0;
            return _this;
        }
        Object.defineProperty(ShaderFloat.prototype, "valueType", {
            get: function () {
                return this._valueType;
            },
            set: function (value) {
                if (this._valueType != value) {
                    this._valueType = value;
                    if (value == lcc$render.ValueType.ARRAY) {
                        this._valueVar.type == lcc$render.VariableType.UNIFORM;
                        this._valueVar._typesel = false;
                    }
                    else {
                        this._valueVar._typesel = true;
                    }
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderFloat.prototype, "valueVar", {
            get: function () {
                return this._valueVar;
            },
            set: function (value) {
                this._valueVar = value;
                this.onUpdateValues();
                this.onRenderUpdateMaterial();
                this.node.emit("shader_update_attribute");
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderFloat.prototype, "value", {
            get: function () {
                return this._values[0];
            },
            set: function (value_) {
                this._values[0] = value_;
                this.onUpdateValues();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderFloat.prototype, "values", {
            get: function () {
                return this._values;
            },
            set: function (value) {
                this._values = value;
                this.onUpdateValues();
            },
            enumerable: false,
            configurable: true
        });
        ShaderFloat.prototype.onLoad = function () {
            this.node.on("render_shape_checked", this.onRenderShapeChecked, this);
            this.node.on("render_update_material", this.onRenderUpdateMaterial, this);
            this.node.on("render_update_attribute", this.onRenderUpdateAttribute, this);
            this.node.on("render_update_vertex", this.onRenderUpdateRenderData, this);
        };
        ShaderFloat.prototype.onDestroy = function () {
            this.node.targetOff(this);
        };
        ShaderFloat.prototype.onEnable = function () {
            this.onStateUpdate(true);
        };
        ShaderFloat.prototype.onDisable = function () {
            this.onStateUpdate(false);
        };
        ShaderFloat.prototype.onStateUpdate = function (enable) {
            this.node.emit("shader_update_tag");
            this.onUpdateValues();
        };
        ShaderFloat.prototype.onRenderShapeChecked = function () {
            if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                this.onUpdateValues();
            }
        };
        ShaderFloat.prototype.onUpdateValues = function () {
            if (this.enabled) {
                if (this._values.length <= 0) {
                    this._values = [VC.valNew()];
                }
                if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                    var rsys = this.getComponent(lcc$render.RenderSystem);
                    if (rsys) {
                        var vc = rsys._verticesCount;
                        if (this._values.length < vc) {
                            var values = this._values;
                            for (var i = 0; i < vc; i++) {
                                var color = values[i];
                                if (color == null) {
                                    values[i] = VC.valNew();
                                }
                            }
                        }
                        this._values.length = vc;
                    }
                }
                else if (this._valueType == lcc$render.ValueType.SINGLE) {
                    this._values.length = 1;
                }
                if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                    this.node.emit("shader_update_attribute");
                }
                else {
                    this.onRenderUpdateMaterial();
                }
            }
        };
        ShaderFloat.prototype.onRenderUpdateMaterial = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._valueVar.unifMacro)) {
                        if (this.enabled && this._valueVar.type === lcc$render.VariableType.UNIFORM) {
                            this.defineMaterialMacro(material, this._valueVar.unifMacro, true);
                            if (this._valueType == lcc$render.ValueType.SINGLE) {
                                material.setProperty(this._valueVar.unifName, this._values[0]);
                            }
                            else {
                                var values = this._values;
                                var _values = [];
                                var defv = VC.valDefault;
                                for (var i = 0, l = values.length; i < l; i++) {
                                    VC.toArray(_values, values[i] || defv, _values.length);
                                }
                                material.setProperty(this._valueVar.unifName, new Float32Array(values.length * VC.typeSize));
                                material.setProperty(this._valueVar.unifName, _values);
                            }
                        }
                        else {
                            this.defineMaterialMacro(material, this._valueVar.unifMacro, false);
                        }
                    }
                }
            }
        };
        ShaderFloat.prototype.getAttributeValues = function (comp) {
            var values = [];
            var vc = comp._verticesCount;
            var defv = VC.valDefault;
            for (var i = 0; i < vc; i++) {
                VC.toArray(values, this._values[i] || defv, values.length);
            }
            return values;
        };
        ShaderFloat.prototype.onRenderUpdateAttribute = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._valueVar.attrMacro)) {
                        if (this.enabled && this._valueVar.type === lcc$render.VariableType.ATTRIBUTE) {
                            this.defineMaterialMacro(material, this._valueVar.attrMacro, true);
                            this._valueOffset = comp.addVertexAttribute({
                                name: this._valueVar.attrName,
                                type: gfx.ATTR_TYPE_FLOAT32,
                                num: VC.typeSize,
                            }, VC.typeSize);
                        }
                        else {
                            this.defineMaterialMacro(material, this._valueVar.attrMacro, false);
                            this._valueOffset = 0;
                        }
                    }
                }
            }
        };
        ShaderFloat.prototype.onRenderUpdateRenderData = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp && this.enabled) {
                if (this._valueOffset > 0) {
                    var assembler = comp._assembler;
                    var values = this.getAttributeValues(comp);
                    var vlen = values.length;
                    var valueOffset = this._valueOffset;
                    var floatsPerVert = assembler.floatsPerVert;
                    var verts = assembler.renderData.vDatas[0];
                    for (var i = 0; i < 4; i++) {
                        var dstOffset = floatsPerVert * i + valueOffset;
                        var srcOffset = VC.typeSize * i;
                        if (srcOffset >= vlen) {
                            srcOffset = 0;
                        }
                        for (var j = 0; j < VC.typeSize; j++) {
                            verts[dstOffset + j] = values[srcOffset + j];
                        }
                    }
                }
            }
        };
        __decorate([
            property({
                type: cc.Enum(lcc$render.ValueType)
            })
        ], ShaderFloat.prototype, "_valueType", void 0);
        __decorate([
            property({
                type: cc.Enum(lcc$render.ValueType),
                tooltip: "值类型"
            })
        ], ShaderFloat.prototype, "valueType", null);
        __decorate([
            property(lcc$render.VariableConfig)
        ], ShaderFloat.prototype, "_valueVar", void 0);
        __decorate([
            property({
                type: lcc$render.VariableConfig,
                tooltip: "值变量"
            })
        ], ShaderFloat.prototype, "valueVar", null);
        __decorate([
            property([VC.valType])
        ], ShaderFloat.prototype, "_values", void 0);
        __decorate([
            property({
                visible: function () {
                    return this._valueType == lcc$render.ValueType.SINGLE && this._valueVar.type == lcc$render.VariableType.UNIFORM;
                },
                type: VC.valType,
                tooltip: "值"
            })
        ], ShaderFloat.prototype, "value", null);
        __decorate([
            property({
                visible: function () {
                    return this._valueType == lcc$render.ValueType.ARRAY || this._valueVar.type == lcc$render.VariableType.ATTRIBUTE;
                },
                type: [VC.valType],
                tooltip: "值数组/值顶点数组"
            })
        ], ShaderFloat.prototype, "values", null);
        ShaderFloat = __decorate([
            ccclass("lcc$render.ShaderFloat"),
            menu("i18n:lcc-render.menu_component/ShaderFloat")
        ], ShaderFloat);
        return ShaderFloat;
    }(lcc$render.ShaderComponent));
    lcc$render.ShaderFloat = ShaderFloat;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var MacroValueType;
    (function (MacroValueType) {
        MacroValueType[MacroValueType["BOOL"] = 0] = "BOOL";
        MacroValueType[MacroValueType["VALUE"] = 1] = "VALUE";
    })(MacroValueType || (MacroValueType = {}));
    var ShaderMacro = (function (_super) {
        __extends(ShaderMacro, _super);
        function ShaderMacro() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._tag = "macro";
            _this._checkMacro = "";
            _this._macroName = "";
            _this._valueType = MacroValueType.BOOL;
            _this._bool = false;
            _this._value = 0;
            return _this;
        }
        Object.defineProperty(ShaderMacro.prototype, "checkMacro", {
            get: function () {
                return this._checkMacro;
            },
            set: function (value) {
                if (this._checkMacro != value) {
                    this._checkMacro = value;
                    this.onRenderUpdateMaterial();
                    this.node.emit("shader_update_attribute");
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderMacro.prototype, "macroName", {
            get: function () {
                return this._macroName;
            },
            set: function (value) {
                if (this._macroName != value) {
                    this._macroName = value;
                    this.onRenderUpdateMaterial();
                    this.node.emit("shader_update_attribute");
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderMacro.prototype, "valueType", {
            get: function () {
                return this._valueType;
            },
            set: function (value_) {
                if (this._valueType != value_) {
                    this._valueType = value_;
                    this.onRenderUpdateMaterial();
                    this.node.emit("shader_update_attribute");
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderMacro.prototype, "bool", {
            get: function () {
                return this._bool;
            },
            set: function (value) {
                if (this._bool != value) {
                    this._bool = value;
                    this.onRenderUpdateMaterial();
                    this.node.emit("shader_update_attribute");
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderMacro.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (value) {
                if (this._value != value) {
                    this._value = value;
                    this.onRenderUpdateMaterial();
                    this.node.emit("shader_update_attribute");
                }
            },
            enumerable: false,
            configurable: true
        });
        ShaderMacro.prototype.onLoad = function () {
            this.node.on("render_update_material", this.onRenderUpdateMaterial, this);
        };
        ShaderMacro.prototype.onDestroy = function () {
            this.node.targetOff(this);
        };
        ShaderMacro.prototype.onEnable = function () {
            this.onStateUpdate(true);
        };
        ShaderMacro.prototype.onDisable = function () {
            this.onStateUpdate(false);
        };
        ShaderMacro.prototype.onStateUpdate = function (enable) {
            this.node.emit("shader_update_tag");
            this.onRenderUpdateMaterial();
            this.node.emit("shader_update_attribute");
        };
        ShaderMacro.prototype.onRenderUpdateMaterial = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp && this._macroName) {
                var material = comp._materials[0];
                if (material) {
                    if (!this._checkMacro || material.getDefine(this._checkMacro)) {
                        if (this.enabled && material.getDefine(this._macroName) !== undefined) {
                            if (this._valueType == MacroValueType.BOOL) {
                                material.define(this._macroName, this._bool);
                            }
                            else {
                                material.define(this._macroName, this._value);
                            }
                        }
                    }
                }
            }
        };
        __decorate([
            property()
        ], ShaderMacro.prototype, "_checkMacro", void 0);
        __decorate([
            property({
                tooltip: "检测宏"
            })
        ], ShaderMacro.prototype, "checkMacro", null);
        __decorate([
            property()
        ], ShaderMacro.prototype, "_macroName", void 0);
        __decorate([
            property({
                tooltip: "定义宏"
            })
        ], ShaderMacro.prototype, "macroName", null);
        __decorate([
            property({
                type: cc.Enum(MacroValueType)
            })
        ], ShaderMacro.prototype, "_valueType", void 0);
        __decorate([
            property({
                type: cc.Enum(MacroValueType),
                tooltip: "值类型"
            })
        ], ShaderMacro.prototype, "valueType", null);
        __decorate([
            property()
        ], ShaderMacro.prototype, "_bool", void 0);
        __decorate([
            property({
                visible: function () {
                    return this._valueType == MacroValueType.BOOL;
                },
                tooltip: "布尔宏值"
            })
        ], ShaderMacro.prototype, "bool", null);
        __decorate([
            property()
        ], ShaderMacro.prototype, "_value", void 0);
        __decorate([
            property({
                visible: function () {
                    return this._valueType == MacroValueType.VALUE;
                },
                tooltip: "数值宏值"
            })
        ], ShaderMacro.prototype, "value", null);
        ShaderMacro = __decorate([
            ccclass("lcc$render.ShaderMacro"),
            menu("i18n:lcc-render.menu_component/ShaderMacro")
        ], ShaderMacro);
        return ShaderMacro;
    }(lcc$render.ShaderComponent));
    lcc$render.ShaderMacro = ShaderMacro;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var gfx = cc.gfx;
    var ShapeType;
    (function (ShapeType) {
        ShapeType[ShapeType["NODE"] = 0] = "NODE";
        ShapeType[ShapeType["CUSTOM"] = 1] = "CUSTOM";
    })(ShapeType || (ShapeType = {}));
    var SizeMode;
    (function (SizeMode) {
        SizeMode[SizeMode["CUSTOM"] = 0] = "CUSTOM";
        SizeMode[SizeMode["TRIMMED"] = 1] = "TRIMMED";
        SizeMode[SizeMode["RAW"] = 2] = "RAW";
    })(SizeMode || (SizeMode = {}));
    var ShaderSpriteFrame = (function (_super) {
        __extends(ShaderSpriteFrame, _super);
        function ShaderSpriteFrame() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._tag = "spriteframe";
            _this._textureVar = new lcc$render.VariableConfig(false, lcc$render.VariableType.UNIFORM, "", false, "", "USE_TEXTURE", false, "texture");
            _this._atlas = null;
            _this._spriteFrame = null;
            _this._useShape = false;
            _this._shapeType = ShapeType.NODE;
            _this._size = cc.size(0, 0);
            _this._anchor = cc.v2(0.5, 0.5);
            _this._syncSizeToNode = true;
            _this._sizeMode = SizeMode.TRIMMED;
            _this._isTrimmedMode = true;
            _this._useUV = false;
            _this._uvVar = new lcc$render.VariableConfig(false, lcc$render.VariableType.ATTRIBUTE, "USE_TEXTURE", true, "a_uv0", "", false, "");
            _this._customUV = false;
            _this._uvIndexes = [];
            _this._useUVRect = false;
            _this._uvRectVar = new lcc$render.VariableConfig(true, lcc$render.VariableType.UNIFORM, "ATTR_UVRECT", false, "a_uvrect", "UNIF_UVRECT", false, "u_uvrect");
            _this._useFrameSize = false;
            _this._frameSizeVar = new lcc$render.VariableConfig(true, lcc$render.VariableType.UNIFORM, "ATTR_FRAMESIZE", false, "a_framesize", "UNIF_FRAMESIZE", false, "u_framesize");
            _this._shapeRect = null;
            _this._uvOffest = 0;
            _this._uvrOffset = 0;
            _this._fsOffset = 0;
            return _this;
        }
        Object.defineProperty(ShaderSpriteFrame.prototype, "textureVar", {
            get: function () {
                return this._textureVar;
            },
            set: function (value) {
                this._textureVar = value;
                this.onRenderUpdateMaterial();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame !== value) {
                    var lastSprite = this._spriteFrame;
                    if (CC_EDITOR) {
                        if (((lastSprite && lastSprite._uuid) === (value && value._uuid))) {
                            return;
                        }
                    }
                    else {
                        if (lastSprite === value) {
                            return;
                        }
                    }
                    this._spriteFrame = value;
                    this._useSpriteFrame(lastSprite);
                    if (CC_EDITOR) {
                        this.node.emit('spriteframe-changed', this);
                    }
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "useShape", {
            get: function () {
                return this._useShape;
            },
            set: function (value) {
                if (this._useShape !== value) {
                    this._useShape = value;
                    if (value) {
                        this.node.emit("shader_useshape", this);
                    }
                    this.node.emit("shader_update_shape");
                    this._useSpriteSize();
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "shapeType", {
            get: function () {
                return this._shapeType;
            },
            set: function (value) {
                if (this._shapeType !== value) {
                    this._shapeType = value;
                    this.node.emit("shader_update_vertex");
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "size", {
            get: function () {
                return this._size;
            },
            set: function (value) {
                this._size = value;
                this._sizeMode = SizeMode.CUSTOM;
                this.node.emit("shader_update_vertex");
                if (this._syncSizeToNode) {
                    this.node.setContentSize(this._size);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "anchor", {
            get: function () {
                return this._anchor;
            },
            set: function (value) {
                this._anchor = value;
                this.node.emit("shader_update_vertex");
                if (this._syncSizeToNode) {
                    this.node.setAnchorPoint(this._anchor);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "syncSizeToNode", {
            get: function () {
                return this._syncSizeToNode;
            },
            set: function (value) {
                if (this._syncSizeToNode !== value) {
                    this._syncSizeToNode = value;
                    this.node.setContentSize(this._size);
                    this.node.setAnchorPoint(this._anchor);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "sizeMode", {
            get: function () {
                return this._sizeMode;
            },
            set: function (value) {
                if (this._sizeMode !== value) {
                    this._sizeMode = value;
                    this._useSpriteSize();
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "trim", {
            get: function () {
                return this._isTrimmedMode;
            },
            set: function (value) {
                if (this._isTrimmedMode !== value) {
                    this._isTrimmedMode = value;
                    this.node.emit("shader_update_vertex");
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "useUV", {
            get: function () {
                return this._useUV;
            },
            set: function (value) {
                if (this._useUV !== value) {
                    this._useUV = value;
                    this.checkUVIndexes();
                    this.node.emit("shader_update_attribute");
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "uvVar", {
            get: function () {
                return this._uvVar;
            },
            set: function (value) {
                this._uvVar = value;
                this.node.emit("shader_update_attribute");
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "customUV", {
            get: function () {
                return this._customUV;
            },
            set: function (value) {
                if (this._customUV !== value) {
                    this._customUV = value;
                    this.checkUVIndexes();
                    this.onRenderUpdateMaterial();
                    this.node.emit("shader_update_attribute");
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "uvIndexes", {
            get: function () {
                return this._uvIndexes;
            },
            set: function (value) {
                this._uvIndexes = value;
                this.checkUVIndexes();
                this.node.emit("shader_update_vertex");
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "useUVRect", {
            get: function () {
                return this._useUVRect;
            },
            set: function (value) {
                if (this._useUVRect !== value) {
                    this._useUVRect = value;
                    this.onRenderUpdateMaterial();
                    this.node.emit("shader_update_attribute");
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "uvRectVar", {
            get: function () {
                return this._uvRectVar;
            },
            set: function (value) {
                this._uvRectVar = value;
                this.onRenderUpdateMaterial();
                this.node.emit("shader_update_attribute");
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "useFrameSize", {
            get: function () {
                return this._useFrameSize;
            },
            set: function (value) {
                if (this._useFrameSize !== value) {
                    this._useFrameSize = value;
                    this.onRenderUpdateMaterial();
                    this.node.emit("shader_update_attribute");
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderSpriteFrame.prototype, "frameSizeVar", {
            get: function () {
                return this._frameSizeVar;
            },
            set: function (value) {
                this._frameSizeVar = value;
                this.onRenderUpdateMaterial();
                this.node.emit("shader_update_attribute");
            },
            enumerable: false,
            configurable: true
        });
        ShaderSpriteFrame.prototype.onLoad = function () {
            this._spriteFrame && this._spriteFrame.ensureLoadTexture();
            this.node.on("shader_useshape", this.onShaderuseShape, this);
            this.node.on("render_check_shape", this.onRenderCheckShape, this);
            this.node.on("render_shape_checked", this.checkUVIndexes, this);
            this.node.on("render_prepare", this.onRenderPrepare, this);
            this.node.on("render_update_material", this.onRenderUpdateMaterial, this);
            this.node.on("render_update_attribute", this.onRenderUpdateAttribute, this);
            this.node.on("render_update_shape", this.onRenderUpdateShape, this);
            this.node.on("render_update_vertex", this.onRenderUpdateRenderData, this);
            this.node.on("render_update_worldvertex", this.updateWorldVerts, this);
            this.node.on("render_validate_render", this.onRenderValidateRender, this);
        };
        ShaderSpriteFrame.prototype.onDestroy = function () {
            this.node.targetOff(this);
        };
        ShaderSpriteFrame.prototype.onEnable = function () {
            this.onStateUpdate(true);
        };
        ShaderSpriteFrame.prototype.onDisable = function () {
            this.onStateUpdate(false);
        };
        ShaderSpriteFrame.prototype.onStateUpdate = function (enable) {
            this.node.emit("shader_update_tag");
            this.onRenderUpdateMaterial();
            this.node.emit("shader_update_attribute");
            if (enable) {
                this._useSpriteFrame(null);
            }
            this.checkUVIndexes();
        };
        ShaderSpriteFrame.prototype.checkUVIndexes = function () {
            if (this.enabled) {
                if (this._useUV) {
                    if (this._customUV) {
                        var rcomp = this.getComponent(lcc$render.RenderSystem);
                        if (rcomp) {
                            var uvi = this._uvIndexes;
                            var vc = rcomp._verticesCount;
                            for (var i = 0; i < vc; i++) {
                                var idx = uvi[i];
                                if (idx == null) {
                                    uvi[i] = i % 4;
                                }
                                else if (idx > 3) {
                                    uvi[i] = 3;
                                }
                                else if (idx < 0) {
                                    uvi[i] = 0;
                                }
                            }
                            if (uvi.length > vc) {
                                this._uvIndexes = uvi.slice(0, vc);
                            }
                        }
                    }
                    else {
                        this._uvIndexes = [];
                    }
                }
                else {
                    this._uvIndexes = [];
                }
            }
        };
        ShaderSpriteFrame.prototype.onShaderuseShape = function (comp) {
            if (this.enabled && this !== comp) {
                this._useShape = false;
            }
        };
        ShaderSpriteFrame.prototype.onRenderCheckShape = function (comp) {
            if (this.enabled && this._useShape) {
                comp.setShaderShape(4, 6);
            }
        };
        ShaderSpriteFrame.prototype.onRenderPrepare = function (comp) {
            if (this.enabled) {
                this.packToDynamicAtlas(comp, this._spriteFrame);
            }
        };
        ShaderSpriteFrame.prototype.getUVRect = function (comp) {
            var rect = new cc.Vec4();
            if (this._spriteFrame) {
                var uv = this._spriteFrame.uv;
                var l = uv[0];
                var b = uv[1];
                var w = uv[6] - l;
                var h = uv[7] - b;
                rect.x = l;
                rect.y = b;
                rect.z = w;
                rect.w = h;
            }
            return rect;
        };
        ShaderSpriteFrame.prototype.onRenderUpdateMaterial = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var texture = this._spriteFrame && this._spriteFrame.getTexture();
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._textureVar.unifMacro)) {
                        if (this.enabled) {
                            this.defineMaterialMacro(material, this._textureVar.unifMacro, true);
                            material.setProperty(this._textureVar.unifName, texture);
                        }
                        else {
                            this.defineMaterialMacro(material, this._textureVar.unifMacro, false);
                        }
                    }
                    if (this.checkMaterialMacro(material, this._uvRectVar.unifMacro)) {
                        if (this.enabled && this._useUVRect && this._uvRectVar.type === lcc$render.VariableType.UNIFORM) {
                            this.defineMaterialMacro(material, this._uvRectVar.unifMacro, true);
                            material.setProperty(this._uvRectVar.unifName, this.getUVRect(comp));
                        }
                        else {
                            this.defineMaterialMacro(material, this._uvRectVar.unifMacro, false);
                        }
                    }
                    if (this.checkMaterialMacro(material, this._frameSizeVar.unifMacro)) {
                        if (this.enabled && this._useFrameSize && this._frameSizeVar.type === lcc$render.VariableType.UNIFORM) {
                            this.defineMaterialMacro(material, this._frameSizeVar.unifMacro, true);
                            if (this._spriteFrame) {
                                var rect = this._spriteFrame._rect;
                                material.setProperty(this._frameSizeVar.unifName, cc.v2(rect.width, rect.height));
                            }
                            else {
                                material.setProperty(this._frameSizeVar.unifName, cc.v2(1, 1));
                            }
                        }
                        else {
                            this.defineMaterialMacro(material, this._frameSizeVar.unifMacro, false);
                        }
                    }
                }
            }
        };
        ShaderSpriteFrame.prototype.onRenderUpdateAttribute = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._uvVar.attrMacro)) {
                        if (this.enabled && this._useUV) {
                            this.defineMaterialMacro(material, this._uvVar.attrMacro, true);
                            this._uvOffest = comp.addVertexAttribute({
                                name: this._uvVar.attrName,
                                type: gfx.ATTR_TYPE_FLOAT32,
                                num: 2
                            }, 2);
                        }
                        else {
                            this.defineMaterialMacro(material, this._uvVar.attrMacro, false);
                            this._uvOffest = 0;
                        }
                    }
                    if (this.checkMaterialMacro(material, this._uvRectVar.attrMacro)) {
                        if (this.enabled && this._useUVRect && this._uvRectVar.type === lcc$render.VariableType.ATTRIBUTE) {
                            this.defineMaterialMacro(material, this._uvRectVar.attrMacro, true);
                            this._uvrOffset = comp.addVertexAttribute({
                                name: this._uvRectVar.attrName,
                                type: gfx.ATTR_TYPE_FLOAT32,
                                num: 4
                            }, 4);
                        }
                        else {
                            this.defineMaterialMacro(material, this._uvRectVar.attrMacro, false);
                            this._uvrOffset = 0;
                        }
                    }
                    if (this.checkMaterialMacro(material, this._frameSizeVar.attrMacro)) {
                        if (this.enabled && this._useFrameSize && this._frameSizeVar.type === lcc$render.VariableType.ATTRIBUTE) {
                            this.defineMaterialMacro(material, this._frameSizeVar.attrMacro, true);
                            this._fsOffset = comp.addVertexAttribute({
                                name: this._frameSizeVar.attrName,
                                type: gfx.ATTR_TYPE_FLOAT32,
                                num: 2
                            }, 2);
                        }
                        else {
                            this.defineMaterialMacro(material, this._frameSizeVar.attrMacro, false);
                            this._fsOffset = 0;
                        }
                    }
                }
            }
        };
        ShaderSpriteFrame.prototype.onRenderUpdateShape = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (this.enabled && this._useShape) {
                var rdata = comp._assembler.renderData;
                rdata.initQuadIndices(rdata.iDatas[0]);
                this._shapeRect = [0, 0, 0, 0];
            }
            else {
                this._shapeRect = null;
            }
        };
        ShaderSpriteFrame.prototype.onRenderUpdateRenderData = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp && this.enabled) {
                var assembler = comp._assembler;
                if (this._useShape) {
                    var cw = void 0, ch = void 0, appx = void 0, appy = void 0, l = void 0, b = void 0, r = void 0, t = void 0;
                    if (this._shapeType == ShapeType.NODE) {
                        var node = this.node;
                        cw = node.width;
                        ch = node.height;
                        appx = node.anchorX * cw;
                        appy = node.anchorY * ch;
                    }
                    else {
                        cw = this._size.width;
                        ch = this._size.height;
                        appx = this._anchor.x * cw;
                        appy = this._anchor.y * ch;
                    }
                    if (this.trim) {
                        l = -appx;
                        b = -appy;
                        r = cw - appx;
                        t = ch - appy;
                    }
                    else {
                        var frame = this._spriteFrame, ow = frame._originalSize.width, oh = frame._originalSize.height, rw = frame._rect.width, rh = frame._rect.height, offset = frame._offset, scaleX = cw / ow, scaleY = ch / oh;
                        var trimLeft = offset.x + (ow - rw) / 2;
                        var trimRight = offset.x - (ow - rw) / 2;
                        var trimBottom = offset.y + (oh - rh) / 2;
                        var trimTop = offset.y - (oh - rh) / 2;
                        l = trimLeft * scaleX - appx;
                        b = trimBottom * scaleY - appy;
                        r = cw + trimRight * scaleX - appx;
                        t = ch + trimTop * scaleY - appy;
                    }
                    var local = this._shapeRect;
                    local[0] = l;
                    local[1] = b;
                    local[2] = r;
                    local[3] = t;
                    this.updateWorldVerts(comp);
                }
                if (this._useUV && this._uvOffest > 0) {
                    var uv = this._spriteFrame.uv;
                    var cuvi = this._uvIndexes;
                    var uvOffset = this._uvOffest;
                    var floatsPerVert = assembler.floatsPerVert;
                    var verts = assembler.renderData.vDatas[0];
                    for (var i = 0; i < 4; i++) {
                        var srcOffset = (this._customUV ? cuvi[i] : i) * 2;
                        var dstOffset = floatsPerVert * i + uvOffset;
                        verts[dstOffset] = uv[srcOffset];
                        verts[dstOffset + 1] = uv[srcOffset + 1];
                    }
                }
                if (this._useUVRect && this._uvrOffset > 0) {
                    var uvr = this.getUVRect(comp);
                    if (uvr) {
                        var uvrOffset = this._uvrOffset;
                        var floatsPerVert = assembler.floatsPerVert;
                        var verts = assembler.renderData.vDatas[0];
                        for (var i = 0; i < 4; i++) {
                            var dstOffset = floatsPerVert * i + uvrOffset;
                            verts[dstOffset] = uvr.x;
                            verts[dstOffset + 1] = uvr.y;
                            verts[dstOffset + 2] = uvr.z;
                            verts[dstOffset + 3] = uvr.w;
                        }
                    }
                }
                if (this._useFrameSize && this._fsOffset > 0) {
                    var rect = this._spriteFrame._rect;
                    var fsOffset = this._fsOffset;
                    var floatsPerVert = assembler.floatsPerVert;
                    var verts = assembler.renderData.vDatas[0];
                    for (var i = 0; i < 4; i++) {
                        var dstOffset = floatsPerVert * i + fsOffset;
                        verts[dstOffset] = rect.width;
                        verts[dstOffset + 1] = rect.height;
                    }
                }
            }
        };
        ShaderSpriteFrame.prototype.updateWorldVerts = function (comp) {
            if (this.enabled && this._useShape) {
                if (CC_NATIVERENDERER) {
                    this.updateWorldVertsNative(comp);
                }
                else {
                    this.updateWorldVertsWebGL(comp);
                }
            }
        };
        ShaderSpriteFrame.prototype.updateWorldVertsWebGL = function (comp) {
            var assembler = comp._assembler;
            var local = this._shapeRect;
            var verts = assembler.renderData.vDatas[0];
            var matrix = comp.node._worldMatrix;
            var matrixm = matrix.m, a = matrixm[0], b = matrixm[1], c = matrixm[4], d = matrixm[5], tx = matrixm[12], ty = matrixm[13];
            var vl = local[0], vr = local[2], vb = local[1], vt = local[3];
            var justTranslate = a === 1 && b === 0 && c === 0 && d === 1;
            var index = 0;
            var floatsPerVert = assembler.floatsPerVert;
            if (justTranslate) {
                verts[index] = vl + tx;
                verts[index + 1] = vb + ty;
                index += floatsPerVert;
                verts[index] = vr + tx;
                verts[index + 1] = vb + ty;
                index += floatsPerVert;
                verts[index] = vl + tx;
                verts[index + 1] = vt + ty;
                index += floatsPerVert;
                verts[index] = vr + tx;
                verts[index + 1] = vt + ty;
            }
            else {
                var al = a * vl, ar = a * vr, bl = b * vl, br = b * vr, cb = c * vb, ct = c * vt, db = d * vb, dt = d * vt;
                verts[index] = al + cb + tx;
                verts[index + 1] = bl + db + ty;
                index += floatsPerVert;
                verts[index] = ar + cb + tx;
                verts[index + 1] = br + db + ty;
                index += floatsPerVert;
                verts[index] = al + ct + tx;
                verts[index + 1] = bl + dt + ty;
                index += floatsPerVert;
                verts[index] = ar + ct + tx;
                verts[index + 1] = br + dt + ty;
            }
        };
        ShaderSpriteFrame.prototype.updateWorldVertsNative = function (comp) {
            var assembler = comp._assembler;
            var local = this._shapeRect;
            var verts = assembler.renderData.vDatas[0];
            var floatsPerVert = assembler.floatsPerVert;
            var vl = local[0], vr = local[2], vb = local[1], vt = local[3];
            var index = 0;
            verts[index] = vl;
            verts[index + 1] = vb;
            index += floatsPerVert;
            verts[index] = vr;
            verts[index + 1] = vb;
            index += floatsPerVert;
            verts[index] = vl;
            verts[index + 1] = vt;
            index += floatsPerVert;
            verts[index] = vr;
            verts[index + 1] = vt;
        };
        ShaderSpriteFrame.prototype.onRenderValidateRender = function (comp) {
            if (this.enabled) {
                var spriteFrame = this._spriteFrame;
                if (spriteFrame &&
                    spriteFrame.textureLoaded()) {
                    return;
                }
                comp.disableRender();
            }
        };
        ShaderSpriteFrame.prototype.packToDynamicAtlas = function (comp, frame) {
            if (CC_TEST)
                return;
            if (!frame._original && cc.dynamicAtlasManager && frame._texture.packable) {
                var packedFrame = cc.dynamicAtlasManager.insertSpriteFrame(frame);
                if (packedFrame) {
                    frame._setDynamicAtlasFrame(packedFrame);
                }
            }
            var material = comp._materials[0];
            if (!material)
                return;
            if (material.getProperty(this._textureVar.unifName) !== frame._texture) {
                this.onRenderUpdateMaterial();
                this.node.emit("shader_update_vertex");
            }
        };
        ShaderSpriteFrame.prototype._useSpriteSize = function () {
            if (this.enabled) {
                if (this._useShape) {
                    if (!this._spriteFrame || !this.isValid)
                        return;
                    if (SizeMode.RAW === this._sizeMode) {
                        var size = this._spriteFrame._originalSize;
                        this._size = cc.size(size);
                    }
                    else if (SizeMode.TRIMMED === this._sizeMode) {
                        var rect = this._spriteFrame._rect;
                        this._size = cc.size(rect.width, rect.height);
                    }
                    if (this._syncSizeToNode) {
                        this.node.setContentSize(this._size);
                    }
                    this.node.emit("shader_update_vertex");
                }
            }
        };
        ShaderSpriteFrame.prototype._useSpriteFrame = function (oldFrame) {
            if (this.enabled) {
                var oldTexture = oldFrame && oldFrame.getTexture();
                if (oldTexture && !oldTexture.loaded) {
                    oldFrame.off('load', this._useSpriteSize, this);
                }
                var spriteFrame = this._spriteFrame;
                if (spriteFrame) {
                    this.onRenderUpdateMaterial();
                    var newTexture = spriteFrame.getTexture();
                    if (newTexture && newTexture.loaded) {
                        this._useSpriteSize();
                    }
                    else {
                        spriteFrame.once('load', this._useSpriteSize, this);
                    }
                }
            }
            this.node.emit("shader_update_vertex");
            if (CC_EDITOR) {
                this._useAtlas(this._spriteFrame);
            }
        };
        ShaderSpriteFrame.prototype._useAtlas = function (spriteFrame) {
            if (CC_EDITOR) {
                if (spriteFrame && spriteFrame._atlasUuid) {
                    var self = this;
                    cc.assetManager.loadAny(spriteFrame._atlasUuid, function (err, asset) {
                        self._atlas = asset;
                    });
                }
                else {
                    this._atlas = null;
                }
            }
        };
        __decorate([
            property(lcc$render.VariableConfig)
        ], ShaderSpriteFrame.prototype, "_textureVar", void 0);
        __decorate([
            property({
                type: lcc$render.VariableConfig,
                tooltip: "纹理变量"
            })
        ], ShaderSpriteFrame.prototype, "textureVar", null);
        __decorate([
            property({
                type: cc.SpriteAtlas,
                tooltip: CC_DEV && 'i18n:COMPONENT.sprite.atlas',
                editorOnly: true,
                visible: true,
                animatable: false
            })
        ], ShaderSpriteFrame.prototype, "_atlas", void 0);
        __decorate([
            property(cc.SpriteFrame)
        ], ShaderSpriteFrame.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "精灵帧对象"
            })
        ], ShaderSpriteFrame.prototype, "spriteFrame", null);
        __decorate([
            property()
        ], ShaderSpriteFrame.prototype, "_useShape", void 0);
        __decorate([
            property({
                tooltip: "使用形状"
            })
        ], ShaderSpriteFrame.prototype, "useShape", null);
        __decorate([
            property({
                type: cc.Enum(ShapeType)
            })
        ], ShaderSpriteFrame.prototype, "_shapeType", void 0);
        __decorate([
            property({
                type: cc.Enum(ShapeType),
                tooltip: "形状类型",
                visible: function () {
                    return this._useShape;
                },
            })
        ], ShaderSpriteFrame.prototype, "shapeType", null);
        __decorate([
            property(cc.Size)
        ], ShaderSpriteFrame.prototype, "_size", void 0);
        __decorate([
            property({
                type: cc.Size,
                tooltip: "形状的尺寸",
                visible: function () {
                    return this._useShape && this._shapeType == ShapeType.CUSTOM;
                }
            })
        ], ShaderSpriteFrame.prototype, "size", null);
        __decorate([
            property(cc.Vec2)
        ], ShaderSpriteFrame.prototype, "_anchor", void 0);
        __decorate([
            property({
                type: cc.Vec2,
                tooltip: "形状的锚点",
                visible: function () {
                    return this._useShape && this._shapeType == ShapeType.CUSTOM;
                },
            })
        ], ShaderSpriteFrame.prototype, "anchor", null);
        __decorate([
            property()
        ], ShaderSpriteFrame.prototype, "_syncSizeToNode", void 0);
        __decorate([
            property({
                tooltip: "同步尺寸到节点",
                visible: function () {
                    return this._useShape && this._shapeType == ShapeType.CUSTOM;
                },
            })
        ], ShaderSpriteFrame.prototype, "syncSizeToNode", null);
        __decorate([
            property({
                type: cc.Enum(SizeMode)
            })
        ], ShaderSpriteFrame.prototype, "_sizeMode", void 0);
        __decorate([
            property({
                type: cc.Enum(SizeMode),
                visible: function () {
                    return this._useShape && this._shapeType == ShapeType.CUSTOM;
                },
                animatable: false,
                tooltip: CC_DEV && 'i18n:COMPONENT.sprite.size_mode'
            })
        ], ShaderSpriteFrame.prototype, "sizeMode", null);
        __decorate([
            property()
        ], ShaderSpriteFrame.prototype, "_isTrimmedMode", void 0);
        __decorate([
            property({
                visible: function () {
                    return this._useShape;
                },
                animatable: false,
                tooltip: CC_DEV && 'i18n:COMPONENT.sprite.trim',
            })
        ], ShaderSpriteFrame.prototype, "trim", null);
        __decorate([
            property()
        ], ShaderSpriteFrame.prototype, "_useUV", void 0);
        __decorate([
            property({
                tooltip: "使用 UV"
            })
        ], ShaderSpriteFrame.prototype, "useUV", null);
        __decorate([
            property(lcc$render.VariableConfig)
        ], ShaderSpriteFrame.prototype, "_uvVar", void 0);
        __decorate([
            property({
                type: lcc$render.VariableConfig,
                tooltip: "UV变量",
                visible: function () {
                    return this._useUV;
                },
            })
        ], ShaderSpriteFrame.prototype, "uvVar", null);
        __decorate([
            property()
        ], ShaderSpriteFrame.prototype, "_customUV", void 0);
        __decorate([
            property({
                tooltip: "自定义 UV",
                visible: function () {
                    return this._useUV;
                },
            })
        ], ShaderSpriteFrame.prototype, "customUV", null);
        __decorate([
            property([cc.Integer])
        ], ShaderSpriteFrame.prototype, "_uvIndexes", void 0);
        __decorate([
            property({
                type: [cc.Integer],
                tooltip: "UV索引表",
                visible: function () {
                    return this._useUV && this._customUV;
                },
            })
        ], ShaderSpriteFrame.prototype, "uvIndexes", null);
        __decorate([
            property()
        ], ShaderSpriteFrame.prototype, "_useUVRect", void 0);
        __decorate([
            property({
                tooltip: "使用UV区域(合图后的区域)"
            })
        ], ShaderSpriteFrame.prototype, "useUVRect", null);
        __decorate([
            property(lcc$render.VariableConfig)
        ], ShaderSpriteFrame.prototype, "_uvRectVar", void 0);
        __decorate([
            property({
                type: lcc$render.VariableConfig,
                tooltip: "UV区域变量",
                visible: function () {
                    return this._useUVRect;
                },
            })
        ], ShaderSpriteFrame.prototype, "uvRectVar", null);
        __decorate([
            property()
        ], ShaderSpriteFrame.prototype, "_useFrameSize", void 0);
        __decorate([
            property({
                tooltip: "使用帧尺寸(像素大小)"
            })
        ], ShaderSpriteFrame.prototype, "useFrameSize", null);
        __decorate([
            property(lcc$render.VariableConfig)
        ], ShaderSpriteFrame.prototype, "_frameSizeVar", void 0);
        __decorate([
            property({
                type: lcc$render.VariableConfig,
                tooltip: "帧尺寸变量",
                visible: function () {
                    return this._useFrameSize;
                },
            })
        ], ShaderSpriteFrame.prototype, "frameSizeVar", null);
        ShaderSpriteFrame = __decorate([
            ccclass("lcc$render.ShaderSpriteFrame"),
            menu("i18n:lcc-render.menu_component/ShaderSpriteFrame")
        ], ShaderSpriteFrame);
        return ShaderSpriteFrame;
    }(lcc$render.ShaderComponent));
    lcc$render.ShaderSpriteFrame = ShaderSpriteFrame;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var gfx = cc.gfx;
    var VC = {
        tag: "vec2",
        varSelect: true,
        varDeftype: lcc$render.VariableType.UNIFORM,
        varAttrMacro: "ATTR_VEC2",
        varAttrMacroCheckOnly: false,
        varAttrName: "a_vec2v",
        varUnifMacro: "UNIF_VEC2",
        varUnifMacroCheckOnly: false,
        varUnifName: "u_vec2v",
        valDefault: cc.v2(),
        valNew: function () { return cc.v2(); },
        valType: cc.Vec2,
        typeSize: 2,
        toArray: function (arr, value, offest) {
            arr[offest] = value.x;
            arr[offest + 1] = value.y;
        }
    };
    var ShaderVec2 = (function (_super) {
        __extends(ShaderVec2, _super);
        function ShaderVec2() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._tag = VC.tag;
            _this._valueType = lcc$render.ValueType.SINGLE;
            _this._valueVar = new lcc$render.VariableConfig(VC.varSelect, VC.varDeftype, VC.varAttrMacro, VC.varAttrMacroCheckOnly, VC.varAttrName, VC.varUnifMacro, VC.varUnifMacroCheckOnly, VC.varUnifName);
            _this._values = [VC.valNew()];
            _this._valueOffset = 0;
            return _this;
        }
        Object.defineProperty(ShaderVec2.prototype, "valueType", {
            get: function () {
                return this._valueType;
            },
            set: function (value) {
                if (this._valueType != value) {
                    this._valueType = value;
                    if (value == lcc$render.ValueType.ARRAY) {
                        this._valueVar.type == lcc$render.VariableType.UNIFORM;
                        this._valueVar._typesel = false;
                    }
                    else {
                        this._valueVar._typesel = true;
                    }
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderVec2.prototype, "valueVar", {
            get: function () {
                return this._valueVar;
            },
            set: function (value) {
                this._valueVar = value;
                this.onUpdateValues();
                this.onRenderUpdateMaterial();
                this.node.emit("shader_update_attribute");
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderVec2.prototype, "value", {
            get: function () {
                return this._values[0];
            },
            set: function (value_) {
                this._values[0] = value_;
                this.onUpdateValues();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderVec2.prototype, "values", {
            get: function () {
                return this._values;
            },
            set: function (value) {
                this._values = value;
                this.onUpdateValues();
            },
            enumerable: false,
            configurable: true
        });
        ShaderVec2.prototype.onLoad = function () {
            this.node.on("render_shape_checked", this.onRenderShapeChecked, this);
            this.node.on("render_update_material", this.onRenderUpdateMaterial, this);
            this.node.on("render_update_attribute", this.onRenderUpdateAttribute, this);
            this.node.on("render_update_vertex", this.onRenderUpdateRenderData, this);
        };
        ShaderVec2.prototype.onDestroy = function () {
            this.node.targetOff(this);
        };
        ShaderVec2.prototype.onEnable = function () {
            this.onStateUpdate(true);
        };
        ShaderVec2.prototype.onDisable = function () {
            this.onStateUpdate(false);
        };
        ShaderVec2.prototype.onStateUpdate = function (enable) {
            this.node.emit("shader_update_tag");
            this.onUpdateValues();
        };
        ShaderVec2.prototype.onRenderShapeChecked = function () {
            if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                this.onUpdateValues();
            }
        };
        ShaderVec2.prototype.onUpdateValues = function () {
            if (this.enabled) {
                if (this._values.length <= 0) {
                    this._values = [VC.valNew()];
                }
                if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                    var rsys = this.getComponent(lcc$render.RenderSystem);
                    if (rsys) {
                        var vc = rsys._verticesCount;
                        if (this._values.length < vc) {
                            var values = this._values;
                            for (var i = 0; i < vc; i++) {
                                var color = values[i];
                                if (color == null) {
                                    values[i] = VC.valNew();
                                }
                            }
                        }
                        this._values.length = vc;
                    }
                }
                else if (this._valueType == lcc$render.ValueType.SINGLE) {
                    this._values.length = 1;
                }
                if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                    this.node.emit("shader_update_attribute");
                }
                else {
                    this.onRenderUpdateMaterial();
                }
            }
        };
        ShaderVec2.prototype.onRenderUpdateMaterial = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._valueVar.unifMacro)) {
                        if (this.enabled && this._valueVar.type === lcc$render.VariableType.UNIFORM) {
                            this.defineMaterialMacro(material, this._valueVar.unifMacro, true);
                            if (this._valueType == lcc$render.ValueType.SINGLE) {
                                material.setProperty(this._valueVar.unifName, this._values[0]);
                            }
                            else {
                                var values = this._values;
                                var _values = [];
                                var defv = VC.valDefault;
                                for (var i = 0, l = values.length; i < l; i++) {
                                    VC.toArray(_values, values[i] || defv, _values.length);
                                }
                                material.setProperty(this._valueVar.unifName, new Float32Array(values.length * VC.typeSize));
                                material.setProperty(this._valueVar.unifName, _values);
                            }
                        }
                        else {
                            this.defineMaterialMacro(material, this._valueVar.unifMacro, false);
                        }
                    }
                }
            }
        };
        ShaderVec2.prototype.getAttributeValues = function (comp) {
            var values = [];
            var vc = comp._verticesCount;
            var defv = VC.valDefault;
            for (var i = 0; i < vc; i++) {
                VC.toArray(values, this._values[i] || defv, values.length);
            }
            return values;
        };
        ShaderVec2.prototype.onRenderUpdateAttribute = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._valueVar.attrMacro)) {
                        if (this.enabled && this._valueVar.type === lcc$render.VariableType.ATTRIBUTE) {
                            this.defineMaterialMacro(material, this._valueVar.attrMacro, true);
                            this._valueOffset = comp.addVertexAttribute({
                                name: this._valueVar.attrName,
                                type: gfx.ATTR_TYPE_FLOAT32,
                                num: VC.typeSize,
                            }, VC.typeSize);
                        }
                        else {
                            this.defineMaterialMacro(material, this._valueVar.attrMacro, false);
                            this._valueOffset = 0;
                        }
                    }
                }
            }
        };
        ShaderVec2.prototype.onRenderUpdateRenderData = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp && this.enabled) {
                if (this._valueOffset > 0) {
                    var assembler = comp._assembler;
                    var values = this.getAttributeValues(comp);
                    var vlen = values.length;
                    var valueOffset = this._valueOffset;
                    var floatsPerVert = assembler.floatsPerVert;
                    var verts = assembler.renderData.vDatas[0];
                    for (var i = 0; i < 4; i++) {
                        var dstOffset = floatsPerVert * i + valueOffset;
                        var srcOffset = VC.typeSize * i;
                        if (srcOffset >= vlen) {
                            srcOffset = 0;
                        }
                        for (var j = 0; j < VC.typeSize; j++) {
                            verts[dstOffset + j] = values[srcOffset + j];
                        }
                    }
                }
            }
        };
        __decorate([
            property({
                type: cc.Enum(lcc$render.ValueType)
            })
        ], ShaderVec2.prototype, "_valueType", void 0);
        __decorate([
            property({
                type: cc.Enum(lcc$render.ValueType),
                tooltip: "值类型"
            })
        ], ShaderVec2.prototype, "valueType", null);
        __decorate([
            property(lcc$render.VariableConfig)
        ], ShaderVec2.prototype, "_valueVar", void 0);
        __decorate([
            property({
                type: lcc$render.VariableConfig,
                tooltip: "值变量"
            })
        ], ShaderVec2.prototype, "valueVar", null);
        __decorate([
            property([VC.valType])
        ], ShaderVec2.prototype, "_values", void 0);
        __decorate([
            property({
                visible: function () {
                    return this._valueType == lcc$render.ValueType.SINGLE && this._valueVar.type == lcc$render.VariableType.UNIFORM;
                },
                type: VC.valType,
                tooltip: "值"
            })
        ], ShaderVec2.prototype, "value", null);
        __decorate([
            property({
                visible: function () {
                    return this._valueType == lcc$render.ValueType.ARRAY || this._valueVar.type == lcc$render.VariableType.ATTRIBUTE;
                },
                type: [VC.valType],
                tooltip: "值数组/值顶点数组"
            })
        ], ShaderVec2.prototype, "values", null);
        ShaderVec2 = __decorate([
            ccclass("lcc$render.ShaderVec2"),
            menu("i18n:lcc-render.menu_component/ShaderVec2")
        ], ShaderVec2);
        return ShaderVec2;
    }(lcc$render.ShaderComponent));
    lcc$render.ShaderVec2 = ShaderVec2;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var gfx = cc.gfx;
    var VC = {
        tag: "vec3",
        varSelect: false,
        varDeftype: lcc$render.VariableType.UNIFORM,
        varAttrMacro: "ATTR_VEC3",
        varAttrMacroCheckOnly: false,
        varAttrName: "a_vec3v",
        varUnifMacro: "UNIF_VEC3",
        varUnifMacroCheckOnly: false,
        varUnifName: "u_vec3v",
        valDefault: cc.v3(),
        valNew: function () { return cc.v3(); },
        valType: cc.Vec3,
        typeSize: 3,
        toArray: function (arr, value, offest) {
            arr[offest] = value.x;
            arr[offest + 1] = value.y;
            arr[offest + 2] = value.z;
        }
    };
    var ShaderVec3 = (function (_super) {
        __extends(ShaderVec3, _super);
        function ShaderVec3() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._tag = VC.tag;
            _this._valueType = lcc$render.ValueType.SINGLE;
            _this._valueVar = new lcc$render.VariableConfig(VC.varSelect, VC.varDeftype, VC.varAttrMacro, VC.varAttrMacroCheckOnly, VC.varAttrName, VC.varUnifMacro, VC.varUnifMacroCheckOnly, VC.varUnifName);
            _this._values = [VC.valNew()];
            _this._valueOffset = 0;
            return _this;
        }
        Object.defineProperty(ShaderVec3.prototype, "valueType", {
            get: function () {
                return this._valueType;
            },
            set: function (value) {
                if (this._valueType != value) {
                    this._valueType = value;
                    if (value == lcc$render.ValueType.ARRAY) {
                        this._valueVar.type == lcc$render.VariableType.UNIFORM;
                        this._valueVar._typesel = false;
                    }
                    else {
                        this._valueVar._typesel = true;
                    }
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderVec3.prototype, "valueVar", {
            get: function () {
                return this._valueVar;
            },
            set: function (value) {
                this._valueVar = value;
                this.onUpdateValues();
                this.onRenderUpdateMaterial();
                this.node.emit("shader_update_attribute");
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderVec3.prototype, "value", {
            get: function () {
                return this._values[0];
            },
            set: function (value_) {
                this._values[0] = value_;
                this.onUpdateValues();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderVec3.prototype, "values", {
            get: function () {
                return this._values;
            },
            set: function (value) {
                this._values = value;
                this.onUpdateValues();
            },
            enumerable: false,
            configurable: true
        });
        ShaderVec3.prototype.onLoad = function () {
            this.node.on("render_shape_checked", this.onRenderShapeChecked, this);
            this.node.on("render_update_material", this.onRenderUpdateMaterial, this);
            this.node.on("render_update_attribute", this.onRenderUpdateAttribute, this);
            this.node.on("render_update_vertex", this.onRenderUpdateRenderData, this);
        };
        ShaderVec3.prototype.onDestroy = function () {
            this.node.targetOff(this);
        };
        ShaderVec3.prototype.onEnable = function () {
            this.onStateUpdate(true);
        };
        ShaderVec3.prototype.onDisable = function () {
            this.onStateUpdate(false);
        };
        ShaderVec3.prototype.onStateUpdate = function (enable) {
            this.node.emit("shader_update_tag");
            this.onUpdateValues();
        };
        ShaderVec3.prototype.onRenderShapeChecked = function () {
            if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                this.onUpdateValues();
            }
        };
        ShaderVec3.prototype.onUpdateValues = function () {
            if (this.enabled) {
                if (this._values.length <= 0) {
                    this._values = [VC.valNew()];
                }
                if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                    var rsys = this.getComponent(lcc$render.RenderSystem);
                    if (rsys) {
                        var vc = rsys._verticesCount;
                        if (this._values.length < vc) {
                            var values = this._values;
                            for (var i = 0; i < vc; i++) {
                                var color = values[i];
                                if (color == null) {
                                    values[i] = VC.valNew();
                                }
                            }
                        }
                        this._values.length = vc;
                    }
                }
                else if (this._valueType == lcc$render.ValueType.SINGLE) {
                    this._values.length = 1;
                }
                if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                    this.node.emit("shader_update_attribute");
                }
                else {
                    this.onRenderUpdateMaterial();
                }
            }
        };
        ShaderVec3.prototype.onRenderUpdateMaterial = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._valueVar.unifMacro)) {
                        if (this.enabled && this._valueVar.type === lcc$render.VariableType.UNIFORM) {
                            this.defineMaterialMacro(material, this._valueVar.unifMacro, true);
                            if (this._valueType == lcc$render.ValueType.SINGLE) {
                                material.setProperty(this._valueVar.unifName, this._values[0]);
                            }
                            else {
                                var values = this._values;
                                var _values = [];
                                var defv = VC.valDefault;
                                for (var i = 0, l = values.length; i < l; i++) {
                                    VC.toArray(_values, values[i] || defv, _values.length);
                                }
                                material.setProperty(this._valueVar.unifName, new Float32Array(values.length * VC.typeSize));
                                material.setProperty(this._valueVar.unifName, _values);
                            }
                        }
                        else {
                            this.defineMaterialMacro(material, this._valueVar.unifMacro, false);
                        }
                    }
                }
            }
        };
        ShaderVec3.prototype.getAttributeValues = function (comp) {
            var values = [];
            var vc = comp._verticesCount;
            var defv = VC.valDefault;
            for (var i = 0; i < vc; i++) {
                VC.toArray(values, this._values[i] || defv, values.length);
            }
            return values;
        };
        ShaderVec3.prototype.onRenderUpdateAttribute = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._valueVar.attrMacro)) {
                        if (this.enabled && this._valueVar.type === lcc$render.VariableType.ATTRIBUTE) {
                            this.defineMaterialMacro(material, this._valueVar.attrMacro, true);
                            this._valueOffset = comp.addVertexAttribute({
                                name: this._valueVar.attrName,
                                type: gfx.ATTR_TYPE_FLOAT32,
                                num: VC.typeSize,
                            }, VC.typeSize);
                        }
                        else {
                            this.defineMaterialMacro(material, this._valueVar.attrMacro, false);
                            this._valueOffset = 0;
                        }
                    }
                }
            }
        };
        ShaderVec3.prototype.onRenderUpdateRenderData = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp && this.enabled) {
                if (this._valueOffset > 0) {
                    var assembler = comp._assembler;
                    var values = this.getAttributeValues(comp);
                    var vlen = values.length;
                    var valueOffset = this._valueOffset;
                    var floatsPerVert = assembler.floatsPerVert;
                    var verts = assembler.renderData.vDatas[0];
                    for (var i = 0; i < 4; i++) {
                        var dstOffset = floatsPerVert * i + valueOffset;
                        var srcOffset = VC.typeSize * i;
                        if (srcOffset >= vlen) {
                            srcOffset = 0;
                        }
                        for (var j = 0; j < VC.typeSize; j++) {
                            verts[dstOffset + j] = values[srcOffset + j];
                        }
                    }
                }
            }
        };
        __decorate([
            property({
                type: cc.Enum(lcc$render.ValueType)
            })
        ], ShaderVec3.prototype, "_valueType", void 0);
        __decorate([
            property({
                type: cc.Enum(lcc$render.ValueType),
                tooltip: "值类型"
            })
        ], ShaderVec3.prototype, "valueType", null);
        __decorate([
            property(lcc$render.VariableConfig)
        ], ShaderVec3.prototype, "_valueVar", void 0);
        __decorate([
            property({
                type: lcc$render.VariableConfig,
                tooltip: "值变量"
            })
        ], ShaderVec3.prototype, "valueVar", null);
        __decorate([
            property([VC.valType])
        ], ShaderVec3.prototype, "_values", void 0);
        __decorate([
            property({
                visible: function () {
                    return this._valueType == lcc$render.ValueType.SINGLE && this._valueVar.type == lcc$render.VariableType.UNIFORM;
                },
                type: VC.valType,
                tooltip: "值"
            })
        ], ShaderVec3.prototype, "value", null);
        __decorate([
            property({
                visible: function () {
                    return this._valueType == lcc$render.ValueType.ARRAY || this._valueVar.type == lcc$render.VariableType.ATTRIBUTE;
                },
                type: [VC.valType],
                tooltip: "值数组/值顶点数组"
            })
        ], ShaderVec3.prototype, "values", null);
        ShaderVec3 = __decorate([
            ccclass("render.ShaderVec3"),
            menu("i18n:lcc-render.menu_component/ShaderVec3")
        ], ShaderVec3);
        return ShaderVec3;
    }(lcc$render.ShaderComponent));
    lcc$render.ShaderVec3 = ShaderVec3;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var gfx = cc.gfx;
    var VC = {
        tag: "vec4",
        varSelect: true,
        varDeftype: lcc$render.VariableType.UNIFORM,
        varAttrMacro: "ATTR_VEC4",
        varAttrMacroCheckOnly: false,
        varAttrName: "a_vec4v",
        varUnifMacro: "UNIF_VEC4",
        varUnifMacroCheckOnly: false,
        varUnifName: "u_vec4v",
        valDefault: new cc.Vec4(),
        valNew: function () { return new cc.Vec4(); },
        valType: cc.Vec4,
        typeSize: 4,
        toArray: function (arr, value, offest) {
            arr[offest] = value.x;
            arr[offest + 1] = value.y;
            arr[offest + 2] = value.z;
            arr[offest + 3] = value.w;
        }
    };
    var ShaderVec4 = (function (_super) {
        __extends(ShaderVec4, _super);
        function ShaderVec4() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._tag = VC.tag;
            _this._valueType = lcc$render.ValueType.SINGLE;
            _this._valueVar = new lcc$render.VariableConfig(VC.varSelect, VC.varDeftype, VC.varAttrMacro, VC.varAttrMacroCheckOnly, VC.varAttrName, VC.varUnifMacro, VC.varUnifMacroCheckOnly, VC.varUnifName);
            _this._values = [VC.valNew()];
            _this._valueOffset = 0;
            return _this;
        }
        Object.defineProperty(ShaderVec4.prototype, "valueType", {
            get: function () {
                return this._valueType;
            },
            set: function (value) {
                if (this._valueType != value) {
                    this._valueType = value;
                    if (value == lcc$render.ValueType.ARRAY) {
                        this._valueVar.type == lcc$render.VariableType.UNIFORM;
                        this._valueVar._typesel = false;
                    }
                    else {
                        this._valueVar._typesel = true;
                    }
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderVec4.prototype, "valueVar", {
            get: function () {
                return this._valueVar;
            },
            set: function (value) {
                this._valueVar = value;
                this.onUpdateValues();
                this.onRenderUpdateMaterial();
                this.node.emit("shader_update_attribute");
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderVec4.prototype, "value", {
            get: function () {
                return this._values[0];
            },
            set: function (value_) {
                this._values[0] = value_;
                this.onUpdateValues();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShaderVec4.prototype, "values", {
            get: function () {
                return this._values;
            },
            set: function (value) {
                this._values = value;
                this.onUpdateValues();
            },
            enumerable: false,
            configurable: true
        });
        ShaderVec4.prototype.onLoad = function () {
            this.node.on("render_shape_checked", this.onRenderShapeChecked, this);
            this.node.on("render_update_material", this.onRenderUpdateMaterial, this);
            this.node.on("render_update_attribute", this.onRenderUpdateAttribute, this);
            this.node.on("render_update_vertex", this.onRenderUpdateRenderData, this);
        };
        ShaderVec4.prototype.onDestroy = function () {
            this.node.targetOff(this);
        };
        ShaderVec4.prototype.onEnable = function () {
            this.onStateUpdate(true);
        };
        ShaderVec4.prototype.onDisable = function () {
            this.onStateUpdate(false);
        };
        ShaderVec4.prototype.onStateUpdate = function (enable) {
            this.node.emit("shader_update_tag");
            this.onUpdateValues();
        };
        ShaderVec4.prototype.onRenderShapeChecked = function () {
            if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                this.onUpdateValues();
            }
        };
        ShaderVec4.prototype.onUpdateValues = function () {
            if (this.enabled) {
                if (this._values.length <= 0) {
                    this._values = [VC.valNew()];
                }
                if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                    var rsys = this.getComponent(lcc$render.RenderSystem);
                    if (rsys) {
                        var vc = rsys._verticesCount;
                        if (this._values.length < vc) {
                            var values = this._values;
                            for (var i = 0; i < vc; i++) {
                                var color = values[i];
                                if (color == null) {
                                    values[i] = VC.valNew();
                                }
                            }
                        }
                        this._values.length = vc;
                    }
                }
                else if (this._valueType == lcc$render.ValueType.SINGLE) {
                    this._values.length = 1;
                }
                if (this._valueVar.type == lcc$render.VariableType.ATTRIBUTE) {
                    this.node.emit("shader_update_attribute");
                }
                else {
                    this.onRenderUpdateMaterial();
                }
            }
        };
        ShaderVec4.prototype.onRenderUpdateMaterial = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._valueVar.unifMacro)) {
                        if (this.enabled && this._valueVar.type === lcc$render.VariableType.UNIFORM) {
                            this.defineMaterialMacro(material, this._valueVar.unifMacro, true);
                            if (this._valueType == lcc$render.ValueType.SINGLE) {
                                material.setProperty(this._valueVar.unifName, this._values[0]);
                            }
                            else {
                                var values = this._values;
                                var _values = [];
                                var defv = VC.valDefault;
                                for (var i = 0, l = values.length; i < l; i++) {
                                    VC.toArray(_values, values[i] || defv, _values.length);
                                }
                                material.setProperty(this._valueVar.unifName, new Float32Array(values.length * VC.typeSize));
                                material.setProperty(this._valueVar.unifName, _values);
                            }
                        }
                        else {
                            this.defineMaterialMacro(material, this._valueVar.unifMacro, false);
                        }
                    }
                }
            }
        };
        ShaderVec4.prototype.getAttributeValues = function (comp) {
            var values = [];
            var vc = comp._verticesCount;
            var defv = VC.valDefault;
            for (var i = 0; i < vc; i++) {
                VC.toArray(values, this._values[i] || defv, values.length);
            }
            return values;
        };
        ShaderVec4.prototype.onRenderUpdateAttribute = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp) {
                var material = comp._materials[0];
                if (material) {
                    if (this.checkMaterialMacro(material, this._valueVar.attrMacro)) {
                        if (this.enabled && this._valueVar.type === lcc$render.VariableType.ATTRIBUTE) {
                            this.defineMaterialMacro(material, this._valueVar.attrMacro, true);
                            this._valueOffset = comp.addVertexAttribute({
                                name: this._valueVar.attrName,
                                type: gfx.ATTR_TYPE_FLOAT32,
                                num: VC.typeSize,
                            }, VC.typeSize);
                        }
                        else {
                            this.defineMaterialMacro(material, this._valueVar.attrMacro, false);
                            this._valueOffset = 0;
                        }
                    }
                }
            }
        };
        ShaderVec4.prototype.onRenderUpdateRenderData = function (comp) {
            if (comp === void 0) { comp = this.getComponent(lcc$render.RenderSystem); }
            if (comp && this.enabled) {
                if (this._valueOffset > 0) {
                    var assembler = comp._assembler;
                    var values = this.getAttributeValues(comp);
                    var vlen = values.length;
                    var valueOffset = this._valueOffset;
                    var floatsPerVert = assembler.floatsPerVert;
                    var verts = assembler.renderData.vDatas[0];
                    for (var i = 0; i < 4; i++) {
                        var dstOffset = floatsPerVert * i + valueOffset;
                        var srcOffset = VC.typeSize * i;
                        if (srcOffset >= vlen) {
                            srcOffset = 0;
                        }
                        for (var j = 0; j < VC.typeSize; j++) {
                            verts[dstOffset + j] = values[srcOffset + j];
                        }
                    }
                }
            }
        };
        __decorate([
            property({
                type: cc.Enum(lcc$render.ValueType)
            })
        ], ShaderVec4.prototype, "_valueType", void 0);
        __decorate([
            property({
                type: cc.Enum(lcc$render.ValueType),
                tooltip: "值类型"
            })
        ], ShaderVec4.prototype, "valueType", null);
        __decorate([
            property(lcc$render.VariableConfig)
        ], ShaderVec4.prototype, "_valueVar", void 0);
        __decorate([
            property({
                type: lcc$render.VariableConfig,
                tooltip: "值变量"
            })
        ], ShaderVec4.prototype, "valueVar", null);
        __decorate([
            property([VC.valType])
        ], ShaderVec4.prototype, "_values", void 0);
        __decorate([
            property({
                visible: function () {
                    return this._valueType == lcc$render.ValueType.SINGLE && this._valueVar.type == lcc$render.VariableType.UNIFORM;
                },
                type: VC.valType,
                tooltip: "值"
            })
        ], ShaderVec4.prototype, "value", null);
        __decorate([
            property({
                visible: function () {
                    return this._valueType == lcc$render.ValueType.ARRAY || this._valueVar.type == lcc$render.VariableType.ATTRIBUTE;
                },
                type: [VC.valType],
                tooltip: "值数组/值顶点数组"
            })
        ], ShaderVec4.prototype, "values", null);
        ShaderVec4 = __decorate([
            ccclass("render.ShaderVec4"),
            menu("i18n:lcc-render.menu_component/ShaderVec4")
        ], ShaderVec4);
        return ShaderVec4;
    }(lcc$render.ShaderComponent));
    lcc$render.ShaderVec4 = ShaderVec4;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectDissolve = (function (_super) {
        __extends(EffectDissolve, _super);
        function EffectDissolve() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._noiseFrame = null;
            _this._degree = 0.7;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderNoiseFrame = null;
            _this.shaderDegree = null;
            return _this;
        }
        Object.defineProperty(EffectDissolve.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectDissolve.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                    if (!this._noiseFrame) {
                        this.noiseFrame = value;
                    }
                    else {
                        this.checkNoiseFrame();
                    }
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectDissolve.prototype, "noiseFrame", {
            get: function () {
                return this._noiseFrame;
            },
            set: function (value) {
                if (this._noiseFrame != value) {
                    this._noiseFrame = value;
                    this.shaderNoiseFrame.spriteFrame = value;
                    this.checkNoiseFrame();
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectDissolve.prototype, "degree", {
            get: function () {
                return this._degree;
            },
            set: function (value) {
                if (this._degree != value) {
                    this._degree = value;
                    this.shaderDegree.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectDissolve.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderNoiseFrame) {
                this.shaderNoiseFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "noiseframe");
                if (!this.shaderNoiseFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "noiseframe";
                    shader._textureVar.unifMacro.name = "USE_NOISE";
                    shader._textureVar.unifName = "noise";
                    shader._uvVar.attrMacro.checkOnly = false;
                    shader._uvVar.attrMacro.name = "NOISEOTHER";
                    shader._uvVar.attrName = "a_uv1";
                    this.shaderNoiseFrame = shader;
                }
            }
            if (!this.shaderDegree) {
                this.shaderDegree = this.getShaderComponent(lcc$render.ShaderFloat, "degree");
                if (!this.shaderDegree) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "degree";
                    shader._valueVar.attrMacro.name = "ATTR_DEGREE";
                    shader._valueVar.attrName = "a_degree";
                    shader._valueVar.unifMacro.name = "UNIF_DEGREE";
                    shader._valueVar.unifName = "u_degree";
                    this.shaderDegree = shader;
                    shader.value = this._degree;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            this.checkMaterial();
        };
        EffectDissolve.prototype.checkNoiseFrame = function () {
            if (this._noiseFrame && this._spriteFrame) {
                if ((this._noiseFrame == this._spriteFrame) || (this._noiseFrame._uuid == this._spriteFrame._uuid)) {
                    this.shaderNoiseFrame.useUV = false;
                }
                else {
                    this.shaderNoiseFrame.useUV = true;
                }
                this.renderSystem.setDirty();
            }
        };
        EffectDissolve.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d_dissolve"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectDissolve.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectDissolve.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectDissolve.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectDissolve.prototype, "spriteFrame", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectDissolve.prototype, "_noiseFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果噪声帧"
            })
        ], EffectDissolve.prototype, "noiseFrame", null);
        __decorate([
            property()
        ], EffectDissolve.prototype, "_degree", void 0);
        __decorate([
            property({
                tooltip: "程度",
                range: [0, 1, 0.01],
            })
        ], EffectDissolve.prototype, "degree", null);
        EffectDissolve = __decorate([
            ccclass("lcc$render.EffectDissolve"),
            menu("i18n:lcc-render.menu_component/EffectDissolve")
        ], EffectDissolve);
        return EffectDissolve;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectDissolve = EffectDissolve;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectFlashLight = (function (_super) {
        __extends(EffectFlashLight, _super);
        function EffectFlashLight() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._lightColor = cc.Color.WHITE;
            _this._lightAngle = 135;
            _this._lightWidth = 0.2;
            _this._enableGradient = true;
            _this._cropAlpha = true;
            _this._moveSpeed = -1;
            _this._moveWidth = 2;
            _this._lightLines = 1;
            _this._lightSpace = 1;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderLightColor = null;
            _this.shaderLightAngle = null;
            _this.shaderLightWidth = null;
            _this.shaderEnableGradient = null;
            _this.shaderCropAlpha = null;
            _this.shaderMoveSpeed = null;
            _this.shaderMoveWidth = null;
            _this.shaderLightLines = null;
            _this.shaderLightSpace = null;
            return _this;
        }
        Object.defineProperty(EffectFlashLight.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFlashLight.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFlashLight.prototype, "lightColor", {
            get: function () {
                return this._lightColor;
            },
            set: function (value) {
                this._lightColor = value;
                this.shaderLightColor.color = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFlashLight.prototype, "lightAngle", {
            get: function () {
                return this._lightAngle;
            },
            set: function (value) {
                if (this._lightAngle != value) {
                    this._lightAngle = value;
                    this.shaderLightAngle.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFlashLight.prototype, "lightWidth", {
            get: function () {
                return this._lightWidth;
            },
            set: function (value) {
                if (this._lightWidth != value) {
                    this._lightWidth = value;
                    this.shaderLightWidth.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFlashLight.prototype, "enableGradient", {
            get: function () {
                return this._enableGradient;
            },
            set: function (value) {
                if (this._enableGradient != value) {
                    this._enableGradient = value;
                    this.shaderEnableGradient.value = value ? 1 : 0;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFlashLight.prototype, "cropAlpha", {
            get: function () {
                return this._cropAlpha;
            },
            set: function (value) {
                if (this._cropAlpha != value) {
                    this._cropAlpha = value;
                    this.shaderCropAlpha.value = value ? 1 : 0;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFlashLight.prototype, "moveSpeed", {
            get: function () {
                return this._moveSpeed;
            },
            set: function (value) {
                if (this._moveSpeed != value) {
                    this._moveSpeed = value;
                    this.shaderMoveSpeed.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFlashLight.prototype, "moveWidth", {
            get: function () {
                return this._moveWidth;
            },
            set: function (value) {
                if (this._moveWidth != value) {
                    this._moveWidth = value;
                    this.shaderMoveWidth.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFlashLight.prototype, "lightLines", {
            get: function () {
                return this._lightLines;
            },
            set: function (value) {
                if (this._lightLines != value) {
                    this._lightLines = value;
                    this.shaderLightLines.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFlashLight.prototype, "lightSpace", {
            get: function () {
                return this._lightSpace;
            },
            set: function (value) {
                if (this._lightSpace != value) {
                    this._lightSpace = value;
                    this.shaderLightSpace.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectFlashLight.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    shader.useUVRect = true;
                    shader._uvRectVar.type = lcc$render.VariableType.ATTRIBUTE;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderLightColor) {
                this.shaderLightColor = this.getShaderComponent(lcc$render.ShaderColor, "lightcolor");
                if (!this.shaderLightColor) {
                    var shader = this.addComponent(lcc$render.ShaderColor);
                    shader.tag = "lightcolor";
                    shader.useNodeColor = false;
                    shader._colorVar.attrMacro.name = "ATTR_LIGHTCOLOR";
                    shader._colorVar.attrName = "a_lightColor";
                    shader._colorVar.unifMacro.name = "UNIF_LIGHTCOLOR";
                    shader._colorVar.unifName = "u_lightColor";
                    this.shaderLightColor = shader;
                    shader.color = this._lightColor;
                }
            }
            if (!this.shaderLightAngle) {
                this.shaderLightAngle = this.getShaderComponent(lcc$render.ShaderFloat, "lightangle");
                if (!this.shaderLightAngle) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "lightangle";
                    shader._valueVar.attrMacro.name = "ATTR_LIGHTANGLE";
                    shader._valueVar.attrName = "a_lightAngle";
                    shader._valueVar.unifMacro.name = "UNIF_LIGHTANGLE";
                    shader._valueVar.unifName = "u_lightAngle";
                    this.shaderLightAngle = shader;
                    shader.value = this._lightAngle;
                }
            }
            if (!this.shaderLightWidth) {
                this.shaderLightWidth = this.getShaderComponent(lcc$render.ShaderFloat, "lightwidth");
                if (!this.shaderLightWidth) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "lightwidth";
                    shader._valueVar.attrMacro.name = "ATTR_LIGHTWIDTH";
                    shader._valueVar.attrName = "a_lightWidth";
                    shader._valueVar.unifMacro.name = "UNIF_LIGHTWIDTH";
                    shader._valueVar.unifName = "u_lightWidth";
                    this.shaderLightWidth = shader;
                    shader.value = this._lightWidth;
                }
            }
            if (!this.shaderEnableGradient) {
                this.shaderEnableGradient = this.getShaderComponent(lcc$render.ShaderFloat, "enablegradient");
                if (!this.shaderEnableGradient) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "enablegradient";
                    shader._valueVar.attrMacro.name = "ATTR_ENABLEGRADIENT";
                    shader._valueVar.attrName = "a_enableGradient";
                    shader._valueVar.unifMacro.name = "UNIF_ENABLEGRADIENT";
                    shader._valueVar.unifName = "u_enableGradient";
                    this.shaderEnableGradient = shader;
                    shader.value = this._enableGradient ? 1 : 0;
                }
            }
            if (!this.shaderCropAlpha) {
                this.shaderCropAlpha = this.getShaderComponent(lcc$render.ShaderFloat, "cropalpha");
                if (!this.shaderCropAlpha) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "cropalpha";
                    shader._valueVar.attrMacro.name = "ATTR_CROPALPHA";
                    shader._valueVar.attrName = "a_cropAlpha";
                    shader._valueVar.unifMacro.name = "UNIF_CROPALPHA";
                    shader._valueVar.unifName = "u_cropAlpha";
                    this.shaderCropAlpha = shader;
                    shader.value = this._cropAlpha ? 1 : 0;
                }
            }
            if (!this.shaderMoveSpeed) {
                this.shaderMoveSpeed = this.getShaderComponent(lcc$render.ShaderFloat, "movespeed");
                if (!this.shaderMoveSpeed) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "movespeed";
                    shader._valueVar.attrMacro.name = "ATTR_MOVESPEED";
                    shader._valueVar.attrName = "a_moveSpeed";
                    shader._valueVar.unifMacro.name = "UNIF_MOVESPEED";
                    shader._valueVar.unifName = "u_moveSpeed";
                    this.shaderMoveSpeed = shader;
                    shader.value = this._moveSpeed;
                }
            }
            if (!this.shaderMoveWidth) {
                this.shaderMoveWidth = this.getShaderComponent(lcc$render.ShaderFloat, "movewidth");
                if (!this.shaderMoveWidth) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "movewidth";
                    shader._valueVar.attrMacro.name = "ATTR_MOVEWIDTH";
                    shader._valueVar.attrName = "a_moveWidth";
                    shader._valueVar.unifMacro.name = "UNIF_MOVEWIDTH";
                    shader._valueVar.unifName = "u_moveWidth";
                    this.shaderMoveWidth = shader;
                    shader.value = this._moveWidth;
                }
            }
            if (!this.shaderLightLines) {
                this.shaderLightLines = this.getShaderComponent(lcc$render.ShaderFloat, "lightlines");
                if (!this.shaderLightLines) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "lightlines";
                    shader._valueVar.attrMacro.name = "ATTR_LIGHTLINES";
                    shader._valueVar.attrName = "a_lightLines";
                    shader._valueVar.unifMacro.name = "UNIF_LIGHTLINES";
                    shader._valueVar.unifName = "u_lightLines";
                    this.shaderLightLines = shader;
                    shader.value = this._lightLines;
                }
            }
            if (!this.shaderLightSpace) {
                this.shaderLightSpace = this.getShaderComponent(lcc$render.ShaderFloat, "lightspace");
                if (!this.shaderLightSpace) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "lightspace";
                    shader._valueVar.attrMacro.name = "ATTR_LIGHTSPACE";
                    shader._valueVar.attrName = "a_lightSpace";
                    shader._valueVar.unifMacro.name = "UNIF_LIGHTSPACE";
                    shader._valueVar.unifName = "u_lightSpace";
                    this.shaderLightSpace = shader;
                    shader.value = this._lightSpace;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            if (CC_EDITOR) {
                this.checkMaterial();
            }
        };
        EffectFlashLight.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d-flash_light"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectFlashLight.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectFlashLight.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectFlashLight.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectFlashLight.prototype, "spriteFrame", null);
        __decorate([
            property(cc.Color)
        ], EffectFlashLight.prototype, "_lightColor", void 0);
        __decorate([
            property({
                type: cc.Color,
                tooltip: "光线颜色"
            })
        ], EffectFlashLight.prototype, "lightColor", null);
        __decorate([
            property()
        ], EffectFlashLight.prototype, "_lightAngle", void 0);
        __decorate([
            property({
                tooltip: "光束倾斜角度",
                range: [0, 360, 1],
            })
        ], EffectFlashLight.prototype, "lightAngle", null);
        __decorate([
            property()
        ], EffectFlashLight.prototype, "_lightWidth", void 0);
        __decorate([
            property({
                tooltip: "光束宽度"
            })
        ], EffectFlashLight.prototype, "lightWidth", null);
        __decorate([
            property()
        ], EffectFlashLight.prototype, "_enableGradient", void 0);
        __decorate([
            property({
                tooltip: "启用光束渐变"
            })
        ], EffectFlashLight.prototype, "enableGradient", null);
        __decorate([
            property()
        ], EffectFlashLight.prototype, "_cropAlpha", void 0);
        __decorate([
            property({
                tooltip: "裁剪掉透明区域上的光"
            })
        ], EffectFlashLight.prototype, "cropAlpha", null);
        __decorate([
            property()
        ], EffectFlashLight.prototype, "_moveSpeed", void 0);
        __decorate([
            property({
                tooltip: "移动速度"
            })
        ], EffectFlashLight.prototype, "moveSpeed", null);
        __decorate([
            property()
        ], EffectFlashLight.prototype, "_moveWidth", void 0);
        __decorate([
            property({
                tooltip: "移动宽度"
            })
        ], EffectFlashLight.prototype, "moveWidth", null);
        __decorate([
            property()
        ], EffectFlashLight.prototype, "_lightLines", void 0);
        __decorate([
            property({
                tooltip: "光束数量",
                range: [1, 20, 1]
            })
        ], EffectFlashLight.prototype, "lightLines", null);
        __decorate([
            property()
        ], EffectFlashLight.prototype, "_lightSpace", void 0);
        __decorate([
            property({
                tooltip: "光束间距"
            })
        ], EffectFlashLight.prototype, "lightSpace", null);
        EffectFlashLight = __decorate([
            ccclass("lcc$render.EffectFlashLight"),
            menu("i18n:lcc-render.menu_component/EffectFlashLight")
        ], EffectFlashLight);
        return EffectFlashLight;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectFlashLight = EffectFlashLight;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectFluxaySuper = (function (_super) {
        __extends(EffectFluxaySuper, _super);
        function EffectFluxaySuper() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._speed = 1;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderSpeed = null;
            return _this;
        }
        Object.defineProperty(EffectFluxaySuper.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFluxaySuper.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectFluxaySuper.prototype, "speed", {
            get: function () {
                return this._speed;
            },
            set: function (value) {
                if (this._speed != value) {
                    this._speed = value;
                    this.shaderSpeed.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectFluxaySuper.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useUV = true;
                    shader.useShape = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderSpeed) {
                this.shaderSpeed = this.getShaderComponent(lcc$render.ShaderFloat, "speed");
                if (!this.shaderSpeed) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "speed";
                    shader._valueVar.attrMacro.name = "ATTR_SPEED";
                    shader._valueVar.attrName = "a_speed";
                    shader._valueVar.unifMacro.name = "UNIF_SPEED";
                    shader._valueVar.unifName = "u_speed";
                    shader.value = 1;
                    this.shaderSpeed = shader;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            if (CC_EDITOR) {
                this.checkMaterial();
            }
        };
        EffectFluxaySuper.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d-fluxay_super"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectFluxaySuper.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectFluxaySuper.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectFluxaySuper.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectFluxaySuper.prototype, "spriteFrame", null);
        __decorate([
            property()
        ], EffectFluxaySuper.prototype, "_speed", void 0);
        __decorate([
            property({
                tooltip: "效果速度"
            })
        ], EffectFluxaySuper.prototype, "speed", null);
        EffectFluxaySuper = __decorate([
            ccclass("lcc$render.EffectFluxaySuper"),
            menu("i18n:lcc-render.menu_component/EffectFluxaySuper")
        ], EffectFluxaySuper);
        return EffectFluxaySuper;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectFluxaySuper = EffectFluxaySuper;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectGaussianBlur = (function (_super) {
        __extends(EffectGaussianBlur, _super);
        function EffectGaussianBlur() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            return _this;
        }
        Object.defineProperty(EffectGaussianBlur.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGaussianBlur.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectGaussianBlur.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    shader.useUVRect = true;
                    shader.useFrameSize = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            if (CC_EDITOR) {
                this.checkMaterial();
            }
        };
        EffectGaussianBlur.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d_gaussian_blur"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectGaussianBlur.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectGaussianBlur.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectGaussianBlur.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectGaussianBlur.prototype, "spriteFrame", null);
        EffectGaussianBlur = __decorate([
            ccclass("lcc$render.EffectGaussianBlur"),
            menu("i18n:lcc-render.menu_component/EffectGaussianBlur")
        ], EffectGaussianBlur);
        return EffectGaussianBlur;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectGaussianBlur = EffectGaussianBlur;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectGlowInner = (function (_super) {
        __extends(EffectGlowInner, _super);
        function EffectGlowInner() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._glowColor = cc.Color.YELLOW;
            _this._glowColorSize = 0.2;
            _this._glowThreshold = 0.1;
            _this._glowFlash = 0.0;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderGlowColor = null;
            _this.shaderGlowColorSize = null;
            _this.shaderGlowThreshold = null;
            _this.shaderGlowFlash = null;
            return _this;
        }
        Object.defineProperty(EffectGlowInner.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGlowInner.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGlowInner.prototype, "glowColor", {
            get: function () {
                return this._glowColor;
            },
            set: function (value) {
                this._glowColor = value;
                this.shaderGlowColor.color = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGlowInner.prototype, "glowColorSize", {
            get: function () {
                return this._glowColorSize;
            },
            set: function (value) {
                if (this._glowColorSize != value) {
                    this._glowColorSize = value;
                    this.shaderGlowColorSize.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGlowInner.prototype, "glowThreshold", {
            get: function () {
                return this._glowThreshold;
            },
            set: function (value) {
                if (this._glowThreshold != value) {
                    this._glowThreshold = value;
                    this.shaderGlowThreshold.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGlowInner.prototype, "glowFlash", {
            get: function () {
                return this._glowFlash;
            },
            set: function (value) {
                if (this._glowFlash != value) {
                    this._glowFlash = value;
                    this.shaderGlowFlash.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectGlowInner.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    shader.useUVRect = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderGlowColor) {
                this.shaderGlowColor = this.getShaderComponent(lcc$render.ShaderColor, "glowColor");
                if (!this.shaderGlowColor) {
                    var shader = this.addComponent(lcc$render.ShaderColor);
                    shader.tag = "glowColor";
                    shader.useNodeColor = false;
                    shader._colorVar.attrMacro.name = "ATTR_GLOWCOLOR";
                    shader._colorVar.attrName = "a_glowColor";
                    shader._colorVar.unifMacro.name = "UNIF_GLOWCOLOR";
                    shader._colorVar.unifName = "u_glowColor";
                    this.shaderGlowColor = shader;
                    shader.color = this._glowColor;
                }
            }
            if (!this.shaderGlowColorSize) {
                this.shaderGlowColorSize = this.getShaderComponent(lcc$render.ShaderFloat, "glowColorSize");
                if (!this.shaderGlowColorSize) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "glowColorSize";
                    shader._valueVar.attrMacro.name = "ATTR_GLOWCOLORSIZE";
                    shader._valueVar.attrName = "a_glowColorSize";
                    shader._valueVar.unifMacro.name = "UNIF_GLOWCOLORSIZE";
                    shader._valueVar.unifName = "u_glowColorSize";
                    this.shaderGlowColorSize = shader;
                    shader.value = this._glowColorSize;
                }
            }
            if (!this.shaderGlowThreshold) {
                this.shaderGlowThreshold = this.getShaderComponent(lcc$render.ShaderFloat, "glowThreshold");
                if (!this.shaderGlowThreshold) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "glowThreshold";
                    shader._valueVar.attrMacro.name = "ATTR_GLOWTHRESHOLD";
                    shader._valueVar.attrName = "a_glowThreshold";
                    shader._valueVar.unifMacro.name = "UNIF_GLOWTHRESHOLD";
                    shader._valueVar.unifName = "u_glowThreshold";
                    this.shaderGlowThreshold = shader;
                    shader.value = this._glowThreshold;
                }
            }
            if (!this.shaderGlowFlash) {
                this.shaderGlowFlash = this.getShaderComponent(lcc$render.ShaderFloat, "glowFlash");
                if (!this.shaderGlowFlash) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "glowFlash";
                    shader._valueVar.attrMacro.name = "ATTR_GLOWFLASH";
                    shader._valueVar.attrName = "a_glowFlash";
                    shader._valueVar.unifMacro.name = "UNIF_GLOWFLASH";
                    shader._valueVar.unifName = "u_glowFlash";
                    this.shaderGlowFlash = shader;
                    shader.value = this._glowFlash;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            if (CC_EDITOR) {
                this.checkMaterial();
            }
        };
        EffectGlowInner.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d_glow_inner"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectGlowInner.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectGlowInner.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectGlowInner.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectGlowInner.prototype, "spriteFrame", null);
        __decorate([
            property(cc.Color)
        ], EffectGlowInner.prototype, "_glowColor", void 0);
        __decorate([
            property({
                type: cc.Color,
                tooltip: "发光颜色"
            })
        ], EffectGlowInner.prototype, "glowColor", null);
        __decorate([
            property()
        ], EffectGlowInner.prototype, "_glowColorSize", void 0);
        __decorate([
            property({
                tooltip: "发光宽度",
                range: [0, 1, 0.1],
            })
        ], EffectGlowInner.prototype, "glowColorSize", null);
        __decorate([
            property()
        ], EffectGlowInner.prototype, "_glowThreshold", void 0);
        __decorate([
            property({
                tooltip: "发光阈值",
                range: [0, 1, 0.1],
            })
        ], EffectGlowInner.prototype, "glowThreshold", null);
        __decorate([
            property()
        ], EffectGlowInner.prototype, "_glowFlash", void 0);
        __decorate([
            property({
                tooltip: "发光闪烁速度",
            })
        ], EffectGlowInner.prototype, "glowFlash", null);
        EffectGlowInner = __decorate([
            ccclass("lcc$render.EffectGlowInner"),
            menu("i18n:lcc-render.menu_component/EffectGlowInner")
        ], EffectGlowInner);
        return EffectGlowInner;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectGlowInner = EffectGlowInner;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectGlowOutter = (function (_super) {
        __extends(EffectGlowOutter, _super);
        function EffectGlowOutter() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._glowColor = cc.Color.YELLOW;
            _this._glowColorSize = 0.1;
            _this._glowThreshold = 1;
            _this._glowFlash = 0.0;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderGlowColor = null;
            _this.shaderGlowColorSize = null;
            _this.shaderGlowThreshold = null;
            _this.shaderGlowFlash = null;
            return _this;
        }
        Object.defineProperty(EffectGlowOutter.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGlowOutter.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGlowOutter.prototype, "glowColor", {
            get: function () {
                return this._glowColor;
            },
            set: function (value) {
                this._glowColor = value;
                this.shaderGlowColor.color = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGlowOutter.prototype, "glowColorSize", {
            get: function () {
                return this._glowColorSize;
            },
            set: function (value) {
                if (this._glowColorSize != value) {
                    this._glowColorSize = value;
                    this.shaderGlowColorSize.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGlowOutter.prototype, "glowThreshold", {
            get: function () {
                return this._glowThreshold;
            },
            set: function (value) {
                if (this._glowThreshold != value) {
                    this._glowThreshold = value;
                    this.shaderGlowThreshold.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectGlowOutter.prototype, "glowFlash", {
            get: function () {
                return this._glowFlash;
            },
            set: function (value) {
                if (this._glowFlash != value) {
                    this._glowFlash = value;
                    this.shaderGlowFlash.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectGlowOutter.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    shader.useUVRect = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderGlowColor) {
                this.shaderGlowColor = this.getShaderComponent(lcc$render.ShaderColor, "glowColor");
                if (!this.shaderGlowColor) {
                    var shader = this.addComponent(lcc$render.ShaderColor);
                    shader.tag = "glowColor";
                    shader.useNodeColor = false;
                    shader._colorVar.attrMacro.name = "ATTR_GLOWCOLOR";
                    shader._colorVar.attrName = "a_glowColor";
                    shader._colorVar.unifMacro.name = "UNIF_GLOWCOLOR";
                    shader._colorVar.unifName = "u_glowColor";
                    this.shaderGlowColor = shader;
                    shader.color = this._glowColor;
                }
            }
            if (!this.shaderGlowColorSize) {
                this.shaderGlowColorSize = this.getShaderComponent(lcc$render.ShaderFloat, "glowColorSize");
                if (!this.shaderGlowColorSize) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "glowColorSize";
                    shader._valueVar.attrMacro.name = "ATTR_GLOWCOLORSIZE";
                    shader._valueVar.attrName = "a_glowColorSize";
                    shader._valueVar.unifMacro.name = "UNIF_GLOWCOLORSIZE";
                    shader._valueVar.unifName = "u_glowColorSize";
                    this.shaderGlowColorSize = shader;
                    shader.value = this._glowColorSize;
                }
            }
            if (!this.shaderGlowThreshold) {
                this.shaderGlowThreshold = this.getShaderComponent(lcc$render.ShaderFloat, "glowThreshold");
                if (!this.shaderGlowThreshold) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "glowThreshold";
                    shader._valueVar.attrMacro.name = "ATTR_GLOWTHRESHOLD";
                    shader._valueVar.attrName = "a_glowThreshold";
                    shader._valueVar.unifMacro.name = "UNIF_GLOWTHRESHOLD";
                    shader._valueVar.unifName = "u_glowThreshold";
                    this.shaderGlowThreshold = shader;
                    shader.value = this._glowThreshold;
                }
            }
            if (!this.shaderGlowFlash) {
                this.shaderGlowFlash = this.getShaderComponent(lcc$render.ShaderFloat, "glowFlash");
                if (!this.shaderGlowFlash) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "glowFlash";
                    shader._valueVar.attrMacro.name = "ATTR_GLOWFLASH";
                    shader._valueVar.attrName = "a_glowFlash";
                    shader._valueVar.unifMacro.name = "UNIF_GLOWFLASH";
                    shader._valueVar.unifName = "u_glowFlash";
                    this.shaderGlowFlash = shader;
                    shader.value = this._glowFlash;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            if (CC_EDITOR) {
                this.checkMaterial();
            }
        };
        EffectGlowOutter.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d_glow_outter"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectGlowOutter.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectGlowOutter.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectGlowOutter.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectGlowOutter.prototype, "spriteFrame", null);
        __decorate([
            property(cc.Color)
        ], EffectGlowOutter.prototype, "_glowColor", void 0);
        __decorate([
            property({
                type: cc.Color,
                tooltip: "发光颜色"
            })
        ], EffectGlowOutter.prototype, "glowColor", null);
        __decorate([
            property()
        ], EffectGlowOutter.prototype, "_glowColorSize", void 0);
        __decorate([
            property({
                tooltip: "发光宽度",
                range: [0, 1, 0.1],
            })
        ], EffectGlowOutter.prototype, "glowColorSize", null);
        __decorate([
            property()
        ], EffectGlowOutter.prototype, "_glowThreshold", void 0);
        __decorate([
            property({
                tooltip: "发光阈值",
                range: [0, 1, 0.1],
            })
        ], EffectGlowOutter.prototype, "glowThreshold", null);
        __decorate([
            property()
        ], EffectGlowOutter.prototype, "_glowFlash", void 0);
        __decorate([
            property({
                tooltip: "发光闪烁速度",
            })
        ], EffectGlowOutter.prototype, "glowFlash", null);
        EffectGlowOutter = __decorate([
            ccclass("lcc$render.EffectGlowOutter"),
            menu("i18n:lcc-render.menu_component/EffectGlowOutter")
        ], EffectGlowOutter);
        return EffectGlowOutter;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectGlowOutter = EffectGlowOutter;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectMosaic = (function (_super) {
        __extends(EffectMosaic, _super);
        function EffectMosaic() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._degree = 0.0;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderDegree = null;
            return _this;
        }
        Object.defineProperty(EffectMosaic.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectMosaic.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectMosaic.prototype, "degree", {
            get: function () {
                return this._degree;
            },
            set: function (value) {
                if (this._degree != value) {
                    this._degree = value;
                    this.shaderDegree.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectMosaic.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    shader.useUVRect = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderDegree) {
                this.shaderDegree = this.getShaderComponent(lcc$render.ShaderFloat, "degree");
                if (!this.shaderDegree) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "degree";
                    shader._valueVar.attrMacro.name = "ATTR_DEGREE";
                    shader._valueVar.attrName = "a_degree";
                    shader._valueVar.unifMacro.name = "UNIF_DEGREE";
                    shader._valueVar.unifName = "u_degree";
                    this.shaderDegree = shader;
                    shader.value = this._degree;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            if (CC_EDITOR) {
                this.checkMaterial();
            }
        };
        EffectMosaic.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d_mosaic"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectMosaic.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectMosaic.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectMosaic.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectMosaic.prototype, "spriteFrame", null);
        __decorate([
            property()
        ], EffectMosaic.prototype, "_degree", void 0);
        __decorate([
            property({
                tooltip: "马赛克程度",
                range: [0, 1, 0.01],
            })
        ], EffectMosaic.prototype, "degree", null);
        EffectMosaic = __decorate([
            ccclass("lcc$render.EffectMosaic"),
            menu("i18n:lcc-render.menu_component/EffectMosaic")
        ], EffectMosaic);
        return EffectMosaic;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectMosaic = EffectMosaic;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectOutline = (function (_super) {
        __extends(EffectOutline, _super);
        function EffectOutline() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._outlineColor = cc.Color.RED;
            _this._outlineWidth = 0.002;
            _this._glowFlash = 0.0;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderOutlineColor = null;
            _this.shaderOutlineWidth = null;
            _this.shaderGlowFlash = null;
            return _this;
        }
        Object.defineProperty(EffectOutline.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectOutline.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectOutline.prototype, "outlineColor", {
            get: function () {
                return this._outlineColor;
            },
            set: function (value) {
                this._outlineColor = value;
                this.shaderOutlineColor.color = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectOutline.prototype, "outlineWidth", {
            get: function () {
                return this._outlineWidth;
            },
            set: function (value) {
                if (this._outlineWidth != value) {
                    this._outlineWidth = value;
                    this.shaderOutlineWidth.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectOutline.prototype, "glowFlash", {
            get: function () {
                return this._glowFlash;
            },
            set: function (value) {
                if (this._glowFlash != value) {
                    this._glowFlash = value;
                    this.shaderGlowFlash.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectOutline.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    shader.useUVRect = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderOutlineColor) {
                this.shaderOutlineColor = this.getShaderComponent(lcc$render.ShaderColor, "outlineColor");
                if (!this.shaderOutlineColor) {
                    var shader = this.addComponent(lcc$render.ShaderColor);
                    shader.tag = "outlineColor";
                    shader.useNodeColor = false;
                    shader._colorVar.attrMacro.name = "ATTR_OUTLINECOLOR";
                    shader._colorVar.attrName = "a_outlineColor";
                    shader._colorVar.unifMacro.name = "UNIF_OUTLINECOLOR";
                    shader._colorVar.unifName = "u_outlineColor";
                    this.shaderOutlineColor = shader;
                    shader.color = this._outlineColor;
                }
            }
            if (!this.shaderOutlineWidth) {
                this.shaderOutlineWidth = this.getShaderComponent(lcc$render.ShaderFloat, "outlineWidth");
                if (!this.shaderOutlineWidth) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "outlineWidth";
                    shader._valueVar.attrMacro.name = "ATTR_OUTLINEWIDTH";
                    shader._valueVar.attrName = "a_outlineWidth";
                    shader._valueVar.unifMacro.name = "UNIF_OUTLINEWIDTH";
                    shader._valueVar.unifName = "u_outlineWidth";
                    this.shaderOutlineWidth = shader;
                    shader.value = this._outlineWidth;
                }
            }
            if (!this.shaderGlowFlash) {
                this.shaderGlowFlash = this.getShaderComponent(lcc$render.ShaderFloat, "glowFlash");
                if (!this.shaderGlowFlash) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "glowFlash";
                    shader._valueVar.attrMacro.name = "ATTR_GLOWFLASH";
                    shader._valueVar.attrName = "a_glowFlash";
                    shader._valueVar.unifMacro.name = "UNIF_GLOWFLASH";
                    shader._valueVar.unifName = "u_glowFlash";
                    this.shaderGlowFlash = shader;
                    shader.value = this._glowFlash;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            if (CC_EDITOR) {
                this.checkMaterial();
            }
        };
        EffectOutline.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d_outline"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectOutline.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectOutline.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectOutline.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectOutline.prototype, "spriteFrame", null);
        __decorate([
            property(cc.Color)
        ], EffectOutline.prototype, "_outlineColor", void 0);
        __decorate([
            property({
                type: cc.Color,
                tooltip: "描边颜色"
            })
        ], EffectOutline.prototype, "outlineColor", null);
        __decorate([
            property()
        ], EffectOutline.prototype, "_outlineWidth", void 0);
        __decorate([
            property({
                tooltip: "描边宽度",
                range: [0, 1, 0.01],
            })
        ], EffectOutline.prototype, "outlineWidth", null);
        __decorate([
            property()
        ], EffectOutline.prototype, "_glowFlash", void 0);
        __decorate([
            property({
                tooltip: "闪烁速度",
            })
        ], EffectOutline.prototype, "glowFlash", null);
        EffectOutline = __decorate([
            ccclass("lcc$render.EffectOutline"),
            menu("i18n:lcc-render.menu_component/EffectOutline")
        ], EffectOutline);
        return EffectOutline;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectOutline = EffectOutline;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var ColorType;
    (function (ColorType) {
        ColorType[ColorType["OLD"] = 0] = "OLD";
        ColorType[ColorType["GRAY"] = 1] = "GRAY";
        ColorType[ColorType["REVERSAL"] = 2] = "REVERSAL";
        ColorType[ColorType["FROZEN"] = 3] = "FROZEN";
        ColorType[ColorType["CARTOON"] = 4] = "CARTOON";
    })(ColorType || (ColorType = {}));
    var EffectPhoto = (function (_super) {
        __extends(EffectPhoto, _super);
        function EffectPhoto() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._colorType = ColorType.OLD;
            _this._degree = 1.0;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderColorType = null;
            _this.shaderDegree = null;
            return _this;
        }
        Object.defineProperty(EffectPhoto.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectPhoto.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectPhoto.prototype, "colorType", {
            get: function () {
                return this._colorType;
            },
            set: function (value) {
                if (this._colorType != value) {
                    this._colorType = value;
                    this.shaderColorType.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectPhoto.prototype, "degree", {
            get: function () {
                return this._degree;
            },
            set: function (value) {
                if (this._degree != value) {
                    this._degree = value;
                    this.shaderDegree.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectPhoto.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderColorType) {
                this.shaderColorType = this.getShaderComponent(lcc$render.ShaderFloat, "colorType");
                if (!this.shaderColorType) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "colorType";
                    shader._valueVar.attrMacro.name = "ATTR_COLORTYPE";
                    shader._valueVar.attrName = "a_colorType";
                    shader._valueVar.unifMacro.name = "UNIF_COLORTYPE";
                    shader._valueVar.unifName = "u_colorType";
                    this.shaderColorType = shader;
                    shader.value = this._colorType;
                }
            }
            if (!this.shaderDegree) {
                this.shaderDegree = this.getShaderComponent(lcc$render.ShaderFloat, "degree");
                if (!this.shaderDegree) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "degree";
                    shader._valueVar.attrMacro.name = "ATTR_DEGREE";
                    shader._valueVar.attrName = "a_degree";
                    shader._valueVar.unifMacro.name = "UNIF_DEGREE";
                    shader._valueVar.unifName = "u_degree";
                    this.shaderDegree = shader;
                    shader.value = this._degree;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            this.checkMaterial();
        };
        EffectPhoto.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d_photo"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectPhoto.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectPhoto.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectPhoto.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectPhoto.prototype, "spriteFrame", null);
        __decorate([
            property({
                type: cc.Enum(ColorType)
            })
        ], EffectPhoto.prototype, "_colorType", void 0);
        __decorate([
            property({
                tooltip: "颜色类型",
                type: cc.Enum(ColorType)
            })
        ], EffectPhoto.prototype, "colorType", null);
        __decorate([
            property()
        ], EffectPhoto.prototype, "_degree", void 0);
        __decorate([
            property({
                tooltip: "程度",
                range: [0, 1, 0.01],
            })
        ], EffectPhoto.prototype, "degree", null);
        EffectPhoto = __decorate([
            ccclass("lcc$render.EffectPhoto"),
            menu("i18n:lcc-render.menu_component/EffectPhoto")
        ], EffectPhoto);
        return EffectPhoto;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectPhoto = EffectPhoto;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectRainDropRipples = (function (_super) {
        __extends(EffectRainDropRipples, _super);
        function EffectRainDropRipples() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._density = 15.0;
            _this._change = true;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderDensity = null;
            _this.shaderChange = null;
            return _this;
        }
        Object.defineProperty(EffectRainDropRipples.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectRainDropRipples.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectRainDropRipples.prototype, "density", {
            get: function () {
                return this._density;
            },
            set: function (value) {
                if (this._density != value) {
                    this._density = value;
                    this.shaderDensity.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectRainDropRipples.prototype, "change", {
            get: function () {
                return this._change;
            },
            set: function (value) {
                if (this._change != value) {
                    this._change = value;
                    this.shaderChange.value = value ? 1 : 0;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectRainDropRipples.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    shader.useUVRect = true;
                    shader.useFrameSize = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderDensity) {
                this.shaderDensity = this.getShaderComponent(lcc$render.ShaderFloat, "density");
                if (!this.shaderDensity) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "density";
                    shader._valueVar.attrMacro.name = "ATTR_DENSITY";
                    shader._valueVar.attrName = "a_density";
                    shader._valueVar.unifMacro.name = "UNIF_DENSITY";
                    shader._valueVar.unifName = "u_density";
                    this.shaderDensity = shader;
                    shader.value = this._density;
                }
            }
            if (!this.shaderChange) {
                this.shaderChange = this.getShaderComponent(lcc$render.ShaderFloat, "change");
                if (!this.shaderChange) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "change";
                    shader._valueVar.attrMacro.name = "ATTR_CHANGE";
                    shader._valueVar.attrName = "a_change";
                    shader._valueVar.unifMacro.name = "UNIF_CHANGE";
                    shader._valueVar.unifName = "u_change";
                    this.shaderChange = shader;
                    shader.value = this._change ? 1 : 0;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            if (CC_EDITOR) {
                this.checkMaterial();
            }
        };
        EffectRainDropRipples.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d_rain_drop_ripples"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectRainDropRipples.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectRainDropRipples.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectRainDropRipples.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectRainDropRipples.prototype, "spriteFrame", null);
        __decorate([
            property()
        ], EffectRainDropRipples.prototype, "_density", void 0);
        __decorate([
            property({
                tooltip: "密度",
            })
        ], EffectRainDropRipples.prototype, "density", null);
        __decorate([
            property()
        ], EffectRainDropRipples.prototype, "_change", void 0);
        __decorate([
            property({
                tooltip: "动态改变",
            })
        ], EffectRainDropRipples.prototype, "change", null);
        EffectRainDropRipples = __decorate([
            ccclass("lcc$render.EffectRainDropRipples"),
            menu("i18n:lcc-render.menu_component/EffectRainDropRipples")
        ], EffectRainDropRipples);
        return EffectRainDropRipples;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectRainDropRipples = EffectRainDropRipples;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectRoundCornerCrop = (function (_super) {
        __extends(EffectRoundCornerCrop, _super);
        function EffectRoundCornerCrop() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._degree = 0.5;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderDegree = null;
            return _this;
        }
        Object.defineProperty(EffectRoundCornerCrop.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectRoundCornerCrop.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectRoundCornerCrop.prototype, "degree", {
            get: function () {
                return this._degree;
            },
            set: function (value) {
                if (this._degree != value) {
                    this._degree = value;
                    this.shaderDegree.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectRoundCornerCrop.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    shader.useUVRect = true;
                    shader.useFrameSize = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderDegree) {
                this.shaderDegree = this.getShaderComponent(lcc$render.ShaderFloat, "degree");
                if (!this.shaderDegree) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "degree";
                    shader._valueVar.attrMacro.name = "ATTR_DEGREE";
                    shader._valueVar.attrName = "a_degree";
                    shader._valueVar.unifMacro.name = "UNIF_DEGREE";
                    shader._valueVar.unifName = "u_degree";
                    this.shaderDegree = shader;
                    shader.value = this._degree;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            if (CC_EDITOR) {
                this.checkMaterial();
            }
        };
        EffectRoundCornerCrop.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d_round_corner_crop"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectRoundCornerCrop.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectRoundCornerCrop.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectRoundCornerCrop.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectRoundCornerCrop.prototype, "spriteFrame", null);
        __decorate([
            property()
        ], EffectRoundCornerCrop.prototype, "_degree", void 0);
        __decorate([
            property({
                tooltip: "程度",
                range: [0, 1, 0.01],
            })
        ], EffectRoundCornerCrop.prototype, "degree", null);
        EffectRoundCornerCrop = __decorate([
            ccclass("lcc$render.EffectRoundCornerCrop"),
            menu("i18n:lcc-render.menu_component/EffectRoundCornerCrop")
        ], EffectRoundCornerCrop);
        return EffectRoundCornerCrop;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectRoundCornerCrop = EffectRoundCornerCrop;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var FadeDirect;
    (function (FadeDirect) {
        FadeDirect[FadeDirect["LEFT_TO_RIGHT"] = 0] = "LEFT_TO_RIGHT";
        FadeDirect[FadeDirect["RIGHT_TO_LEFT"] = 1] = "RIGHT_TO_LEFT";
        FadeDirect[FadeDirect["TOP_TO_BOTTOM"] = 2] = "TOP_TO_BOTTOM";
        FadeDirect[FadeDirect["BOTTOM_TO_TOP"] = 3] = "BOTTOM_TO_TOP";
    })(FadeDirect || (FadeDirect = {}));
    var EffectSmoothTransform = (function (_super) {
        __extends(EffectSmoothTransform, _super);
        function EffectSmoothTransform() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._fadeWidth = 0.2;
            _this._fadeDirect = FadeDirect.LEFT_TO_RIGHT;
            _this._degree = 0.5;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderFadeWidth = null;
            _this.shaderFadeDirect = null;
            _this.shaderDegree = null;
            return _this;
        }
        Object.defineProperty(EffectSmoothTransform.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectSmoothTransform.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectSmoothTransform.prototype, "fadeWidth", {
            get: function () {
                return this._fadeWidth;
            },
            set: function (value) {
                if (this._fadeWidth != value) {
                    this._fadeWidth = value;
                    this.shaderFadeWidth.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectSmoothTransform.prototype, "fadeDirect", {
            get: function () {
                return this._fadeDirect;
            },
            set: function (value) {
                if (this._fadeDirect != value) {
                    this._fadeDirect = value;
                    this.shaderFadeDirect.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectSmoothTransform.prototype, "degree", {
            get: function () {
                return this._degree;
            },
            set: function (value) {
                if (this._degree != value) {
                    this._degree = value;
                    this.shaderDegree.value = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectSmoothTransform.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useShape = true;
                    shader.useUV = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderFadeWidth) {
                this.shaderFadeWidth = this.getShaderComponent(lcc$render.ShaderFloat, "fadeWidth");
                if (!this.shaderFadeWidth) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "fadeWidth";
                    shader._valueVar.attrMacro.name = "ATTR_FADEWIDTH";
                    shader._valueVar.attrName = "a_fadeWidth";
                    shader._valueVar.unifMacro.name = "UNIF_FADEWIDTH";
                    shader._valueVar.unifName = "u_fadeWidth";
                    this.shaderFadeWidth = shader;
                    shader.value = this._fadeWidth;
                }
            }
            if (!this.shaderFadeDirect) {
                this.shaderFadeDirect = this.getShaderComponent(lcc$render.ShaderFloat, "fadeDirect");
                if (!this.shaderFadeDirect) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "fadeDirect";
                    shader._valueVar.attrMacro.name = "ATTR_FADEDIRECT";
                    shader._valueVar.attrName = "a_fadeDirect";
                    shader._valueVar.unifMacro.name = "UNIF_FADEDIRECT";
                    shader._valueVar.unifName = "u_fadeDirect";
                    this.shaderFadeDirect = shader;
                    shader.value = this._fadeDirect;
                }
            }
            if (!this.shaderDegree) {
                this.shaderDegree = this.getShaderComponent(lcc$render.ShaderFloat, "degree");
                if (!this.shaderDegree) {
                    var shader = this.addComponent(lcc$render.ShaderFloat);
                    shader.tag = "degree";
                    shader._valueVar.attrMacro.name = "ATTR_DEGREE";
                    shader._valueVar.attrName = "a_degree";
                    shader._valueVar.unifMacro.name = "UNIF_DEGREE";
                    shader._valueVar.unifName = "u_degree";
                    this.shaderDegree = shader;
                    shader.value = this._degree;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            this.checkMaterial();
        };
        EffectSmoothTransform.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d_transform"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectSmoothTransform.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectSmoothTransform.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectSmoothTransform.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectSmoothTransform.prototype, "spriteFrame", null);
        __decorate([
            property()
        ], EffectSmoothTransform.prototype, "_fadeWidth", void 0);
        __decorate([
            property({
                tooltip: "过渡宽度",
                range: [0, 1, 0.01],
            })
        ], EffectSmoothTransform.prototype, "fadeWidth", null);
        __decorate([
            property({
                type: cc.Enum(FadeDirect)
            })
        ], EffectSmoothTransform.prototype, "_fadeDirect", void 0);
        __decorate([
            property({
                tooltip: "过渡方向",
                type: cc.Enum(FadeDirect)
            })
        ], EffectSmoothTransform.prototype, "fadeDirect", null);
        __decorate([
            property()
        ], EffectSmoothTransform.prototype, "_degree", void 0);
        __decorate([
            property({
                tooltip: "程度",
                range: [0, 1, 0.01],
            })
        ], EffectSmoothTransform.prototype, "degree", null);
        EffectSmoothTransform = __decorate([
            ccclass("lcc$render.EffectSmoothTransform"),
            menu("i18n:lcc-render.menu_component/EffectSmoothTransform")
        ], EffectSmoothTransform);
        return EffectSmoothTransform;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectSmoothTransform = EffectSmoothTransform;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var EffectSpriteMask = (function (_super) {
        __extends(EffectSpriteMask, _super);
        function EffectSpriteMask() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._material = null;
            _this._spriteFrame = null;
            _this._maskFrame = null;
            _this.renderSystem = null;
            _this.shaderSpriteFrame = null;
            _this.shaderMaskFrame = null;
            return _this;
        }
        Object.defineProperty(EffectSpriteMask.prototype, "material", {
            get: function () {
                return this._material;
            },
            set: function (value) {
                if (this._material != value) {
                    this._material = value;
                    this.renderSystem.setMaterial(0, value);
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectSpriteMask.prototype, "spriteFrame", {
            get: function () {
                return this._spriteFrame;
            },
            set: function (value) {
                if (this._spriteFrame != value) {
                    this._spriteFrame = value;
                    this.shaderSpriteFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EffectSpriteMask.prototype, "maskFrame", {
            get: function () {
                return this._maskFrame;
            },
            set: function (value) {
                if (this._maskFrame != value) {
                    this._maskFrame = value;
                    this.shaderMaskFrame.spriteFrame = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        EffectSpriteMask.prototype.onEnable = function () {
            if (!this.shaderSpriteFrame) {
                this.shaderSpriteFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "spriteframe");
                if (!this.shaderSpriteFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "spriteframe";
                    shader.useUV = true;
                    shader.useShape = true;
                    this.shaderSpriteFrame = shader;
                }
            }
            if (!this.shaderMaskFrame) {
                this.shaderMaskFrame = this.getShaderComponent(lcc$render.ShaderSpriteFrame, "maskframe");
                if (!this.shaderMaskFrame) {
                    var shader = this.addComponent(lcc$render.ShaderSpriteFrame);
                    shader.tag = "maskframe";
                    shader.useUV = true;
                    shader._textureVar.unifMacro.name = "USE_MASK";
                    shader._textureVar.unifName = "mask";
                    shader._uvVar.attrMacro.name = "USE_MASK";
                    shader._uvVar.attrName = "a_uv1";
                    this.shaderMaskFrame = shader;
                }
            }
            if (!this.renderSystem) {
                this.renderSystem = this.getComponent(lcc$render.RenderSystem);
                if (!this.renderSystem) {
                    this.renderSystem = this.addComponent(lcc$render.RenderSystem);
                }
            }
            if (CC_EDITOR) {
                this.checkMaterial();
            }
        };
        EffectSpriteMask.prototype.checkMaterial = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!CC_EDITOR) return [3, 2];
                            if (!!this._material) return [3, 2];
                            _a = this;
                            return [4, lcc$render.Utils.getAssetByUUID(lcc$render.UUID.materials["lcc-2d-sprite_mask"])];
                        case 1:
                            _a.material = _b.sent();
                            _b.label = 2;
                        case 2: return [2];
                    }
                });
            });
        };
        __decorate([
            property(cc.Material)
        ], EffectSpriteMask.prototype, "_material", void 0);
        __decorate([
            property({
                type: cc.Material,
                tooltip: "效果材质"
            })
        ], EffectSpriteMask.prototype, "material", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectSpriteMask.prototype, "_spriteFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果精灵帧"
            })
        ], EffectSpriteMask.prototype, "spriteFrame", null);
        __decorate([
            property(cc.SpriteFrame)
        ], EffectSpriteMask.prototype, "_maskFrame", void 0);
        __decorate([
            property({
                type: cc.SpriteFrame,
                tooltip: "效果遮罩帧"
            })
        ], EffectSpriteMask.prototype, "maskFrame", null);
        EffectSpriteMask = __decorate([
            ccclass("lcc$render.EffectSpriteMask"),
            menu("i18n:lcc-render.menu_component/EffectSpriteMask")
        ], EffectSpriteMask);
        return EffectSpriteMask;
    }(lcc$render.ShaderComponent));
    lcc$render.EffectSpriteMask = EffectSpriteMask;
})(lcc$render || (lcc$render = {}));
var lcc$render;
(function (lcc$render) {
    var RenderAssembler = (function (_super) {
        __extends(RenderAssembler, _super);
        function RenderAssembler() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.verticesCount = 4;
            _this.indicesCount = 6;
            _this.floatsPerVert = 0;
            _this.renderData = null;
            return _this;
        }
        RenderAssembler.prototype.init = function (rcomp) {
            _super.prototype.init.call(this, rcomp);
            rcomp.setDirty();
        };
        Object.defineProperty(RenderAssembler.prototype, "verticesFloats", {
            get: function () {
                return this.verticesCount * this.floatsPerVert;
            },
            enumerable: false,
            configurable: true
        });
        RenderAssembler.prototype.getBuffer = function () {
            return cc.renderer._handle.getBuffer("mesh", this._renderComp.getVfmt());
        };
        RenderAssembler.prototype.resetRenderData = function () {
            var data = new cc.RenderData();
            data.init(this);
            data.createFlexData(0, this.verticesCount, this.indicesCount, this._renderComp.getVfmt());
            this.renderData = data;
            return data;
        };
        RenderAssembler.prototype.fillBuffers = function (comp, renderer) {
            if (renderer.worldMatDirty) {
                comp.updateWorldVerts();
            }
            var renderData = this.renderData;
            var vData = renderData.vDatas[0];
            var iData = renderData.iDatas[0];
            var buffer = this.getBuffer();
            var offsetInfo = buffer.request(this.verticesCount, this.indicesCount);
            var vertexOffset = offsetInfo.byteOffset >> 2, vbuf = buffer._vData;
            if (vData.length + vertexOffset > vbuf.length) {
                vbuf.set(vData.subarray(0, vbuf.length - vertexOffset), vertexOffset);
            }
            else {
                vbuf.set(vData, vertexOffset);
            }
            var ibuf = buffer._iData, indiceOffset = offsetInfo.indiceOffset, vertexId = offsetInfo.vertexOffset;
            for (var i = 0, l = iData.length; i < l; i++) {
                ibuf[indiceOffset++] = vertexId + iData[i];
            }
        };
        RenderAssembler.prototype.updateRenderData = function (sprite) {
            sprite.updateRenderData();
        };
        return RenderAssembler;
    }(cc.Assembler));
    cc.Assembler.register(lcc$render.RenderSystem, RenderAssembler);
})(lcc$render || (lcc$render = {}));

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZyYW1ld29yay9zcmMvcmVuZGVyL3NoYWRlci9TaGFkZXJDb21wb25lbnQudHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9zeXN0ZW0vUmVuZGVyU3lzdGVtLnRzIiwiZnJhbWV3b3JrL3NyYy9GcmFtZXdvcmsudHMiLCJmcmFtZXdvcmsvc3JjL2NvbW1vbi9VdGlscy50cyIsImZyYW1ld29yay9zcmMvY29tbW9uL1VVSUQudHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9zaGFkZXIvU2hhZGVyQ29sb3IudHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9zaGFkZXIvU2hhZGVyRmxvYXQudHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9zaGFkZXIvU2hhZGVyTWFjcm8udHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9zaGFkZXIvU2hhZGVyU3ByaXRlRnJhbWUudHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9zaGFkZXIvU2hhZGVyVmVjMi50cyIsImZyYW1ld29yay9zcmMvcmVuZGVyL3NoYWRlci9TaGFkZXJWZWMzLnRzIiwiZnJhbWV3b3JrL3NyYy9yZW5kZXIvc2hhZGVyL1NoYWRlclZlYzQudHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9lZmZlY3QvRWZmZWN0RGlzc29sdmUudHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9lZmZlY3QvRWZmZWN0Rmxhc2hMaWdodC50cyIsImZyYW1ld29yay9zcmMvcmVuZGVyL2VmZmVjdC9FZmZlY3RGbHV4YXlTdXBlci50cyIsImZyYW1ld29yay9zcmMvcmVuZGVyL2VmZmVjdC9FZmZlY3RHYXVzc2lhbkJsdXIudHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9lZmZlY3QvRWZmZWN0R2xvd0lubmVyLnRzIiwiZnJhbWV3b3JrL3NyYy9yZW5kZXIvZWZmZWN0L0VmZmVjdEdsb3dPdXR0ZXIudHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9lZmZlY3QvRWZmZWN0TW9zYWljLnRzIiwiZnJhbWV3b3JrL3NyYy9yZW5kZXIvZWZmZWN0L0VmZmVjdE91dGxpbmUudHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9lZmZlY3QvRWZmZWN0UGhvdG8udHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9lZmZlY3QvRWZmZWN0UmFpbkRyb3BSaXBwbGVzLnRzIiwiZnJhbWV3b3JrL3NyYy9yZW5kZXIvZWZmZWN0L0VmZmVjdFJvdW5kQ29ybmVyQ3JvcC50cyIsImZyYW1ld29yay9zcmMvcmVuZGVyL2VmZmVjdC9FZmZlY3RTbW9vdGhUcmFuc2Zvcm0udHMiLCJmcmFtZXdvcmsvc3JjL3JlbmRlci9lZmZlY3QvRWZmZWN0U3ByaXRlTWFzay50cyIsImZyYW1ld29yay9zcmMvcmVuZGVyL3N5c3RlbS9SZW5kZXJBc3NlbWJsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLElBQU8sVUFBVSxDQTZMaEI7QUE3TEQsV0FBTyxVQUFVO0lBRVgsSUFBQSxLQUF5QyxFQUFFLENBQUMsVUFBVSxFQUFyRCxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxpQkFBaUIsdUJBQWlCLENBQUM7SUFNN0Q7UUFXQztZQUFZLGdCQUFhO2lCQUFiLFVBQWEsRUFBYixxQkFBYSxFQUFiLElBQWE7Z0JBQWIsMkJBQWE7O1lBUHpCLFNBQUksR0FBVSxFQUFFLENBQUM7WUFLakIsY0FBUyxHQUFXLEtBQUssQ0FBQztZQUd6QixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDNUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDO1FBQ3JDLENBQUM7UUFWRDtZQUhDLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUcsY0FBYzthQUN4QixDQUFDO2lEQUNlO1FBS2pCO1lBSEMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxPQUFPO2FBQ2pCLENBQUM7c0RBQ3dCO1FBVGQsV0FBVztZQUR2QixPQUFPLENBQUMsd0JBQXdCLENBQUM7V0FDckIsV0FBVyxDQWV2QjtRQUFELGtCQUFDO0tBZkQsQUFlQyxJQUFBO0lBZlksc0JBQVcsY0FldkIsQ0FBQTtJQUtELElBQVksWUFVWDtJQVZELFdBQVksWUFBWTtRQUl2Qix5REFBYSxDQUFBO1FBS2IscURBQU8sQ0FBQTtJQUNSLENBQUMsRUFWVyxZQUFZLEdBQVosdUJBQVksS0FBWix1QkFBWSxRQVV2QjtJQUFBLENBQUM7SUFLRixJQUFZLFNBVVg7SUFWRCxXQUFZLFNBQVM7UUFJcEIsNkNBQU0sQ0FBQTtRQUtOLDJDQUFLLENBQUE7SUFDTixDQUFDLEVBVlcsU0FBUyxHQUFULG9CQUFTLEtBQVQsb0JBQVMsUUFVcEI7SUFBQSxDQUFDO0lBTUY7UUErQ0M7WUFBWSxnQkFBYTtpQkFBYixVQUFhLEVBQWIscUJBQWEsRUFBYixJQUFhO2dCQUFiLDJCQUFhOztZQTdDekIsYUFBUSxHQUFXLElBQUksQ0FBQztZQVN4QixTQUFJLEdBQWdCLFlBQVksQ0FBQyxPQUFPLENBQUM7WUFTekMsY0FBUyxHQUFlLElBQUksV0FBVyxFQUFFLENBQUM7WUFRMUMsYUFBUSxHQUFVLEVBQUUsQ0FBQztZQVNyQixjQUFTLEdBQWUsSUFBSSxXQUFXLEVBQUUsQ0FBQztZQVExQyxhQUFRLEdBQVUsRUFBRSxDQUFDO1lBR3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUM7WUFDOUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQztZQUN0RSxJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDaEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQztZQUN0RSxJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDakMsQ0FBQztRQXBERDtZQURDLFFBQVEsRUFBRTt3REFDYTtRQVN4QjtZQVBDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQzVCLE9BQU8sRUFBRyxNQUFNO2dCQUNoQixPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDdEIsQ0FBQzthQUNELENBQUM7b0RBQ3VDO1FBU3pDO1lBUEMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxXQUFXO2dCQUNsQixPQUFPLEVBQUcsU0FBUztnQkFDbkIsT0FBTztvQkFDTixPQUFPLElBQUksQ0FBQyxJQUFJLElBQUksWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDNUMsQ0FBQzthQUNELENBQUM7eURBQ3dDO1FBUTFDO1lBTkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxPQUFPO2dCQUNqQixPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLElBQUksSUFBSSxZQUFZLENBQUMsU0FBUyxDQUFDO2dCQUM1QyxDQUFDO2FBQ0QsQ0FBQzt3REFDbUI7UUFTckI7WUFQQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLFdBQVc7Z0JBQ2xCLE9BQU8sRUFBRyxTQUFTO2dCQUNuQixPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLElBQUksSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDO2dCQUMxQyxDQUFDO2FBQ0QsQ0FBQzt5REFDd0M7UUFRMUM7WUFOQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTyxFQUFHLE9BQU87Z0JBQ2pCLE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsSUFBSSxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUM7Z0JBQzFDLENBQUM7YUFDRCxDQUFDO3dEQUNtQjtRQTdDVCxjQUFjO1lBRDFCLE9BQU8sQ0FBQywyQkFBMkIsQ0FBQztXQUN4QixjQUFjLENBdUQxQjtRQUFELHFCQUFDO0tBdkRELEFBdURDLElBQUE7SUF2RFkseUJBQWMsaUJBdUQxQixDQUFBO0lBSUQ7UUFBcUMsbUNBQVk7UUFBakQ7WUFBQSxxRUFxRUM7WUFsRUEsVUFBSSxHQUFVLEVBQUUsQ0FBQzs7UUFrRWxCLENBQUM7UUE5REEsc0JBQUksZ0NBQUc7aUJBQVA7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2xCLENBQUM7aUJBQ0QsVUFBUSxLQUFZO2dCQUNuQixJQUFHLElBQUksQ0FBQyxJQUFJLElBQUksS0FBSyxFQUFDO29CQUNyQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztvQkFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztpQkFDcEM7WUFDRixDQUFDOzs7V0FOQTtRQWFTLDRDQUFrQixHQUE1QixVQUE2QixRQUFvQixFQUFFLEtBQWlCO1lBQ25FLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDeEUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ2xELENBQUM7UUFRUyw2Q0FBbUIsR0FBN0IsVUFBOEIsUUFBb0IsRUFBRSxLQUFpQixFQUFFLEtBQW9CO1lBQzFGLElBQUcsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUM7Z0JBQ2pDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQzthQUNuQztRQUNGLENBQUM7UUFPUyw0Q0FBa0IsR0FBNUIsVUFBd0QsSUFBb0IsRUFBRSxHQUFXO1lBQ3hGLElBQUcsR0FBRyxLQUFLLFNBQVMsRUFBQztnQkFDcEIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQy9CO2lCQUFJO2dCQUNKLEtBQWdCLFVBQXdCLEVBQXhCLEtBQUEsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBeEIsY0FBd0IsRUFBeEIsSUFBd0IsRUFBQztvQkFBckMsSUFBSSxJQUFJLFNBQUE7b0JBQ1gsSUFBRyxJQUFJLENBQUMsSUFBSSxJQUFJLEdBQUcsRUFBQzt3QkFDbkIsT0FBTyxJQUFJLENBQUM7cUJBQ1o7aUJBQ0Q7YUFDRDtRQUNGLENBQUM7UUFPUyw2Q0FBbUIsR0FBN0IsVUFBeUQsSUFBb0IsRUFBRSxHQUFXO1lBQ3pGLElBQUcsR0FBRyxLQUFLLFNBQVMsRUFBQztnQkFDcEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2hDO2lCQUFJO2dCQUNKLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3JDLE9BQU8sS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFDLENBQUMsSUFBSyxDQUFDLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzdDO1FBQ0YsQ0FBQztRQWpFRDtZQURDLFFBQVEsRUFBRTtxREFDTTtRQUlqQjtZQUhDLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDO2tEQUdEO1FBVFcsZUFBZTtZQUYzQixPQUFPLENBQUMsNEJBQTRCLENBQUM7WUFDckMsaUJBQWlCLEVBQUU7V0FDUCxlQUFlLENBcUUzQjtRQUFELHNCQUFDO0tBckVELEFBcUVDLENBckVvQyxFQUFFLENBQUMsU0FBUyxHQXFFaEQ7SUFyRVksMEJBQWUsa0JBcUUzQixDQUFBO0FBRUQsQ0FBQyxFQTdMTSxVQUFVLEtBQVYsVUFBVSxRQTZMaEI7QUM1TEQsSUFBTyxVQUFVLENBc25CaEI7QUF0bkJELFdBQU8sVUFBVTtJQUVYLElBQUEsS0FBNkIsRUFBRSxDQUFDLFVBQVUsRUFBekMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFBLEVBQUUsSUFBSSxVQUFrQixDQUFDO0lBR2pELElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUM7SUFVaEIsQ0FBQztJQUtGLElBQVksa0JBVVg7SUFWRCxXQUFZLGtCQUFrQjtRQUk3QixxRUFBUyxDQUFBO1FBS1QsaUVBQU8sQ0FBQTtJQUNSLENBQUMsRUFWVyxrQkFBa0IsR0FBbEIsNkJBQWtCLEtBQWxCLDZCQUFrQixRQVU3QjtJQUFBLENBQUM7SUFHRixJQUFNLGFBQWEsR0FBRyxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDO0lBS3ZGLElBQUssU0FVSjtJQVZELFdBQUssU0FBUztRQUliLDZDQUFNLENBQUE7UUFLTix5Q0FBSSxDQUFBO0lBQ0wsQ0FBQyxFQVZJLFNBQVMsS0FBVCxTQUFTLFFBVWI7SUFJRDtRQUFrQyxnQ0FBa0I7UUFBcEQ7WUFBQSxxRUFna0JDO1lBeGpCQSxlQUFTLEdBQVUsQ0FBQyxDQUFDO1lBS3JCLGdCQUFVLEdBQVcsSUFBSSxDQUFDO1lBSzFCLGlCQUFXLEdBQVcsSUFBSSxDQUFDO1lBS3hCLGlCQUFXLEdBQVcsSUFBSSxDQUFDO1lBSzlCLGNBQVEsR0FBTyxJQUFJLENBQUM7WUFLcEIsa0JBQVksR0FBcUIsSUFBSSxDQUFDO1lBS3RDLGdCQUFVLEdBQWEsU0FBUyxDQUFDLElBQUksQ0FBQztZQUt0QyxvQkFBYyxHQUFVLENBQUMsQ0FBQztZQUsxQixtQkFBYSxHQUFVLENBQUMsQ0FBQztZQUt6QixnQkFBVSxHQUFhLElBQUksQ0FBQztZQXFjNUIscUJBQWUsR0FBeUIsRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDO1lBaUJ2RSxxQkFBZSxHQUEwQixFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQzs7UUFxRG5GLENBQUM7UUF6Z0JHLCtCQUFRLEdBQVI7WUFDSSxpQkFBTSxRQUFRLFdBQUUsQ0FBQztZQUVqQixJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzNFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDaEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMseUJBQXlCLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNsRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDdkUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDZCxDQUFDO1FBRUQsZ0NBQVMsR0FBVDtZQUNGLGlCQUFNLFNBQVMsV0FBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLENBQUM7UUFLRCxtQ0FBWSxHQUFaO1lBQ0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RCLENBQUM7UUFLRCxvQ0FBYSxHQUFiO1lBRUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RCLENBQUM7UUFLRCxvQ0FBYSxHQUFiO1lBRUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RCLENBQUM7UUFLTyx1Q0FBZ0IsR0FBeEI7WUFDQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFFckIsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDO1lBRWpDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxDQUFDO1lBRTNDLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFDO2dCQUNwQyxJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7YUFDdkI7WUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM5QyxDQUFDO1FBT0QscUNBQWMsR0FBZCxVQUFlLGFBQW9CLEVBQUUsWUFBbUI7WUFDdkQsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQ25DLElBQUksQ0FBQyxjQUFjLEdBQUcsYUFBYSxDQUFDO1lBQ3BDLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDO1FBQ25DLENBQUM7UUFLTyx3Q0FBaUIsR0FBekI7WUFDQyxJQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxDQUFDLElBQUksRUFBQztnQkFDckMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO2FBQ3JCO1FBQ0YsQ0FBQztRQUtELG9DQUFhLEdBQWI7WUFFTyxpQkFBTSxhQUFhLFdBQUUsQ0FBQztRQUM3QixDQUFDO1FBS0QsK0JBQVEsR0FBUjtZQUNDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBRXhCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN0QixDQUFDO1FBS1Msd0NBQWlCLEdBQTNCO1lBSUMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7WUFDdkMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFakMsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBRSxDQUFDO1FBQ3hDLENBQUM7UUFLUyx1Q0FBZ0IsR0FBMUI7WUFDQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxFQUNWLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUNqQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLEVBQUUsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxFQUNsRCxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDZixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7WUFDVixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7WUFDVixDQUFDLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQztZQUNkLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDO1lBQ2QsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUM1QixLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2IsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNiLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkIsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUViLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3pCLENBQUM7UUFLRCx1Q0FBZ0IsR0FBaEI7WUFDQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDckIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3JCLENBQUM7UUFLUyxvQ0FBYSxHQUF2QjtZQUVDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxDQUFDO1FBRXhDLENBQUM7UUFLUyxxQ0FBYyxHQUF4QjtZQUNDLElBQUcsSUFBSSxDQUFDLFVBQVUsRUFBQztnQkFHVCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFFeEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUVwQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQzthQUMzQjtRQUNSLENBQUM7UUFNRCwrQ0FBd0IsR0FBeEIsVUFBeUIsSUFBVztZQUNuQyxLQUFjLFVBQWlCLEVBQWpCLEtBQUEsSUFBSSxDQUFDLFlBQVksRUFBakIsY0FBaUIsRUFBakIsSUFBaUIsRUFBQztnQkFBNUIsSUFBSSxFQUFFLFNBQUE7Z0JBQ1QsSUFBRyxFQUFFLENBQUMsSUFBSSxJQUFJLElBQUksRUFBQztvQkFDbEIsT0FBTyxLQUFLLENBQUM7aUJBQ2I7YUFDRDtZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2IsQ0FBQztRQU9ELHlDQUFrQixHQUFsQixVQUFtQixLQUFxQixFQUFFLFNBQWdCO1lBQ25ELElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBRWhCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDdEMsSUFBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFDO2dCQUM1QyxNQUFNLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQztnQkFDakMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzlCLFNBQVMsQ0FBQyxhQUFhLElBQUksU0FBUyxDQUFDO2dCQUNyQyxJQUFHLFNBQVMsRUFBQztvQkFDWixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztpQkFDL0Q7YUFDRDtpQkFBSTtnQkFDSyxJQUFHLFNBQVMsRUFBQztvQkFDVCxNQUFNLENBQUMsS0FBSyxDQUFDLDBCQUEwQixHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDekQ7cUJBQUk7b0JBQ2IsRUFBRSxDQUFDLEtBQUssQ0FBQywwQkFBMEIsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2xEO2FBQ0s7WUFDUCxPQUFPLE1BQU0sQ0FBQztRQUNmLENBQUM7UUFLRSw4QkFBTyxHQUFQO1lBQ0YsSUFBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUM7Z0JBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxHQUFHLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUN4RDtZQUNLLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN6QixDQUFDO1FBS00sc0NBQWUsR0FBekI7WUFDQyxJQUFHLElBQUksQ0FBQyxXQUFXLEVBQUM7Z0JBSVYsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztnQkFFekMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBRSxhQUFhLENBQUUsQ0FBQztnQkFDdEMsU0FBUyxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7Z0JBQzVCLElBQUcsU0FBUyxFQUFDO29CQUNaLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO2lCQUMvRDtnQkFFUSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFFaEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7YUFDNUI7UUFDUixDQUFDO1FBS1Msa0NBQVcsR0FBckI7WUFDQyxJQUFHLElBQUksQ0FBQyxXQUFXLEVBQUM7Z0JBSW5CLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7Z0JBRWhDLFNBQVMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztnQkFDOUMsU0FBUyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO2dCQUU1QyxTQUFTLENBQUMsZUFBZSxFQUFFLENBQUM7Z0JBRTVCLElBQUcsSUFBSSxDQUFDLFVBQVUsS0FBSyxTQUFTLENBQUMsSUFBSSxFQUFDO29CQUNyQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztpQkFDekI7cUJBQUk7b0JBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQzVDO2dCQUVELElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2FBQ3pCO1FBQ0YsQ0FBQztRQUtTLG1DQUFZLEdBQXRCO1lBRUMsSUFBRyxJQUFJLENBQUMsV0FBVyxFQUFDO2dCQUduQixJQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxDQUFDLElBQUksRUFBQztvQkFDckMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7aUJBQ3hCO2dCQUVELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUc3QyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQzthQUN6QjtRQUNGLENBQUM7UUFFRSx1Q0FBZ0IsR0FBaEI7WUFDRixJQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxDQUFDLElBQUksRUFBQztnQkFDckMsSUFBSSxpQkFBaUIsRUFBRTtvQkFDdEIsSUFBSSxDQUFDLDBCQUEwQixFQUFFLENBQUM7aUJBQ2xDO3FCQUFNO29CQUNOLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO2lCQUNqQzthQUNEO2lCQUFJO2dCQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ2xEO1FBQ0MsQ0FBQztRQUVELGdEQUF5QixHQUF6QjtZQUVGLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDMUIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUM1QixJQUFJLEtBQUssR0FBRyxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUczQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNwQyxJQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBQyxFQUNsQixDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUM5RCxFQUFFLEdBQUcsT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsR0FBRyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFdkMsSUFBSSxFQUFFLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQzVCLEVBQUUsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQVFqQyxJQUFJLGFBQWEsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRTdELElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNkLElBQUksYUFBYSxHQUFHLFNBQVMsQ0FBQyxhQUFhLENBQUM7WUFDNUMsSUFBSSxhQUFhLEVBQUU7Z0JBRWYsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDekIsS0FBSyxJQUFJLGFBQWEsQ0FBQztnQkFFdkIsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDekIsS0FBSyxJQUFJLGFBQWEsQ0FBQztnQkFFdkIsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDekIsS0FBSyxJQUFJLGFBQWEsQ0FBQztnQkFFdkIsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQzthQUM1QjtpQkFBTTtnQkFDSCxJQUFJLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUM1QixFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFDeEIsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQ3hCLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUt6QixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQzVCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQzlCLEtBQUssSUFBSSxhQUFhLENBQUM7Z0JBRXZCLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDNUIsS0FBSyxDQUFDLEtBQUssR0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDOUIsS0FBSyxJQUFJLGFBQWEsQ0FBQztnQkFFdkIsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUM1QixLQUFLLENBQUMsS0FBSyxHQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUM5QixLQUFLLElBQUksYUFBYSxDQUFDO2dCQUV2QixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQzVCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7YUFDakM7UUFDTCxDQUFDO1FBRUQsaURBQTBCLEdBQTFCO1lBRUYsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUMxQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQzVCLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksYUFBYSxHQUFHLFNBQVMsQ0FBQyxhQUFhLENBQUM7WUFFNUMsSUFBSSxFQUFFLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUNiLEVBQUUsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQ2IsRUFBRSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDYixFQUFFLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRWxCLElBQUksS0FBSyxHQUFXLENBQUMsQ0FBQztZQUV0QixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3BCLEtBQUssSUFBSSxhQUFhLENBQUM7WUFFdkIsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNsQixLQUFLLENBQUMsS0FBSyxHQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNwQixLQUFLLElBQUksYUFBYSxDQUFDO1lBRXZCLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDbEIsS0FBSyxDQUFDLEtBQUssR0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEIsS0FBSyxJQUFJLGFBQWEsQ0FBQztZQUV2QixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLENBQUM7UUFTTSx5Q0FBa0IsR0FBNUIsVUFBd0QsSUFBb0IsRUFBRSxHQUFXO1lBQ3hGLElBQUcsR0FBRyxLQUFLLFNBQVMsRUFBQztnQkFDcEIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQy9CO2lCQUFJO2dCQUNKLEtBQWdCLFVBQXdCLEVBQXhCLEtBQUEsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBeEIsY0FBd0IsRUFBeEIsSUFBd0IsRUFBQztvQkFBckMsSUFBSSxJQUFJLFNBQUE7b0JBQ1gsSUFBRyxJQUFJLENBQUMsSUFBSSxJQUFJLEdBQUcsRUFBQzt3QkFDbkIsT0FBTyxJQUFJLENBQUM7cUJBQ1o7aUJBQ0Q7YUFDRDtRQUNGLENBQUM7UUFPUywwQ0FBbUIsR0FBN0IsVUFBeUQsSUFBb0IsRUFBRSxHQUFXO1lBQ3pGLElBQUcsR0FBRyxLQUFLLFNBQVMsRUFBQztnQkFDcEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2hDO2lCQUFJO2dCQUNKLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3JDLE9BQU8sS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFDLENBQUMsSUFBSyxDQUFDLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzdDO1FBQ0YsQ0FBQztRQUlELHdDQUFpQixHQUFqQjtZQUVDLGlCQUFNLGlCQUFpQixXQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2pCLENBQUM7UUFFRCxzQ0FBZSxHQUFmO1lBQ08sSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzdCLENBQUM7UUFFRSxzQ0FBZSxHQUFmO1lBRUksSUFBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUM7Z0JBRW5CLE9BQU8sSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO2FBQy9CO1lBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEQsQ0FBQztRQVVELHNCQUFJLHdDQUFjO2lCQUFsQjtnQkFDQyxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7WUFDN0IsQ0FBQztpQkFDRCxVQUFtQixLQUEwQjtnQkFDNUMsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLEtBQUs7b0JBQUUsT0FBTztnQkFDM0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFNUIsSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDaEQsQ0FBQzs7O1dBUEE7UUFlRCxzQkFBSSx3Q0FBYztpQkFBbEI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQzdCLENBQUM7aUJBQ0QsVUFBbUIsS0FBMEI7Z0JBQzVDLElBQUksSUFBSSxDQUFDLGVBQWUsS0FBSyxLQUFLO29CQUFFLE9BQU87Z0JBQzNDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO2dCQUM3QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDN0IsQ0FBQzs7O1dBTEE7UUFPRSxrQ0FBVyxHQUFYLFVBQWEsS0FBSyxFQUFFLFFBQVE7WUFDOUIsSUFBSSxXQUFXLEdBQUcsaUJBQU0sV0FBVyxZQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztZQUVyRCxJQUFJLElBQUksQ0FBQyxlQUFlLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUztnQkFDMUQsSUFBSSxDQUFDLGVBQWUsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsRUFBRTtnQkFDMUQsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ3BEO1lBRUQsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBRWQsT0FBTyxXQUFXLENBQUM7UUFDdkIsQ0FBQztRQUVELHVDQUFnQixHQUFoQixVQUFrQixLQUFLO1lBQ25CLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ2pCLElBQUksSUFBSSxDQUFDLGVBQWUsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTO29CQUMxRCxJQUFJLENBQUMsZUFBZSxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLG1CQUFtQixFQUFFO29CQUN2RCxPQUFPO2lCQUNWO2FBQ0o7WUFFRCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDcEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3ZDLElBQUksUUFBUSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDNUIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQzNDO1FBQ0wsQ0FBQztRQUVELCtDQUF3QixHQUF4QixVQUEwQixRQUFRO1lBQzlCLFFBQVEsQ0FBQyxRQUFRLENBQ2IsSUFBSSxFQUNKLEdBQUcsQ0FBQyxjQUFjLEVBQ2xCLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFDMUMsR0FBRyxDQUFDLGNBQWMsRUFDbEIsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUM3QyxDQUFDO1FBQ04sQ0FBQztRQXRqQko7WUFOQyxRQUFRLENBQUM7Z0JBQ1QsVUFBVSxFQUFHLElBQUk7Z0JBQ2pCLFlBQVksRUFBRyxLQUFLO2dCQUNwQixRQUFRLEVBQUcsSUFBSTtnQkFDZixPQUFPLEVBQUcsY0FBYzthQUN4QixDQUFDO3VEQUNtQjtRQXdmckI7WUFMQyxRQUFRLENBQUM7Z0JBQ1QsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLElBQUksRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVc7Z0JBQzFCLE9BQU8sRUFBRSxNQUFNLElBQUksd0NBQXdDO2FBQzNELENBQUM7MERBR0Q7UUFlRDtZQUxDLFFBQVEsQ0FBQztnQkFDVCxVQUFVLEVBQUUsS0FBSztnQkFDWCxJQUFJLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXO2dCQUMxQixPQUFPLEVBQUUsTUFBTSxJQUFJLHdDQUF3QzthQUNqRSxDQUFDOzBEQUdEO1FBbmhCVyxZQUFZO1lBRnhCLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQztZQUNsQyxJQUFJLENBQUMsNkNBQTZDLENBQUM7V0FDdkMsWUFBWSxDQWdrQnhCO1FBQUQsbUJBQUM7S0Foa0JELEFBZ2tCQyxDQWhrQmlDLEVBQUUsQ0FBQyxlQUFlLEdBZ2tCbkQ7SUFoa0JZLHVCQUFZLGVBZ2tCeEIsQ0FBQTtBQUVELENBQUMsRUF0bkJNLFVBQVUsS0FBVixVQUFVLFFBc25CaEI7QUN0bkJELE1BQU0sQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO0FDRC9CLElBQU8sVUFBVSxDQW9CaEI7QUFwQkQsV0FBTyxVQUFVO0lBRWpCO1FBQUE7UUFnQkEsQ0FBQztRQVpPLG9CQUFjLEdBQXJCLFVBQTBDLElBQVc7WUFDcEQsT0FBTyxJQUFJLE9BQU8sQ0FBSSxVQUFDLE9BQU87Z0JBQzdCLEVBQUUsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUUsSUFBSSxDQUFFLEVBQUUsVUFBVSxHQUFHLEVBQUUsS0FBTztvQkFDdkQsSUFBRyxDQUFDLEdBQUcsSUFBSSxLQUFLLEVBQUM7d0JBQ2hCLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDZjt5QkFBSTt3QkFDSixFQUFFLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxDQUFDO3dCQUN0QyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2Q7Z0JBQ0YsQ0FBQyxDQUFDLENBQUM7WUFDSixDQUFDLENBQUMsQ0FBQztRQUNKLENBQUM7UUFDRixZQUFDO0lBQUQsQ0FoQkEsQUFnQkMsSUFBQTtJQWhCWSxnQkFBSyxRQWdCakIsQ0FBQTtBQUVELENBQUMsRUFwQk0sVUFBVSxLQUFWLFVBQVUsUUFvQmhCO0FDcEJELElBQU8sVUFBVSxDQTZDaEI7QUE3Q0QsV0FBTyxVQUFVOztJQUtOLGVBQUksR0FBRztRQUlqQixPQUFPO1lBQ04sR0FBQyxvQkFBb0IsSUFBSSxzQ0FBc0M7WUFDL0QsR0FBQyxxQkFBcUIsSUFBSSxzQ0FBc0M7WUFDaEUsR0FBQyxvQkFBb0IsSUFBSSxzQ0FBc0M7WUFDL0QsR0FBQyxtQkFBbUIsSUFBSSxzQ0FBc0M7WUFDOUQsR0FBQyxvQkFBb0IsSUFBSSxzQ0FBc0M7WUFDL0QsR0FBQyxlQUFlLElBQUksc0NBQXNDO1lBQzFELEdBQUMsY0FBYyxJQUFJLHNDQUFzQztZQUN6RCxHQUFDLGdCQUFnQixJQUFJLHNDQUFzQztZQUMzRCxHQUFDLHNCQUFzQixJQUFJLHNDQUFzQztZQUNqRSxHQUFDLDBCQUEwQixJQUFJLHNDQUFzQztZQUNyRSxHQUFDLGlCQUFpQixJQUFJLHNDQUFzQztZQUM1RCxHQUFDLGtCQUFrQixJQUFJLHNDQUFzQztZQUM3RCxHQUFDLDBCQUEwQixJQUFJLHNDQUFzQztlQUNyRTtRQUtELFNBQVM7WUFDUixHQUFDLG9CQUFvQixJQUFJLHNDQUFzQztZQUMvRCxHQUFDLHFCQUFxQixJQUFJLHNDQUFzQztZQUNoRSxHQUFDLG9CQUFvQixJQUFJLHNDQUFzQztZQUMvRCxHQUFDLG1CQUFtQixJQUFJLHNDQUFzQztZQUM5RCxHQUFDLG9CQUFvQixJQUFJLHNDQUFzQztZQUMvRCxHQUFDLGVBQWUsSUFBSSxzQ0FBc0M7WUFDMUQsR0FBQyxjQUFjLElBQUksc0NBQXNDO1lBQ3pELEdBQUMsZ0JBQWdCLElBQUksc0NBQXNDO1lBQzNELEdBQUMsc0JBQXNCLElBQUksc0NBQXNDO1lBQ2pFLEdBQUMsMEJBQTBCLElBQUksc0NBQXNDO1lBQ3JFLEdBQUMsaUJBQWlCLElBQUksc0NBQXNDO1lBQzVELEdBQUMsa0JBQWtCLElBQUksc0NBQXNDO1lBQzdELEdBQUMsMEJBQTBCLElBQUksc0NBQXNDO2VBQ3JFO0tBQ0QsQ0FBQTtBQUVELENBQUMsRUE3Q00sVUFBVSxLQUFWLFVBQVUsUUE2Q2hCO0FDNUNELElBQU8sVUFBVSxDQW9VaEI7QUFwVUQsV0FBTyxVQUFVO0lBRVgsSUFBQSxLQUE2QixFQUFFLENBQUMsVUFBVSxFQUF6QyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxJQUFJLFVBQWtCLENBQUM7SUFHakQsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQztJQUVqQixJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7SUFJN0I7UUFBaUMsK0JBQWU7UUFBaEQ7WUFBQSxxRUF1VEM7WUFyVEcsVUFBSSxHQUFVLE9BQU8sQ0FBQztZQUt6QixnQkFBVSxHQUFhLFdBQUEsU0FBUyxDQUFDLE1BQU0sQ0FBQztZQXFCeEMsZUFBUyxHQUFrQixJQUFJLFdBQUEsY0FBYyxDQUM1QyxJQUFJLEVBQUUsV0FBQSxZQUFZLENBQUMsT0FBTyxFQUMxQixZQUFZLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBaUJqRSxhQUFPLEdBQWMsQ0FBRSxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBRSxDQUFDO1lBR3hDLG1CQUFhLEdBQVcsS0FBSyxDQUFDO1lBcURuQixrQkFBWSxHQUFVLENBQUMsQ0FBQzs7UUFnTnBDLENBQUM7UUEzU0Esc0JBQUksa0NBQVM7aUJBQWI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3hCLENBQUM7aUJBQ0QsVUFBYyxLQUFlO2dCQUM1QixJQUFHLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSyxFQUFDO29CQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztvQkFDeEIsSUFBRyxLQUFLLElBQUksV0FBQSxTQUFTLENBQUMsS0FBSyxFQUFDO3dCQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLFdBQUEsWUFBWSxDQUFDLE9BQU8sQ0FBQzt3QkFDNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO3FCQUNuQzt5QkFBSTt3QkFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7cUJBQ2xDO2lCQUNWO1lBQ0YsQ0FBQzs7O1dBWEE7UUFxQkQsc0JBQUksaUNBQVE7aUJBQVo7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3ZCLENBQUM7aUJBQ0QsVUFBYSxLQUFvQjtnQkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDNUIsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7Z0JBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFFeEMsQ0FBQzs7O1dBUEg7UUFpQkQsc0JBQUkscUNBQVk7aUJBQWhCO2dCQUNDLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUMzQixDQUFDO2lCQUNELFVBQWlCLEtBQWE7Z0JBQzdCLElBQUcsSUFBSSxDQUFDLGFBQWEsSUFBSSxLQUFLLEVBQUM7b0JBQ3JCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO29CQUMzQixJQUFHLEtBQUssRUFBQzt3QkFDTCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztxQkFDN0I7eUJBQUk7d0JBQ0QsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO3FCQUN6QjtpQkFDVjtZQUNGLENBQUM7OztXQVZBO1FBbUJELHNCQUFJLDhCQUFLO2lCQUFUO2dCQUNDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QixDQUFDO2lCQUNELFVBQVUsS0FBYztnQkFDakIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO2dCQUM5QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsQ0FBQzs7O1dBTEE7UUFjRCxzQkFBSSwrQkFBTTtpQkFBVjtnQkFDQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDckIsQ0FBQztpQkFDRCxVQUFXLEtBQWdCO2dCQUNwQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztnQkFDakMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixDQUFDOzs7V0FMQTtRQVlFLDRCQUFNLEdBQU47WUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2hGLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUM1RSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDMUUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMzRSxDQUFDO1FBRUQsK0JBQVMsR0FBVDtZQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hCLENBQUM7UUFFSiw4QkFBUSxHQUFSO1lBQ0MsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFDcEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoQyxDQUFDO1FBRUQsK0JBQVMsR0FBVDtZQUNPLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsQ0FBQztRQUVPLG1DQUFhLEdBQXJCLFVBQXNCLE1BQWM7WUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUNwQyxJQUFHLElBQUksQ0FBQyxhQUFhLEVBQUM7Z0JBQ3JCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2FBQzFCO2lCQUFJO2dCQUNKLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN0QjtRQUNDLENBQUM7UUFFSSx3Q0FBa0IsR0FBMUI7WUFDQyxJQUFHLElBQUksQ0FBQyxhQUFhLEVBQUM7Z0JBQ1osSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQy9CO1FBQ0YsQ0FBQztRQUtVLDBDQUFvQixHQUE1QjtZQUNJLElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsU0FBUyxFQUFDO2dCQUM3QyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDekI7UUFDTCxDQUFDO1FBS0ksb0NBQWMsR0FBdEI7WUFDQyxJQUFHLElBQUksQ0FBQyxPQUFPLEVBQUM7Z0JBQ04sSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7Z0JBQzVCLElBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFDO29CQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLENBQUUsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFFLENBQUM7aUJBQ3JDO2dCQUNELElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsU0FBUyxFQUFDO29CQUM3QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7b0JBQzNDLElBQUcsSUFBSSxFQUFDO3dCQUNKLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7d0JBQzdCLElBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsRUFBRSxFQUFDOzRCQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDOzRCQUMxQixLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFDO2dDQUN2QixJQUFJLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQ3RCLElBQUcsS0FBSyxJQUFJLElBQUksRUFBQztvQ0FDYixNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDO2lDQUM5Qjs2QkFDSjt5QkFDSjt3QkFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7cUJBQzVCO2lCQUNKO3FCQUFLLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFBLFNBQVMsQ0FBQyxNQUFNLEVBQUM7b0JBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztpQkFDM0I7Z0JBQ0QsSUFBRyxJQUFJLENBQUMsYUFBYSxFQUFDO29CQUNsQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztvQkFDN0IsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUM7d0JBQy9DLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDO3FCQUNwQztpQkFDSjtnQkFDRCxJQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLFdBQUEsWUFBWSxDQUFDLFNBQVMsRUFBQztvQkFDN0MsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztpQkFDN0M7cUJBQUk7b0JBQ0QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7aUJBQ2pDO2FBQ0o7UUFDUixDQUFDO1FBRU8sc0NBQWdCLEdBQXhCLFVBQXlCLEtBQWM7WUFFdEMsRUFBRSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFFOUMsT0FBTyxXQUFXLENBQUMsSUFBSSxDQUFDO1FBQ3pCLENBQUM7UUFNTyx3Q0FBa0IsR0FBMUIsVUFBMkIsSUFBaUI7WUFDM0MsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUM7WUFDcEUsSUFBSSxNQUFNLEdBQVksRUFBRSxDQUFDO1lBQ3pCLElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEtBQUssV0FBQSxZQUFZLENBQUMsU0FBUyxFQUFDO2dCQUN4QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBQztvQkFDaEQsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFFeEIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNoRTthQUNKO1lBQ1AsT0FBTyxNQUFNLENBQUM7UUFDZixDQUFDO1FBTU8sc0NBQWdCLEdBQXhCLFVBQXlCLElBQWlCO1lBQ3pDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDO1lBQzlELElBQUksTUFBTSxHQUFhLEVBQUUsQ0FBQztZQUMxQixJQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxLQUFLLFdBQUEsWUFBWSxDQUFDLE9BQU8sRUFBQztnQkFDNUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUM7b0JBQ2hELElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3hCLElBQUcsV0FBVyxFQUFDO3dCQUVYLEVBQUUsQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUMxQyxDQUFDLEdBQUcsV0FBVyxDQUFDO3FCQUNuQjtvQkFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQ3hFO2FBQ0o7WUFDUCxPQUFPLE1BQU0sQ0FBQztRQUNmLENBQUM7UUFFVSw0Q0FBc0IsR0FBOUIsVUFBK0IsSUFBbUQ7WUFBbkQscUJBQUEsRUFBQSxPQUFvQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDO1lBQ3BGLElBQUcsSUFBSSxFQUFDO2dCQUdQLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLElBQUksUUFBUSxFQUFFO29CQUNiLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFFO3dCQUNoRSxJQUFHLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEtBQUssV0FBQSxZQUFZLENBQUMsT0FBTyxFQUFDOzRCQUM3QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDOzRCQUNuRSxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ3pDLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFBLFNBQVMsQ0FBQyxNQUFNLEVBQUM7Z0NBQ25DLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NkJBQzVEO2lDQUFJO2dDQUNELElBQUksT0FBTyxHQUFZLEVBQUUsQ0FBQztnQ0FDMUIsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBQztvQ0FDekMsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO29DQUNsQixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQ0FDcEM7Z0NBQ0QsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxJQUFJLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQ25GLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7NkJBQzFEO3lCQUVuQjs2QkFBSTs0QkFDSixJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO3lCQUNwRTtxQkFFRDtpQkFDRDthQUNEO1FBQ0MsQ0FBQztRQUVJLDZDQUF1QixHQUEvQixVQUFnQyxJQUFtRDtZQUFuRCxxQkFBQSxFQUFBLE9BQW9CLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUM7WUFDbEYsSUFBRyxJQUFJLEVBQUM7Z0JBRVAsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsSUFBSSxRQUFRLEVBQUU7b0JBQ2IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQ2hFLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksS0FBSyxXQUFBLFlBQVksQ0FBQyxTQUFTLEVBQUM7NEJBQ2pFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ25FLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDO2dDQUMzQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRO2dDQUM3QixJQUFJLEVBQUUsR0FBRyxDQUFDLGVBQWU7Z0NBQ3pCLEdBQUcsRUFBRSxDQUFDO2dDQUNOLFNBQVMsRUFBRSxJQUFJOzZCQUNmLEVBQUUsQ0FBQyxDQUFDLENBQUM7eUJBRU47NkJBQUk7NEJBQ0osSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQzs0QkFDcEUsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7eUJBQ3RCO3FCQUVEO2lCQUNEO2FBQ0Q7UUFDRixDQUFDO1FBRU8sOENBQXdCLEdBQWhDLFVBQWlDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUM3RSxJQUFHLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFDO2dCQUU3QixJQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxFQUFDO29CQUV4QixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO29CQUNoQyxJQUFJLFNBQVMsR0FBRyxTQUFTLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDbkQsSUFBSSxTQUFTLEVBQUU7d0JBQ2QsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUMzQyxJQUFJLElBQUksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO3dCQUN6QixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO3dCQUNwQyxJQUFJLGFBQWEsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO3dCQUM1QyxLQUFLLElBQUksQ0FBQyxHQUFHLFdBQVcsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTs0QkFDdEYsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO3lCQUNoRDtxQkFDRDtpQkFDRDthQUNLO1FBQ1IsQ0FBQztRQS9TRDtZQUhDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFBLFNBQVMsQ0FBQzthQUN4QixDQUFDO3VEQUNzQztRQUt4QztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFBLFNBQVMsQ0FBQztnQkFDeEIsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQztvREFHRDtRQWNEO1lBREMsUUFBUSxDQUFDLFdBQUEsY0FBYyxDQUFDO3NEQUd3QztRQUtqRTtZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsV0FBQSxjQUFjO2dCQUNyQixPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDO21EQUdEO1FBVUQ7WUFESSxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7b0RBQ2dCO1FBR3hDO1lBREMsUUFBUSxFQUFFOzBEQUNtQjtRQUk5QjtZQUhDLFFBQVEsQ0FBQztnQkFDSCxPQUFPLEVBQUcsU0FBUzthQUN6QixDQUFDO3VEQUdEO1FBbUJEO1lBUEMsUUFBUSxDQUFDO2dCQUNULE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLFdBQUEsU0FBUyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFBLFlBQVksQ0FBQyxPQUFPLENBQUM7Z0JBQzNGLENBQUM7Z0JBQ0QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxLQUFLO2dCQUNmLE9BQU8sRUFBRyxLQUFLO2FBQ2YsQ0FBQztnREFHRDtRQWNEO1lBUEMsUUFBUSxDQUFDO2dCQUNULE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLFdBQUEsU0FBUyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFBLFlBQVksQ0FBQyxTQUFTLENBQUM7Z0JBQzVGLENBQUM7Z0JBQ0QsSUFBSSxFQUFHLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztnQkFDakIsT0FBTyxFQUFHLGFBQWE7YUFDdkIsQ0FBQztpREFHRDtRQTdGVyxXQUFXO1lBRnZCLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQztZQUNqQyxJQUFJLENBQUMsNENBQTRDLENBQUM7V0FDdEMsV0FBVyxDQXVUdkI7UUFBRCxrQkFBQztLQXZURCxBQXVUQyxDQXZUZ0MsV0FBQSxlQUFlLEdBdVQvQztJQXZUWSxzQkFBVyxjQXVUdkIsQ0FBQTtBQUVELENBQUMsRUFwVU0sVUFBVSxLQUFWLFVBQVUsUUFvVWhCO0FDcFVELElBQU8sVUFBVSxDQXlSaEI7QUF6UkQsV0FBTyxVQUFVO0lBRVgsSUFBQSxLQUE2QixFQUFFLENBQUMsVUFBVSxFQUF6QyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxJQUFJLFVBQWtCLENBQUM7SUFHakQsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQztJQUtqQixJQUFNLEVBQUUsR0FBRTtRQUNULEdBQUcsRUFBRyxPQUFPO1FBRWIsU0FBUyxFQUFHLElBQUk7UUFDaEIsVUFBVSxFQUFHLFdBQUEsWUFBWSxDQUFDLE9BQU87UUFDakMsWUFBWSxFQUFHLFlBQVk7UUFDM0IscUJBQXFCLEVBQUcsS0FBSztRQUM3QixXQUFXLEVBQUcsVUFBVTtRQUN4QixZQUFZLEVBQUcsWUFBWTtRQUMzQixxQkFBcUIsRUFBRyxLQUFLO1FBQzdCLFdBQVcsRUFBRyxVQUFVO1FBRXhCLFVBQVUsRUFBRyxDQUFDO1FBQ2QsTUFBTSxFQUFHLGNBQU0sT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzFCLE9BQU8sRUFBRyxFQUFFLENBQUMsS0FBSztRQUVsQixRQUFRLEVBQUcsQ0FBQztRQUVaLE9BQU8sRUFBRyxVQUFDLEdBQVksRUFBRSxLQUFZLEVBQUUsTUFBYTtZQUNuRCxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLENBQUM7S0FDSixDQUFBO0lBSUQ7UUFBaUMsK0JBQWU7UUFBaEQ7WUFBQSxxRUFvUEM7WUFsUEEsVUFBSSxHQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFLckIsZ0JBQVUsR0FBYSxXQUFBLFNBQVMsQ0FBQyxNQUFNLENBQUM7WUFxQnhDLGVBQVMsR0FBa0IsSUFBSSxXQUFBLGNBQWMsQ0FDNUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUMsVUFBVSxFQUMzQixFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxFQUFFLENBQUMsV0FBVyxFQUN6RCxFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUM7WUFnQjVELGFBQU8sR0FBMEIsQ0FBRSxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUUsQ0FBQztZQW1DdEMsa0JBQVksR0FBVSxDQUFDLENBQUM7O1FBa0twQyxDQUFDO1FBeE9BLHNCQUFJLGtDQUFTO2lCQUFiO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUN4QixDQUFDO2lCQUNELFVBQWMsS0FBZTtnQkFDNUIsSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUssRUFBQztvQkFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7b0JBQ3hCLElBQUcsS0FBSyxJQUFJLFdBQUEsU0FBUyxDQUFDLEtBQUssRUFBQzt3QkFDZixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFBLFlBQVksQ0FBQyxPQUFPLENBQUM7d0JBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztxQkFDbkM7eUJBQUk7d0JBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO3FCQUNsQztpQkFDVjtZQUNGLENBQUM7OztXQVhBO1FBc0JELHNCQUFJLGlDQUFRO2lCQUFaO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUN2QixDQUFDO2lCQUNELFVBQWEsS0FBb0I7Z0JBQ2hDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2dCQUN2QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ3hDLENBQUM7OztXQU5IO1FBa0JELHNCQUFJLDhCQUFLO2lCQUFUO2dCQUNDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QixDQUFDO2lCQUNELFVBQVUsTUFBMkI7Z0JBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDO2dCQUN6QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsQ0FBQzs7O1dBSkE7UUFhRCxzQkFBSSwrQkFBTTtpQkFBVjtnQkFDQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDckIsQ0FBQztpQkFDRCxVQUFXLEtBQTRCO2dCQUN0QyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3ZCLENBQUM7OztXQUpBO1FBV0UsNEJBQU0sR0FBTjtZQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN0RSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDaEYsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMseUJBQXlCLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzVFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN4RSxDQUFDO1FBRUQsK0JBQVMsR0FBVDtZQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hCLENBQUM7UUFFSiw4QkFBUSxHQUFSO1lBQ08sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoQyxDQUFDO1FBRUQsK0JBQVMsR0FBVDtZQUNPLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsQ0FBQztRQUVPLG1DQUFhLEdBQXJCLFVBQXNCLE1BQWM7WUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUNwQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkIsQ0FBQztRQUtVLDBDQUFvQixHQUE1QjtZQUNJLElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsU0FBUyxFQUFDO2dCQUM3QyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDekI7UUFDTCxDQUFDO1FBS0ksb0NBQWMsR0FBdEI7WUFDQyxJQUFHLElBQUksQ0FBQyxPQUFPLEVBQUM7Z0JBQ04sSUFBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUM7b0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBRSxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUUsQ0FBQztpQkFDbEM7Z0JBQ0QsSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFBLFlBQVksQ0FBQyxTQUFTLEVBQUM7b0JBQzdDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztvQkFDM0MsSUFBRyxJQUFJLEVBQUM7d0JBQ0osSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQzt3QkFDN0IsSUFBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxFQUFFLEVBQUM7NEJBQ3hCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7NEJBQzFCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUM7Z0NBQ3ZCLElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDdEIsSUFBRyxLQUFLLElBQUksSUFBSSxFQUFDO29DQUNiLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUM7aUNBQzNCOzZCQUNKO3lCQUNKO3dCQUNELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztxQkFDNUI7aUJBQ0o7cUJBQUssSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLFdBQUEsU0FBUyxDQUFDLE1BQU0sRUFBQztvQkFDekMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2lCQUMzQjtnQkFDRCxJQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLFdBQUEsWUFBWSxDQUFDLFNBQVMsRUFBQztvQkFDN0MsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztpQkFDN0M7cUJBQUk7b0JBQ0QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7aUJBQ2pDO2FBQ1Y7UUFDRixDQUFDO1FBRVUsNENBQXNCLEdBQTlCLFVBQWdDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUNyRixJQUFHLElBQUksRUFBQztnQkFHUCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLFFBQVEsRUFBRTtvQkFDYixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTt3QkFDaEUsSUFBRyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxLQUFLLFdBQUEsWUFBWSxDQUFDLE9BQU8sRUFBQzs0QkFDN0MsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQzs0QkFDbkUsSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLFdBQUEsU0FBUyxDQUFDLE1BQU0sRUFBQztnQ0FDbkMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NkJBQ2xFO2lDQUFJO2dDQUNELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7Z0NBQzFCLElBQUksT0FBTyxHQUFZLEVBQUUsQ0FBQztnQ0FDMUIsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQztnQ0FDekIsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBQztvQ0FDekMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7aUNBQzFEO2dDQUNELFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxZQUFZLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQ0FDN0YsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQzs2QkFDMUQ7eUJBRW5COzZCQUFJOzRCQUNKLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7eUJBQ3BFO3FCQUVEO2lCQUNEO2FBQ0Q7UUFDQyxDQUFDO1FBTUksd0NBQWtCLEdBQTFCLFVBQTJCLElBQWlCO1lBQzNDLElBQUksTUFBTSxHQUFZLEVBQUUsQ0FBQztZQUN6QixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1lBQ3ZCLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUM7WUFDekIsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBQztnQkFDdkIsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzlEO1lBQ1AsT0FBTyxNQUFNLENBQUM7UUFDZixDQUFDO1FBRU8sNkNBQXVCLEdBQS9CLFVBQWdDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUNsRixJQUFHLElBQUksRUFBQztnQkFFUCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLFFBQVEsRUFBRTtvQkFDYixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTt3QkFDaEUsSUFBRyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxLQUFLLFdBQUEsWUFBWSxDQUFDLFNBQVMsRUFBQzs0QkFDakUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQzs0QkFDbkUsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7Z0NBQzNDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVE7Z0NBQzdCLElBQUksRUFBRSxHQUFHLENBQUMsaUJBQWlCO2dDQUMzQixHQUFHLEVBQUUsRUFBRSxDQUFDLFFBQVE7NkJBQ2hCLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3lCQUVoQjs2QkFBSTs0QkFDSixJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDOzRCQUNwRSxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQzt5QkFDdEI7cUJBRUQ7aUJBQ0Q7YUFDRDtRQUNGLENBQUM7UUFFTyw4Q0FBd0IsR0FBaEMsVUFBaUMsSUFBbUQ7WUFBbkQscUJBQUEsRUFBQSxPQUFvQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDO1lBQzdFLElBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUM7Z0JBRTdCLElBQUcsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLEVBQUM7b0JBRXhCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7b0JBQ2hDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDM0MsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztvQkFDekIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDcEMsSUFBSSxhQUFhLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQztvQkFDNUMsSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzNDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQzNCLElBQUksU0FBUyxHQUFHLGFBQWEsR0FBRyxDQUFDLEdBQUcsV0FBVyxDQUFDO3dCQUNoRCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQzt3QkFDaEMsSUFBRyxTQUFTLElBQUksSUFBSSxFQUFDOzRCQUNwQixTQUFTLEdBQUcsQ0FBQyxDQUFDO3lCQUNkO3dCQUNELEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsRUFBRSxFQUFDOzRCQUNuQyxLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUM7eUJBQzdDO3FCQUNEO2lCQUNEO2FBQ0s7UUFDUixDQUFDO1FBNU9EO1lBSEMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQUEsU0FBUyxDQUFDO2FBQ3hCLENBQUM7dURBQ3NDO1FBS3hDO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQUEsU0FBUyxDQUFDO2dCQUN4QixPQUFPLEVBQUcsS0FBSzthQUNmLENBQUM7b0RBR0Q7UUFjRDtZQURDLFFBQVEsQ0FBQyxXQUFBLGNBQWMsQ0FBQztzREFJbUM7UUFLNUQ7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLFdBQUEsY0FBYztnQkFDckIsT0FBTyxFQUFHLEtBQUs7YUFDZixDQUFDO21EQUdEO1FBU0Q7WUFESSxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7b0RBQ3VCO1FBU2pEO1lBUEMsUUFBUSxDQUFDO2dCQUNULE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLFdBQUEsU0FBUyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFBLFlBQVksQ0FBQyxPQUFPLENBQUM7Z0JBQzNGLENBQUM7Z0JBQ0QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxPQUFPO2dCQUNqQixPQUFPLEVBQUcsR0FBRzthQUNiLENBQUM7Z0RBR0Q7UUFhRDtZQVBDLFFBQVEsQ0FBQztnQkFDVCxPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFBLFNBQVMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsU0FBUyxDQUFDO2dCQUM1RixDQUFDO2dCQUNELElBQUksRUFBRyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7Z0JBQ25CLE9BQU8sRUFBRyxXQUFXO2FBQ3JCLENBQUM7aURBR0Q7UUF6RVcsV0FBVztZQUZ2QixPQUFPLENBQUMsd0JBQXdCLENBQUM7WUFDakMsSUFBSSxDQUFDLDRDQUE0QyxDQUFDO1dBQ3RDLFdBQVcsQ0FvUHZCO1FBQUQsa0JBQUM7S0FwUEQsQUFvUEMsQ0FwUGdDLFdBQUEsZUFBZSxHQW9QL0M7SUFwUFksc0JBQVcsY0FvUHZCLENBQUE7QUFFRCxDQUFDLEVBelJNLFVBQVUsS0FBVixVQUFVLFFBeVJoQjtBQ3pSRCxJQUFPLFVBQVUsQ0EySmhCO0FBM0pELFdBQU8sVUFBVTtJQUVYLElBQUEsS0FBNkIsRUFBRSxDQUFDLFVBQVUsRUFBekMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFBLEVBQUUsSUFBSSxVQUFrQixDQUFDO0lBRWpELElBQUssY0FVSjtJQVZELFdBQUssY0FBYztRQUlsQixtREFBSSxDQUFBO1FBS0oscURBQUssQ0FBQTtJQUNOLENBQUMsRUFWSSxjQUFjLEtBQWQsY0FBYyxRQVVsQjtJQUlEO1FBQWlDLCtCQUFlO1FBQWhEO1lBQUEscUVBdUlDO1lBcklBLFVBQUksR0FBVSxPQUFPLENBQUM7WUFHdEIsaUJBQVcsR0FBVSxFQUFFLENBQUM7WUFnQnhCLGdCQUFVLEdBQVUsRUFBRSxDQUFDO1lBa0J2QixnQkFBVSxHQUFrQixjQUFjLENBQUMsSUFBSSxDQUFDO1lBaUJoRCxXQUFLLEdBQVcsS0FBSyxDQUFDO1lBbUJ0QixZQUFNLEdBQVUsQ0FBQyxDQUFDOztRQTREbkIsQ0FBQztRQTlIQSxzQkFBSSxtQ0FBVTtpQkFBZDtnQkFDQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDekIsQ0FBQztpQkFDRCxVQUFlLEtBQVk7Z0JBQzFCLElBQUcsSUFBSSxDQUFDLFdBQVcsSUFBSSxLQUFLLEVBQUM7b0JBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO29CQUN6QixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztpQkFDMUM7WUFDRixDQUFDOzs7V0FQQTtRQWNELHNCQUFJLGtDQUFTO2lCQUFiO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUN4QixDQUFDO2lCQUNELFVBQWMsS0FBWTtnQkFDekIsSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUssRUFBQztvQkFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO29CQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO2lCQUMxQztZQUNGLENBQUM7OztXQVBBO1FBaUJELHNCQUFJLGtDQUFTO2lCQUFiO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUN4QixDQUFDO2lCQUNELFVBQWMsTUFBcUI7Z0JBQ2xDLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxNQUFNLEVBQUM7b0JBQzVCLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDO29CQUN6QixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztpQkFDMUM7WUFDRixDQUFDOzs7V0FQQTtRQWlCRCxzQkFBSSw2QkFBSTtpQkFBUjtnQkFDQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDbkIsQ0FBQztpQkFDRCxVQUFTLEtBQWE7Z0JBQ3JCLElBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxLQUFLLEVBQUM7b0JBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO29CQUNuQixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztpQkFDMUM7WUFDRixDQUFDOzs7V0FQQTtRQWlCRCxzQkFBSSw4QkFBSztpQkFBVDtnQkFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDcEIsQ0FBQztpQkFDRCxVQUFVLEtBQVk7Z0JBQ3JCLElBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUM7b0JBQ3ZCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNwQixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztpQkFDMUM7WUFDRixDQUFDOzs7V0FQQTtRQVNFLDRCQUFNLEdBQU47WUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUUsQ0FBQztRQUVELCtCQUFTLEdBQVQ7WUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QixDQUFDO1FBRUosOEJBQVEsR0FBUjtZQUNPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEMsQ0FBQztRQUVELCtCQUFTLEdBQVQ7WUFDTyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFFTyxtQ0FBYSxHQUFyQixVQUFzQixNQUFjO1lBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDcEMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUMzQyxDQUFDO1FBRVUsNENBQXNCLEdBQTlCLFVBQWdDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUNyRixJQUFHLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFDO2dCQUcxQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLFFBQVEsRUFBRTtvQkFDYixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRTt3QkFDOUQsSUFBRyxJQUFJLENBQUMsT0FBTyxJQUFJLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLFNBQVMsRUFBQzs0QkFDcEUsSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLGNBQWMsQ0FBQyxJQUFJLEVBQUM7Z0NBQ3pDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7NkJBQzdDO2lDQUFJO2dDQUNKLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7NkJBQzlDO3lCQUVEO3FCQUVEO2lCQUNEO2FBQ0Q7UUFDQyxDQUFDO1FBaklKO1lBREMsUUFBUSxFQUFFO3dEQUNhO1FBSXhCO1lBSEMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxLQUFLO2FBQ2YsQ0FBQztxREFHRDtRQVVEO1lBREMsUUFBUSxFQUFFO3VEQUNZO1FBSXZCO1lBSEMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxLQUFLO2FBQ2YsQ0FBQztvREFHRDtRQVlEO1lBSEMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQzthQUM5QixDQUFDO3VEQUM4QztRQUtoRDtZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQzlCLE9BQU8sRUFBRyxLQUFLO2FBQ2YsQ0FBQztvREFHRDtRQVVEO1lBREMsUUFBUSxFQUFFO2tEQUNXO1FBT3RCO1lBTkMsUUFBUSxDQUFDO2dCQUNULE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLGNBQWMsQ0FBQyxJQUFJLENBQUM7Z0JBQy9DLENBQUM7Z0JBQ0QsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQzsrQ0FHRDtRQVVEO1lBREMsUUFBUSxFQUFFO21EQUNPO1FBT2xCO1lBTkMsUUFBUSxDQUFDO2dCQUNULE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLGNBQWMsQ0FBQyxLQUFLLENBQUM7Z0JBQ2hELENBQUM7Z0JBQ0QsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQztnREFHRDtRQXBGVyxXQUFXO1lBRnZCLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQztZQUNqQyxJQUFJLENBQUMsNENBQTRDLENBQUM7V0FDdEMsV0FBVyxDQXVJdkI7UUFBRCxrQkFBQztLQXZJRCxBQXVJQyxDQXZJZ0MsV0FBQSxlQUFlLEdBdUkvQztJQXZJWSxzQkFBVyxjQXVJdkIsQ0FBQTtBQUVELENBQUMsRUEzSk0sVUFBVSxLQUFWLFVBQVUsUUEySmhCO0FDM0pELElBQU8sVUFBVSxDQTY1QmhCO0FBNzVCRCxXQUFPLFVBQVU7SUFFWCxJQUFBLEtBQTZCLEVBQUUsQ0FBQyxVQUFVLEVBQXpDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBQSxFQUFFLElBQUksVUFBa0IsQ0FBQztJQUdqRCxJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDO0lBRWpCLElBQUssU0FVSjtJQVZELFdBQUssU0FBUztRQUliLHlDQUFRLENBQUE7UUFLUiw2Q0FBTSxDQUFBO0lBQ1AsQ0FBQyxFQVZJLFNBQVMsS0FBVCxTQUFTLFFBVWI7SUFFRCxJQUFLLFFBZUo7SUFmRCxXQUFLLFFBQVE7UUFJWiwyQ0FBVSxDQUFBO1FBS1YsNkNBQU8sQ0FBQTtRQUtQLHFDQUFHLENBQUE7SUFDSixDQUFDLEVBZkksUUFBUSxLQUFSLFFBQVEsUUFlWjtJQUlEO1FBQXVDLHFDQUFlO1FBQXREO1lBQUEscUVBcTNCQztZQW4zQkEsVUFBSSxHQUFVLGFBQWEsQ0FBQztZQUc1QixpQkFBVyxHQUFrQixJQUFJLFdBQUEsY0FBYyxDQUM5QyxLQUFLLEVBQUUsV0FBQSxZQUFZLENBQUMsT0FBTyxFQUMzQixFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBb0JqRCxZQUFNLEdBQW1CLElBQUksQ0FBQztZQUc5QixrQkFBWSxHQUFrQixJQUFJLENBQUM7WUErQmhDLGVBQVMsR0FBVyxLQUFLLENBQUM7WUFxQjFCLGdCQUFVLEdBQWEsU0FBUyxDQUFDLElBQUksQ0FBQztZQW1CekMsV0FBSyxHQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO1lBcUI3QixhQUFPLEdBQVcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFvQi9CLHFCQUFlLEdBQVcsSUFBSSxDQUFDO1lBcUJsQyxlQUFTLEdBQVksUUFBUSxDQUFDLE9BQU8sQ0FBQztZQW9CdEMsb0JBQWMsR0FBVyxJQUFJLENBQUM7WUFtQjNCLFlBQU0sR0FBVyxLQUFLLENBQUM7WUFnQjFCLFlBQU0sR0FBa0IsSUFBSSxXQUFBLGNBQWMsQ0FDekMsS0FBSyxFQUFFLFdBQUEsWUFBWSxDQUFDLFNBQVMsRUFDN0IsYUFBYSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztZQWlCM0MsZUFBUyxHQUFXLEtBQUssQ0FBQztZQW9CN0IsZ0JBQVUsR0FBWSxFQUFFLENBQUM7WUFrQnRCLGdCQUFVLEdBQVcsS0FBSyxDQUFDO1lBZ0I5QixnQkFBVSxHQUFrQixJQUFJLFdBQUEsY0FBYyxDQUM3QyxJQUFJLEVBQUUsV0FBQSxZQUFZLENBQUMsT0FBTyxFQUMxQixhQUFhLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBa0JsRSxtQkFBYSxHQUFXLEtBQUssQ0FBQztZQWdCakMsbUJBQWEsR0FBa0IsSUFBSSxXQUFBLGNBQWMsQ0FDaEQsSUFBSSxFQUFFLFdBQUEsWUFBWSxDQUFDLE9BQU8sRUFDMUIsZ0JBQWdCLEVBQUUsS0FBSyxFQUFFLGFBQWEsRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUUsYUFBYSxDQUFDLENBQUM7WUFvQmpGLGdCQUFVLEdBQWEsSUFBSSxDQUFDO1lBS2pCLGVBQVMsR0FBVSxDQUFDLENBQUM7WUFLckIsZ0JBQVUsR0FBVSxDQUFDLENBQUM7WUFLdEIsZUFBUyxHQUFVLENBQUMsQ0FBQzs7UUF5Z0JqQyxDQUFDO1FBejJCQSxzQkFBSSx5Q0FBVTtpQkFBZDtnQkFDQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDekIsQ0FBQztpQkFDRCxVQUFlLEtBQW9CO2dCQUNsQyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztnQkFDekIsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDL0IsQ0FBQzs7O1dBSkE7UUFxQkQsc0JBQUksMENBQVc7aUJBQWY7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQzFCLENBQUM7aUJBQ0QsVUFBZ0IsS0FBb0I7Z0JBQ25DLElBQUcsSUFBSSxDQUFDLFlBQVksS0FBSyxLQUFLLEVBQUM7b0JBQzlCLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ25DLElBQUksU0FBUyxFQUFFO3dCQUVkLElBQUksQ0FBQyxDQUFDLFVBQVUsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7NEJBQ2xFLE9BQU87eUJBQ1A7cUJBQ0Q7eUJBQ0k7d0JBQ0osSUFBSSxVQUFVLEtBQUssS0FBSyxFQUFFOzRCQUN6QixPQUFPO3lCQUNQO3FCQUNEO29CQUNELElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO29CQUMxQixJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUNqQyxJQUFJLFNBQVMsRUFBRTt3QkFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsQ0FBQztxQkFDNUM7aUJBQ0Q7WUFDRixDQUFDOzs7V0FyQkE7UUE0QkQsc0JBQUksdUNBQVE7aUJBQVo7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3ZCLENBQUM7aUJBQ0QsVUFBYSxLQUFhO2dCQUN6QixJQUFHLElBQUksQ0FBQyxTQUFTLEtBQUssS0FBSyxFQUFDO29CQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDdkIsSUFBRyxLQUFLLEVBQUM7d0JBQ1IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLENBQUM7cUJBQ3hDO29CQUNELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7b0JBQzdCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztpQkFDL0I7WUFDRixDQUFDOzs7V0FWQTtRQXVCRCxzQkFBSSx3Q0FBUztpQkFBYjtnQkFDQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDeEIsQ0FBQztpQkFDRCxVQUFjLEtBQWU7Z0JBQzVCLElBQUcsSUFBSSxDQUFDLFVBQVUsS0FBSyxLQUFLLEVBQUM7b0JBQzVCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO29CQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2lCQUN2QztZQUNGLENBQUM7OztXQU5BO1FBaUJELHNCQUFJLG1DQUFJO2lCQUFSO2dCQUNDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztZQUNuQixDQUFDO2lCQUNELFVBQVMsS0FBYTtnQkFDckIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQztnQkFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDdkMsSUFBRyxJQUFJLENBQUMsZUFBZSxFQUFDO29CQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3JDO1lBQ0YsQ0FBQzs7O1dBUkE7UUFtQkQsc0JBQUkscUNBQU07aUJBQVY7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3JCLENBQUM7aUJBQ0QsVUFBVyxLQUFhO2dCQUN2QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDdkMsSUFBRyxJQUFJLENBQUMsZUFBZSxFQUFDO29CQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3ZDO1lBQ0YsQ0FBQzs7O1dBUEE7UUFpQkQsc0JBQUksNkNBQWM7aUJBQWxCO2dCQUNDLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUM3QixDQUFDO2lCQUNELFVBQW1CLEtBQWE7Z0JBQy9CLElBQUcsSUFBSSxDQUFDLGVBQWUsS0FBSyxLQUFLLEVBQUM7b0JBQ2pDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO29CQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDdkM7WUFDRixDQUFDOzs7V0FQQTtRQXFCRCxzQkFBSSx1Q0FBUTtpQkFBWjtnQkFDQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDdkIsQ0FBQztpQkFDRCxVQUFhLEtBQWM7Z0JBQzFCLElBQUcsSUFBSSxDQUFDLFNBQVMsS0FBSyxLQUFLLEVBQUM7b0JBQzNCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUN2QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7aUJBQ3RCO1lBQ0YsQ0FBQzs7O1dBTkE7UUFpQkQsc0JBQUksbUNBQUk7aUJBQVI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO1lBQzVCLENBQUM7aUJBQ0QsVUFBUyxLQUFhO2dCQUNyQixJQUFHLElBQUksQ0FBQyxjQUFjLEtBQUssS0FBSyxFQUFDO29CQUNoQyxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztvQkFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztpQkFDdkM7WUFDQyxDQUFDOzs7V0FOSDtRQWFELHNCQUFJLG9DQUFLO2lCQUFUO2dCQUNDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNwQixDQUFDO2lCQUNELFVBQVUsS0FBYTtnQkFDdEIsSUFBRyxJQUFJLENBQUMsTUFBTSxLQUFLLEtBQUssRUFBQztvQkFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztpQkFDMUM7WUFDRixDQUFDOzs7V0FQQTtRQW9CRCxzQkFBSSxvQ0FBSztpQkFBVDtnQkFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDcEIsQ0FBQztpQkFDRCxVQUFVLEtBQW9CO2dCQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUMzQyxDQUFDOzs7V0FKQTtRQWNELHNCQUFJLHVDQUFRO2lCQUFaO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUN2QixDQUFDO2lCQUNELFVBQWEsS0FBYTtnQkFDekIsSUFBRyxJQUFJLENBQUMsU0FBUyxLQUFLLEtBQUssRUFBQztvQkFDM0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDdEIsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7b0JBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUM7aUJBQzFDO1lBQ0YsQ0FBQzs7O1dBUkE7UUFtQkQsc0JBQUksd0NBQVM7aUJBQWI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3hCLENBQUM7aUJBQ0QsVUFBYyxLQUFjO2dCQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztnQkFDeEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBQ3hDLENBQUM7OztXQUxBO1FBWUQsc0JBQUksd0NBQVM7aUJBQWI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3hCLENBQUM7aUJBQ0QsVUFBYyxLQUFhO2dCQUMxQixJQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFDO29CQUM1QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztvQkFDeEIsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7b0JBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUM7aUJBQzFDO1lBQ0YsQ0FBQzs7O1dBUEE7UUFvQkQsc0JBQUksd0NBQVM7aUJBQWI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3hCLENBQUM7aUJBQ0QsVUFBYyxLQUFvQjtnQkFDakMsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQzNDLENBQUM7OztXQUxBO1FBWUQsc0JBQUksMkNBQVk7aUJBQWhCO2dCQUNDLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUMzQixDQUFDO2lCQUNELFVBQWlCLEtBQWE7Z0JBQzdCLElBQUcsSUFBSSxDQUFDLGFBQWEsS0FBSyxLQUFLLEVBQUM7b0JBQy9CLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO29CQUMzQixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztpQkFDMUM7WUFDRixDQUFDOzs7V0FQQTtRQW9CRCxzQkFBSSwyQ0FBWTtpQkFBaEI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQzNCLENBQUM7aUJBQ0QsVUFBaUIsS0FBb0I7Z0JBQ3BDLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUMzQyxDQUFDOzs7V0FMQTtRQTJCRSxrQ0FBTSxHQUFOO1lBQ0ksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFDM0QsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNsRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2hFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDM0QsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2hGLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUM1RSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN2RSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDeEUsQ0FBQztRQUVELHFDQUFTLEdBQVQ7WUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5QixDQUFDO1FBRUosb0NBQVEsR0FBUjtZQUNPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEMsQ0FBQztRQUVELHFDQUFTLEdBQVQ7WUFDTyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFFTyx5Q0FBYSxHQUFyQixVQUFzQixNQUFjO1lBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDcEMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUMxQyxJQUFHLE1BQU0sRUFBQztnQkFDVCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzNCO1lBQ0QsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXZCLENBQUM7UUFLTywwQ0FBYyxHQUF0QjtZQUNDLElBQUcsSUFBSSxDQUFDLE9BQU8sRUFBQztnQkFDZixJQUFHLElBQUksQ0FBQyxNQUFNLEVBQUM7b0JBQ2QsSUFBRyxJQUFJLENBQUMsU0FBUyxFQUFDO3dCQUNqQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7d0JBQzVDLElBQUcsS0FBSyxFQUFDOzRCQUNSLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7NEJBQzFCLElBQUksRUFBRSxHQUFHLEtBQUssQ0FBQyxjQUFjLENBQUM7NEJBQzlCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUM7Z0NBQzFCLElBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDakIsSUFBRyxHQUFHLElBQUksSUFBSSxFQUFDO29DQUNkLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lDQUNmO3FDQUFLLElBQUcsR0FBRyxHQUFHLENBQUMsRUFBQztvQ0FDaEIsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQ0FDWDtxQ0FBSyxJQUFHLEdBQUcsR0FBRyxDQUFDLEVBQUM7b0NBQ2hCLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7aUNBQ1g7NkJBQ0Q7NEJBQ0QsSUFBRyxHQUFHLENBQUMsTUFBTSxHQUFHLEVBQUUsRUFBQztnQ0FDbEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQzs2QkFDbkM7eUJBQ0Q7cUJBQ0Q7eUJBQUk7d0JBQ0osSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7cUJBQ3JCO2lCQUNEO3FCQUFJO29CQUNKLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO2lCQUNyQjthQUNEO1FBQ0YsQ0FBQztRQUVPLDRDQUFnQixHQUF4QixVQUF5QixJQUFpQjtZQUN6QyxJQUFHLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxLQUFLLElBQUksRUFBQztnQkFDaEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7YUFDdkI7UUFDRixDQUFDO1FBRU8sOENBQWtCLEdBQTFCLFVBQTJCLElBQWlCO1lBQzNDLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFDO2dCQUNqQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUMxQjtRQUNGLENBQUM7UUFFTywyQ0FBZSxHQUF2QixVQUF3QixJQUFpQjtZQUN4QyxJQUFHLElBQUksQ0FBQyxPQUFPLEVBQUM7Z0JBRWYsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDakQ7UUFDRixDQUFDO1FBRU8scUNBQVMsR0FBakIsVUFBa0IsSUFBaUI7WUFDbEMsSUFBSSxJQUFJLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDekIsSUFBRyxJQUFJLENBQUMsWUFBWSxFQUFDO2dCQUVwQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNkLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDZCxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNsQixJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNsQixJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDWCxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDWCxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDWCxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNYO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDYixDQUFDO1FBRVUsa0RBQXNCLEdBQTlCLFVBQWdDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUNyRixJQUFHLElBQUksRUFBQztnQkFDUCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBR2xFLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLElBQUksUUFBUSxFQUFFO29CQUNiLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxFQUFFO3dCQUNsRSxJQUFHLElBQUksQ0FBQyxPQUFPLEVBQUM7NEJBQ2YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQzs0QkFDckUsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQzt5QkFDekQ7NkJBQUk7NEJBQ0osSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQzt5QkFDdEU7cUJBQ0Q7b0JBQ0QsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQ2pFLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxLQUFLLFdBQUEsWUFBWSxDQUFDLE9BQU8sRUFBQzs0QkFDbkYsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQzs0QkFDcEUsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7eUJBRXJFOzZCQUFJOzRCQUNKLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7eUJBQ3JFO3FCQUNEO29CQUNELElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxFQUFFO3dCQUNwRSxJQUFHLElBQUksQ0FBQyxPQUFPLElBQUcsSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksS0FBSyxXQUFBLFlBQVksQ0FBQyxPQUFPLEVBQUM7NEJBQ3hGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ3ZFLElBQUcsSUFBSSxDQUFDLFlBQVksRUFBQztnQ0FFcEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUM7Z0NBQ25DLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzZCQUNsRjtpQ0FBSztnQ0FDTCxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7NkJBQy9EO3lCQUVEOzZCQUFJOzRCQUNKLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7eUJBQ3hFO3FCQUNEO2lCQUNEO2FBQ0Q7UUFDRixDQUFDO1FBRU8sbURBQXVCLEdBQS9CLFVBQWdDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUNsRixJQUFHLElBQUksRUFBQztnQkFFUCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLFFBQVEsRUFBRTtvQkFDYixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRTt3QkFDN0QsSUFBRyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUM7NEJBQzlCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ2hFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDO2dDQUN4QyxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRO2dDQUMxQixJQUFJLEVBQUUsR0FBRyxDQUFDLGlCQUFpQjtnQ0FDM0IsR0FBRyxFQUFFLENBQUM7NkJBQ04sRUFBRSxDQUFDLENBQUMsQ0FBQzt5QkFFTjs2QkFBSTs0QkFDSixJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDOzRCQUNqRSxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQzt5QkFDbkI7cUJBQ0Q7b0JBQ0QsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQ2pFLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxLQUFLLFdBQUEsWUFBWSxDQUFDLFNBQVMsRUFBQzs0QkFDckYsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQzs0QkFDcEUsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7Z0NBQ3pDLElBQUksRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVE7Z0NBQzlCLElBQUksRUFBRSxHQUFHLENBQUMsaUJBQWlCO2dDQUMzQixHQUFHLEVBQUUsQ0FBQzs2QkFDTixFQUFFLENBQUMsQ0FBQyxDQUFDO3lCQUVOOzZCQUFJOzRCQUNKLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7NEJBQ3JFLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO3lCQUNwQjtxQkFDRDtvQkFDRCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsRUFBRTt3QkFDcEUsSUFBRyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUssV0FBQSxZQUFZLENBQUMsU0FBUyxFQUFDOzRCQUMzRixJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDOzRCQUN2RSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztnQ0FDeEMsSUFBSSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUTtnQ0FDakMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxpQkFBaUI7Z0NBQzNCLEdBQUcsRUFBRSxDQUFDOzZCQUNOLEVBQUUsQ0FBQyxDQUFDLENBQUM7eUJBRU47NkJBQUk7NEJBQ0osSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQzs0QkFDeEUsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7eUJBQ25CO3FCQUNEO2lCQUNEO2FBQ0Q7UUFDRixDQUFDO1FBRU8sK0NBQW1CLEdBQTNCLFVBQTRCLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUM5RSxJQUFHLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBQztnQkFFakMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7Z0JBRXZDLEtBQUssQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUV2QyxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFFLENBQUM7YUFDakM7aUJBQUk7Z0JBQ0osSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDdkI7UUFDRixDQUFDO1FBRU8sb0RBQXdCLEdBQWhDLFVBQWlDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUM3RSxJQUFHLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFDO2dCQUc3QixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO2dCQUdoQyxJQUFHLElBQUksQ0FBQyxTQUFTLEVBQUM7b0JBRWpCLElBQUksRUFBRSxTQUFBLEVBQUUsRUFBRSxTQUFBLEVBQUUsSUFBSSxTQUFBLEVBQUUsSUFBSSxTQUFBLEVBQUUsQ0FBQyxTQUFBLEVBQUUsQ0FBQyxTQUFBLEVBQUUsQ0FBQyxTQUFBLEVBQUUsQ0FBQyxTQUFBLENBQUM7b0JBQ25DLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFDO3dCQUNwQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO3dCQUNyQixFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQzt3QkFDaEIsRUFBRSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7d0JBQ2pCLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQzt3QkFDekIsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO3FCQUN6Qjt5QkFBSTt3QkFDSixFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7d0JBQ3RCLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzt3QkFDdkIsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQzt3QkFDM0IsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztxQkFDM0I7b0JBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO3dCQUNkLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQzt3QkFDVixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7d0JBQ1YsQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUM7d0JBQ2QsQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUM7cUJBQ2Q7eUJBQ0k7d0JBQ0osSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFFNUIsRUFBRSxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLEVBQUUsR0FBRyxLQUFLLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFFL0QsRUFBRSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEVBQUUsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFFL0MsTUFBTSxHQUFHLEtBQUssQ0FBQyxPQUFPLEVBQ3RCLE1BQU0sR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFLE1BQU0sR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO3dCQUNwQyxJQUFJLFFBQVEsR0FBRyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDeEMsSUFBSSxTQUFTLEdBQUcsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ3pDLElBQUksVUFBVSxHQUFHLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUMxQyxJQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDdkMsQ0FBQyxHQUFHLFFBQVEsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDO3dCQUM3QixDQUFDLEdBQUcsVUFBVSxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQy9CLENBQUMsR0FBRyxFQUFFLEdBQUcsU0FBUyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQ25DLENBQUMsR0FBRyxFQUFFLEdBQUcsT0FBTyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUM7cUJBQ2pDO29CQUVELElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7b0JBQzVCLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ2IsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDYixLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNiLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBRWIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUM1QjtnQkFHRCxJQUFHLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLEVBQUM7b0JBRXBDLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDO29CQUM5QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO29CQUMzQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO29CQUM5QixJQUFJLGFBQWEsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO29CQUM1QyxJQUFJLEtBQUssR0FBRyxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDM0MsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTt3QkFDM0IsSUFBSSxTQUFTLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDbkQsSUFBSSxTQUFTLEdBQUcsYUFBYSxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUM7d0JBQzdDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBQ2pDLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQztxQkFDekM7aUJBQ0Q7Z0JBR0QsSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxFQUFDO29CQUN6QyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMvQixJQUFHLEdBQUcsRUFBQzt3QkFDTixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO3dCQUNoQyxJQUFJLGFBQWEsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO3dCQUM1QyxJQUFJLEtBQUssR0FBRyxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDM0MsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTs0QkFDM0IsSUFBSSxTQUFTLEdBQUcsYUFBYSxHQUFHLENBQUMsR0FBRyxTQUFTLENBQUM7NEJBQzlDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDOzRCQUN6QixLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7NEJBQzdCLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQzs0QkFDN0IsS0FBSyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO3lCQUM3QjtxQkFDRDtpQkFDRDtnQkFHRCxJQUFHLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLEVBQUM7b0JBRTNDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDO29CQUNuQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO29CQUM5QixJQUFJLGFBQWEsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO29CQUM1QyxJQUFJLEtBQUssR0FBRyxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDM0MsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTt3QkFDM0IsSUFBSSxTQUFTLEdBQUcsYUFBYSxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUM7d0JBQzdDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO3dCQUM5QixLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7cUJBQ25DO2lCQUNEO2FBQ0s7UUFDUixDQUFDO1FBRUUsNENBQWdCLEdBQWhCLFVBQWlCLElBQWlCO1lBQ3BDLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFDO2dCQUNqQyxJQUFJLGlCQUFpQixFQUFFO29CQUN0QixJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2xDO3FCQUFNO29CQUNOLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDakM7YUFDRDtRQUNDLENBQUM7UUFFRCxpREFBcUIsR0FBckIsVUFBc0IsSUFBaUI7WUFFekMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUMxQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQzVCLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRzNDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ3BDLElBQUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxDQUFDLEVBQ2xCLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQzlELEVBQUUsR0FBRyxPQUFPLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxHQUFHLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUV2QyxJQUFJLEVBQUUsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDNUIsRUFBRSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBUWpDLElBQUksYUFBYSxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFN0QsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ2QsSUFBSSxhQUFhLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQztZQUM1QyxJQUFJLGFBQWEsRUFBRTtnQkFFZixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDdkIsS0FBSyxDQUFDLEtBQUssR0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUN6QixLQUFLLElBQUksYUFBYSxDQUFDO2dCQUV2QixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDdkIsS0FBSyxDQUFDLEtBQUssR0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUN6QixLQUFLLElBQUksYUFBYSxDQUFDO2dCQUV2QixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDdkIsS0FBSyxDQUFDLEtBQUssR0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUN6QixLQUFLLElBQUksYUFBYSxDQUFDO2dCQUV2QixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDdkIsS0FBSyxDQUFDLEtBQUssR0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO2FBQzVCO2lCQUFNO2dCQUNILElBQUksRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQzVCLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUN4QixFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFDeEIsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBS3pCLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDNUIsS0FBSyxDQUFDLEtBQUssR0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDOUIsS0FBSyxJQUFJLGFBQWEsQ0FBQztnQkFFdkIsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUM1QixLQUFLLENBQUMsS0FBSyxHQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUM5QixLQUFLLElBQUksYUFBYSxDQUFDO2dCQUV2QixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQzVCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQzlCLEtBQUssSUFBSSxhQUFhLENBQUM7Z0JBRXZCLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztnQkFDNUIsS0FBSyxDQUFDLEtBQUssR0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQzthQUNqQztRQUNMLENBQUM7UUFFRCxrREFBc0IsR0FBdEIsVUFBdUIsSUFBaUI7WUFFMUMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUMxQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQzVCLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksYUFBYSxHQUFHLFNBQVMsQ0FBQyxhQUFhLENBQUM7WUFFNUMsSUFBSSxFQUFFLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUNiLEVBQUUsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQ2IsRUFBRSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDYixFQUFFLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRWxCLElBQUksS0FBSyxHQUFXLENBQUMsQ0FBQztZQUV0QixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3BCLEtBQUssSUFBSSxhQUFhLENBQUM7WUFFdkIsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNsQixLQUFLLENBQUMsS0FBSyxHQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNwQixLQUFLLElBQUksYUFBYSxDQUFDO1lBRXZCLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDbEIsS0FBSyxDQUFDLEtBQUssR0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEIsS0FBSyxJQUFJLGFBQWEsQ0FBQztZQUV2QixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLEtBQUssQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLENBQUM7UUFFTyxrREFBc0IsR0FBOUIsVUFBZ0MsSUFBaUI7WUFDN0MsSUFBRyxJQUFJLENBQUMsT0FBTyxFQUFDO2dCQUNaLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ3BDLElBQUksV0FBVztvQkFDWCxXQUFXLENBQUMsYUFBYSxFQUFFLEVBQUU7b0JBQzdCLE9BQU87aUJBQ25CO2dCQUVRLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUN4QjtRQUNMLENBQUM7UUFFRCw4Q0FBa0IsR0FBbEIsVUFBbUIsSUFBSSxFQUFFLEtBQUs7WUFDMUIsSUFBSSxPQUFPO2dCQUFFLE9BQU87WUFFcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDLG1CQUFtQixJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFO2dCQUN2RSxJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUMsbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBRWxFLElBQUksV0FBVyxFQUFFO29CQUNiLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztpQkFDNUM7YUFDSjtZQUNELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEMsSUFBSSxDQUFDLFFBQVE7Z0JBQUUsT0FBTztZQUV0QixJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsS0FBSyxLQUFLLENBQUMsUUFBUSxFQUFFO2dCQUM3RSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQzthQUNqQztRQUNSLENBQUM7UUFFVSwwQ0FBYyxHQUF0QjtZQUNJLElBQUcsSUFBSSxDQUFDLE9BQU8sRUFBQztnQkFDWixJQUFHLElBQUksQ0FBQyxTQUFTLEVBQUM7b0JBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTzt3QkFBRyxPQUFPO29CQUNqRCxJQUFJLFFBQVEsQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLFNBQVMsRUFBRTt3QkFFakMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUM7d0JBQzFELElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDZjt5QkFBTSxJQUFJLFFBQVEsQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLFNBQVMsRUFBRTt3QkFFM0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUM7d0JBQ25DLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztxQkFDOUM7b0JBQ0QsSUFBRyxJQUFJLENBQUMsZUFBZSxFQUFDO3dCQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQ3JDO29CQUNELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7aUJBQzlCO2FBQ0o7UUFDTCxDQUFDO1FBRU8sMkNBQWUsR0FBdkIsVUFBeUIsUUFBdUI7WUFDNUMsSUFBRyxJQUFJLENBQUMsT0FBTyxFQUFDO2dCQUNaLElBQUksVUFBVSxHQUFHLFFBQVEsSUFBSSxRQUFRLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ25ELElBQUksVUFBVSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRTtvQkFDbEMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDbkQ7Z0JBRUQsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDcEMsSUFBSSxXQUFXLEVBQUU7b0JBRXpCLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO29CQUNsQixJQUFJLFVBQVUsR0FBRyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7b0JBQzFDLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxNQUFNLEVBQUU7d0JBQ2pDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztxQkFDekI7eUJBQ0k7d0JBQ0QsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztxQkFDdkQ7aUJBQ0o7YUFDVjtZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDakMsSUFBSSxTQUFTLEVBQUU7Z0JBQ1gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDckM7UUFDUixDQUFDO1FBRVUscUNBQVMsR0FBakIsVUFBa0IsV0FBMEI7WUFDOUMsSUFBRyxTQUFTLEVBQUM7Z0JBR1osSUFBSSxXQUFXLElBQUksV0FBVyxDQUFDLFVBQVUsRUFBRTtvQkFDMUMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO29CQUVoQixFQUFFLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLFVBQVUsR0FBRyxFQUFFLEtBQUs7d0JBQ3BELElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNwQyxDQUFDLENBQUMsQ0FBQztpQkFDSDtxQkFBTTtvQkFDTixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztpQkFDbkI7YUFDRDtRQUNDLENBQUM7UUEvMkJKO1lBREMsUUFBUSxDQUFDLFdBQUEsY0FBYyxDQUFDOzhEQUd3QjtRQUtqRDtZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsV0FBQSxjQUFjO2dCQUNyQixPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDOzJEQUdEO1FBYUQ7WUFQQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFFLEVBQUUsQ0FBQyxXQUFXO2dCQUNwQixPQUFPLEVBQUUsTUFBTSxJQUFJLDZCQUE2QjtnQkFDaEQsVUFBVSxFQUFFLElBQUk7Z0JBQ2hCLE9BQU8sRUFBRSxJQUFJO2dCQUNiLFVBQVUsRUFBRSxLQUFLO2FBQ2pCLENBQUM7eURBQzRCO1FBRzlCO1lBREksUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7K0RBQ087UUFLbkM7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxXQUFXO2dCQUNyQixPQUFPLEVBQUcsT0FBTzthQUNqQixDQUFDOzREQUdEO1FBd0JFO1lBREMsUUFBUSxFQUFFOzREQUNlO1FBSTdCO1lBSEMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBQyxNQUFNO2FBQ2QsQ0FBQzt5REFHRDtRQWVFO1lBSEMsUUFBUSxDQUFDO2dCQUNaLElBQUksRUFBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQzthQUN6QixDQUFDOzZEQUN1QztRQVF6QztZQVBDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7Z0JBQ3pCLE9BQU8sRUFBQyxNQUFNO2dCQUNkLE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO2dCQUN2QixDQUFDO2FBQ0QsQ0FBQzswREFHRDtRQVNEO1lBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0RBQ1c7UUFRN0I7WUFQQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFDLEVBQUUsQ0FBQyxJQUFJO2dCQUNaLE9BQU8sRUFBRyxPQUFPO2dCQUNqQixPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLFNBQVMsQ0FBQyxNQUFNLENBQUM7Z0JBQzlELENBQUM7YUFDRCxDQUFDO3FEQUdEO1FBV0Q7WUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzswREFDZ0I7UUFRbEM7WUFQQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFDLEVBQUUsQ0FBQyxJQUFJO2dCQUNaLE9BQU8sRUFBRyxPQUFPO2dCQUNqQixPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLFNBQVMsQ0FBQyxNQUFNLENBQUM7Z0JBQzlELENBQUM7YUFDRCxDQUFDO3VEQUdEO1FBVUU7WUFEQyxRQUFRLEVBQUU7a0VBQ29CO1FBT2xDO1lBTkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBQyxTQUFTO2dCQUNqQixPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLFNBQVMsQ0FBQyxNQUFNLENBQUM7Z0JBQzlELENBQUM7YUFDRCxDQUFDOytEQUdEO1FBWUQ7WUFISSxRQUFRLENBQUM7Z0JBQ04sSUFBSSxFQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQzNCLENBQUM7NERBQ2lDO1FBU3RDO1lBUkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDeEIsT0FBTztvQkFDTixPQUFPLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxTQUFTLENBQUMsTUFBTSxDQUFDO2dCQUM5RCxDQUFDO2dCQUNELFVBQVUsRUFBRSxLQUFLO2dCQUNqQixPQUFPLEVBQUUsTUFBTSxJQUFJLGlDQUFpQzthQUNwRCxDQUFDO3lEQUdEO1FBU0Q7WUFESSxRQUFRLEVBQUU7aUVBQ2dCO1FBUTlCO1lBUEMsUUFBUSxDQUFDO2dCQUNULE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO2dCQUN2QixDQUFDO2dCQUNELFVBQVUsRUFBRSxLQUFLO2dCQUNqQixPQUFPLEVBQUUsTUFBTSxJQUFJLDRCQUE0QjthQUMvQyxDQUFDO3FEQUdEO1FBU0U7WUFEQyxRQUFRLEVBQUU7eURBQ1k7UUFJMUI7WUFIQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTyxFQUFDLE9BQU87YUFDZixDQUFDO3NEQUdEO1FBVUQ7WUFEQyxRQUFRLENBQUMsV0FBQSxjQUFjLENBQUM7eURBR3FCO1FBUTlDO1lBUEMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxXQUFBLGNBQWM7Z0JBQ3JCLE9BQU8sRUFBRyxNQUFNO2dCQUNoQixPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDcEIsQ0FBQzthQUNELENBQUM7c0RBR0Q7UUFPRTtZQURDLFFBQVEsRUFBRTs0REFDZTtRQU83QjtZQU5DLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUMsUUFBUTtnQkFDaEIsT0FBTztvQkFDTixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBQ3BCLENBQUM7YUFDRCxDQUFDO3lEQUdEO1FBV0Q7WUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7NkRBQ0U7UUFRekI7WUFQQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztnQkFDbkIsT0FBTyxFQUFHLE9BQU87Z0JBQ2pCLE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUM7Z0JBQ3RDLENBQUM7YUFDRCxDQUFDOzBEQUdEO1FBUUU7WUFEQyxRQUFRLEVBQUU7NkRBQ2dCO1FBSTlCO1lBSEMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBQyxnQkFBZ0I7YUFDeEIsQ0FBQzswREFHRDtRQVVEO1lBREMsUUFBUSxDQUFDLFdBQUEsY0FBYyxDQUFDOzZEQUc0QztRQVFyRTtZQVBDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsV0FBQSxjQUFjO2dCQUNyQixPQUFPLEVBQUcsUUFBUTtnQkFDbEIsT0FBTztvQkFDTixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7Z0JBQ3hCLENBQUM7YUFDRCxDQUFDOzBEQUdEO1FBUUU7WUFEQyxRQUFRLEVBQUU7Z0VBQ21CO1FBSWpDO1lBSEMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBQyxhQUFhO2FBQ3JCLENBQUM7NkRBR0Q7UUFVRDtZQURDLFFBQVEsQ0FBQyxXQUFBLGNBQWMsQ0FBQztnRUFHd0Q7UUFRakY7WUFQQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLFdBQUEsY0FBYztnQkFDckIsT0FBTyxFQUFHLE9BQU87Z0JBQ2pCLE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO2dCQUMzQixDQUFDO2FBQ0QsQ0FBQzs2REFHRDtRQW5WVyxpQkFBaUI7WUFGN0IsT0FBTyxDQUFDLDhCQUE4QixDQUFDO1lBQ3ZDLElBQUksQ0FBQyxrREFBa0QsQ0FBQztXQUM1QyxpQkFBaUIsQ0FxM0I3QjtRQUFELHdCQUFDO0tBcjNCRCxBQXEzQkMsQ0FyM0JzQyxXQUFBLGVBQWUsR0FxM0JyRDtJQXIzQlksNEJBQWlCLG9CQXEzQjdCLENBQUE7QUFFRCxDQUFDLEVBNzVCTSxVQUFVLEtBQVYsVUFBVSxRQTY1QmhCO0FDNzVCRCxJQUFPLFVBQVUsQ0EwUmhCO0FBMVJELFdBQU8sVUFBVTtJQUVYLElBQUEsS0FBNkIsRUFBRSxDQUFDLFVBQVUsRUFBekMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFBLEVBQUUsSUFBSSxVQUFrQixDQUFDO0lBR2pELElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUM7SUFLakIsSUFBTSxFQUFFLEdBQUU7UUFDVCxHQUFHLEVBQUcsTUFBTTtRQUVaLFNBQVMsRUFBRyxJQUFJO1FBQ2hCLFVBQVUsRUFBRyxXQUFBLFlBQVksQ0FBQyxPQUFPO1FBQ2pDLFlBQVksRUFBRyxXQUFXO1FBQzFCLHFCQUFxQixFQUFHLEtBQUs7UUFDN0IsV0FBVyxFQUFHLFNBQVM7UUFDdkIsWUFBWSxFQUFHLFdBQVc7UUFDMUIscUJBQXFCLEVBQUcsS0FBSztRQUM3QixXQUFXLEVBQUcsU0FBUztRQUV2QixVQUFVLEVBQUcsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUNwQixNQUFNLEVBQUcsY0FBTSxPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDaEMsT0FBTyxFQUFHLEVBQUUsQ0FBQyxJQUFJO1FBRWpCLFFBQVEsRUFBRyxDQUFDO1FBRVosT0FBTyxFQUFHLFVBQUMsR0FBWSxFQUFFLEtBQWEsRUFBRSxNQUFhO1lBQ3BELEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUMzQixDQUFDO0tBQ0QsQ0FBQTtJQUlEO1FBQWdDLDhCQUFlO1FBQS9DO1lBQUEscUVBb1BDO1lBbFBBLFVBQUksR0FBVSxFQUFFLENBQUMsR0FBRyxDQUFDO1lBS3JCLGdCQUFVLEdBQWEsV0FBQSxTQUFTLENBQUMsTUFBTSxDQUFDO1lBcUJ4QyxlQUFTLEdBQWtCLElBQUksV0FBQSxjQUFjLENBQzVDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLFVBQVUsRUFDM0IsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMscUJBQXFCLEVBQUUsRUFBRSxDQUFDLFdBQVcsRUFDekQsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMscUJBQXFCLEVBQUUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBZ0I1RCxhQUFPLEdBQTBCLENBQUUsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFFLENBQUM7WUFtQ3RDLGtCQUFZLEdBQVUsQ0FBQyxDQUFDOztRQWtLcEMsQ0FBQztRQXhPQSxzQkFBSSxpQ0FBUztpQkFBYjtnQkFDQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDeEIsQ0FBQztpQkFDRCxVQUFjLEtBQWU7Z0JBQzVCLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUM7b0JBQzNCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO29CQUN4QixJQUFHLEtBQUssSUFBSSxXQUFBLFNBQVMsQ0FBQyxLQUFLLEVBQUM7d0JBQ2YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsT0FBTyxDQUFDO3dCQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7cUJBQ25DO3lCQUFJO3dCQUNELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztxQkFDbEM7aUJBQ1Y7WUFDRixDQUFDOzs7V0FYQTtRQXNCRCxzQkFBSSxnQ0FBUTtpQkFBWjtnQkFDQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDdkIsQ0FBQztpQkFDRCxVQUFhLEtBQW9CO2dCQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztnQkFDdkIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUN0QixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUN4QyxDQUFDOzs7V0FOSDtRQWtCRCxzQkFBSSw2QkFBSztpQkFBVDtnQkFDQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEIsQ0FBQztpQkFDRCxVQUFVLE1BQTJCO2dCQUNwQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQztnQkFDekIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3ZCLENBQUM7OztXQUpBO1FBYUQsc0JBQUksOEJBQU07aUJBQVY7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3JCLENBQUM7aUJBQ0QsVUFBVyxLQUE0QjtnQkFDdEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixDQUFDOzs7V0FKQTtRQVdFLDJCQUFNLEdBQU47WUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2hGLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUM1RSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDeEUsQ0FBQztRQUVELDhCQUFTLEdBQVQ7WUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QixDQUFDO1FBRUosNkJBQVEsR0FBUjtZQUNPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEMsQ0FBQztRQUVELDhCQUFTLEdBQVQ7WUFDTyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFFTyxrQ0FBYSxHQUFyQixVQUFzQixNQUFjO1lBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDcEMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3ZCLENBQUM7UUFLVSx5Q0FBb0IsR0FBNUI7WUFDSSxJQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLFdBQUEsWUFBWSxDQUFDLFNBQVMsRUFBQztnQkFDN0MsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3pCO1FBQ0wsQ0FBQztRQUtJLG1DQUFjLEdBQXRCO1lBQ0MsSUFBRyxJQUFJLENBQUMsT0FBTyxFQUFDO2dCQUNOLElBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFDO29CQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLENBQUUsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFFLENBQUM7aUJBQ2xDO2dCQUNELElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsU0FBUyxFQUFDO29CQUM3QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7b0JBQzNDLElBQUcsSUFBSSxFQUFDO3dCQUNKLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7d0JBQzdCLElBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsRUFBRSxFQUFDOzRCQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDOzRCQUMxQixLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFDO2dDQUN2QixJQUFJLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQ3RCLElBQUcsS0FBSyxJQUFJLElBQUksRUFBQztvQ0FDYixNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO2lDQUMzQjs2QkFDSjt5QkFDSjt3QkFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7cUJBQzVCO2lCQUNKO3FCQUFLLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFBLFNBQVMsQ0FBQyxNQUFNLEVBQUM7b0JBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztpQkFDM0I7Z0JBQ0QsSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFBLFlBQVksQ0FBQyxTQUFTLEVBQUM7b0JBQzdDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUM7aUJBQzdDO3FCQUFJO29CQUNELElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2lCQUNqQzthQUNWO1FBQ0YsQ0FBQztRQUVVLDJDQUFzQixHQUE5QixVQUFnQyxJQUFtRDtZQUFuRCxxQkFBQSxFQUFBLE9BQW9CLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUM7WUFDckYsSUFBRyxJQUFJLEVBQUM7Z0JBR1AsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsSUFBSSxRQUFRLEVBQUU7b0JBQ2IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQ2hFLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksS0FBSyxXQUFBLFlBQVksQ0FBQyxPQUFPLEVBQUM7NEJBQzdDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ25FLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFBLFNBQVMsQ0FBQyxNQUFNLEVBQUM7Z0NBQ25DLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUNsRTtpQ0FBSTtnQ0FDRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO2dDQUMxQixJQUFJLE9BQU8sR0FBWSxFQUFFLENBQUM7Z0NBQzFCLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUM7Z0NBQ3pCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUM7b0NBQ3pDLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lDQUMxRDtnQ0FDRCxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLElBQUksWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0NBQzdGLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7NkJBQzFEO3lCQUVuQjs2QkFBSTs0QkFDSixJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO3lCQUNwRTtxQkFFRDtpQkFDRDthQUNEO1FBQ0MsQ0FBQztRQU1JLHVDQUFrQixHQUExQixVQUEyQixJQUFpQjtZQUMzQyxJQUFJLE1BQU0sR0FBWSxFQUFFLENBQUM7WUFDekIsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztZQUN2QixJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDO1lBQ3pCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUM7Z0JBQ3ZCLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUM5RDtZQUNQLE9BQU8sTUFBTSxDQUFDO1FBQ2YsQ0FBQztRQUVPLDRDQUF1QixHQUEvQixVQUFnQyxJQUFtRDtZQUFuRCxxQkFBQSxFQUFBLE9BQW9CLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUM7WUFDbEYsSUFBRyxJQUFJLEVBQUM7Z0JBRVAsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsSUFBSSxRQUFRLEVBQUU7b0JBQ2IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQ2hFLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksS0FBSyxXQUFBLFlBQVksQ0FBQyxTQUFTLEVBQUM7NEJBQ2pFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ25FLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDO2dDQUMzQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRO2dDQUM3QixJQUFJLEVBQUUsR0FBRyxDQUFDLGlCQUFpQjtnQ0FDM0IsR0FBRyxFQUFFLEVBQUUsQ0FBQyxRQUFROzZCQUNoQixFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzt5QkFFaEI7NkJBQUk7NEJBQ0osSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQzs0QkFDcEUsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7eUJBQ3RCO3FCQUVEO2lCQUNEO2FBQ0Q7UUFDRixDQUFDO1FBRU8sNkNBQXdCLEdBQWhDLFVBQWlDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUM3RSxJQUFHLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFDO2dCQUU3QixJQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxFQUFDO29CQUV4QixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO29CQUNoQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzNDLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7b0JBQ3pCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ3BDLElBQUksYUFBYSxHQUFHLFNBQVMsQ0FBQyxhQUFhLENBQUM7b0JBQzVDLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMzQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO3dCQUMzQixJQUFJLFNBQVMsR0FBRyxhQUFhLEdBQUcsQ0FBQyxHQUFHLFdBQVcsQ0FBQzt3QkFDaEQsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7d0JBQ2hDLElBQUcsU0FBUyxJQUFJLElBQUksRUFBQzs0QkFDcEIsU0FBUyxHQUFHLENBQUMsQ0FBQzt5QkFDZDt3QkFDRCxLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBQzs0QkFDbkMsS0FBSyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDO3lCQUM3QztxQkFDRDtpQkFDRDthQUNLO1FBQ1IsQ0FBQztRQTVPRDtZQUhDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFBLFNBQVMsQ0FBQzthQUN4QixDQUFDO3NEQUNzQztRQUt4QztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFBLFNBQVMsQ0FBQztnQkFDeEIsT0FBTyxFQUFHLEtBQUs7YUFDZixDQUFDO21EQUdEO1FBY0Q7WUFEQyxRQUFRLENBQUMsV0FBQSxjQUFjLENBQUM7cURBSW1DO1FBSzVEO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxXQUFBLGNBQWM7Z0JBQ3JCLE9BQU8sRUFBRyxLQUFLO2FBQ2YsQ0FBQztrREFHRDtRQVNEO1lBREksUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDO21EQUN1QjtRQVNqRDtZQVBDLFFBQVEsQ0FBQztnQkFDVCxPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFBLFNBQVMsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsT0FBTyxDQUFDO2dCQUMzRixDQUFDO2dCQUNELElBQUksRUFBRyxFQUFFLENBQUMsT0FBTztnQkFDakIsT0FBTyxFQUFHLEdBQUc7YUFDYixDQUFDOytDQUdEO1FBYUQ7WUFQQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTztvQkFDTixPQUFPLElBQUksQ0FBQyxVQUFVLElBQUksV0FBQSxTQUFTLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLFdBQUEsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDNUYsQ0FBQztnQkFDRCxJQUFJLEVBQUcsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO2dCQUNuQixPQUFPLEVBQUcsV0FBVzthQUNyQixDQUFDO2dEQUdEO1FBekVXLFVBQVU7WUFGdEIsT0FBTyxDQUFDLHVCQUF1QixDQUFDO1lBQ2hDLElBQUksQ0FBQywyQ0FBMkMsQ0FBQztXQUNyQyxVQUFVLENBb1B0QjtRQUFELGlCQUFDO0tBcFBELEFBb1BDLENBcFArQixXQUFBLGVBQWUsR0FvUDlDO0lBcFBZLHFCQUFVLGFBb1B0QixDQUFBO0FBRUQsQ0FBQyxFQTFSTSxVQUFVLEtBQVYsVUFBVSxRQTBSaEI7QUMxUkQsSUFBTyxVQUFVLENBMlJoQjtBQTNSRCxXQUFPLFVBQVU7SUFFWCxJQUFBLEtBQTZCLEVBQUUsQ0FBQyxVQUFVLEVBQXpDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBQSxFQUFFLElBQUksVUFBa0IsQ0FBQztJQUdqRCxJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDO0lBS2pCLElBQU0sRUFBRSxHQUFFO1FBQ1QsR0FBRyxFQUFHLE1BQU07UUFFWixTQUFTLEVBQUcsS0FBSztRQUNqQixVQUFVLEVBQUcsV0FBQSxZQUFZLENBQUMsT0FBTztRQUNqQyxZQUFZLEVBQUcsV0FBVztRQUMxQixxQkFBcUIsRUFBRyxLQUFLO1FBQzdCLFdBQVcsRUFBRyxTQUFTO1FBQ3ZCLFlBQVksRUFBRyxXQUFXO1FBQzFCLHFCQUFxQixFQUFHLEtBQUs7UUFDN0IsV0FBVyxFQUFHLFNBQVM7UUFFdkIsVUFBVSxFQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUU7UUFDcEIsTUFBTSxFQUFHLGNBQU0sT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLE9BQU8sRUFBRyxFQUFFLENBQUMsSUFBSTtRQUVqQixRQUFRLEVBQUcsQ0FBQztRQUVaLE9BQU8sRUFBRyxVQUFDLEdBQVksRUFBRSxLQUFhLEVBQUUsTUFBYTtZQUNwRCxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUN0QixHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDMUIsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzNCLENBQUM7S0FDRCxDQUFBO0lBSUQ7UUFBZ0MsOEJBQWU7UUFBL0M7WUFBQSxxRUFvUEM7WUFsUEEsVUFBSSxHQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFLckIsZ0JBQVUsR0FBYSxXQUFBLFNBQVMsQ0FBQyxNQUFNLENBQUM7WUFxQnhDLGVBQVMsR0FBa0IsSUFBSSxXQUFBLGNBQWMsQ0FDNUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUMsVUFBVSxFQUMzQixFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxFQUFFLENBQUMsV0FBVyxFQUN6RCxFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUM7WUFnQjVELGFBQU8sR0FBMEIsQ0FBRSxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUUsQ0FBQztZQW1DdEMsa0JBQVksR0FBVSxDQUFDLENBQUM7O1FBa0twQyxDQUFDO1FBeE9BLHNCQUFJLGlDQUFTO2lCQUFiO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUN4QixDQUFDO2lCQUNELFVBQWMsS0FBZTtnQkFDNUIsSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUssRUFBQztvQkFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7b0JBQ3hCLElBQUcsS0FBSyxJQUFJLFdBQUEsU0FBUyxDQUFDLEtBQUssRUFBQzt3QkFDZixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFBLFlBQVksQ0FBQyxPQUFPLENBQUM7d0JBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztxQkFDbkM7eUJBQUk7d0JBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO3FCQUNsQztpQkFDVjtZQUNGLENBQUM7OztXQVhBO1FBc0JELHNCQUFJLGdDQUFRO2lCQUFaO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUN2QixDQUFDO2lCQUNELFVBQWEsS0FBb0I7Z0JBQ2hDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2dCQUN2QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ3hDLENBQUM7OztXQU5IO1FBa0JELHNCQUFJLDZCQUFLO2lCQUFUO2dCQUNDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QixDQUFDO2lCQUNELFVBQVUsTUFBMkI7Z0JBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDO2dCQUN6QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsQ0FBQzs7O1dBSkE7UUFhRCxzQkFBSSw4QkFBTTtpQkFBVjtnQkFDQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDckIsQ0FBQztpQkFDRCxVQUFXLEtBQTRCO2dCQUN0QyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3ZCLENBQUM7OztXQUpBO1FBV0UsMkJBQU0sR0FBTjtZQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN0RSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDaEYsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMseUJBQXlCLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzVFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN4RSxDQUFDO1FBRUQsOEJBQVMsR0FBVDtZQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hCLENBQUM7UUFFSiw2QkFBUSxHQUFSO1lBQ08sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoQyxDQUFDO1FBRUQsOEJBQVMsR0FBVDtZQUNPLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsQ0FBQztRQUVPLGtDQUFhLEdBQXJCLFVBQXNCLE1BQWM7WUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUNwQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkIsQ0FBQztRQUtVLHlDQUFvQixHQUE1QjtZQUNJLElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsU0FBUyxFQUFDO2dCQUM3QyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDekI7UUFDTCxDQUFDO1FBS0ksbUNBQWMsR0FBdEI7WUFDQyxJQUFHLElBQUksQ0FBQyxPQUFPLEVBQUM7Z0JBQ04sSUFBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUM7b0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBRSxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUUsQ0FBQztpQkFDbEM7Z0JBQ0QsSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFBLFlBQVksQ0FBQyxTQUFTLEVBQUM7b0JBQzdDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztvQkFDM0MsSUFBRyxJQUFJLEVBQUM7d0JBQ0osSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQzt3QkFDN0IsSUFBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxFQUFFLEVBQUM7NEJBQ3hCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7NEJBQzFCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUM7Z0NBQ3ZCLElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDdEIsSUFBRyxLQUFLLElBQUksSUFBSSxFQUFDO29DQUNiLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUM7aUNBQzNCOzZCQUNKO3lCQUNKO3dCQUNELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztxQkFDNUI7aUJBQ0o7cUJBQUssSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLFdBQUEsU0FBUyxDQUFDLE1BQU0sRUFBQztvQkFDekMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2lCQUMzQjtnQkFDRCxJQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLFdBQUEsWUFBWSxDQUFDLFNBQVMsRUFBQztvQkFDN0MsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztpQkFDN0M7cUJBQUk7b0JBQ0QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7aUJBQ2pDO2FBQ1Y7UUFDRixDQUFDO1FBRVUsMkNBQXNCLEdBQTlCLFVBQWdDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUNyRixJQUFHLElBQUksRUFBQztnQkFHUCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLFFBQVEsRUFBRTtvQkFDYixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTt3QkFDaEUsSUFBRyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxLQUFLLFdBQUEsWUFBWSxDQUFDLE9BQU8sRUFBQzs0QkFDN0MsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQzs0QkFDbkUsSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLFdBQUEsU0FBUyxDQUFDLE1BQU0sRUFBQztnQ0FDbkMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NkJBQ2xFO2lDQUFJO2dDQUNELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7Z0NBQzFCLElBQUksT0FBTyxHQUFZLEVBQUUsQ0FBQztnQ0FDMUIsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQztnQ0FDekIsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBQztvQ0FDekMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7aUNBQzFEO2dDQUNELFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxZQUFZLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQ0FDN0YsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQzs2QkFDMUQ7eUJBRW5COzZCQUFJOzRCQUNKLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7eUJBQ3BFO3FCQUVEO2lCQUNEO2FBQ0Q7UUFDQyxDQUFDO1FBTUksdUNBQWtCLEdBQTFCLFVBQTJCLElBQWlCO1lBQzNDLElBQUksTUFBTSxHQUFZLEVBQUUsQ0FBQztZQUN6QixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1lBQ3ZCLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUM7WUFDekIsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBQztnQkFDdkIsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzlEO1lBQ1AsT0FBTyxNQUFNLENBQUM7UUFDZixDQUFDO1FBRU8sNENBQXVCLEdBQS9CLFVBQWdDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUNsRixJQUFHLElBQUksRUFBQztnQkFFUCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLFFBQVEsRUFBRTtvQkFDYixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTt3QkFDaEUsSUFBRyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxLQUFLLFdBQUEsWUFBWSxDQUFDLFNBQVMsRUFBQzs0QkFDakUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQzs0QkFDbkUsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7Z0NBQzNDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVE7Z0NBQzdCLElBQUksRUFBRSxHQUFHLENBQUMsaUJBQWlCO2dDQUMzQixHQUFHLEVBQUUsRUFBRSxDQUFDLFFBQVE7NkJBQ2hCLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3lCQUVoQjs2QkFBSTs0QkFDSixJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDOzRCQUNwRSxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQzt5QkFDdEI7cUJBRUQ7aUJBQ0Q7YUFDRDtRQUNGLENBQUM7UUFFTyw2Q0FBd0IsR0FBaEMsVUFBaUMsSUFBbUQ7WUFBbkQscUJBQUEsRUFBQSxPQUFvQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDO1lBQzdFLElBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUM7Z0JBRTdCLElBQUcsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLEVBQUM7b0JBRXhCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7b0JBQ2hDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDM0MsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztvQkFDekIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDcEMsSUFBSSxhQUFhLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQztvQkFDNUMsSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzNDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQzNCLElBQUksU0FBUyxHQUFHLGFBQWEsR0FBRyxDQUFDLEdBQUcsV0FBVyxDQUFDO3dCQUNoRCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQzt3QkFDaEMsSUFBRyxTQUFTLElBQUksSUFBSSxFQUFDOzRCQUNwQixTQUFTLEdBQUcsQ0FBQyxDQUFDO3lCQUNkO3dCQUNELEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsRUFBRSxFQUFDOzRCQUNuQyxLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUM7eUJBQzdDO3FCQUNEO2lCQUNEO2FBQ0s7UUFDUixDQUFDO1FBNU9EO1lBSEMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQUEsU0FBUyxDQUFDO2FBQ3hCLENBQUM7c0RBQ3NDO1FBS3hDO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQUEsU0FBUyxDQUFDO2dCQUN4QixPQUFPLEVBQUcsS0FBSzthQUNmLENBQUM7bURBR0Q7UUFjRDtZQURDLFFBQVEsQ0FBQyxXQUFBLGNBQWMsQ0FBQztxREFJbUM7UUFLNUQ7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLFdBQUEsY0FBYztnQkFDckIsT0FBTyxFQUFHLEtBQUs7YUFDZixDQUFDO2tEQUdEO1FBU0Q7WUFESSxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7bURBQ3VCO1FBU2pEO1lBUEMsUUFBUSxDQUFDO2dCQUNULE9BQU87b0JBQ04sT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLFdBQUEsU0FBUyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFBLFlBQVksQ0FBQyxPQUFPLENBQUM7Z0JBQzNGLENBQUM7Z0JBQ0QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxPQUFPO2dCQUNqQixPQUFPLEVBQUcsR0FBRzthQUNiLENBQUM7K0NBR0Q7UUFhRDtZQVBDLFFBQVEsQ0FBQztnQkFDVCxPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFBLFNBQVMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsU0FBUyxDQUFDO2dCQUM1RixDQUFDO2dCQUNELElBQUksRUFBRyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7Z0JBQ25CLE9BQU8sRUFBRyxXQUFXO2FBQ3JCLENBQUM7Z0RBR0Q7UUF6RVcsVUFBVTtZQUZ0QixPQUFPLENBQUMsbUJBQW1CLENBQUM7WUFDNUIsSUFBSSxDQUFDLDJDQUEyQyxDQUFDO1dBQ3JDLFVBQVUsQ0FvUHRCO1FBQUQsaUJBQUM7S0FwUEQsQUFvUEMsQ0FwUCtCLFdBQUEsZUFBZSxHQW9QOUM7SUFwUFkscUJBQVUsYUFvUHRCLENBQUE7QUFFRCxDQUFDLEVBM1JNLFVBQVUsS0FBVixVQUFVLFFBMlJoQjtBQzNSRCxJQUFPLFVBQVUsQ0E0UmhCO0FBNVJELFdBQU8sVUFBVTtJQUVYLElBQUEsS0FBNkIsRUFBRSxDQUFDLFVBQVUsRUFBekMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFBLEVBQUUsSUFBSSxVQUFrQixDQUFDO0lBR2pELElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUM7SUFLakIsSUFBTSxFQUFFLEdBQUU7UUFDVCxHQUFHLEVBQUcsTUFBTTtRQUVaLFNBQVMsRUFBRyxJQUFJO1FBQ2hCLFVBQVUsRUFBRyxXQUFBLFlBQVksQ0FBQyxPQUFPO1FBQ2pDLFlBQVksRUFBRyxXQUFXO1FBQzFCLHFCQUFxQixFQUFHLEtBQUs7UUFDN0IsV0FBVyxFQUFHLFNBQVM7UUFDdkIsWUFBWSxFQUFHLFdBQVc7UUFDMUIscUJBQXFCLEVBQUcsS0FBSztRQUM3QixXQUFXLEVBQUcsU0FBUztRQUV2QixVQUFVLEVBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxFQUFFO1FBQzFCLE1BQU0sRUFBRyxjQUFNLE9BQU8sSUFBSSxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3RDLE9BQU8sRUFBRyxFQUFFLENBQUMsSUFBSTtRQUVqQixRQUFRLEVBQUcsQ0FBQztRQUVaLE9BQU8sRUFBRyxVQUFDLEdBQVksRUFBRSxLQUFhLEVBQUUsTUFBYTtZQUNwRCxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUN0QixHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDMUIsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzFCLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUMzQixDQUFDO0tBQ0QsQ0FBQTtJQUlEO1FBQWdDLDhCQUFlO1FBQS9DO1lBQUEscUVBb1BDO1lBbFBBLFVBQUksR0FBVSxFQUFFLENBQUMsR0FBRyxDQUFDO1lBS3JCLGdCQUFVLEdBQWEsV0FBQSxTQUFTLENBQUMsTUFBTSxDQUFDO1lBcUJ4QyxlQUFTLEdBQWtCLElBQUksV0FBQSxjQUFjLENBQzVDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLFVBQVUsRUFDM0IsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMscUJBQXFCLEVBQUUsRUFBRSxDQUFDLFdBQVcsRUFDekQsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMscUJBQXFCLEVBQUUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBZ0I1RCxhQUFPLEdBQTBCLENBQUUsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFFLENBQUM7WUFtQ3RDLGtCQUFZLEdBQVUsQ0FBQyxDQUFDOztRQWtLcEMsQ0FBQztRQXhPQSxzQkFBSSxpQ0FBUztpQkFBYjtnQkFDQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDeEIsQ0FBQztpQkFDRCxVQUFjLEtBQWU7Z0JBQzVCLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUM7b0JBQzNCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO29CQUN4QixJQUFHLEtBQUssSUFBSSxXQUFBLFNBQVMsQ0FBQyxLQUFLLEVBQUM7d0JBQ2YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsT0FBTyxDQUFDO3dCQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7cUJBQ25DO3lCQUFJO3dCQUNELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztxQkFDbEM7aUJBQ1Y7WUFDRixDQUFDOzs7V0FYQTtRQXNCRCxzQkFBSSxnQ0FBUTtpQkFBWjtnQkFDQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDdkIsQ0FBQztpQkFDRCxVQUFhLEtBQW9CO2dCQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztnQkFDdkIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUN0QixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUN4QyxDQUFDOzs7V0FOSDtRQWtCRCxzQkFBSSw2QkFBSztpQkFBVDtnQkFDQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEIsQ0FBQztpQkFDRCxVQUFVLE1BQTJCO2dCQUNwQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQztnQkFDekIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3ZCLENBQUM7OztXQUpBO1FBYUQsc0JBQUksOEJBQU07aUJBQVY7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3JCLENBQUM7aUJBQ0QsVUFBVyxLQUE0QjtnQkFDdEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixDQUFDOzs7V0FKQTtRQVdFLDJCQUFNLEdBQU47WUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2hGLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUM1RSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDeEUsQ0FBQztRQUVELDhCQUFTLEdBQVQ7WUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QixDQUFDO1FBRUosNkJBQVEsR0FBUjtZQUNPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEMsQ0FBQztRQUVELDhCQUFTLEdBQVQ7WUFDTyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFFTyxrQ0FBYSxHQUFyQixVQUFzQixNQUFjO1lBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDcEMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3ZCLENBQUM7UUFLVSx5Q0FBb0IsR0FBNUI7WUFDSSxJQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLFdBQUEsWUFBWSxDQUFDLFNBQVMsRUFBQztnQkFDN0MsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3pCO1FBQ0wsQ0FBQztRQUtJLG1DQUFjLEdBQXRCO1lBQ0MsSUFBRyxJQUFJLENBQUMsT0FBTyxFQUFDO2dCQUNOLElBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFDO29CQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLENBQUUsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFFLENBQUM7aUJBQ2xDO2dCQUNELElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsU0FBUyxFQUFDO29CQUM3QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7b0JBQzNDLElBQUcsSUFBSSxFQUFDO3dCQUNKLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7d0JBQzdCLElBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsRUFBRSxFQUFDOzRCQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDOzRCQUMxQixLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFDO2dDQUN2QixJQUFJLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQ3RCLElBQUcsS0FBSyxJQUFJLElBQUksRUFBQztvQ0FDYixNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO2lDQUMzQjs2QkFDSjt5QkFDSjt3QkFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7cUJBQzVCO2lCQUNKO3FCQUFLLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFBLFNBQVMsQ0FBQyxNQUFNLEVBQUM7b0JBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztpQkFDM0I7Z0JBQ0QsSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFBLFlBQVksQ0FBQyxTQUFTLEVBQUM7b0JBQzdDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUM7aUJBQzdDO3FCQUFJO29CQUNELElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2lCQUNqQzthQUNWO1FBQ0YsQ0FBQztRQUVVLDJDQUFzQixHQUE5QixVQUFnQyxJQUFtRDtZQUFuRCxxQkFBQSxFQUFBLE9BQW9CLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUM7WUFDckYsSUFBRyxJQUFJLEVBQUM7Z0JBR1AsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsSUFBSSxRQUFRLEVBQUU7b0JBQ2IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQ2hFLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksS0FBSyxXQUFBLFlBQVksQ0FBQyxPQUFPLEVBQUM7NEJBQzdDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ25FLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFBLFNBQVMsQ0FBQyxNQUFNLEVBQUM7Z0NBQ25DLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUNsRTtpQ0FBSTtnQ0FDRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO2dDQUMxQixJQUFJLE9BQU8sR0FBWSxFQUFFLENBQUM7Z0NBQzFCLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUM7Z0NBQ3pCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUM7b0NBQ3pDLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lDQUMxRDtnQ0FDRCxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLElBQUksWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0NBQzdGLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7NkJBQzFEO3lCQUVuQjs2QkFBSTs0QkFDSixJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO3lCQUNwRTtxQkFFRDtpQkFDRDthQUNEO1FBQ0MsQ0FBQztRQU1JLHVDQUFrQixHQUExQixVQUEyQixJQUFpQjtZQUMzQyxJQUFJLE1BQU0sR0FBWSxFQUFFLENBQUM7WUFDekIsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztZQUN2QixJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDO1lBQ3pCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUM7Z0JBQ3ZCLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUM5RDtZQUNQLE9BQU8sTUFBTSxDQUFDO1FBQ2YsQ0FBQztRQUVPLDRDQUF1QixHQUEvQixVQUFnQyxJQUFtRDtZQUFuRCxxQkFBQSxFQUFBLE9BQW9CLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUM7WUFDbEYsSUFBRyxJQUFJLEVBQUM7Z0JBRVAsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsSUFBSSxRQUFRLEVBQUU7b0JBQ2IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQ2hFLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksS0FBSyxXQUFBLFlBQVksQ0FBQyxTQUFTLEVBQUM7NEJBQ2pFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ25FLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDO2dDQUMzQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRO2dDQUM3QixJQUFJLEVBQUUsR0FBRyxDQUFDLGlCQUFpQjtnQ0FDM0IsR0FBRyxFQUFFLEVBQUUsQ0FBQyxRQUFROzZCQUNoQixFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzt5QkFFaEI7NkJBQUk7NEJBQ0osSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQzs0QkFDcEUsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7eUJBQ3RCO3FCQUVEO2lCQUNEO2FBQ0Q7UUFDRixDQUFDO1FBRU8sNkNBQXdCLEdBQWhDLFVBQWlDLElBQW1EO1lBQW5ELHFCQUFBLEVBQUEsT0FBb0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQztZQUM3RSxJQUFHLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFDO2dCQUU3QixJQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxFQUFDO29CQUV4QixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO29CQUNoQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzNDLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7b0JBQ3pCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ3BDLElBQUksYUFBYSxHQUFHLFNBQVMsQ0FBQyxhQUFhLENBQUM7b0JBQzVDLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMzQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO3dCQUMzQixJQUFJLFNBQVMsR0FBRyxhQUFhLEdBQUcsQ0FBQyxHQUFHLFdBQVcsQ0FBQzt3QkFDaEQsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7d0JBQ2hDLElBQUcsU0FBUyxJQUFJLElBQUksRUFBQzs0QkFDcEIsU0FBUyxHQUFHLENBQUMsQ0FBQzt5QkFDZDt3QkFDRCxLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBQzs0QkFDbkMsS0FBSyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDO3lCQUM3QztxQkFDRDtpQkFDRDthQUNLO1FBQ1IsQ0FBQztRQTVPRDtZQUhDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFBLFNBQVMsQ0FBQzthQUN4QixDQUFDO3NEQUNzQztRQUt4QztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFBLFNBQVMsQ0FBQztnQkFDeEIsT0FBTyxFQUFHLEtBQUs7YUFDZixDQUFDO21EQUdEO1FBY0Q7WUFEQyxRQUFRLENBQUMsV0FBQSxjQUFjLENBQUM7cURBSW1DO1FBSzVEO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxXQUFBLGNBQWM7Z0JBQ3JCLE9BQU8sRUFBRyxLQUFLO2FBQ2YsQ0FBQztrREFHRDtRQVNEO1lBREksUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDO21EQUN1QjtRQVNqRDtZQVBDLFFBQVEsQ0FBQztnQkFDVCxPQUFPO29CQUNOLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFBLFNBQVMsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksV0FBQSxZQUFZLENBQUMsT0FBTyxDQUFDO2dCQUMzRixDQUFDO2dCQUNELElBQUksRUFBRyxFQUFFLENBQUMsT0FBTztnQkFDakIsT0FBTyxFQUFHLEdBQUc7YUFDYixDQUFDOytDQUdEO1FBYUQ7WUFQQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTztvQkFDTixPQUFPLElBQUksQ0FBQyxVQUFVLElBQUksV0FBQSxTQUFTLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLFdBQUEsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDNUYsQ0FBQztnQkFDRCxJQUFJLEVBQUcsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO2dCQUNuQixPQUFPLEVBQUcsV0FBVzthQUNyQixDQUFDO2dEQUdEO1FBekVXLFVBQVU7WUFGdEIsT0FBTyxDQUFDLG1CQUFtQixDQUFDO1lBQzVCLElBQUksQ0FBQywyQ0FBMkMsQ0FBQztXQUNyQyxVQUFVLENBb1B0QjtRQUFELGlCQUFDO0tBcFBELEFBb1BDLENBcFArQixXQUFBLGVBQWUsR0FvUDlDO0lBcFBZLHFCQUFVLGFBb1B0QixDQUFBO0FBRUQsQ0FBQyxFQTVSTSxVQUFVLEtBQVYsVUFBVSxRQTRSaEI7QUM1UkQsSUFBTyxVQUFVLENBa0toQjtBQWxLRCxXQUFPLFVBQVU7SUFFWCxJQUFBLEtBQTRCLEVBQUUsQ0FBQyxVQUFVLEVBQXhDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBQSxFQUFFLElBQUksVUFBaUIsQ0FBQztJQU9oRDtRQUFvQyxrQ0FBZTtRQUFuRDtZQUFBLHFFQXVKQztZQXBKQSxlQUFTLEdBQWdCLElBQUksQ0FBQztZQWdCOUIsa0JBQVksR0FBbUIsSUFBSSxDQUFDO1lBcUJwQyxpQkFBVyxHQUFtQixJQUFJLENBQUM7WUFpQm5DLGFBQU8sR0FBVyxHQUFHLENBQUM7WUFldEIsa0JBQVksR0FBZ0IsSUFBSSxDQUFDO1lBQ2pDLHVCQUFpQixHQUFxQixJQUFJLENBQUM7WUFDM0Msc0JBQWdCLEdBQXFCLElBQUksQ0FBQztZQUMxQyxrQkFBWSxHQUFlLElBQUksQ0FBQzs7UUE0RWpDLENBQUM7UUEvSUEsc0JBQUksb0NBQVE7aUJBQVo7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3ZCLENBQUM7aUJBQ0QsVUFBYSxLQUFpQjtnQkFDN0IsSUFBRyxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBQztvQkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDeEM7WUFDRixDQUFDOzs7V0FOQTtRQWNELHNCQUFJLHVDQUFXO2lCQUFmO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztZQUMxQixDQUFDO2lCQUNELFVBQWdCLEtBQW9CO2dCQUNuQyxJQUFHLElBQUksQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFDO29CQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztvQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7b0JBQzNDLElBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFDO3dCQUNwQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztxQkFDeEI7eUJBQUk7d0JBQ1EsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO3FCQUMxQjtpQkFDVjtZQUNGLENBQUM7OztXQVhBO1FBbUJELHNCQUFJLHNDQUFVO2lCQUFkO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUN6QixDQUFDO2lCQUNELFVBQWUsS0FBb0I7Z0JBQ2xDLElBQUcsSUFBSSxDQUFDLFdBQVcsSUFBSSxLQUFLLEVBQUM7b0JBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO29CQUN6QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztvQkFDakMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2lCQUNoQztZQUNGLENBQUM7OztXQVBBO1FBZUQsc0JBQUksa0NBQU07aUJBQVY7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3JCLENBQUM7aUJBQ0QsVUFBVyxLQUFZO2dCQUN0QixJQUFHLElBQUksQ0FBQyxPQUFPLElBQUksS0FBSyxFQUFDO29CQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztvQkFDckIsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2lCQUNoQztZQUNGLENBQUM7OztXQU5BO1FBZUQsaUNBQVEsR0FBUjtZQUNDLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7Z0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxpQkFBaUIsRUFBRSxhQUFhLENBQUMsQ0FBQztnQkFDbkYsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztvQkFDMUIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLGlCQUFpQixDQUFDLENBQUM7b0JBQ2xELE1BQU0sQ0FBQyxHQUFHLEdBQUcsYUFBYSxDQUFDO29CQUMzQixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDdkIsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUM7aUJBQ2hDO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFDO2dCQUN6QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsaUJBQWlCLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQ2pGLElBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUM7b0JBQ3pCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxpQkFBaUIsQ0FBQyxDQUFDO29CQUNsRCxNQUFNLENBQUMsR0FBRyxHQUFHLFlBQVksQ0FBQztvQkFDMUIsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQztvQkFDcEMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO29CQUN0QyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUMxQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsWUFBWSxDQUFDO29CQUM1QyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7b0JBRTdDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUM7aUJBQy9CO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztnQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQ25FLElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO29CQUNyQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDO29CQUN0QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFDO29CQUNoRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7b0JBQ3ZDLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxhQUFhLENBQUM7b0JBQ2hELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztvQkFDdkMsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUM7b0JBQzNCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztpQkFDNUI7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO2dCQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztnQkFDcEQsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7b0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQyxDQUFDO2lCQUNwRDthQUNLO1lBQ1AsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RCLENBQUM7UUFLVSx3Q0FBZSxHQUF2QjtZQUNJLElBQUcsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFDO2dCQUVyQyxJQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFDO29CQUM5RixJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztpQkFDdkM7cUJBQUk7b0JBQ0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7aUJBQ3RDO2dCQUNELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDaEM7UUFDTCxDQUFDO1FBS0Usc0NBQWEsR0FBbkI7Ozs7OztpQ0FDSSxTQUFTLEVBQVQsY0FBUztpQ0FDUixDQUFDLElBQUksQ0FBQyxTQUFTLEVBQWYsY0FBZTs0QkFDakIsS0FBQSxJQUFJLENBQUE7NEJBQVksV0FBTSxXQUFBLEtBQUssQ0FBQyxjQUFjLENBQWMsV0FBQSxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBQTs7NEJBQTFGLEdBQUssUUFBUSxHQUFHLFNBQTBFLENBQUM7Ozs7OztTQUc3RjtRQW5KRDtZQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO3lEQUNRO1FBSzlCO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsUUFBUTtnQkFDbEIsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQztzREFHRDtRQVNEO1lBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7NERBQ1c7UUFLcEM7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxXQUFXO2dCQUNyQixPQUFPLEVBQUcsT0FBTzthQUNqQixDQUFDO3lEQUdEO1FBY0Q7WUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzsyREFDVTtRQUtuQztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLFdBQVc7Z0JBQ3JCLE9BQU8sRUFBRyxPQUFPO2FBQ2pCLENBQUM7d0RBR0Q7UUFVRDtZQURDLFFBQVEsRUFBRTt1REFDVztRQUt0QjtZQUpDLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUcsSUFBSTtnQkFDZCxLQUFLLEVBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQzthQUNwQixDQUFDO29EQUdEO1FBaEVXLGNBQWM7WUFGMUIsT0FBTyxDQUFDLDJCQUEyQixDQUFDO1lBQ3BDLElBQUksQ0FBQywrQ0FBK0MsQ0FBQztXQUN6QyxjQUFjLENBdUoxQjtRQUFELHFCQUFDO0tBdkpELEFBdUpDLENBdkptQyxXQUFBLGVBQWUsR0F1SmxEO0lBdkpZLHlCQUFjLGlCQXVKMUIsQ0FBQTtBQUVELENBQUMsRUFsS00sVUFBVSxLQUFWLFVBQVUsUUFrS2hCO0FDbEtELElBQU8sVUFBVSxDQXdWaEI7QUF4VkQsV0FBTyxVQUFVO0lBRVgsSUFBQSxLQUE0QixFQUFFLENBQUMsVUFBVSxFQUF4QyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxJQUFJLFVBQWlCLENBQUM7SUFJaEQ7UUFBc0Msb0NBQWU7UUFBckQ7WUFBQSxxRUFnVkM7WUE3VUEsZUFBUyxHQUFnQixJQUFJLENBQUM7WUFnQjlCLGtCQUFZLEdBQW1CLElBQUksQ0FBQztZQWdCcEMsaUJBQVcsR0FBYSxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztZQWN2QyxpQkFBVyxHQUFXLEdBQUcsQ0FBQztZQWdCMUIsaUJBQVcsR0FBVyxHQUFHLENBQUM7WUFlMUIscUJBQWUsR0FBWSxJQUFJLENBQUM7WUFlaEMsZ0JBQVUsR0FBWSxJQUFJLENBQUM7WUFlM0IsZ0JBQVUsR0FBVyxDQUFDLENBQUMsQ0FBQztZQWV4QixnQkFBVSxHQUFXLENBQUMsQ0FBQztZQWV2QixpQkFBVyxHQUFXLENBQUMsQ0FBQztZQWdCeEIsaUJBQVcsR0FBVyxDQUFDLENBQUM7WUFjeEIsa0JBQVksR0FBZ0IsSUFBSSxDQUFDO1lBQ2pDLHVCQUFpQixHQUFxQixJQUFJLENBQUM7WUFDM0Msc0JBQWdCLEdBQWUsSUFBSSxDQUFDO1lBQ3BDLHNCQUFnQixHQUFlLElBQUksQ0FBQztZQUNwQyxzQkFBZ0IsR0FBZSxJQUFJLENBQUM7WUFDcEMsMEJBQW9CLEdBQWUsSUFBSSxDQUFDO1lBQ3hDLHFCQUFlLEdBQWUsSUFBSSxDQUFDO1lBQ25DLHFCQUFlLEdBQWUsSUFBSSxDQUFDO1lBQ25DLHFCQUFlLEdBQWUsSUFBSSxDQUFDO1lBQ25DLHNCQUFnQixHQUFlLElBQUksQ0FBQztZQUNwQyxzQkFBZ0IsR0FBZSxJQUFJLENBQUM7O1FBNEpyQyxDQUFDO1FBeFVBLHNCQUFJLHNDQUFRO2lCQUFaO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUN2QixDQUFDO2lCQUNELFVBQWEsS0FBaUI7Z0JBQzdCLElBQUcsSUFBSSxDQUFDLFNBQVMsSUFBSSxLQUFLLEVBQUM7b0JBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7aUJBQ3hDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRCxzQkFBSSx5Q0FBVztpQkFBZjtnQkFDQyxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDMUIsQ0FBQztpQkFDRCxVQUFnQixLQUFvQjtnQkFDbkMsSUFBRyxJQUFJLENBQUMsWUFBWSxJQUFJLEtBQUssRUFBQztvQkFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7b0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2lCQUMzQztZQUNGLENBQUM7OztXQU5BO1FBY0Qsc0JBQUksd0NBQVU7aUJBQWQ7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ3pCLENBQUM7aUJBQ0QsVUFBZSxLQUFjO2dCQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztnQkFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDckMsQ0FBQzs7O1dBSkE7UUFZRCxzQkFBSSx3Q0FBVTtpQkFBZDtnQkFDQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDekIsQ0FBQztpQkFDRCxVQUFlLEtBQVk7Z0JBQzFCLElBQUcsSUFBSSxDQUFDLFdBQVcsSUFBSSxLQUFLLEVBQUM7b0JBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO29CQUN6QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztpQkFDcEM7WUFDRixDQUFDOzs7V0FOQTtRQWFELHNCQUFJLHdDQUFVO2lCQUFkO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUN6QixDQUFDO2lCQUNELFVBQWUsS0FBWTtnQkFDMUIsSUFBRyxJQUFJLENBQUMsV0FBVyxJQUFJLEtBQUssRUFBQztvQkFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7b0JBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2lCQUNwQztZQUNGLENBQUM7OztXQU5BO1FBYUQsc0JBQUksNENBQWM7aUJBQWxCO2dCQUNDLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUM3QixDQUFDO2lCQUNELFVBQW1CLEtBQWE7Z0JBQy9CLElBQUcsSUFBSSxDQUFDLGVBQWUsSUFBSSxLQUFLLEVBQUM7b0JBQ2hDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO29CQUM3QixJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2hEO1lBQ0YsQ0FBQzs7O1dBTkE7UUFhRCxzQkFBSSx1Q0FBUztpQkFBYjtnQkFDQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDeEIsQ0FBQztpQkFDRCxVQUFjLEtBQWE7Z0JBQzFCLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUM7b0JBQzNCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO29CQUN4QixJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMzQztZQUNGLENBQUM7OztXQU5BO1FBYUQsc0JBQUksdUNBQVM7aUJBQWI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3hCLENBQUM7aUJBQ0QsVUFBYyxLQUFZO2dCQUN6QixJQUFHLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSyxFQUFDO29CQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztvQkFDeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2lCQUNuQztZQUNGLENBQUM7OztXQU5BO1FBYUQsc0JBQUksdUNBQVM7aUJBQWI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3hCLENBQUM7aUJBQ0QsVUFBYyxLQUFZO2dCQUN6QixJQUFHLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSyxFQUFDO29CQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztvQkFDeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2lCQUNuQztZQUNGLENBQUM7OztXQU5BO1FBY0Qsc0JBQUksd0NBQVU7aUJBQWQ7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ3pCLENBQUM7aUJBQ0QsVUFBZSxLQUFZO2dCQUMxQixJQUFHLElBQUksQ0FBQyxXQUFXLElBQUksS0FBSyxFQUFDO29CQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztvQkFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQ3BDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFhRCxzQkFBSSx3Q0FBVTtpQkFBZDtnQkFDQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDekIsQ0FBQztpQkFDRCxVQUFlLEtBQVk7Z0JBQzFCLElBQUcsSUFBSSxDQUFDLFdBQVcsSUFBSSxLQUFLLEVBQUM7b0JBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO29CQUN6QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztpQkFDcEM7WUFDRixDQUFDOzs7V0FOQTtRQXNCRSxtQ0FBUSxHQUFSO1lBQ0YsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztnQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLGlCQUFpQixFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUNuRixJQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO29CQUMxQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsaUJBQWlCLENBQUMsQ0FBQztvQkFDbEQsTUFBTSxDQUFDLEdBQUcsR0FBRyxhQUFhLENBQUM7b0JBQzNCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUN2QixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDcEIsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3hCLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLFdBQUEsWUFBWSxDQUFDLFNBQVMsQ0FBQztvQkFDaEQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE1BQU0sQ0FBQztpQkFDaEM7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUM7Z0JBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQzNFLElBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUM7b0JBQ3pCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxZQUFZLENBQUM7b0JBQzFCLE1BQU0sQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO29CQUM1QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7b0JBQ3BELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGNBQWMsQ0FBQztvQkFDM0MsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGlCQUFpQixDQUFDO29CQUNwRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxjQUFjLENBQUM7b0JBQzNDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUM7b0JBQy9CLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztpQkFDaEM7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUM7Z0JBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQzNFLElBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUM7b0JBQ3pCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxZQUFZLENBQUM7b0JBQzFCLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQztvQkFDcEQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsY0FBYyxDQUFDO29CQUMzQyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7b0JBQ3BELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGNBQWMsQ0FBQztvQkFDM0MsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQztvQkFDL0IsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO2lCQUNoQzthQUNEO1lBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBQztnQkFDekIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLFdBQVcsRUFBRSxZQUFZLENBQUMsQ0FBQztnQkFDM0UsSUFBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBQztvQkFDekIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxNQUFNLENBQUMsR0FBRyxHQUFHLFlBQVksQ0FBQztvQkFDMUIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGlCQUFpQixDQUFDO29CQUNwRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxjQUFjLENBQUM7b0JBQzNDLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQztvQkFDcEQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsY0FBYyxDQUFDO29CQUMzQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsTUFBTSxDQUFDO29CQUMvQixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7aUJBQ2hDO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFDO2dCQUM3QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsV0FBVyxFQUFFLGdCQUFnQixDQUFDLENBQUM7Z0JBQ25GLElBQUcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUM7b0JBQzdCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDOUIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLHFCQUFxQixDQUFDO29CQUN4RCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxrQkFBa0IsQ0FBQztvQkFDL0MsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLHFCQUFxQixDQUFDO29CQUN4RCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxrQkFBa0IsQ0FBQztvQkFDL0MsSUFBSSxDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQztvQkFDbkMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDNUM7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFDO2dCQUN4QixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLFdBQVcsRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFDekUsSUFBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUM7b0JBQ3hCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUM7b0JBQ3pCLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbkQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDO29CQUMxQyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ25ELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztvQkFDMUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUM7b0JBQzlCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3ZDO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBQztnQkFDeEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ3pFLElBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFDO29CQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsV0FBVyxDQUFDO29CQUN6QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ25ELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztvQkFDMUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGdCQUFnQixDQUFDO29CQUNuRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUM7b0JBQzFDLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDO29CQUM5QixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7aUJBQy9CO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBQztnQkFDeEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ3pFLElBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFDO29CQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsV0FBVyxDQUFDO29CQUN6QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ25ELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztvQkFDMUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGdCQUFnQixDQUFDO29CQUNuRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUM7b0JBQzFDLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDO29CQUM5QixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7aUJBQy9CO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFDO2dCQUN6QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFDO2dCQUMzRSxJQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFDO29CQUN6QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsWUFBWSxDQUFDO29CQUMxQixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7b0JBQ3BELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGNBQWMsQ0FBQztvQkFDM0MsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGlCQUFpQixDQUFDO29CQUNwRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxjQUFjLENBQUM7b0JBQzNDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUM7b0JBQy9CLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztpQkFDaEM7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUM7Z0JBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQzNFLElBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUM7b0JBQ3pCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxZQUFZLENBQUM7b0JBQzFCLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQztvQkFDcEQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsY0FBYyxDQUFDO29CQUMzQyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7b0JBQ3BELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGNBQWMsQ0FBQztvQkFDM0MsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQztvQkFDL0IsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO2lCQUNoQzthQUNEO1lBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7Z0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQyxDQUFDO2dCQUNwRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztvQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7aUJBQ3BEO2FBQ0Q7WUFDRCxJQUFHLFNBQVMsRUFBQztnQkFDWixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7YUFDckI7UUFDRixDQUFDO1FBS0ssd0NBQWEsR0FBbkI7Ozs7OztpQ0FDSSxTQUFTLEVBQVQsY0FBUztpQ0FDUixDQUFDLElBQUksQ0FBQyxTQUFTLEVBQWYsY0FBZTs0QkFDakIsS0FBQSxJQUFJLENBQUE7NEJBQVksV0FBTSxXQUFBLEtBQUssQ0FBQyxjQUFjLENBQWMsV0FBQSxJQUFJLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBQTs7NEJBQTdGLEdBQUssUUFBUSxHQUFHLFNBQTZFLENBQUM7Ozs7OztTQUdoRztRQTVVRDtZQURJLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDOzJEQUNLO1FBSzlCO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsUUFBUTtnQkFDbEIsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQzt3REFHRDtRQVNEO1lBREksUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7OERBQ1E7UUFLcEM7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxXQUFXO2dCQUNyQixPQUFPLEVBQUcsT0FBTzthQUNqQixDQUFDOzJEQUdEO1FBU0Q7WUFESSxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzs2REFDaUI7UUFLdkM7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxLQUFLO2dCQUNmLE9BQU8sRUFBRyxNQUFNO2FBQ2hCLENBQUM7MERBR0Q7UUFPRDtZQURJLFFBQVEsRUFBRTs2REFDWTtRQUsxQjtZQUpDLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUcsUUFBUTtnQkFDbEIsS0FBSyxFQUFHLENBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7YUFDbkIsQ0FBQzswREFHRDtRQVNEO1lBREksUUFBUSxFQUFFOzZEQUNZO1FBSTFCO1lBSEMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxNQUFNO2FBQ2hCLENBQUM7MERBR0Q7UUFTRDtZQURJLFFBQVEsRUFBRTtpRUFDa0I7UUFJaEM7WUFIQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTyxFQUFHLFFBQVE7YUFDbEIsQ0FBQzs4REFHRDtRQVNEO1lBREksUUFBUSxFQUFFOzREQUNhO1FBSTNCO1lBSEMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxZQUFZO2FBQ3RCLENBQUM7eURBR0Q7UUFTRDtZQURJLFFBQVEsRUFBRTs0REFDVTtRQUl4QjtZQUhDLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDO3lEQUdEO1FBU0Q7WUFESSxRQUFRLEVBQUU7NERBQ1M7UUFJdkI7WUFIQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQzt5REFHRDtRQVNEO1lBREksUUFBUSxFQUFFOzZEQUNVO1FBS3hCO1lBSkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxNQUFNO2dCQUNoQixLQUFLLEVBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQzthQUNoQixDQUFDOzBEQUdEO1FBU0Q7WUFESSxRQUFRLEVBQUU7NkRBQ1U7UUFJeEI7WUFIQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQzswREFHRDtRQWxLVyxnQkFBZ0I7WUFGNUIsT0FBTyxDQUFDLDZCQUE2QixDQUFDO1lBQ3RDLElBQUksQ0FBQyxpREFBaUQsQ0FBQztXQUMzQyxnQkFBZ0IsQ0FnVjVCO1FBQUQsdUJBQUM7S0FoVkQsQUFnVkMsQ0FoVnFDLFdBQUEsZUFBZSxHQWdWcEQ7SUFoVlksMkJBQWdCLG1CQWdWNUIsQ0FBQTtBQUVELENBQUMsRUF4Vk0sVUFBVSxLQUFWLFVBQVUsUUF3VmhCO0FDeFZELElBQU8sVUFBVSxDQTRHaEI7QUE1R0QsV0FBTyxVQUFVO0lBRVgsSUFBQSxLQUE0QixFQUFFLENBQUMsVUFBVSxFQUF4QyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxJQUFJLFVBQWlCLENBQUM7SUFJaEQ7UUFBdUMscUNBQWU7UUFBdEQ7WUFBQSxxRUFvR0M7WUFqR0EsZUFBUyxHQUFnQixJQUFJLENBQUM7WUFnQjlCLGtCQUFZLEdBQW1CLElBQUksQ0FBQztZQWdCcEMsWUFBTSxHQUFXLENBQUMsQ0FBQztZQWNuQixrQkFBWSxHQUFnQixJQUFJLENBQUM7WUFDakMsdUJBQWlCLEdBQXFCLElBQUksQ0FBQztZQUMzQyxpQkFBVyxHQUFlLElBQUksQ0FBQzs7UUFpRGhDLENBQUM7UUE1RkEsc0JBQUksdUNBQVE7aUJBQVo7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3ZCLENBQUM7aUJBQ0QsVUFBYSxLQUFpQjtnQkFDN0IsSUFBRyxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBQztvQkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDeEM7WUFDRixDQUFDOzs7V0FOQTtRQWNELHNCQUFJLDBDQUFXO2lCQUFmO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztZQUMxQixDQUFDO2lCQUNELFVBQWdCLEtBQW9CO2dCQUNuQyxJQUFHLElBQUksQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFDO29CQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztvQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7aUJBQzNDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFhRCxzQkFBSSxvQ0FBSztpQkFBVDtnQkFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDcEIsQ0FBQztpQkFDRCxVQUFVLEtBQVk7Z0JBQ3JCLElBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUM7b0JBQ3ZCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQy9CO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRSxvQ0FBUSxHQUFSO1lBQ0YsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztnQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLGlCQUFpQixFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUNuRixJQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO29CQUMxQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsaUJBQWlCLENBQUMsQ0FBQztvQkFDbEQsTUFBTSxDQUFDLEdBQUcsR0FBRyxhQUFhLENBQUM7b0JBQzNCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO29CQUNwQixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDdkIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE1BQU0sQ0FBQztpQkFDaEM7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFDO2dCQUNwQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLFdBQVcsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDakUsSUFBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUM7b0JBQ3BCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUM7b0JBQ3JCLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxZQUFZLENBQUM7b0JBQy9DLE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQztvQkFDdEMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLFlBQVksQ0FBQztvQkFDL0MsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO29CQUN0QyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztvQkFDakIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7aUJBQzFCO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztnQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7Z0JBQ3BELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO29CQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztpQkFDcEQ7YUFDRDtZQUNELElBQUcsU0FBUyxFQUFDO2dCQUNaLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUNyQjtRQUNGLENBQUM7UUFLSyx5Q0FBYSxHQUFuQjs7Ozs7O2lDQUNJLFNBQVMsRUFBVCxjQUFTO2lDQUNSLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBZixjQUFlOzRCQUNqQixLQUFBLElBQUksQ0FBQTs0QkFBWSxXQUFNLFdBQUEsS0FBSyxDQUFDLGNBQWMsQ0FBYyxXQUFBLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsQ0FBQyxFQUFBOzs0QkFBOUYsR0FBSyxRQUFRLEdBQUcsU0FBOEUsQ0FBQzs7Ozs7O1NBR2pHO1FBaEdEO1lBREksUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7NERBQ0s7UUFLOUI7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxRQUFRO2dCQUNsQixPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDO3lEQUdEO1FBU0Q7WUFESSxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzsrREFDUTtRQUtwQztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLFdBQVc7Z0JBQ3JCLE9BQU8sRUFBRyxPQUFPO2FBQ2pCLENBQUM7NERBR0Q7UUFTRDtZQURJLFFBQVEsRUFBRTt5REFDSztRQUluQjtZQUhDLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDO3NEQUdEO1FBekNXLGlCQUFpQjtZQUY3QixPQUFPLENBQUMsOEJBQThCLENBQUM7WUFDdkMsSUFBSSxDQUFDLGtEQUFrRCxDQUFDO1dBQzVDLGlCQUFpQixDQW9HN0I7UUFBRCx3QkFBQztLQXBHRCxBQW9HQyxDQXBHc0MsV0FBQSxlQUFlLEdBb0dyRDtJQXBHWSw0QkFBaUIsb0JBb0c3QixDQUFBO0FBRUQsQ0FBQyxFQTVHTSxVQUFVLEtBQVYsVUFBVSxRQTRHaEI7QUM1R0QsSUFBTyxVQUFVLENBaUZoQjtBQWpGRCxXQUFPLFVBQVU7SUFFWCxJQUFBLEtBQTRCLEVBQUUsQ0FBQyxVQUFVLEVBQXhDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBQSxFQUFFLElBQUksVUFBaUIsQ0FBQztJQUloRDtRQUF3QyxzQ0FBZTtRQUF2RDtZQUFBLHFFQXlFQztZQXRFQSxlQUFTLEdBQWdCLElBQUksQ0FBQztZQWdCOUIsa0JBQVksR0FBbUIsSUFBSSxDQUFDO1lBZXBDLGtCQUFZLEdBQWdCLElBQUksQ0FBQztZQUNqQyx1QkFBaUIsR0FBcUIsSUFBSSxDQUFDOztRQXNDNUMsQ0FBQztRQWpFQSxzQkFBSSx3Q0FBUTtpQkFBWjtnQkFDQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDdkIsQ0FBQztpQkFDRCxVQUFhLEtBQWlCO2dCQUM3QixJQUFHLElBQUksQ0FBQyxTQUFTLElBQUksS0FBSyxFQUFDO29CQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUN4QztZQUNGLENBQUM7OztXQU5BO1FBY0Qsc0JBQUksMkNBQVc7aUJBQWY7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQzFCLENBQUM7aUJBQ0QsVUFBZ0IsS0FBb0I7Z0JBQ25DLElBQUcsSUFBSSxDQUFDLFlBQVksSUFBSSxLQUFLLEVBQUM7b0JBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO29CQUMxQixJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztpQkFDM0M7WUFDRixDQUFDOzs7V0FOQTtRQWFELHFDQUFRLEdBQVI7WUFDQyxJQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO2dCQUMxQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsaUJBQWlCLEVBQUUsYUFBYSxDQUFDLENBQUM7Z0JBQ25GLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7b0JBQzFCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxpQkFBaUIsQ0FBQyxDQUFDO29CQUNsRCxNQUFNLENBQUMsR0FBRyxHQUFHLGFBQWEsQ0FBQztvQkFDM0IsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBQ3ZCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO29CQUNwQixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDeEIsTUFBTSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7b0JBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUM7aUJBQ2hDO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztnQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7Z0JBQ3BELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO29CQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztpQkFDcEQ7YUFDRDtZQUNELElBQUcsU0FBUyxFQUFDO2dCQUNaLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUNyQjtRQUNGLENBQUM7UUFLSywwQ0FBYSxHQUFuQjs7Ozs7O2lDQUNJLFNBQVMsRUFBVCxjQUFTO2lDQUNSLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBZixjQUFlOzRCQUNqQixLQUFBLElBQUksQ0FBQTs0QkFBWSxXQUFNLFdBQUEsS0FBSyxDQUFDLGNBQWMsQ0FBYyxXQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxFQUFBOzs0QkFBL0YsR0FBSyxRQUFRLEdBQUcsU0FBK0UsQ0FBQzs7Ozs7O1NBR2xHO1FBckVEO1lBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7NkRBQ1E7UUFLOUI7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxRQUFRO2dCQUNsQixPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDOzBEQUdEO1FBU0Q7WUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQztnRUFDVztRQUtwQztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLFdBQVc7Z0JBQ3JCLE9BQU8sRUFBRyxPQUFPO2FBQ2pCLENBQUM7NkRBR0Q7UUExQlcsa0JBQWtCO1lBRjlCLE9BQU8sQ0FBQywrQkFBK0IsQ0FBQztZQUN4QyxJQUFJLENBQUMsbURBQW1ELENBQUM7V0FDN0Msa0JBQWtCLENBeUU5QjtRQUFELHlCQUFDO0tBekVELEFBeUVDLENBekV1QyxXQUFBLGVBQWUsR0F5RXREO0lBekVZLDZCQUFrQixxQkF5RTlCLENBQUE7QUFFRCxDQUFDLEVBakZNLFVBQVUsS0FBVixVQUFVLFFBaUZoQjtBQ2pGRCxJQUFPLFVBQVUsQ0FzTWhCO0FBdE1ELFdBQU8sVUFBVTtJQUVYLElBQUEsS0FBNEIsRUFBRSxDQUFDLFVBQVUsRUFBeEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFBLEVBQUUsSUFBSSxVQUFpQixDQUFDO0lBSWhEO1FBQXFDLG1DQUFlO1FBQXBEO1lBQUEscUVBOExDO1lBM0xBLGVBQVMsR0FBZ0IsSUFBSSxDQUFDO1lBZ0I5QixrQkFBWSxHQUFtQixJQUFJLENBQUM7WUFnQnBDLGdCQUFVLEdBQWEsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFjdkMsb0JBQWMsR0FBVyxHQUFHLENBQUM7WUFnQjdCLG9CQUFjLEdBQVcsR0FBRyxDQUFDO1lBZ0I3QixnQkFBVSxHQUFXLEdBQUcsQ0FBQztZQWN6QixrQkFBWSxHQUFnQixJQUFJLENBQUM7WUFDakMsdUJBQWlCLEdBQXFCLElBQUksQ0FBQztZQUMzQyxxQkFBZSxHQUFlLElBQUksQ0FBQztZQUNuQyx5QkFBbUIsR0FBZSxJQUFJLENBQUM7WUFDdkMseUJBQW1CLEdBQWUsSUFBSSxDQUFDO1lBQ3ZDLHFCQUFlLEdBQWUsSUFBSSxDQUFDOztRQTBGcEMsQ0FBQztRQXRMQSxzQkFBSSxxQ0FBUTtpQkFBWjtnQkFDQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDdkIsQ0FBQztpQkFDRCxVQUFhLEtBQWlCO2dCQUM3QixJQUFHLElBQUksQ0FBQyxTQUFTLElBQUksS0FBSyxFQUFDO29CQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUN4QztZQUNGLENBQUM7OztXQU5BO1FBY0Qsc0JBQUksd0NBQVc7aUJBQWY7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQzFCLENBQUM7aUJBQ0QsVUFBZ0IsS0FBb0I7Z0JBQ25DLElBQUcsSUFBSSxDQUFDLFlBQVksSUFBSSxLQUFLLEVBQUM7b0JBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO29CQUMxQixJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztpQkFDM0M7WUFDRixDQUFDOzs7V0FOQTtRQWNELHNCQUFJLHNDQUFTO2lCQUFiO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUN4QixDQUFDO2lCQUNELFVBQWMsS0FBYztnQkFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNwQyxDQUFDOzs7V0FKQTtRQVlELHNCQUFJLDBDQUFhO2lCQUFqQjtnQkFDQyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7WUFDNUIsQ0FBQztpQkFDRCxVQUFrQixLQUFZO2dCQUM3QixJQUFHLElBQUksQ0FBQyxjQUFjLElBQUksS0FBSyxFQUFDO29CQUMvQixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztvQkFDNUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQ3ZDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRCxzQkFBSSwwQ0FBYTtpQkFBakI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO1lBQzVCLENBQUM7aUJBQ0QsVUFBa0IsS0FBWTtnQkFDN0IsSUFBRyxJQUFJLENBQUMsY0FBYyxJQUFJLEtBQUssRUFBQztvQkFDL0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7b0JBQzVCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2lCQUN2QztZQUNGLENBQUM7OztXQU5BO1FBYUQsc0JBQUksc0NBQVM7aUJBQWI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3hCLENBQUM7aUJBQ0QsVUFBYyxLQUFZO2dCQUN6QixJQUFHLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSyxFQUFDO29CQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztvQkFDeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2lCQUNuQztZQUNGLENBQUM7OztXQU5BO1FBaUJFLGtDQUFRLEdBQVI7WUFDRixJQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO2dCQUMxQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsaUJBQWlCLEVBQUUsYUFBYSxDQUFDLENBQUM7Z0JBQ25GLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7b0JBQzFCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxpQkFBaUIsQ0FBQyxDQUFDO29CQUNsRCxNQUFNLENBQUMsR0FBRyxHQUFHLGFBQWEsQ0FBQztvQkFDM0IsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBQ3ZCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO29CQUNwQixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDeEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE1BQU0sQ0FBQztpQkFDaEM7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFDO2dCQUN4QixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLFdBQVcsRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFDekUsSUFBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUM7b0JBQ3hCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUM7b0JBQ3pCLE1BQU0sQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO29CQUM1QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ25ELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztvQkFDMUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGdCQUFnQixDQUFDO29CQUNuRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUM7b0JBQzFDLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDO29CQUM5QixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7aUJBQy9CO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFDO2dCQUM1QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsV0FBVyxFQUFFLGVBQWUsQ0FBQyxDQUFDO2dCQUNqRixJQUFHLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFDO29CQUM1QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsZUFBZSxDQUFDO29CQUM3QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsb0JBQW9CLENBQUM7b0JBQ3ZELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGlCQUFpQixDQUFDO29CQUM5QyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsb0JBQW9CLENBQUM7b0JBQ3ZELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGlCQUFpQixDQUFDO29CQUM5QyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsTUFBTSxDQUFDO29CQUNsQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7aUJBQ25DO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFDO2dCQUM1QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsV0FBVyxFQUFFLGVBQWUsQ0FBQyxDQUFDO2dCQUNqRixJQUFHLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFDO29CQUM1QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsZUFBZSxDQUFDO29CQUM3QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsb0JBQW9CLENBQUM7b0JBQ3ZELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGlCQUFpQixDQUFDO29CQUM5QyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsb0JBQW9CLENBQUM7b0JBQ3ZELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGlCQUFpQixDQUFDO29CQUM5QyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsTUFBTSxDQUFDO29CQUNsQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7aUJBQ25DO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBQztnQkFDeEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ3pFLElBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFDO29CQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsV0FBVyxDQUFDO29CQUN6QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ25ELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztvQkFDMUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGdCQUFnQixDQUFDO29CQUNuRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUM7b0JBQzFDLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDO29CQUM5QixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7aUJBQy9CO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztnQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7Z0JBQ3BELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO29CQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztpQkFDcEQ7YUFDRDtZQUNELElBQUcsU0FBUyxFQUFDO2dCQUNaLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUNyQjtRQUNGLENBQUM7UUFLSyx1Q0FBYSxHQUFuQjs7Ozs7O2lDQUNJLFNBQVMsRUFBVCxjQUFTO2lDQUNSLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBZixjQUFlOzRCQUNqQixLQUFBLElBQUksQ0FBQTs0QkFBWSxXQUFNLFdBQUEsS0FBSyxDQUFDLGNBQWMsQ0FBYyxXQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxFQUFBOzs0QkFBNUYsR0FBSyxRQUFRLEdBQUcsU0FBNEUsQ0FBQzs7Ozs7O1NBRy9GO1FBMUxEO1lBREksUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7MERBQ0s7UUFLOUI7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxRQUFRO2dCQUNsQixPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDO3VEQUdEO1FBU0Q7WUFESSxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzs2REFDUTtRQUtwQztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLFdBQVc7Z0JBQ3JCLE9BQU8sRUFBRyxPQUFPO2FBQ2pCLENBQUM7MERBR0Q7UUFTRDtZQURJLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzJEQUNpQjtRQUt2QztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLEtBQUs7Z0JBQ2YsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQzt3REFHRDtRQU9EO1lBREksUUFBUSxFQUFFOytEQUNlO1FBSzdCO1lBSkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxNQUFNO2dCQUNoQixLQUFLLEVBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQzthQUNuQixDQUFDOzREQUdEO1FBU0Q7WUFESSxRQUFRLEVBQUU7K0RBQ2U7UUFLN0I7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTyxFQUFHLE1BQU07Z0JBQ2hCLEtBQUssRUFBRyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDO2FBQ25CLENBQUM7NERBR0Q7UUFTRDtZQURJLFFBQVEsRUFBRTsyREFDVztRQUl6QjtZQUhDLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUcsUUFBUTthQUNsQixDQUFDO3dEQUdEO1FBdkZXLGVBQWU7WUFGM0IsT0FBTyxDQUFDLDRCQUE0QixDQUFDO1lBQ3JDLElBQUksQ0FBQyxnREFBZ0QsQ0FBQztXQUMxQyxlQUFlLENBOEwzQjtRQUFELHNCQUFDO0tBOUxELEFBOExDLENBOUxvQyxXQUFBLGVBQWUsR0E4TG5EO0lBOUxZLDBCQUFlLGtCQThMM0IsQ0FBQTtBQUVELENBQUMsRUF0TU0sVUFBVSxLQUFWLFVBQVUsUUFzTWhCO0FDdE1ELElBQU8sVUFBVSxDQXNNaEI7QUF0TUQsV0FBTyxVQUFVO0lBRVgsSUFBQSxLQUE0QixFQUFFLENBQUMsVUFBVSxFQUF4QyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxJQUFJLFVBQWlCLENBQUM7SUFJaEQ7UUFBc0Msb0NBQWU7UUFBckQ7WUFBQSxxRUE4TEM7WUEzTEEsZUFBUyxHQUFnQixJQUFJLENBQUM7WUFnQjlCLGtCQUFZLEdBQW1CLElBQUksQ0FBQztZQWdCcEMsZ0JBQVUsR0FBYSxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQWN2QyxvQkFBYyxHQUFXLEdBQUcsQ0FBQztZQWdCN0Isb0JBQWMsR0FBVyxDQUFDLENBQUM7WUFnQjNCLGdCQUFVLEdBQVcsR0FBRyxDQUFDO1lBY3pCLGtCQUFZLEdBQWdCLElBQUksQ0FBQztZQUNqQyx1QkFBaUIsR0FBcUIsSUFBSSxDQUFDO1lBQzNDLHFCQUFlLEdBQWUsSUFBSSxDQUFDO1lBQ25DLHlCQUFtQixHQUFlLElBQUksQ0FBQztZQUN2Qyx5QkFBbUIsR0FBZSxJQUFJLENBQUM7WUFDdkMscUJBQWUsR0FBZSxJQUFJLENBQUM7O1FBMEZwQyxDQUFDO1FBdExBLHNCQUFJLHNDQUFRO2lCQUFaO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUN2QixDQUFDO2lCQUNELFVBQWEsS0FBaUI7Z0JBQzdCLElBQUcsSUFBSSxDQUFDLFNBQVMsSUFBSSxLQUFLLEVBQUM7b0JBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7aUJBQ3hDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRCxzQkFBSSx5Q0FBVztpQkFBZjtnQkFDQyxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDMUIsQ0FBQztpQkFDRCxVQUFnQixLQUFvQjtnQkFDbkMsSUFBRyxJQUFJLENBQUMsWUFBWSxJQUFJLEtBQUssRUFBQztvQkFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7b0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2lCQUMzQztZQUNGLENBQUM7OztXQU5BO1FBY0Qsc0JBQUksdUNBQVM7aUJBQWI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3hCLENBQUM7aUJBQ0QsVUFBYyxLQUFjO2dCQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztnQkFDeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLENBQUM7OztXQUpBO1FBWUQsc0JBQUksMkNBQWE7aUJBQWpCO2dCQUNDLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztZQUM1QixDQUFDO2lCQUNELFVBQWtCLEtBQVk7Z0JBQzdCLElBQUcsSUFBSSxDQUFDLGNBQWMsSUFBSSxLQUFLLEVBQUM7b0JBQy9CLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO29CQUM1QixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztpQkFDdkM7WUFDRixDQUFDOzs7V0FOQTtRQWNELHNCQUFJLDJDQUFhO2lCQUFqQjtnQkFDQyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7WUFDNUIsQ0FBQztpQkFDRCxVQUFrQixLQUFZO2dCQUM3QixJQUFHLElBQUksQ0FBQyxjQUFjLElBQUksS0FBSyxFQUFDO29CQUMvQixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztvQkFDNUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQ3ZDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFhRCxzQkFBSSx1Q0FBUztpQkFBYjtnQkFDQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDeEIsQ0FBQztpQkFDRCxVQUFjLEtBQVk7Z0JBQ3pCLElBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUM7b0JBQzNCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO29CQUN4QixJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQ25DO1lBQ0YsQ0FBQzs7O1dBTkE7UUFpQkQsbUNBQVEsR0FBUjtZQUNDLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7Z0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxpQkFBaUIsRUFBRSxhQUFhLENBQUMsQ0FBQztnQkFDbkYsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztvQkFDMUIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLGlCQUFpQixDQUFDLENBQUM7b0JBQ2xELE1BQU0sQ0FBQyxHQUFHLEdBQUcsYUFBYSxDQUFDO29CQUMzQixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDdkIsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7b0JBQ3BCLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO29CQUN4QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxDQUFDO2lCQUNoQzthQUNEO1lBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUM7Z0JBQ3hCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDO2dCQUN6RSxJQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBQztvQkFDeEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxNQUFNLENBQUMsR0FBRyxHQUFHLFdBQVcsQ0FBQztvQkFDekIsTUFBTSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7b0JBQzVCLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbkQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDO29CQUMxQyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ25ELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztvQkFDMUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUM7b0JBQzlCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztpQkFDL0I7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUM7Z0JBQzVCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsZUFBZSxDQUFDLENBQUM7Z0JBQ2pGLElBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUM7b0JBQzVCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxlQUFlLENBQUM7b0JBQzdCLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxvQkFBb0IsQ0FBQztvQkFDdkQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsaUJBQWlCLENBQUM7b0JBQzlDLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxvQkFBb0IsQ0FBQztvQkFDdkQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsaUJBQWlCLENBQUM7b0JBQzlDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxNQUFNLENBQUM7b0JBQ2xDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztpQkFDbkM7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUM7Z0JBQzVCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsZUFBZSxDQUFDLENBQUM7Z0JBQ2pGLElBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUM7b0JBQzVCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxlQUFlLENBQUM7b0JBQzdCLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxvQkFBb0IsQ0FBQztvQkFDdkQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsaUJBQWlCLENBQUM7b0JBQzlDLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxvQkFBb0IsQ0FBQztvQkFDdkQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsaUJBQWlCLENBQUM7b0JBQzlDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxNQUFNLENBQUM7b0JBQ2xDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztpQkFDbkM7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFDO2dCQUN4QixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLFdBQVcsRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFDekUsSUFBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUM7b0JBQ3hCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUM7b0JBQ3pCLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbkQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDO29CQUMxQyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ25ELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztvQkFDMUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUM7b0JBQzlCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztpQkFDL0I7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO2dCQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztnQkFDcEQsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7b0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQyxDQUFDO2lCQUNwRDthQUNEO1lBQ0QsSUFBRyxTQUFTLEVBQUM7Z0JBQ1osSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO2FBQ3JCO1FBQ0YsQ0FBQztRQUtLLHdDQUFhLEdBQW5COzs7Ozs7aUNBQ0ksU0FBUyxFQUFULGNBQVM7aUNBQ1IsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFmLGNBQWU7NEJBQ2pCLEtBQUEsSUFBSSxDQUFBOzRCQUFZLFdBQU0sV0FBQSxLQUFLLENBQUMsY0FBYyxDQUFjLFdBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLEVBQUE7OzRCQUE3RixHQUFLLFFBQVEsR0FBRyxTQUE2RSxDQUFDOzs7Ozs7U0FHaEc7UUExTEQ7WUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQzsyREFDUTtRQUs5QjtZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLFFBQVE7Z0JBQ2xCLE9BQU8sRUFBRyxNQUFNO2FBQ2hCLENBQUM7d0RBR0Q7UUFTRDtZQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDOzhEQUNXO1FBS3BDO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsV0FBVztnQkFDckIsT0FBTyxFQUFHLE9BQU87YUFDakIsQ0FBQzsyREFHRDtRQVNEO1lBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7NERBQ29CO1FBS3ZDO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsS0FBSztnQkFDZixPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDO3lEQUdEO1FBT0Q7WUFEQyxRQUFRLEVBQUU7Z0VBQ2tCO1FBSzdCO1lBSkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxNQUFNO2dCQUNoQixLQUFLLEVBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQzthQUNuQixDQUFDOzZEQUdEO1FBU0Q7WUFEQyxRQUFRLEVBQUU7Z0VBQ2dCO1FBSzNCO1lBSkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxNQUFNO2dCQUNoQixLQUFLLEVBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQzthQUNuQixDQUFDOzZEQUdEO1FBU0Q7WUFEQyxRQUFRLEVBQUU7NERBQ2M7UUFJekI7WUFIQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTyxFQUFHLFFBQVE7YUFDbEIsQ0FBQzt5REFHRDtRQXZGVyxnQkFBZ0I7WUFGNUIsT0FBTyxDQUFDLDZCQUE2QixDQUFDO1lBQ3RDLElBQUksQ0FBQyxpREFBaUQsQ0FBQztXQUMzQyxnQkFBZ0IsQ0E4TDVCO1FBQUQsdUJBQUM7S0E5TEQsQUE4TEMsQ0E5THFDLFdBQUEsZUFBZSxHQThMcEQ7SUE5TFksMkJBQWdCLG1CQThMNUIsQ0FBQTtBQUVELENBQUMsRUF0TU0sVUFBVSxLQUFWLFVBQVUsUUFzTWhCO0FDdE1ELElBQU8sVUFBVSxDQThHaEI7QUE5R0QsV0FBTyxVQUFVO0lBRVgsSUFBQSxLQUE0QixFQUFFLENBQUMsVUFBVSxFQUF4QyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxJQUFJLFVBQWlCLENBQUM7SUFJaEQ7UUFBa0MsZ0NBQWU7UUFBakQ7WUFBQSxxRUFzR0M7WUFuR0EsZUFBUyxHQUFnQixJQUFJLENBQUM7WUFnQjlCLGtCQUFZLEdBQW1CLElBQUksQ0FBQztZQWdCcEMsYUFBTyxHQUFXLEdBQUcsQ0FBQztZQWV0QixrQkFBWSxHQUFnQixJQUFJLENBQUM7WUFDakMsdUJBQWlCLEdBQXFCLElBQUksQ0FBQztZQUMzQyxrQkFBWSxHQUFlLElBQUksQ0FBQzs7UUFrRGpDLENBQUM7UUE5RkEsc0JBQUksa0NBQVE7aUJBQVo7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3ZCLENBQUM7aUJBQ0QsVUFBYSxLQUFpQjtnQkFDN0IsSUFBRyxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBQztvQkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDeEM7WUFDRixDQUFDOzs7V0FOQTtRQWNELHNCQUFJLHFDQUFXO2lCQUFmO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztZQUMxQixDQUFDO2lCQUNELFVBQWdCLEtBQW9CO2dCQUNuQyxJQUFHLElBQUksQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFDO29CQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztvQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7aUJBQzNDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRCxzQkFBSSxnQ0FBTTtpQkFBVjtnQkFDQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDckIsQ0FBQztpQkFDRCxVQUFXLEtBQVk7Z0JBQ3RCLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxLQUFLLEVBQUM7b0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO29CQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQ2hDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRSwrQkFBUSxHQUFSO1lBQ0YsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztnQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLGlCQUFpQixFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUNuRixJQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO29CQUMxQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsaUJBQWlCLENBQUMsQ0FBQztvQkFDbEQsTUFBTSxDQUFDLEdBQUcsR0FBRyxhQUFhLENBQUM7b0JBQzNCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUN2QixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDcEIsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUM7aUJBQ2hDO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztnQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQ25FLElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO29CQUNyQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDO29CQUN0QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFDO29CQUNoRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7b0JBQ3ZDLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxhQUFhLENBQUM7b0JBQ2hELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztvQkFDdkMsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUM7b0JBQzNCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztpQkFDNUI7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO2dCQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztnQkFDcEQsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7b0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQyxDQUFDO2lCQUNwRDthQUNEO1lBQ0QsSUFBRyxTQUFTLEVBQUM7Z0JBQ1osSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO2FBQ3JCO1FBQ0YsQ0FBQztRQUtLLG9DQUFhLEdBQW5COzs7Ozs7aUNBQ0ksU0FBUyxFQUFULGNBQVM7aUNBQ1IsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFmLGNBQWU7NEJBQ2pCLEtBQUEsSUFBSSxDQUFBOzRCQUFZLFdBQU0sV0FBQSxLQUFLLENBQUMsY0FBYyxDQUFjLFdBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFBOzs0QkFBeEYsR0FBSyxRQUFRLEdBQUcsU0FBd0UsQ0FBQzs7Ozs7O1NBRzNGO1FBbEdEO1lBREksUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7dURBQ0s7UUFLOUI7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxRQUFRO2dCQUNsQixPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDO29EQUdEO1FBU0Q7WUFESSxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzswREFDUTtRQUtwQztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLFdBQVc7Z0JBQ3JCLE9BQU8sRUFBRyxPQUFPO2FBQ2pCLENBQUM7dURBR0Q7UUFTRDtZQURJLFFBQVEsRUFBRTtxREFDUTtRQUt0QjtZQUpDLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUcsT0FBTztnQkFDakIsS0FBSyxFQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUM7YUFDcEIsQ0FBQztrREFHRDtRQTFDVyxZQUFZO1lBRnhCLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQztZQUNsQyxJQUFJLENBQUMsNkNBQTZDLENBQUM7V0FDdkMsWUFBWSxDQXNHeEI7UUFBRCxtQkFBQztLQXRHRCxBQXNHQyxDQXRHaUMsV0FBQSxlQUFlLEdBc0doRDtJQXRHWSx1QkFBWSxlQXNHeEIsQ0FBQTtBQUVELENBQUMsRUE5R00sVUFBVSxLQUFWLFVBQVUsUUE4R2hCO0FDOUdELElBQU8sVUFBVSxDQXdLaEI7QUF4S0QsV0FBTyxVQUFVO0lBRVgsSUFBQSxLQUE0QixFQUFFLENBQUMsVUFBVSxFQUF4QyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxJQUFJLFVBQWlCLENBQUM7SUFJaEQ7UUFBbUMsaUNBQWU7UUFBbEQ7WUFBQSxxRUFnS0M7WUE3SkEsZUFBUyxHQUFnQixJQUFJLENBQUM7WUFnQjlCLGtCQUFZLEdBQW1CLElBQUksQ0FBQztZQWdCcEMsbUJBQWEsR0FBYSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztZQWN2QyxtQkFBYSxHQUFXLEtBQUssQ0FBQztZQWdCOUIsZ0JBQVUsR0FBVyxHQUFHLENBQUM7WUFjekIsa0JBQVksR0FBZ0IsSUFBSSxDQUFDO1lBQ2pDLHVCQUFpQixHQUFxQixJQUFJLENBQUM7WUFDM0Msd0JBQWtCLEdBQWUsSUFBSSxDQUFDO1lBQ3RDLHdCQUFrQixHQUFlLElBQUksQ0FBQztZQUN0QyxxQkFBZSxHQUFlLElBQUksQ0FBQzs7UUE2RXBDLENBQUM7UUF4SkEsc0JBQUksbUNBQVE7aUJBQVo7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3ZCLENBQUM7aUJBQ0QsVUFBYSxLQUFpQjtnQkFDN0IsSUFBRyxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBQztvQkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDeEM7WUFDRixDQUFDOzs7V0FOQTtRQWNELHNCQUFJLHNDQUFXO2lCQUFmO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztZQUMxQixDQUFDO2lCQUNELFVBQWdCLEtBQW9CO2dCQUNuQyxJQUFHLElBQUksQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFDO29CQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztvQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7aUJBQzNDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRCxzQkFBSSx1Q0FBWTtpQkFBaEI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQzNCLENBQUM7aUJBQ0QsVUFBaUIsS0FBYztnQkFDOUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ3ZDLENBQUM7OztXQUpBO1FBWUQsc0JBQUksdUNBQVk7aUJBQWhCO2dCQUNDLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUMzQixDQUFDO2lCQUNELFVBQWlCLEtBQVk7Z0JBQzVCLElBQUcsSUFBSSxDQUFDLGFBQWEsSUFBSSxLQUFLLEVBQUM7b0JBQzlCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO29CQUMzQixJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztpQkFDdEM7WUFDRixDQUFDOzs7V0FOQTtRQWFELHNCQUFJLG9DQUFTO2lCQUFiO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUN4QixDQUFDO2lCQUNELFVBQWMsS0FBWTtnQkFDekIsSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUssRUFBQztvQkFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztpQkFDbkM7WUFDRixDQUFDOzs7V0FOQTtRQWdCRCxnQ0FBUSxHQUFSO1lBQ0MsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztnQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLGlCQUFpQixFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUNuRixJQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO29CQUMxQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsaUJBQWlCLENBQUMsQ0FBQztvQkFDbEQsTUFBTSxDQUFDLEdBQUcsR0FBRyxhQUFhLENBQUM7b0JBQzNCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUN2QixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDcEIsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUM7aUJBQ2hDO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFDO2dCQUMzQixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsV0FBVyxFQUFFLGNBQWMsQ0FBQyxDQUFDO2dCQUMvRSxJQUFHLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFDO29CQUMzQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsY0FBYyxDQUFDO29CQUM1QixNQUFNLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztvQkFDNUIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLG1CQUFtQixDQUFDO29CQUN0RCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDN0MsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLG1CQUFtQixDQUFDO29CQUN0RCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDN0MsSUFBSSxDQUFDLGtCQUFrQixHQUFHLE1BQU0sQ0FBQztvQkFDakMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO2lCQUNsQzthQUNEO1lBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBQztnQkFDM0IsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLFdBQVcsRUFBRSxjQUFjLENBQUMsQ0FBQztnQkFDL0UsSUFBRyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBQztvQkFDM0IsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxNQUFNLENBQUMsR0FBRyxHQUFHLGNBQWMsQ0FBQztvQkFDNUIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLG1CQUFtQixDQUFDO29CQUN0RCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDN0MsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLG1CQUFtQixDQUFDO29CQUN0RCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDN0MsSUFBSSxDQUFDLGtCQUFrQixHQUFHLE1BQU0sQ0FBQztvQkFDakMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO2lCQUNsQzthQUNEO1lBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUM7Z0JBQ3hCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDO2dCQUN6RSxJQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBQztvQkFDeEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxNQUFNLENBQUMsR0FBRyxHQUFHLFdBQVcsQ0FBQztvQkFDekIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGdCQUFnQixDQUFDO29CQUNuRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUM7b0JBQzFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxnQkFBZ0IsQ0FBQztvQkFDbkQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDO29CQUMxQyxJQUFJLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQztvQkFDOUIsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO2lCQUMvQjthQUNEO1lBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7Z0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQyxDQUFDO2dCQUNwRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztvQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7aUJBQ3BEO2FBQ0Q7WUFDRCxJQUFHLFNBQVMsRUFBQztnQkFDWixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7YUFDckI7UUFDRixDQUFDO1FBS0sscUNBQWEsR0FBbkI7Ozs7OztpQ0FDSSxTQUFTLEVBQVQsY0FBUztpQ0FDUixDQUFDLElBQUksQ0FBQyxTQUFTLEVBQWYsY0FBZTs0QkFDakIsS0FBQSxJQUFJLENBQUE7NEJBQVksV0FBTSxXQUFBLEtBQUssQ0FBQyxjQUFjLENBQWMsV0FBQSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUMsRUFBQTs7NEJBQXpGLEdBQUssUUFBUSxHQUFHLFNBQXlFLENBQUM7Ozs7OztTQUc1RjtRQTVKRDtZQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO3dEQUNRO1FBSzlCO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsUUFBUTtnQkFDbEIsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQztxREFHRDtRQVNEO1lBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7MkRBQ1c7UUFLcEM7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxXQUFXO2dCQUNyQixPQUFPLEVBQUcsT0FBTzthQUNqQixDQUFDO3dEQUdEO1FBU0Q7WUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzs0REFDb0I7UUFLdkM7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxLQUFLO2dCQUNmLE9BQU8sRUFBRyxNQUFNO2FBQ2hCLENBQUM7eURBR0Q7UUFPRDtZQURDLFFBQVEsRUFBRTs0REFDbUI7UUFLOUI7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTyxFQUFHLE1BQU07Z0JBQ2hCLEtBQUssRUFBRyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDO2FBQ3BCLENBQUM7eURBR0Q7UUFTRDtZQURDLFFBQVEsRUFBRTt5REFDYztRQUl6QjtZQUhDLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDO3NEQUdEO1FBdkVXLGFBQWE7WUFGekIsT0FBTyxDQUFDLDBCQUEwQixDQUFDO1lBQ25DLElBQUksQ0FBQyw4Q0FBOEMsQ0FBQztXQUN4QyxhQUFhLENBZ0t6QjtRQUFELG9CQUFDO0tBaEtELEFBZ0tDLENBaEtrQyxXQUFBLGVBQWUsR0FnS2pEO0lBaEtZLHdCQUFhLGdCQWdLekIsQ0FBQTtBQUVELENBQUMsRUF4S00sVUFBVSxLQUFWLFVBQVUsUUF3S2hCO0FDeEtELElBQU8sVUFBVSxDQXNKaEI7QUF0SkQsV0FBTyxVQUFVO0lBRVgsSUFBQSxLQUE0QixFQUFFLENBQUMsVUFBVSxFQUF4QyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxJQUFJLFVBQWlCLENBQUM7SUFLaEQsSUFBSyxTQU1KO0lBTkQsV0FBSyxTQUFTO1FBQ2IsdUNBQU8sQ0FBQTtRQUNQLHlDQUFJLENBQUE7UUFDSixpREFBUSxDQUFBO1FBQ1IsNkNBQU0sQ0FBQTtRQUNOLCtDQUFPLENBQUE7SUFDUixDQUFDLEVBTkksU0FBUyxLQUFULFNBQVMsUUFNYjtJQUlEO1FBQWlDLCtCQUFlO1FBQWhEO1lBQUEscUVBbUlDO1lBaElBLGVBQVMsR0FBZ0IsSUFBSSxDQUFDO1lBZ0I5QixrQkFBWSxHQUFtQixJQUFJLENBQUM7WUFrQnBDLGdCQUFVLEdBQWMsU0FBUyxDQUFDLEdBQUcsQ0FBQztZQWdCdEMsYUFBTyxHQUFXLEdBQUcsQ0FBQztZQWV0QixrQkFBWSxHQUFnQixJQUFJLENBQUM7WUFDakMsdUJBQWlCLEdBQXFCLElBQUksQ0FBQztZQUMzQyxxQkFBZSxHQUFlLElBQUksQ0FBQztZQUNuQyxrQkFBWSxHQUFlLElBQUksQ0FBQzs7UUE0RGpDLENBQUM7UUEzSEEsc0JBQUksaUNBQVE7aUJBQVo7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3ZCLENBQUM7aUJBQ0QsVUFBYSxLQUFpQjtnQkFDN0IsSUFBRyxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBQztvQkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDeEM7WUFDRixDQUFDOzs7V0FOQTtRQWNELHNCQUFJLG9DQUFXO2lCQUFmO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztZQUMxQixDQUFDO2lCQUNELFVBQWdCLEtBQW9CO2dCQUNuQyxJQUFHLElBQUksQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFDO29CQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztvQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7aUJBQzNDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFnQkQsc0JBQUksa0NBQVM7aUJBQWI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3hCLENBQUM7aUJBQ0QsVUFBYyxLQUFlO2dCQUM1QixJQUFHLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSyxFQUFDO29CQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztvQkFDeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2lCQUNuQztZQUNGLENBQUM7OztXQU5BO1FBY0Qsc0JBQUksK0JBQU07aUJBQVY7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3JCLENBQUM7aUJBQ0QsVUFBVyxLQUFZO2dCQUN0QixJQUFHLElBQUksQ0FBQyxPQUFPLElBQUksS0FBSyxFQUFDO29CQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztvQkFDckIsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2lCQUNoQztZQUNGLENBQUM7OztXQU5BO1FBZUUsOEJBQVEsR0FBUjtZQUNGLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7Z0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxpQkFBaUIsRUFBRSxhQUFhLENBQUMsQ0FBQztnQkFDbkYsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztvQkFDMUIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLGlCQUFpQixDQUFDLENBQUM7b0JBQ2xELE1BQU0sQ0FBQyxHQUFHLEdBQUcsYUFBYSxDQUFDO29CQUMzQixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDdkIsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUM7aUJBQ2hDO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBQztnQkFDeEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ3pFLElBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFDO29CQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsV0FBVyxDQUFDO29CQUN6QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ25ELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztvQkFDMUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGdCQUFnQixDQUFDO29CQUNuRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUM7b0JBQzFDLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDO29CQUM5QixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7aUJBQy9CO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztnQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQ25FLElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO29CQUNyQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDO29CQUN0QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFDO29CQUNoRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7b0JBQ3ZDLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxhQUFhLENBQUM7b0JBQ2hELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztvQkFDdkMsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUM7b0JBQzNCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztpQkFDNUI7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO2dCQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztnQkFDcEQsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7b0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQyxDQUFDO2lCQUNwRDthQUNEO1lBQ0QsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RCLENBQUM7UUFLSyxtQ0FBYSxHQUFuQjs7Ozs7O2lDQUNJLFNBQVMsRUFBVCxjQUFTO2lDQUNSLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBZixjQUFlOzRCQUNqQixLQUFBLElBQUksQ0FBQTs0QkFBWSxXQUFNLFdBQUEsS0FBSyxDQUFDLGNBQWMsQ0FBYyxXQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBQTs7NEJBQXZGLEdBQUssUUFBUSxHQUFHLFNBQXVFLENBQUM7Ozs7OztTQUcxRjtRQS9IRDtZQURJLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO3NEQUNLO1FBSzlCO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsUUFBUTtnQkFDbEIsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQzttREFHRDtRQVNEO1lBREksUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7eURBQ1E7UUFLcEM7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxXQUFXO2dCQUNyQixPQUFPLEVBQUcsT0FBTzthQUNqQixDQUFDO3NEQUdEO1FBV0Q7WUFISSxRQUFRLENBQUM7Z0JBQ1osSUFBSSxFQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO2FBQ3pCLENBQUM7dURBQ29DO1FBS3RDO1lBSkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxNQUFNO2dCQUNoQixJQUFJLEVBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7YUFDekIsQ0FBQztvREFHRDtRQVNEO1lBREksUUFBUSxFQUFFO29EQUNRO1FBS3RCO1lBSkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxJQUFJO2dCQUNkLEtBQUssRUFBRyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDO2FBQ3BCLENBQUM7aURBR0Q7UUE1RFcsV0FBVztZQUZ2QixPQUFPLENBQUMsd0JBQXdCLENBQUM7WUFDakMsSUFBSSxDQUFDLDRDQUE0QyxDQUFDO1dBQ3RDLFdBQVcsQ0FtSXZCO1FBQUQsa0JBQUM7S0FuSUQsQUFtSUMsQ0FuSWdDLFdBQUEsZUFBZSxHQW1JL0M7SUFuSVksc0JBQVcsY0FtSXZCLENBQUE7QUFFRCxDQUFDLEVBdEpNLFVBQVUsS0FBVixVQUFVLFFBc0poQjtBQ3RKRCxJQUFPLFVBQVUsQ0EySWhCO0FBM0lELFdBQU8sVUFBVTtJQUVYLElBQUEsS0FBNEIsRUFBRSxDQUFDLFVBQVUsRUFBeEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFBLEVBQUUsSUFBSSxVQUFpQixDQUFDO0lBSWhEO1FBQTJDLHlDQUFlO1FBQTFEO1lBQUEscUVBbUlDO1lBaElBLGVBQVMsR0FBZ0IsSUFBSSxDQUFDO1lBZ0I5QixrQkFBWSxHQUFtQixJQUFJLENBQUM7WUFnQnBDLGNBQVEsR0FBVyxJQUFJLENBQUM7WUFleEIsYUFBTyxHQUFZLElBQUksQ0FBQztZQWN4QixrQkFBWSxHQUFnQixJQUFJLENBQUM7WUFDakMsdUJBQWlCLEdBQXFCLElBQUksQ0FBQztZQUMzQyxtQkFBYSxHQUFlLElBQUksQ0FBQztZQUNqQyxrQkFBWSxHQUFlLElBQUksQ0FBQzs7UUFnRWpDLENBQUM7UUEzSEEsc0JBQUksMkNBQVE7aUJBQVo7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3ZCLENBQUM7aUJBQ0QsVUFBYSxLQUFpQjtnQkFDN0IsSUFBRyxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBQztvQkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDeEM7WUFDRixDQUFDOzs7V0FOQTtRQWNELHNCQUFJLDhDQUFXO2lCQUFmO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztZQUMxQixDQUFDO2lCQUNELFVBQWdCLEtBQW9CO2dCQUNuQyxJQUFHLElBQUksQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFDO29CQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztvQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7aUJBQzNDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFhRCxzQkFBSSwwQ0FBTztpQkFBWDtnQkFDQyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDdEIsQ0FBQztpQkFDRCxVQUFZLEtBQVk7Z0JBQ3ZCLElBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxLQUFLLEVBQUM7b0JBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO29CQUN0QixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQ2pDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFhRCxzQkFBSSx5Q0FBTTtpQkFBVjtnQkFDQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDckIsQ0FBQztpQkFDRCxVQUFXLEtBQWE7Z0JBQ3ZCLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxLQUFLLEVBQUM7b0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO29CQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUN4QztZQUNGLENBQUM7OztXQU5BO1FBZUUsd0NBQVEsR0FBUjtZQUNGLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7Z0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxpQkFBaUIsRUFBRSxhQUFhLENBQUMsQ0FBQztnQkFDbkYsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztvQkFDMUIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLGlCQUFpQixDQUFDLENBQUM7b0JBQ2xELE1BQU0sQ0FBQyxHQUFHLEdBQUcsYUFBYSxDQUFDO29CQUMzQixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDdkIsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7b0JBQ3BCLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO29CQUN4QixNQUFNLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztvQkFDM0IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE1BQU0sQ0FBQztpQkFDaEM7YUFDSztZQUNQLElBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFDO2dCQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFDckUsSUFBRyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUM7b0JBQ3RCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxTQUFTLENBQUM7b0JBQ3ZCLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxjQUFjLENBQUM7b0JBQ2pELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztvQkFDeEMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGNBQWMsQ0FBQztvQkFDakQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDO29CQUN4QyxJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQztvQkFDNUIsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO2lCQUM3QjthQUNLO1lBQ1AsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7Z0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUNuRSxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztvQkFDckIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxNQUFNLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQztvQkFDdEIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGFBQWEsQ0FBQztvQkFDaEQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO29CQUN2QyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFDO29CQUNoRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7b0JBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDO29CQUMzQixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNwQzthQUNLO1lBQ1AsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7Z0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQyxDQUFDO2dCQUNwRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztvQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7aUJBQ3BEO2FBQ0Q7WUFDRCxJQUFHLFNBQVMsRUFBQztnQkFDWixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7YUFDckI7UUFDRixDQUFDO1FBS0ssNkNBQWEsR0FBbkI7Ozs7OztpQ0FDSSxTQUFTLEVBQVQsY0FBUztpQ0FDUixDQUFDLElBQUksQ0FBQyxTQUFTLEVBQWYsY0FBZTs0QkFDakIsS0FBQSxJQUFJLENBQUE7NEJBQVksV0FBTSxXQUFBLEtBQUssQ0FBQyxjQUFjLENBQWMsV0FBQSxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBQTs7NEJBQW5HLEdBQUssUUFBUSxHQUFHLFNBQW1GLENBQUM7Ozs7OztTQUd0RztRQS9IRDtZQURJLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO2dFQUNLO1FBSzlCO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsUUFBUTtnQkFDbEIsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQzs2REFHRDtRQVNEO1lBREksUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7bUVBQ1E7UUFLcEM7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxXQUFXO2dCQUNyQixPQUFPLEVBQUcsT0FBTzthQUNqQixDQUFDO2dFQUdEO1FBU0Q7WUFESSxRQUFRLEVBQUU7K0RBQ1U7UUFJeEI7WUFIQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTyxFQUFHLElBQUk7YUFDZCxDQUFDOzREQUdEO1FBU0Q7WUFESSxRQUFRLEVBQUU7OERBQ1U7UUFJeEI7WUFIQyxRQUFRLENBQUM7Z0JBQ1QsT0FBTyxFQUFHLE1BQU07YUFDaEIsQ0FBQzsyREFHRDtRQXhEVyxxQkFBcUI7WUFGakMsT0FBTyxDQUFDLGtDQUFrQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxzREFBc0QsQ0FBQztXQUNoRCxxQkFBcUIsQ0FtSWpDO1FBQUQsNEJBQUM7S0FuSUQsQUFtSUMsQ0FuSTBDLFdBQUEsZUFBZSxHQW1JekQ7SUFuSVksZ0NBQXFCLHdCQW1JakMsQ0FBQTtBQUVELENBQUMsRUEzSU0sVUFBVSxLQUFWLFVBQVUsUUEySWhCO0FDM0lELElBQU8sVUFBVSxDQStHaEI7QUEvR0QsV0FBTyxVQUFVO0lBRVgsSUFBQSxLQUE0QixFQUFFLENBQUMsVUFBVSxFQUF4QyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxJQUFJLFVBQWlCLENBQUM7SUFJaEQ7UUFBMkMseUNBQWU7UUFBMUQ7WUFBQSxxRUF1R0M7WUFwR0EsZUFBUyxHQUFnQixJQUFJLENBQUM7WUFnQjlCLGtCQUFZLEdBQW1CLElBQUksQ0FBQztZQWdCcEMsYUFBTyxHQUFXLEdBQUcsQ0FBQztZQWV0QixrQkFBWSxHQUFnQixJQUFJLENBQUM7WUFDakMsdUJBQWlCLEdBQXFCLElBQUksQ0FBQztZQUMzQyxrQkFBWSxHQUFlLElBQUksQ0FBQzs7UUFtRGpDLENBQUM7UUEvRkEsc0JBQUksMkNBQVE7aUJBQVo7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3ZCLENBQUM7aUJBQ0QsVUFBYSxLQUFpQjtnQkFDN0IsSUFBRyxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssRUFBQztvQkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDeEM7WUFDRixDQUFDOzs7V0FOQTtRQWNELHNCQUFJLDhDQUFXO2lCQUFmO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztZQUMxQixDQUFDO2lCQUNELFVBQWdCLEtBQW9CO2dCQUNuQyxJQUFHLElBQUksQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFDO29CQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztvQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7aUJBQzNDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRCxzQkFBSSx5Q0FBTTtpQkFBVjtnQkFDQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDckIsQ0FBQztpQkFDRCxVQUFXLEtBQVk7Z0JBQ3RCLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxLQUFLLEVBQUM7b0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO29CQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQ2hDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRCx3Q0FBUSxHQUFSO1lBQ0MsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztnQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLGlCQUFpQixFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUNuRixJQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO29CQUMxQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsaUJBQWlCLENBQUMsQ0FBQztvQkFDbEQsTUFBTSxDQUFDLEdBQUcsR0FBRyxhQUFhLENBQUM7b0JBQzNCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUN2QixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDcEIsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3hCLE1BQU0sQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO29CQUMzQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxDQUFDO2lCQUNoQzthQUNEO1lBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7Z0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUNuRSxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztvQkFDckIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxNQUFNLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQztvQkFDdEIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGFBQWEsQ0FBQztvQkFDaEQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO29CQUN2QyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFDO29CQUNoRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7b0JBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDO29CQUMzQixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7aUJBQzVCO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztnQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7Z0JBQ3BELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO29CQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztpQkFDcEQ7YUFDRDtZQUNELElBQUcsU0FBUyxFQUFDO2dCQUNaLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUNyQjtRQUNGLENBQUM7UUFLSyw2Q0FBYSxHQUFuQjs7Ozs7O2lDQUNJLFNBQVMsRUFBVCxjQUFTO2lDQUNSLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBZixjQUFlOzRCQUNqQixLQUFBLElBQUksQ0FBQTs0QkFBWSxXQUFNLFdBQUEsS0FBSyxDQUFDLGNBQWMsQ0FBYyxXQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFBOzs0QkFBbkcsR0FBSyxRQUFRLEdBQUcsU0FBbUYsQ0FBQzs7Ozs7O1NBR3RHO1FBbkdEO1lBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0VBQ1E7UUFLOUI7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxRQUFRO2dCQUNsQixPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDOzZEQUdEO1FBU0Q7WUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzttRUFDVztRQUtwQztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLFdBQVc7Z0JBQ3JCLE9BQU8sRUFBRyxPQUFPO2FBQ2pCLENBQUM7Z0VBR0Q7UUFTRDtZQURJLFFBQVEsRUFBRTs4REFDUTtRQUt0QjtZQUpDLFFBQVEsQ0FBQztnQkFDVCxPQUFPLEVBQUcsSUFBSTtnQkFDZCxLQUFLLEVBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQzthQUNwQixDQUFDOzJEQUdEO1FBMUNXLHFCQUFxQjtZQUZqQyxPQUFPLENBQUMsa0NBQWtDLENBQUM7WUFDM0MsSUFBSSxDQUFDLHNEQUFzRCxDQUFDO1dBQ2hELHFCQUFxQixDQXVHakM7UUFBRCw0QkFBQztLQXZHRCxBQXVHQyxDQXZHMEMsV0FBQSxlQUFlLEdBdUd6RDtJQXZHWSxnQ0FBcUIsd0JBdUdqQyxDQUFBO0FBRUQsQ0FBQyxFQS9HTSxVQUFVLEtBQVYsVUFBVSxRQStHaEI7QUMvR0QsSUFBTyxVQUFVLENBc0xoQjtBQXRMRCxXQUFPLFVBQVU7SUFFWCxJQUFBLEtBQTRCLEVBQUUsQ0FBQyxVQUFVLEVBQXhDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBQSxFQUFFLElBQUksVUFBaUIsQ0FBQztJQUtoRCxJQUFLLFVBS0o7SUFMRCxXQUFLLFVBQVU7UUFDZCw2REFBaUIsQ0FBQTtRQUNqQiw2REFBYSxDQUFBO1FBQ2IsNkRBQWEsQ0FBQTtRQUNiLDZEQUFhLENBQUE7SUFDZCxDQUFDLEVBTEksVUFBVSxLQUFWLFVBQVUsUUFLZDtJQU9EO1FBQTJDLHlDQUFlO1FBQTFEO1lBQUEscUVBaUtDO1lBOUpBLGVBQVMsR0FBZ0IsSUFBSSxDQUFDO1lBZ0I5QixrQkFBWSxHQUFtQixJQUFJLENBQUM7WUFnQnBDLGdCQUFVLEdBQVcsR0FBRyxDQUFDO1lBa0J6QixpQkFBVyxHQUFlLFVBQVUsQ0FBQyxhQUFhLENBQUM7WUFnQm5ELGFBQU8sR0FBVyxHQUFHLENBQUM7WUFldEIsa0JBQVksR0FBZ0IsSUFBSSxDQUFDO1lBQ2pDLHVCQUFpQixHQUFxQixJQUFJLENBQUM7WUFDM0MscUJBQWUsR0FBZSxJQUFJLENBQUM7WUFDbkMsc0JBQWdCLEdBQWUsSUFBSSxDQUFDO1lBQ3BDLGtCQUFZLEdBQWUsSUFBSSxDQUFDOztRQXlFakMsQ0FBQztRQXpKQSxzQkFBSSwyQ0FBUTtpQkFBWjtnQkFDQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDdkIsQ0FBQztpQkFDRCxVQUFhLEtBQWlCO2dCQUM3QixJQUFHLElBQUksQ0FBQyxTQUFTLElBQUksS0FBSyxFQUFDO29CQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUN4QztZQUNGLENBQUM7OztXQU5BO1FBY0Qsc0JBQUksOENBQVc7aUJBQWY7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQzFCLENBQUM7aUJBQ0QsVUFBZ0IsS0FBb0I7Z0JBQ25DLElBQUcsSUFBSSxDQUFDLFlBQVksSUFBSSxLQUFLLEVBQUM7b0JBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO29CQUMxQixJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztpQkFDM0M7WUFDRixDQUFDOzs7V0FOQTtRQWNELHNCQUFJLDRDQUFTO2lCQUFiO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUN4QixDQUFDO2lCQUNELFVBQWMsS0FBWTtnQkFDekIsSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUssRUFBQztvQkFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztpQkFDbkM7WUFDRixDQUFDOzs7V0FOQTtRQWdCRCxzQkFBSSw2Q0FBVTtpQkFBZDtnQkFDQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDekIsQ0FBQztpQkFDRCxVQUFlLEtBQWdCO2dCQUM5QixJQUFHLElBQUksQ0FBQyxXQUFXLElBQUksS0FBSyxFQUFDO29CQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztvQkFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQ3BDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRCxzQkFBSSx5Q0FBTTtpQkFBVjtnQkFDQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDckIsQ0FBQztpQkFDRCxVQUFXLEtBQVk7Z0JBQ3RCLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxLQUFLLEVBQUM7b0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO29CQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQ2hDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFnQkQsd0NBQVEsR0FBUjtZQUNDLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7Z0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxpQkFBaUIsRUFBRSxhQUFhLENBQUMsQ0FBQztnQkFDbkYsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztvQkFDMUIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLGlCQUFpQixDQUFDLENBQUM7b0JBQ2xELE1BQU0sQ0FBQyxHQUFHLEdBQUcsYUFBYSxDQUFDO29CQUMzQixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDdkIsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUM7aUJBQ2hDO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBQztnQkFDeEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBQSxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ3pFLElBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFDO29CQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsV0FBVyxDQUFDO29CQUN6QixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ25ELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztvQkFDMUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGdCQUFnQixDQUFDO29CQUNuRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUM7b0JBQzFDLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDO29CQUM5QixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7aUJBQy9CO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFDO2dCQUN6QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFDO2dCQUMzRSxJQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFDO29CQUN6QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsV0FBVyxDQUFDLENBQUM7b0JBQzVDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsWUFBWSxDQUFDO29CQUMxQixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7b0JBQ3BELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLGNBQWMsQ0FBQztvQkFDM0MsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGlCQUFpQixDQUFDO29CQUNwRCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxjQUFjLENBQUM7b0JBQzNDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUM7b0JBQy9CLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztpQkFDaEM7YUFDRDtZQUNELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO2dCQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFBLFdBQVcsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDbkUsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7b0JBQ3JCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUM7b0JBQ3RCLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxhQUFhLENBQUM7b0JBQ2hELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztvQkFDdkMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGFBQWEsQ0FBQztvQkFDaEQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO29CQUN2QyxJQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQztvQkFDM0IsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO2lCQUM1QjthQUNEO1lBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUM7Z0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFBLFlBQVksQ0FBQyxDQUFDO2dCQUNwRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztvQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7aUJBQ3BEO2FBQ0Q7WUFDRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDdEIsQ0FBQztRQUtLLDZDQUFhLEdBQW5COzs7Ozs7aUNBQ0ksU0FBUyxFQUFULGNBQVM7aUNBQ1IsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFmLGNBQWU7NEJBQ2pCLEtBQUEsSUFBSSxDQUFBOzRCQUFZLFdBQU0sV0FBQSxLQUFLLENBQUMsY0FBYyxDQUFjLFdBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLEVBQUE7OzRCQUEzRixHQUFLLFFBQVEsR0FBRyxTQUEyRSxDQUFDOzs7Ozs7U0FHOUY7UUE3SkQ7WUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQztnRUFDUTtRQUs5QjtZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLFFBQVE7Z0JBQ2xCLE9BQU8sRUFBRyxNQUFNO2FBQ2hCLENBQUM7NkRBR0Q7UUFTRDtZQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO21FQUNXO1FBS3BDO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsV0FBVztnQkFDckIsT0FBTyxFQUFHLE9BQU87YUFDakIsQ0FBQztnRUFHRDtRQVNEO1lBREMsUUFBUSxFQUFFO2lFQUNjO1FBS3pCO1lBSkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxNQUFNO2dCQUNoQixLQUFLLEVBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQzthQUNwQixDQUFDOzhEQUdEO1FBV0Q7WUFISSxRQUFRLENBQUM7Z0JBQ1osSUFBSSxFQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO2FBQzFCLENBQUM7a0VBQ2lEO1FBS25EO1lBSkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxNQUFNO2dCQUNoQixJQUFJLEVBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7YUFDMUIsQ0FBQzsrREFHRDtRQVNEO1lBREMsUUFBUSxFQUFFOzhEQUNXO1FBS3RCO1lBSkMsUUFBUSxDQUFDO2dCQUNULE9BQU8sRUFBRyxJQUFJO2dCQUNkLEtBQUssRUFBRyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDO2FBQ3BCLENBQUM7MkRBR0Q7UUE1RVcscUJBQXFCO1lBRmpDLE9BQU8sQ0FBQyxrQ0FBa0MsQ0FBQztZQUMzQyxJQUFJLENBQUMsc0RBQXNELENBQUM7V0FDaEQscUJBQXFCLENBaUtqQztRQUFELDRCQUFDO0tBaktELEFBaUtDLENBakswQyxXQUFBLGVBQWUsR0FpS3pEO0lBaktZLGdDQUFxQix3QkFpS2pDLENBQUE7QUFFRCxDQUFDLEVBdExNLFVBQVUsS0FBVixVQUFVLFFBc0xoQjtBQ3RMRCxJQUFPLFVBQVUsQ0E2R2hCO0FBN0dELFdBQU8sVUFBVTtJQUVYLElBQUEsS0FBNEIsRUFBRSxDQUFDLFVBQVUsRUFBeEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFBLEVBQUUsSUFBSSxVQUFpQixDQUFDO0lBSWhEO1FBQXNDLG9DQUFlO1FBQXJEO1lBQUEscUVBcUdDO1lBbEdBLGVBQVMsR0FBZ0IsSUFBSSxDQUFDO1lBZ0I5QixrQkFBWSxHQUFtQixJQUFJLENBQUM7WUFnQnBDLGdCQUFVLEdBQW1CLElBQUksQ0FBQztZQWVsQyxrQkFBWSxHQUFnQixJQUFJLENBQUM7WUFDakMsdUJBQWlCLEdBQXFCLElBQUksQ0FBQztZQUMzQyxxQkFBZSxHQUFxQixJQUFJLENBQUM7O1FBaUQxQyxDQUFDO1FBN0ZBLHNCQUFJLHNDQUFRO2lCQUFaO2dCQUNDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUN2QixDQUFDO2lCQUNELFVBQWEsS0FBaUI7Z0JBQzdCLElBQUcsSUFBSSxDQUFDLFNBQVMsSUFBSSxLQUFLLEVBQUM7b0JBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7aUJBQ3hDO1lBQ0YsQ0FBQzs7O1dBTkE7UUFjRCxzQkFBSSx5Q0FBVztpQkFBZjtnQkFDQyxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDMUIsQ0FBQztpQkFDRCxVQUFnQixLQUFvQjtnQkFDbkMsSUFBRyxJQUFJLENBQUMsWUFBWSxJQUFJLEtBQUssRUFBQztvQkFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7b0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2lCQUMzQztZQUNGLENBQUM7OztXQU5BO1FBY0Qsc0JBQUksdUNBQVM7aUJBQWI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3hCLENBQUM7aUJBQ0QsVUFBYyxLQUFvQjtnQkFDakMsSUFBRyxJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUssRUFBQztvQkFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztpQkFDekM7WUFDRixDQUFDOzs7V0FOQTtRQWNFLG1DQUFRLEdBQVI7WUFDRixJQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO2dCQUMxQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsaUJBQWlCLEVBQUUsYUFBYSxDQUFDLENBQUM7Z0JBQ25GLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7b0JBQzFCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxpQkFBaUIsQ0FBQyxDQUFDO29CQUNsRCxNQUFNLENBQUMsR0FBRyxHQUFHLGFBQWEsQ0FBQztvQkFDM0IsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7b0JBQ3BCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUN2QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxDQUFDO2lCQUNoQzthQUNEO1lBQ0QsSUFBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUM7Z0JBQ3hCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQUEsaUJBQWlCLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQy9FLElBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFDO29CQUN4QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsaUJBQWlCLENBQUMsQ0FBQztvQkFDbEQsTUFBTSxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUM7b0JBQ3pCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO29CQUNwQixNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO29CQUMvQyxNQUFNLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7b0JBQ3JDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUM7b0JBQzFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztvQkFDakMsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUM7aUJBQzlCO2FBQ0Q7WUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQztnQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQUEsWUFBWSxDQUFDLENBQUM7Z0JBQ3BELElBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDO29CQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBQSxZQUFZLENBQUMsQ0FBQztpQkFDcEQ7YUFDRDtZQUNELElBQUcsU0FBUyxFQUFDO2dCQUNaLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUNyQjtRQUNGLENBQUM7UUFLSyx3Q0FBYSxHQUFuQjs7Ozs7O2lDQUNJLFNBQVMsRUFBVCxjQUFTO2lDQUNSLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBZixjQUFlOzRCQUNqQixLQUFBLElBQUksQ0FBQTs0QkFBWSxXQUFNLFdBQUEsS0FBSyxDQUFDLGNBQWMsQ0FBYyxXQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFBOzs0QkFBN0YsR0FBSyxRQUFRLEdBQUcsU0FBNkUsQ0FBQzs7Ozs7O1NBR2hHO1FBakdEO1lBREksUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7MkRBQ0s7UUFLOUI7WUFKQyxRQUFRLENBQUM7Z0JBQ1QsSUFBSSxFQUFHLEVBQUUsQ0FBQyxRQUFRO2dCQUNsQixPQUFPLEVBQUcsTUFBTTthQUNoQixDQUFDO3dEQUdEO1FBU0Q7WUFESSxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzs4REFDUTtRQUtwQztZQUpDLFFBQVEsQ0FBQztnQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLFdBQVc7Z0JBQ3JCLE9BQU8sRUFBRyxPQUFPO2FBQ2pCLENBQUM7MkRBR0Q7UUFTRDtZQURJLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDOzREQUNNO1FBS2xDO1lBSkMsUUFBUSxDQUFDO2dCQUNULElBQUksRUFBRyxFQUFFLENBQUMsV0FBVztnQkFDckIsT0FBTyxFQUFHLE9BQU87YUFDakIsQ0FBQzt5REFHRDtRQTFDVyxnQkFBZ0I7WUFGNUIsT0FBTyxDQUFDLDZCQUE2QixDQUFDO1lBQ3RDLElBQUksQ0FBQyxpREFBaUQsQ0FBQztXQUMzQyxnQkFBZ0IsQ0FxRzVCO1FBQUQsdUJBQUM7S0FyR0QsQUFxR0MsQ0FyR3FDLFdBQUEsZUFBZSxHQXFHcEQ7SUFyR1ksMkJBQWdCLG1CQXFHNUIsQ0FBQTtBQUVELENBQUMsRUE3R00sVUFBVSxLQUFWLFVBQVUsUUE2R2hCO0FDN0dELElBQU8sVUFBVSxDQStGaEI7QUEvRkQsV0FBTyxVQUFVO0lBTWpCO1FBQThCLG1DQUFZO1FBQTFDO1lBQUEscUVBb0ZDO1lBbkZHLG1CQUFhLEdBQUcsQ0FBQyxDQUFDO1lBQ2xCLGtCQUFZLEdBQUcsQ0FBQyxDQUFDO1lBQ2pCLG1CQUFhLEdBQUcsQ0FBQyxDQUFDO1lBTXJCLGdCQUFVLEdBQWtCLElBQUksQ0FBQzs7UUEyRWxDLENBQUM7UUF0RUEsOEJBQUksR0FBSixVQUFLLEtBQWtCO1lBQ3RCLGlCQUFNLElBQUksWUFBQyxLQUFLLENBQUMsQ0FBQztZQUNsQixLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQztRQUtFLHNCQUFJLDJDQUFjO2lCQUFsQjtnQkFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUNuRCxDQUFDOzs7V0FBQTtRQUtELG1DQUFTLEdBQVQ7WUFFSSxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBQ2hGLENBQUM7UUFLRSx5Q0FBZSxHQUFmO1lBRUksSUFBSSxJQUFJLEdBQUcsSUFBSSxFQUFFLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV0QixJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1lBQzFGLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBRXZCLE9BQU8sSUFBSSxDQUFDO1FBQ1YsQ0FBQztRQUVELHFDQUFXLEdBQVgsVUFBWSxJQUFpQixFQUFFLFFBQVE7WUFDbkMsSUFBSSxRQUFRLENBQUMsYUFBYSxFQUFFO2dCQUN4QixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUMzQjtZQUVELElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDakMsSUFBSSxLQUFLLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQyxJQUFJLEtBQUssR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRWpDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQWMsQ0FBQztZQUMxQyxJQUFJLFVBQVUsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBS3ZFLElBQUksWUFBWSxHQUFHLFVBQVUsQ0FBQyxVQUFVLElBQUksQ0FBQyxFQUN6QyxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUV6QixJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsWUFBWSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQzNDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQzthQUN6RTtpQkFBTTtnQkFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsQ0FBQzthQUNqQztZQUdELElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQ3BCLFlBQVksR0FBRyxVQUFVLENBQUMsWUFBWSxFQUN0QyxRQUFRLEdBQUcsVUFBVSxDQUFDLFlBQVksQ0FBQztZQUN2QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUMxQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzlDO1FBQ0wsQ0FBQztRQUVELDBDQUFnQixHQUFoQixVQUFpQixNQUFtQjtZQUNoQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM5QixDQUFDO1FBQ0wsc0JBQUM7SUFBRCxDQXBGQSxBQW9GQyxDQXBGNkIsRUFBRSxDQUFDLFNBQVMsR0FvRnpDO0lBR0QsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsV0FBQSxZQUFZLEVBQUUsZUFBZSxDQUFDLENBQUM7QUFFckQsQ0FBQyxFQS9GTSxVQUFVLEtBQVYsVUFBVSxRQStGaEIiLCJmaWxlIjoibGNjLXJlbmRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5tb2R1bGUgbGNjJHJlbmRlciB7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHksIGV4ZWN1dGVJbkVkaXRNb2RlfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG4vKipcclxuICog5a6P6YWN572uXHJcbiAqL1xyXG5AY2NjbGFzcyhcImxjYyRyZW5kZXIuTWFjcm9Db25maWdcIilcclxuZXhwb3J0IGNsYXNzIE1hY3JvQ29uZmlnIHtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5a6P5ZCN56ewKOS4uuepuuihqOekuuS4jeWQr+eUqClcIlxyXG5cdH0pXHJcblx0bmFtZTpzdHJpbmcgPSBcIlwiO1xyXG5cclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5Y+q55So5LqO5qOA5p+lXCJcclxuXHR9KVxyXG5cdGNoZWNrT25seTpib29sZWFuID0gZmFsc2U7XHJcblxyXG5cdGNvbnN0cnVjdG9yKC4uLnBhcmFtczphbnkpe1xyXG5cdFx0dGhpcy5uYW1lID0gcGFyYW1zWzBdIHx8IFwiXCI7XHJcblx0XHR0aGlzLmNoZWNrT25seSA9IHBhcmFtc1sxXSB8fCBmYWxzZTtcclxuXHR9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDnnYDoibLlmajlj5jph49cclxuICovXHJcbmV4cG9ydCBlbnVtIFZhcmlhYmxlVHlwZSB7XHJcblx0LyoqXHJcblx0ICog5bGe5oCnXHJcblx0ICovXHJcblx0QVRUUklCVVRFID0gMSxcclxuXHJcblx0LyoqXHJcblx0ICog5LiA6Iis5Y+Y6YePXHJcblx0ICovXHJcblx0VU5JRk9STSxcclxufTtcclxuXHJcbi8qKlxyXG4gKiDlgLznsbvlnotcclxuICovXHJcbmV4cG9ydCBlbnVtIFZhbHVlVHlwZSB7XHJcblx0LyoqXHJcblx0ICog5Y2VXHJcblx0ICovXHJcblx0U0lOR0xFLFxyXG5cclxuXHQvKipcclxuXHQgKiDmlbDnu4RcclxuXHQgKi9cclxuXHRBUlJBWSxcclxufTtcclxuXHJcbi8qKlxyXG4gKiDlj5jph4/phY3nva5cclxuICovXHJcbkBjY2NsYXNzKFwibGNjJHJlbmRlci5WYXJpYWJsZUNvbmZpZ1wiKVxyXG5leHBvcnQgY2xhc3MgVmFyaWFibGVDb25maWcge1xyXG5cdEBwcm9wZXJ0eSgpXHJcblx0X3R5cGVzZWw6Ym9vbGVhbiA9IHRydWU7XHJcblxyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuRW51bShWYXJpYWJsZVR5cGUpLFxyXG5cdFx0dG9vbHRpcCA6IFwi5Y+Y6YeP57G75Z6LXCIsXHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdHlwZXNlbDtcclxuXHRcdH1cclxuXHR9KVxyXG5cdHR5cGU6VmFyaWFibGVUeXBlID0gVmFyaWFibGVUeXBlLlVOSUZPUk07XHJcblxyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogTWFjcm9Db25maWcsXHJcblx0XHR0b29sdGlwIDogXCLlsZ7mgKflj5jph4/lro/lrprkuYlcIixcclxuXHRcdHZpc2libGUgKCl7XHJcblx0XHRcdHJldHVybiB0aGlzLnR5cGUgPT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURTtcclxuXHRcdH1cclxuXHR9KVxyXG5cdGF0dHJNYWNybzpNYWNyb0NvbmZpZyA9IG5ldyBNYWNyb0NvbmZpZygpO1xyXG5cclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5bGe5oCn5Y+Y6YeP5ZCNXCIsXHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy50eXBlID09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEU7XHJcblx0XHR9XHJcblx0fSlcclxuXHRhdHRyTmFtZTpzdHJpbmcgPSBcIlwiO1xyXG5cclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6IE1hY3JvQ29uZmlnLFxyXG5cdFx0dG9vbHRpcCA6IFwi5LiA6Iis5Y+Y6YeP5a6P5a6a5LmJXCIsXHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy50eXBlID09IFZhcmlhYmxlVHlwZS5VTklGT1JNO1xyXG5cdFx0fVxyXG5cdH0pXHJcblx0dW5pZk1hY3JvOk1hY3JvQ29uZmlnID0gbmV3IE1hY3JvQ29uZmlnKCk7XHJcblxyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0b29sdGlwIDogXCLkuIDoiKzlj5jph4/lkI1cIixcclxuXHRcdHZpc2libGUgKCl7XHJcblx0XHRcdHJldHVybiB0aGlzLnR5cGUgPT0gVmFyaWFibGVUeXBlLlVOSUZPUk07XHJcblx0XHR9XHJcblx0fSlcclxuXHR1bmlmTmFtZTpzdHJpbmcgPSBcIlwiO1xyXG5cclxuXHRjb25zdHJ1Y3RvciguLi5wYXJhbXM6YW55KXtcclxuXHRcdHRoaXMuX3R5cGVzZWwgPSBwYXJhbXNbMF07XHJcblx0XHR0aGlzLnR5cGUgPSBwYXJhbXNbMV0gfHwgVmFyaWFibGVUeXBlLlVOSUZPUk07XHJcblx0XHR0aGlzLmF0dHJNYWNybyA9IG5ldyBNYWNyb0NvbmZpZyhwYXJhbXNbMl0gfHwgXCJcIiwgcGFyYW1zWzNdIHx8IGZhbHNlKTtcclxuXHRcdHRoaXMuYXR0ck5hbWUgPSBwYXJhbXNbNF0gfHwgXCJcIjtcclxuXHRcdHRoaXMudW5pZk1hY3JvID0gbmV3IE1hY3JvQ29uZmlnKHBhcmFtc1s1XSB8fCBcIlwiLCBwYXJhbXNbNl0gfHwgZmFsc2UpO1xyXG5cdFx0dGhpcy51bmlmTmFtZSA9IHBhcmFtc1s3XSB8fCBcIlwiO1xyXG5cdH1cclxufVxyXG5cclxuQGNjY2xhc3MoXCJsY2MkcmVuZGVyLlNoYWRlckNvbXBvbmVudFwiKVxyXG5AZXhlY3V0ZUluRWRpdE1vZGUoKVxyXG5leHBvcnQgY2xhc3MgU2hhZGVyQ29tcG9uZW50IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuXHRcclxuXHRAcHJvcGVydHkoKVxyXG5cdF90YWc6c3RyaW5nID0gXCJcIjtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi57uE5Lu25qCH562+XCJcclxuXHR9KVxyXG5cdGdldCB0YWcoKXtcclxuXHRcdHJldHVybiB0aGlzLl90YWc7XHJcblx0fVxyXG5cdHNldCB0YWcodmFsdWU6c3RyaW5nKXtcclxuXHRcdGlmKHRoaXMuX3RhZyAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3RhZyA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfdGFnXCIpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICog5qOA5p+l5p2Q6LSo5Lit55qE5a6PXHJcblx0ICogQHBhcmFtIG1hdGVyaWFsIFxyXG5cdCAqIEBwYXJhbSBtYWNybyBcclxuXHQgKi9cclxuXHRwcm90ZWN0ZWQgY2hlY2tNYXRlcmlhbE1hY3JvKG1hdGVyaWFsOmNjLk1hdGVyaWFsLCBtYWNybzpNYWNyb0NvbmZpZyl7XHJcblx0XHRyZXR1cm4gIW1hY3JvLm5hbWUgfHwgKG1hY3JvLmNoZWNrT25seSA/IG1hdGVyaWFsLmdldERlZmluZShtYWNyby5uYW1lKSA6IFxyXG5cdFx0XHQobWF0ZXJpYWwuZ2V0RGVmaW5lKG1hY3JvLm5hbWUpICE9PSB1bmRlZmluZWQpKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICog5a6a5LmJ5p2Q6LSo5Lit55qE5a6PXHJcblx0ICogQHBhcmFtIG1hdGVyaWFsIFxyXG5cdCAqIEBwYXJhbSBtYWNybyBcclxuXHQgKiBAcGFyYW0gdmFsdWUgXHJcblx0ICovXHJcblx0cHJvdGVjdGVkIGRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWw6Y2MuTWF0ZXJpYWwsIG1hY3JvOk1hY3JvQ29uZmlnLCB2YWx1ZTpudW1iZXJ8Ym9vbGVhbil7XHJcblx0XHRpZihtYWNyby5uYW1lICYmICFtYWNyby5jaGVja09ubHkpe1xyXG5cdFx0XHRtYXRlcmlhbC5kZWZpbmUobWFjcm8ubmFtZSwgdmFsdWUpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiDojrflvpfnnYDoibLlmajnu4Tku7ZcclxuXHQgKiBAcGFyYW0gdHlwZSBcclxuXHQgKiBAcGFyYW0gdGFnIFxyXG5cdCAqL1xyXG5cdHByb3RlY3RlZCBnZXRTaGFkZXJDb21wb25lbnQ8VCBleHRlbmRzIFNoYWRlckNvbXBvbmVudD4odHlwZToge3Byb3RvdHlwZTogVH0sIHRhZz86c3RyaW5nKXtcclxuXHRcdGlmKHRhZyA9PT0gdW5kZWZpbmVkKXtcclxuXHRcdFx0cmV0dXJuIHRoaXMuZ2V0Q29tcG9uZW50KHR5cGUpO1xyXG5cdFx0fWVsc2V7XHJcblx0XHRcdGZvcihsZXQgY29tcCBvZiB0aGlzLmdldENvbXBvbmVudHModHlwZSkpe1xyXG5cdFx0XHRcdGlmKGNvbXAuX3RhZyA9PSB0YWcpe1xyXG5cdFx0XHRcdFx0cmV0dXJuIGNvbXA7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIOiOt+W+l+edgOiJsuWZqOe7hOS7tuaVsOe7hFxyXG5cdCAqIEBwYXJhbSB0eXBlIFxyXG5cdCAqIEBwYXJhbSB0YWcgXHJcblx0ICovXHJcblx0cHJvdGVjdGVkIGdldFNoYWRlckNvbXBvbmVudHM8VCBleHRlbmRzIFNoYWRlckNvbXBvbmVudD4odHlwZToge3Byb3RvdHlwZTogVH0sIHRhZz86c3RyaW5nKXtcclxuXHRcdGlmKHRhZyA9PT0gdW5kZWZpbmVkKXtcclxuXHRcdFx0cmV0dXJuIHRoaXMuZ2V0Q29tcG9uZW50cyh0eXBlKTtcclxuXHRcdH1lbHNle1xyXG5cdFx0XHRsZXQgY29tcHMgPSB0aGlzLmdldENvbXBvbmVudHModHlwZSk7XHJcblx0XHRcdHJldHVybiBjb21wcy5maWx0ZXIoKGEpPT57IGEuX3RhZyA9PSB0YWc7IH0pO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxufVxyXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID0gXCIuLi9zaGFkZXIvU2hhZGVyQ29tcG9uZW50LnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2MkcmVuZGVyIHtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eSwgbWVudSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbi8vQHRzLWlnbm9yZVxyXG5sZXQgZ2Z4ID0gY2MuZ2Z4O1xyXG5cclxuLyoqXHJcbiAqIOmhtueCueWxnuaAp1xyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBWZXJ0ZXhBdHRyaWJ1dGUge1xyXG5cdG5hbWUgOiBzdHJpbmc7XHJcblx0dHlwZSA6IG51bWJlcjtcclxuXHRudW0gOiBudW1iZXI7XHJcblx0bm9ybWFsaXplID86IGJvb2xlYW47XHJcbn07XHJcblxyXG4vKipcclxuICog552A6Imy5Zmo5Y+Y6YeP57G75Z6LXHJcbiAqL1xyXG5leHBvcnQgZW51bSBTaGFkZXJWYXJpYWJsZVR5cGUge1xyXG5cdC8qKlxyXG5cdCAqIOmhtueCueWxnuaAp1xyXG5cdCAqL1xyXG5cdEFUVFJJQlVURSxcclxuXHJcblx0LyoqXHJcblx0ICog5LiA6Ie05Y+Y6YePXHJcblx0ICovXHJcblx0VU5JRk9STSxcclxufTtcclxuXHJcbi8vIOS9jee9rlxyXG5jb25zdCBBVFRSX1BPU0lUSU9OID0geyBuYW1lOiBnZnguQVRUUl9QT1NJVElPTiwgdHlwZTogZ2Z4LkFUVFJfVFlQRV9GTE9BVDMyLCBudW06IDIgfTtcclxuXHJcbi8qKlxyXG4gKiDlvaLnirbnsbvlnotcclxuICovXHJcbmVudW0gU2hhcGVUeXBlIHtcclxuXHQvKipcclxuXHQgKiDnnYDoibLlmajlvaLnirZcclxuXHQgKi9cclxuXHRTSEFERVIsXHJcblxyXG5cdC8qKlxyXG5cdCAqIOiKgueCueW9oueKtlxyXG5cdCAqL1xyXG5cdE5PREUsXHJcbn1cclxuXHJcbkBjY2NsYXNzKFwibGNjJHJlbmRlci5SZW5kZXJTeXN0ZW1cIilcclxuQG1lbnUoXCJpMThuOmxjYy1yZW5kZXIubWVudV9jb21wb25lbnQvUmVuZGVyU3lzdGVtXCIpXHJcbmV4cG9ydCBjbGFzcyBSZW5kZXJTeXN0ZW0gZXh0ZW5kcyBjYy5SZW5kZXJDb21wb25lbnQge1xyXG5cclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0ZWRpdG9yT25seSA6IHRydWUsXHJcblx0XHRzZXJpYWxpemFibGUgOiBmYWxzZSxcclxuXHRcdHJlYWRvbmx5IDogdHJ1ZSxcclxuXHRcdHRvb2x0aXAgOiBcIue7n+iuoeWxnuaAp+aJgOS9v+eUqOeahOepuumXtOWkp+Wwj1wiXHJcblx0fSlcclxuXHRhdHRyc1NpemU6bnVtYmVyID0gMDtcclxuXHRcclxuICAgIC8qKlxyXG4gICAgICog5p2Q6LSo5rGh5p+TXHJcbiAgICAgKi9cclxuXHRfbWF0c0RpcnR5OmJvb2xlYW4gPSB0cnVlO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIOW9oueKtuaxoeafk1xyXG5cdCAqL1xyXG5cdF9zaGFwZURpcnR5OmJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICog5bGe5oCn5rGh5p+TXHJcbiAgICAgKi9cclxuICAgIF9hdHRyc0RpcnR5OmJvb2xlYW4gPSB0cnVlO1xyXG5cclxuXHQvKipcclxuXHQgKiDpobbngrnmoLzlvI9cclxuXHQgKi9cclxuXHRfdkZvcm1hdDphbnkgPSBudWxsO1xyXG5cclxuXHQvKipcclxuXHQgKiDpobbngrnlsZ7mgKdcclxuXHQgKi9cclxuXHRfdkF0dHJpYnV0ZXM6VmVydGV4QXR0cmlidXRlW10gPSBudWxsO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIOW9oueKtuexu+Wei1xyXG5cdCAqL1xyXG5cdF9zaGFwZVR5cGU6U2hhcGVUeXBlID0gU2hhcGVUeXBlLk5PREU7XHJcblxyXG5cdC8qKlxyXG5cdCAqIOmhtueCueaVsOmHj1xyXG5cdCAqL1xyXG5cdF92ZXJ0aWNlc0NvdW50Om51bWJlciA9IDA7XHJcblxyXG5cdC8qKlxyXG5cdCAqIOe0ouW8leaVsOmHj1xyXG5cdCAqL1xyXG5cdF9pbmRpY2VzQ291bnQ6bnVtYmVyID0gMDtcclxuXHJcblx0LyoqXHJcblx0ICog6IqC54K555+p5b2i5Yy65Z+fXHJcblx0ICovXHJcblx0X2xvY2FsUmVjdDogbnVtYmVyW10gPSBudWxsO1xyXG5cdFxyXG4gICAgb25FbmFibGUoKXtcclxuICAgICAgICBzdXBlci5vbkVuYWJsZSgpO1xyXG4gICAgICAgIC8vIEB0cy1pZ25vcmVcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuU0laRV9DSEFOR0VELCB0aGlzLm9uTm9kZVNpemVDaGFuZ2VkLCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oXCJzaGFkZXJfdXBkYXRlX21hdGVyaWFsXCIsIHRoaXMuc2V0TWF0c0RpcnR5LCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oXCJzaGFkZXJfdXBkYXRlX2F0dHJpYnV0ZVwiLCB0aGlzLnNldEF0dHJzRGlydHksIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihcInNoYWRlcl91cGRhdGVfc2hhcGVcIiwgdGhpcy5jaGVja1JlbmRlclNoYXBlLCB0aGlzKTtcclxuXHRcdHRoaXMubm9kZS5vbihcInNoYWRlcl91cGRhdGVfdmVydGV4XCIsIHRoaXMuc2V0VmVydHNEaXJ0eSwgdGhpcyk7XHJcblx0XHR0aGlzLnNldERpcnR5KCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EaXNhYmxlKCl7XHJcblx0XHRzdXBlci5vbkRpc2FibGUoKTtcclxuXHRcdHRoaXMubm9kZS50YXJnZXRPZmYodGhpcyk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDorr7nva7mnZDotKjmsaHmn5NcclxuXHQgKi9cclxuXHRzZXRNYXRzRGlydHkoKXtcclxuXHRcdHRoaXMuX21hdHNEaXJ0eSA9IHRydWU7XHJcblx0XHR0aGlzLnNldFZlcnRzRGlydHkoKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIOiuvue9ruWxnuaAp+axoeafk1xyXG5cdCAqL1xyXG5cdHNldEF0dHJzRGlydHkoKXtcclxuXHRcdC8vRWRpdG9yLmxvZyhcInNldEF0dHJzRGlydHlcIik7XHJcblx0XHR0aGlzLl9hdHRyc0RpcnR5ID0gdHJ1ZTtcclxuXHRcdHRoaXMuc2V0U2hhcGVEaXJ0eSgpO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICog6K6+572u5b2i54q25rGh5p+TXHJcblx0ICovXHJcblx0c2V0U2hhcGVEaXJ0eSgpe1xyXG5cdFx0Ly9FZGl0b3IubG9nKFwic2V0U2hhcGVEaXJ0eVwiKTtcclxuXHRcdHRoaXMuX3NoYXBlRGlydHkgPSB0cnVlO1xyXG5cdFx0dGhpcy5zZXRWZXJ0c0RpcnR5KCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDmo4Dmn6XmuLLmn5PlvaLnirZcclxuXHQgKi9cclxuXHRwcml2YXRlIGNoZWNrUmVuZGVyU2hhcGUoKXtcclxuXHRcdHRoaXMuc2V0U2hhcGVEaXJ0eSgpO1xyXG5cclxuXHRcdHRoaXMuX3NoYXBlVHlwZSA9IFNoYXBlVHlwZS5OT0RFO1xyXG5cclxuXHRcdHRoaXMubm9kZS5lbWl0KFwicmVuZGVyX2NoZWNrX3NoYXBlXCIsIHRoaXMpO1xyXG5cclxuXHRcdGlmKHRoaXMuX3NoYXBlVHlwZSA9PSBTaGFwZVR5cGUuTk9ERSl7XHJcblx0XHRcdHRoaXMuX3ZlcnRpY2VzQ291bnQgPSA0O1xyXG5cdFx0XHR0aGlzLl9pbmRpY2VzQ291bnQgPSA2O1xyXG5cdFx0fVxyXG5cclxuXHRcdHRoaXMubm9kZS5lbWl0KFwicmVuZGVyX3NoYXBlX2NoZWNrZWRcIiwgdGhpcyk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIOiuvue9ruedgOiJsuWZqOW9oueKtlxyXG5cdCAqIEBwYXJhbSB2ZXJ0aWNlc0NvdW50IFxyXG5cdCAqIEBwYXJhbSBpbmRpY2VzQ291bnQgXHJcblx0ICovXHJcblx0c2V0U2hhZGVyU2hhcGUodmVydGljZXNDb3VudDpudW1iZXIsIGluZGljZXNDb3VudDpudW1iZXIpe1xyXG5cdFx0dGhpcy5fc2hhcGVUeXBlID0gU2hhcGVUeXBlLlNIQURFUjtcclxuXHRcdHRoaXMuX3ZlcnRpY2VzQ291bnQgPSB2ZXJ0aWNlc0NvdW50O1xyXG5cdFx0dGhpcy5faW5kaWNlc0NvdW50ID0gaW5kaWNlc0NvdW50O1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICog5b2T6IqC54K55bC65a+45Y+Y5YyWXHJcblx0ICovXHJcblx0cHJpdmF0ZSBvbk5vZGVTaXplQ2hhbmdlZCgpe1xyXG5cdFx0aWYodGhpcy5fc2hhcGVUeXBlID09PSBTaGFwZVR5cGUuTk9ERSl7XHJcblx0XHRcdHRoaXMuc2V0VmVydHNEaXJ0eSgpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICog6K6+572u6aG254K55rGh5p+TXHJcblx0ICovXHJcblx0c2V0VmVydHNEaXJ0eSgpe1xyXG5cdFx0Ly8gQHRzLWlnbm9yZVxyXG4gICAgICAgIHN1cGVyLnNldFZlcnRzRGlydHkoKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICog6K6+572u5omA5pyJ5rGh5p+TXHJcblx0ICovXHJcblx0c2V0RGlydHkoKXtcclxuXHRcdHRoaXMuY2hlY2tSZW5kZXJTaGFwZSgpO1xyXG5cdFx0dGhpcy5fbWF0c0RpcnR5ID0gdHJ1ZTtcclxuXHRcdHRoaXMuX2F0dHJzRGlydHkgPSB0cnVlO1xyXG5cdFx0dGhpcy5fc2hhcGVEaXJ0eSA9IHRydWU7XHJcblx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHR0aGlzLnNldFZlcnRzRGlydHkoKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICog5pu05paw5Li66IqC54K55b2i54q2XHJcblx0ICovXHJcblx0cHJvdGVjdGVkIHVwZGF0ZVRvTm9kZVNoYXBlKCl7XHJcblx0XHQvL0VkaXRvci5sb2coXCJ1cGRhdGVUb05vZGVTaGFwZVwiKTtcclxuXHRcdFxyXG5cdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0bGV0IHJkYXRhID0gdGhpcy5fYXNzZW1ibGVyLnJlbmRlckRhdGE7XHJcblx0XHRyZGF0YS5pbml0UXVhZEluZGljZXMocmRhdGEuaURhdGFzWzBdKTtcclxuXHJcbiAgICAgICAgdGhpcy5fbG9jYWxSZWN0ID0gWyAwLCAwLCAwLCAwIF07XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDmm7TmlrDoioLngrnpobbngrlcclxuXHQgKi9cclxuXHRwcm90ZWN0ZWQgdXBkYXRlTm9kZVZlcnRleCgpe1xyXG5cdFx0bGV0IG5vZGUgPSB0aGlzLm5vZGUsXHJcbiAgICAgICAgICAgIGN3ID0gbm9kZS53aWR0aCwgY2ggPSBub2RlLmhlaWdodCxcclxuICAgICAgICAgICAgYXBweCA9IG5vZGUuYW5jaG9yWCAqIGN3LCBhcHB5ID0gbm9kZS5hbmNob3JZICogY2gsXHJcbiAgICAgICAgICAgIGwsIGIsIHIsIHQ7XHJcbiAgICAgICAgbCA9IC1hcHB4O1xyXG4gICAgICAgIGIgPSAtYXBweTtcclxuICAgICAgICByID0gY3cgLSBhcHB4O1xyXG4gICAgICAgIHQgPSBjaCAtIGFwcHk7XHJcbiAgICAgICAgbGV0IGxvY2FsID0gdGhpcy5fbG9jYWxSZWN0O1xyXG4gICAgICAgIGxvY2FsWzBdID0gbDtcclxuICAgICAgICBsb2NhbFsxXSA9IGI7XHJcbiAgICAgICAgbG9jYWxbMl0gPSByO1xyXG5cdFx0bG9jYWxbM10gPSB0O1xyXG5cdFx0XHJcblx0XHR0aGlzLnVwZGF0ZVdvcmxkVmVydHMoKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICog5pu05paw5riy5p+T5pWw5o2uXHJcblx0ICovXHJcblx0dXBkYXRlUmVuZGVyRGF0YSgpe1xyXG5cdFx0dGhpcy5wcmVwYXJlUmVuZGVyKCk7XHJcblx0XHR0aGlzLnVwZGF0ZU1hdGVyaWFsKCk7XHJcblx0XHR0aGlzLnVwZGF0ZUF0dHJpYnV0ZSgpO1xyXG5cdFx0dGhpcy51cGRhdGVTaGFwZSgpO1xyXG5cdFx0dGhpcy51cGRhdGVWZXJ0ZXgoKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIOWHhuWkh+a4suafk1xyXG5cdCAqL1xyXG5cdHByb3RlY3RlZCBwcmVwYXJlUmVuZGVyKCl7XHJcblxyXG5cdFx0dGhpcy5ub2RlLmVtaXQoXCJyZW5kZXJfcHJlcGFyZVwiLCB0aGlzKTtcclxuXHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDmm7TmlrDmnZDotKhcclxuXHQgKi9cclxuXHRwcm90ZWN0ZWQgdXBkYXRlTWF0ZXJpYWwoKXtcclxuXHRcdGlmKHRoaXMuX21hdHNEaXJ0eSl7XHJcblx0XHRcdC8vRWRpdG9yLmxvZyhcInVwZGF0ZU1hdGVyaWFsXCIpO1xyXG5cdFx0XHRcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmVtaXQoXCJyZW5kZXJfdXBkYXRlX21hdGVyaWFsXCIsIHRoaXMpO1xyXG5cclxuXHRcdFx0dGhpcy5fdXBkYXRlQmxlbmRGdW5jKGZhbHNlKTtcclxuXHRcdFx0XHJcbiAgICAgICAgICAgIHRoaXMuX21hdHNEaXJ0eSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIOajgOafpemhtueCueWxnuaAp+WQjVxyXG5cdCAqIEBwYXJhbSBuYW1lIFxyXG5cdCAqL1xyXG5cdGNoZWNrVmVydGV4QXR0cmlidXRlTmFtZShuYW1lOnN0cmluZyl7XHJcblx0XHRmb3IobGV0IHZhIG9mIHRoaXMuX3ZBdHRyaWJ1dGVzKXtcclxuXHRcdFx0aWYodmEubmFtZSA9PSBuYW1lKXtcclxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHJldHVybiB0cnVlO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICog5re75Yqg6aG254K5XHJcblx0ICogQHBhcmFtIHZhdHRyIOWxnuaAp+WvueixoVxyXG5cdCAqIEByZXR1cm5zIOaVsOaNruWBj+enu1xyXG5cdCAqL1xyXG5cdGFkZFZlcnRleEF0dHJpYnV0ZSh2YXR0cjpWZXJ0ZXhBdHRyaWJ1dGUsIGZsb2F0c2l6ZTpudW1iZXIpOm51bWJlcntcclxuICAgICAgICBsZXQgb2ZmZXN0ID0gLTE7XHJcbiAgICAgICAgLy8gQHRzLWlnbm9yZVxyXG4gICAgICAgIGxldCBhc3NlbWJsZXIgPSB0aGlzLl9hc3NlbWJsZXI7XHJcblx0XHRpZih0aGlzLmNoZWNrVmVydGV4QXR0cmlidXRlTmFtZSh2YXR0ci5uYW1lKSl7XHJcblx0XHRcdG9mZmVzdCA9IGFzc2VtYmxlci5mbG9hdHNQZXJWZXJ0O1xyXG5cdFx0XHR0aGlzLl92QXR0cmlidXRlcy5wdXNoKHZhdHRyKTtcclxuXHRcdFx0YXNzZW1ibGVyLmZsb2F0c1BlclZlcnQgKz0gZmxvYXRzaXplO1xyXG5cdFx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHRcdHRoaXMuYXR0cnNTaXplID0gYXNzZW1ibGVyLmZsb2F0c1BlclZlcnQgKiB0aGlzLl92ZXJ0aWNlc0NvdW50O1xyXG5cdFx0XHR9XHJcblx0XHR9ZWxzZXtcclxuICAgICAgICAgICAgaWYoQ0NfRURJVE9SKXtcclxuICAgICAgICAgICAgICAgIEVkaXRvci5lcnJvcihcImF0dHJpYnV0ZSBuYW1lIGNvbmZsaWN0IFwiICsgdmF0dHIubmFtZSk7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG5cdFx0XHRcdGNjLmVycm9yKFwiYXR0cmlidXRlIG5hbWUgY29uZmxpY3QgXCIgKyB2YXR0ci5uYW1lKTtcclxuXHRcdFx0fVxyXG4gICAgICAgIH1cclxuXHRcdHJldHVybiBvZmZlc3Q7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDojrflvpfpobbngrnmoLzlvI9cclxuXHQgKi9cclxuICAgIGdldFZmbXQoKSB7XHJcblx0XHRpZighdGhpcy5fdkZvcm1hdCl7XHJcblx0XHRcdHRoaXMuX3ZGb3JtYXQgPSBuZXcgZ2Z4LlZlcnRleEZvcm1hdCh0aGlzLl92QXR0cmlidXRlcyk7XHJcblx0XHR9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3ZGb3JtYXQ7XHJcbiAgICB9XHJcblx0XHJcblx0LyoqXHJcblx0ICog5pu05paw5bGe5oCnXHJcblx0ICovXHJcblx0cHJvdGVjdGVkIHVwZGF0ZUF0dHJpYnV0ZSgpe1xyXG5cdFx0aWYodGhpcy5fYXR0cnNEaXJ0eSl7XHJcbiAgICAgICAgICAgIC8vRWRpdG9yLmxvZyhcIl91cGRhdGVBdHRyaWJ1dGVcIik7XHJcblxyXG4gICAgICAgICAgICAvLyBAdHMtaWdub3JlXHJcbiAgICAgICAgICAgIGxldCBhc3NlbWJsZXIgPSB0aGlzLl9hc3NlbWJsZXI7XHJcblxyXG5cdFx0XHR0aGlzLl92Rm9ybWF0ID0gbnVsbDtcclxuXHRcdFx0dGhpcy5fdkF0dHJpYnV0ZXMgPSBbIEFUVFJfUE9TSVRJT04gXTtcclxuXHRcdFx0YXNzZW1ibGVyLmZsb2F0c1BlclZlcnQgPSAyO1xyXG5cdFx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHRcdHRoaXMuYXR0cnNTaXplID0gYXNzZW1ibGVyLmZsb2F0c1BlclZlcnQgKiB0aGlzLl92ZXJ0aWNlc0NvdW50O1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZW1pdChcInJlbmRlcl91cGRhdGVfYXR0cmlidXRlXCIsIHRoaXMpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5fYXR0cnNEaXJ0eSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHR9XHJcbiAgICBcclxuXHQvKipcclxuXHQgKiDmm7TmlrDlvaLnirZcclxuXHQgKi9cclxuXHRwcm90ZWN0ZWQgdXBkYXRlU2hhcGUoKXtcclxuXHRcdGlmKHRoaXMuX3NoYXBlRGlydHkpe1xyXG5cdFx0XHQvL0VkaXRvci5sb2coXCJ1cGRhdGVTaGFwZVwiKTsgXHJcblxyXG5cdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdGxldCBhc3NlbWJsZXIgPSB0aGlzLl9hc3NlbWJsZXI7XHJcblxyXG5cdFx0XHRhc3NlbWJsZXIudmVydGljZXNDb3VudCA9IHRoaXMuX3ZlcnRpY2VzQ291bnQ7XHJcblx0XHRcdGFzc2VtYmxlci5pbmRpY2VzQ291bnQgPSB0aGlzLl9pbmRpY2VzQ291bnQ7XHJcblxyXG5cdFx0XHRhc3NlbWJsZXIucmVzZXRSZW5kZXJEYXRhKCk7XHJcblxyXG5cdFx0XHRpZih0aGlzLl9zaGFwZVR5cGUgPT09IFNoYXBlVHlwZS5OT0RFKXtcclxuXHRcdFx0XHR0aGlzLnVwZGF0ZVRvTm9kZVNoYXBlKCk7XHJcblx0XHRcdH1lbHNle1xyXG5cdFx0XHRcdHRoaXMubm9kZS5lbWl0KFwicmVuZGVyX3VwZGF0ZV9zaGFwZVwiLCB0aGlzKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dGhpcy5fc2hhcGVEaXJ0eSA9IGZhbHNlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICog5pu05paw6aG254K5XHJcblx0ICovXHJcblx0cHJvdGVjdGVkIHVwZGF0ZVZlcnRleCgpe1xyXG5cdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0aWYodGhpcy5fdmVydHNEaXJ0eSl7XHJcblx0XHRcdC8vRWRpdG9yLmxvZyhcIl91cGRhdGVSZW5kZXJEYXRhXCIpO1xyXG5cclxuXHRcdFx0aWYodGhpcy5fc2hhcGVUeXBlID09PSBTaGFwZVR5cGUuTk9ERSl7XHJcblx0XHRcdFx0dGhpcy51cGRhdGVOb2RlVmVydGV4KCk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHRoaXMubm9kZS5lbWl0KFwicmVuZGVyX3VwZGF0ZV92ZXJ0ZXhcIiwgdGhpcyk7XHJcblxyXG5cdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdHRoaXMuX3ZlcnRzRGlydHkgPSBmYWxzZTtcclxuXHRcdH1cclxuXHR9XHJcbiAgICBcclxuICAgIHVwZGF0ZVdvcmxkVmVydHMoKSB7XHJcblx0XHRpZih0aGlzLl9zaGFwZVR5cGUgPT09IFNoYXBlVHlwZS5OT0RFKXtcclxuXHRcdFx0aWYgKENDX05BVElWRVJFTkRFUkVSKSB7XHJcblx0XHRcdFx0dGhpcy51cGRhdGVOb2RlV29ybGRWZXJ0c05hdGl2ZSgpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdHRoaXMudXBkYXRlTm9kZVdvcmxkVmVydHNXZWJHTCgpO1xyXG5cdFx0XHR9XHJcblx0XHR9ZWxzZXtcclxuXHRcdFx0dGhpcy5ub2RlLmVtaXQoXCJyZW5kZXJfdXBkYXRlX3dvcmxkdmVydGV4XCIsIHRoaXMpO1xyXG5cdFx0fVxyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZU5vZGVXb3JsZFZlcnRzV2ViR0woKSB7XHJcblx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRsZXQgYXNzZW1ibGVyID0gdGhpcy5fYXNzZW1ibGVyO1xyXG4gICAgICAgIGxldCBsb2NhbCA9IHRoaXMuX2xvY2FsUmVjdDtcclxuICAgICAgICBsZXQgdmVydHMgPSBhc3NlbWJsZXIucmVuZGVyRGF0YS52RGF0YXNbMF07XHJcblxyXG5cdFx0Ly9AdHMtaWdub3JlXHJcbiAgICAgICAgbGV0IG1hdHJpeCA9IHRoaXMubm9kZS5fd29ybGRNYXRyaXg7XHJcbiAgICAgICAgbGV0IG1hdHJpeG0gPSBtYXRyaXgubSxcclxuICAgICAgICAgICAgYSA9IG1hdHJpeG1bMF0sIGIgPSBtYXRyaXhtWzFdLCBjID0gbWF0cml4bVs0XSwgZCA9IG1hdHJpeG1bNV0sXHJcbiAgICAgICAgICAgIHR4ID0gbWF0cml4bVsxMl0sIHR5ID0gbWF0cml4bVsxM107XHJcblxyXG4gICAgICAgIGxldCB2bCA9IGxvY2FsWzBdLCB2ciA9IGxvY2FsWzJdLFxyXG4gICAgICAgICAgICB2YiA9IGxvY2FsWzFdLCB2dCA9IGxvY2FsWzNdO1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8qXHJcbiAgICAgICAgbTAwID0gMSwgbTAxID0gMCwgbTAyID0gMCwgbTAzID0gMCxcclxuICAgICAgICBtMDQgPSAwLCBtMDUgPSAxLCBtMDYgPSAwLCBtMDcgPSAwLFxyXG4gICAgICAgIG0wOCA9IDAsIG0wOSA9IDAsIG0xMCA9IDEsIG0xMSA9IDAsXHJcbiAgICAgICAgbTEyID0gMCwgbTEzID0gMCwgbTE0ID0gMCwgbTE1ID0gMVxyXG4gICAgICAgICovXHJcbiAgICAgICAgbGV0IGp1c3RUcmFuc2xhdGUgPSBhID09PSAxICYmIGIgPT09IDAgJiYgYyA9PT0gMCAmJiBkID09PSAxO1xyXG5cclxuICAgICAgICBsZXQgaW5kZXggPSAwO1xyXG4gICAgICAgIGxldCBmbG9hdHNQZXJWZXJ0ID0gYXNzZW1ibGVyLmZsb2F0c1BlclZlcnQ7XHJcbiAgICAgICAgaWYgKGp1c3RUcmFuc2xhdGUpIHtcclxuICAgICAgICAgICAgLy8gbGVmdCBib3R0b21cclxuICAgICAgICAgICAgdmVydHNbaW5kZXhdID0gdmwgKyB0eDtcclxuICAgICAgICAgICAgdmVydHNbaW5kZXgrMV0gPSB2YiArIHR5O1xyXG4gICAgICAgICAgICBpbmRleCArPSBmbG9hdHNQZXJWZXJ0O1xyXG4gICAgICAgICAgICAvLyByaWdodCBib3R0b21cclxuICAgICAgICAgICAgdmVydHNbaW5kZXhdID0gdnIgKyB0eDtcclxuICAgICAgICAgICAgdmVydHNbaW5kZXgrMV0gPSB2YiArIHR5O1xyXG4gICAgICAgICAgICBpbmRleCArPSBmbG9hdHNQZXJWZXJ0O1xyXG4gICAgICAgICAgICAvLyBsZWZ0IHRvcFxyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleF0gPSB2bCArIHR4O1xyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IHZ0ICsgdHk7XHJcbiAgICAgICAgICAgIGluZGV4ICs9IGZsb2F0c1BlclZlcnQ7XHJcbiAgICAgICAgICAgIC8vIHJpZ2h0IHRvcFxyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleF0gPSB2ciArIHR4O1xyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IHZ0ICsgdHk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbGV0IGFsID0gYSAqIHZsLCBhciA9IGEgKiB2cixcclxuICAgICAgICAgICAgYmwgPSBiICogdmwsIGJyID0gYiAqIHZyLFxyXG4gICAgICAgICAgICBjYiA9IGMgKiB2YiwgY3QgPSBjICogdnQsXHJcbiAgICAgICAgICAgIGRiID0gZCAqIHZiLCBkdCA9IGQgKiB2dDtcclxuXHJcbiAgICAgICAgICAgIC8vIGxlZnQgYm90dG9tXHJcbiAgICAgICAgICAgIC8vIG5ld3ggPSB2bCAqIGEgKyB2YiAqIGMgKyB0eFxyXG4gICAgICAgICAgICAvLyBuZXd5ID0gdmwgKiBiICsgdmIgKiBkICsgdHlcclxuICAgICAgICAgICAgdmVydHNbaW5kZXhdID0gYWwgKyBjYiArIHR4O1xyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IGJsICsgZGIgKyB0eTtcclxuICAgICAgICAgICAgaW5kZXggKz0gZmxvYXRzUGVyVmVydDtcclxuICAgICAgICAgICAgLy8gcmlnaHQgYm90dG9tXHJcbiAgICAgICAgICAgIHZlcnRzW2luZGV4XSA9IGFyICsgY2IgKyB0eDtcclxuICAgICAgICAgICAgdmVydHNbaW5kZXgrMV0gPSBiciArIGRiICsgdHk7XHJcbiAgICAgICAgICAgIGluZGV4ICs9IGZsb2F0c1BlclZlcnQ7XHJcbiAgICAgICAgICAgIC8vIGxlZnQgdG9wXHJcbiAgICAgICAgICAgIHZlcnRzW2luZGV4XSA9IGFsICsgY3QgKyB0eDtcclxuICAgICAgICAgICAgdmVydHNbaW5kZXgrMV0gPSBibCArIGR0ICsgdHk7XHJcbiAgICAgICAgICAgIGluZGV4ICs9IGZsb2F0c1BlclZlcnQ7XHJcbiAgICAgICAgICAgIC8vIHJpZ2h0IHRvcFxyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleF0gPSBhciArIGN0ICsgdHg7XHJcbiAgICAgICAgICAgIHZlcnRzW2luZGV4KzFdID0gYnIgKyBkdCArIHR5O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVOb2RlV29ybGRWZXJ0c05hdGl2ZSgpIHtcclxuXHRcdC8vIEB0cy1pZ25vcmVcclxuXHRcdGxldCBhc3NlbWJsZXIgPSB0aGlzLl9hc3NlbWJsZXI7XHJcbiAgICAgICAgbGV0IGxvY2FsID0gdGhpcy5fbG9jYWxSZWN0O1xyXG4gICAgICAgIGxldCB2ZXJ0cyA9IGFzc2VtYmxlci5yZW5kZXJEYXRhLnZEYXRhc1swXTtcclxuICAgICAgICBsZXQgZmxvYXRzUGVyVmVydCA9IGFzc2VtYmxlci5mbG9hdHNQZXJWZXJ0O1xyXG5cdFx0XHJcbiAgICAgICAgbGV0IHZsID0gbG9jYWxbMF0sXHJcbiAgICAgICAgICAgIHZyID0gbG9jYWxbMl0sXHJcbiAgICAgICAgICAgIHZiID0gbG9jYWxbMV0sXHJcbiAgICAgICAgICAgIHZ0ID0gbG9jYWxbM107XHJcbiAgICAgIFxyXG4gICAgICAgIGxldCBpbmRleDogbnVtYmVyID0gMDtcclxuICAgICAgICAvLyBsZWZ0IGJvdHRvbVxyXG4gICAgICAgIHZlcnRzW2luZGV4XSA9IHZsO1xyXG4gICAgICAgIHZlcnRzW2luZGV4KzFdID0gdmI7XHJcbiAgICAgICAgaW5kZXggKz0gZmxvYXRzUGVyVmVydDtcclxuICAgICAgICAvLyByaWdodCBib3R0b21cclxuICAgICAgICB2ZXJ0c1tpbmRleF0gPSB2cjtcclxuICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IHZiO1xyXG4gICAgICAgIGluZGV4ICs9IGZsb2F0c1BlclZlcnQ7XHJcbiAgICAgICAgLy8gbGVmdCB0b3BcclxuICAgICAgICB2ZXJ0c1tpbmRleF0gPSB2bDtcclxuICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IHZ0O1xyXG4gICAgICAgIGluZGV4ICs9IGZsb2F0c1BlclZlcnQ7XHJcbiAgICAgICAgLy8gcmlnaHQgdG9wXHJcbiAgICAgICAgdmVydHNbaW5kZXhdID0gdnI7XHJcbiAgICAgICAgdmVydHNbaW5kZXgrMV0gPSB2dDtcclxuICAgIH1cclxuXHJcblx0Ly8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIOedgOiJsuWZqOe7hOS7tlxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIOiOt+W+l+edgOiJsuWZqOe7hOS7tlxyXG5cdCAqIEBwYXJhbSB0eXBlIFxyXG5cdCAqIEBwYXJhbSB0YWcgXHJcblx0ICovXHJcblx0cHJvdGVjdGVkIGdldFNoYWRlckNvbXBvbmVudDxUIGV4dGVuZHMgU2hhZGVyQ29tcG9uZW50Pih0eXBlOiB7cHJvdG90eXBlOiBUfSwgdGFnPzpzdHJpbmcpe1xyXG5cdFx0aWYodGFnID09PSB1bmRlZmluZWQpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5nZXRDb21wb25lbnQodHlwZSk7XHJcblx0XHR9ZWxzZXtcclxuXHRcdFx0Zm9yKGxldCBjb21wIG9mIHRoaXMuZ2V0Q29tcG9uZW50cyh0eXBlKSl7XHJcblx0XHRcdFx0aWYoY29tcC5fdGFnID09IHRhZyl7XHJcblx0XHRcdFx0XHRyZXR1cm4gY29tcDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICog6I635b6X552A6Imy5Zmo57uE5Lu25pWw57uEXHJcblx0ICogQHBhcmFtIHR5cGUgXHJcblx0ICogQHBhcmFtIHRhZyBcclxuXHQgKi9cclxuXHRwcm90ZWN0ZWQgZ2V0U2hhZGVyQ29tcG9uZW50czxUIGV4dGVuZHMgU2hhZGVyQ29tcG9uZW50Pih0eXBlOiB7cHJvdG90eXBlOiBUfSwgdGFnPzpzdHJpbmcpe1xyXG5cdFx0aWYodGFnID09PSB1bmRlZmluZWQpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5nZXRDb21wb25lbnRzKHR5cGUpO1xyXG5cdFx0fWVsc2V7XHJcblx0XHRcdGxldCBjb21wcyA9IHRoaXMuZ2V0Q29tcG9uZW50cyh0eXBlKTtcclxuXHRcdFx0cmV0dXJuIGNvbXBzLmZpbHRlcigoYSk9PnsgYS5fdGFnID09IHRhZzsgfSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQvLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0g5YW85a65IFJlbmRlckNvbXBvbmVudFxyXG5cclxuXHRfYWN0aXZhdGVNYXRlcmlhbCgpe1xyXG5cdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0c3VwZXIuX2FjdGl2YXRlTWF0ZXJpYWwoKTtcclxuXHRcdHRoaXMuc2V0RGlydHkoKTtcclxuXHR9XHJcblxyXG5cdF91cGRhdGVNYXRlcmlhbCAoKSB7XHJcbiAgICAgICAgdGhpcy51cGRhdGVNYXRlcmlhbCgpO1xyXG5cdH1cclxuXHJcbiAgICBfdmFsaWRhdGVSZW5kZXIgKCkge1xyXG4gICAgICAgIC8vIEB0cy1pZ25vcmVcclxuICAgICAgICBpZighdGhpcy5fbWF0ZXJpYWxzWzBdKXtcclxuICAgICAgICAgICAgLy8gQHRzLWlnbm9yZVxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5kaXNhYmxlUmVuZGVyKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLm5vZGUuZW1pdChcInJlbmRlcl92YWxpZGF0ZV9yZW5kZXJcIiwgdGhpcyk7XHJcblx0fVxyXG5cdFxyXG5cdC8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSBCbGVuZEZ1bmNcclxuXHJcblx0X3NyY0JsZW5kRmFjdG9yOiBjYy5tYWNyby5CbGVuZEZhY3RvciA9IGNjLm1hY3JvLkJsZW5kRmFjdG9yLlNSQ19BTFBIQTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0YW5pbWF0YWJsZTogZmFsc2UsXHJcblx0XHR0eXBlOiBjYy5tYWNyby5CbGVuZEZhY3RvcixcclxuXHRcdHRvb2x0aXA6IENDX0RFViAmJiAnaTE4bjpDT01QT05FTlQuc3ByaXRlLnNyY19ibGVuZF9mYWN0b3InLFxyXG5cdH0pXHJcblx0Z2V0IHNyY0JsZW5kRmFjdG9yKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fc3JjQmxlbmRGYWN0b3I7XHJcblx0fVxyXG5cdHNldCBzcmNCbGVuZEZhY3Rvcih2YWx1ZTpjYy5tYWNyby5CbGVuZEZhY3Rvcil7XHJcblx0XHRpZiAodGhpcy5fc3JjQmxlbmRGYWN0b3IgPT09IHZhbHVlKSByZXR1cm47XHJcblx0XHR0aGlzLl9zcmNCbGVuZEZhY3RvciA9IHZhbHVlO1xyXG5cdFx0dGhpcy5fdXBkYXRlQmxlbmRGdW5jKHRydWUpO1xyXG5cdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0dGhpcy5fb25CbGVuZENoYW5nZWQgJiYgdGhpcy5fb25CbGVuZENoYW5nZWQoKTtcclxuXHR9XHJcblxyXG5cdF9kc3RCbGVuZEZhY3RvcjogY2MubWFjcm8uQmxlbmRGYWN0b3IgPSAgY2MubWFjcm8uQmxlbmRGYWN0b3IuT05FX01JTlVTX1NSQ19BTFBIQTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0YW5pbWF0YWJsZTogZmFsc2UsXHJcbiAgICAgICAgdHlwZTogY2MubWFjcm8uQmxlbmRGYWN0b3IsXHJcbiAgICAgICAgdG9vbHRpcDogQ0NfREVWICYmICdpMThuOkNPTVBPTkVOVC5zcHJpdGUuZHN0X2JsZW5kX2ZhY3RvcicsXHJcblx0fSlcclxuXHRnZXQgZHN0QmxlbmRGYWN0b3IoKXtcclxuXHRcdHJldHVybiB0aGlzLl9kc3RCbGVuZEZhY3RvcjtcclxuXHR9XHJcblx0c2V0IGRzdEJsZW5kRmFjdG9yKHZhbHVlOmNjLm1hY3JvLkJsZW5kRmFjdG9yKXtcclxuXHRcdGlmICh0aGlzLl9kc3RCbGVuZEZhY3RvciA9PT0gdmFsdWUpIHJldHVybjtcclxuXHRcdHRoaXMuX2RzdEJsZW5kRmFjdG9yID0gdmFsdWU7XHJcblx0XHR0aGlzLl91cGRhdGVCbGVuZEZ1bmModHJ1ZSk7XHJcblx0fVxyXG5cclxuICAgIHNldE1hdGVyaWFsIChpbmRleCwgbWF0ZXJpYWwpIHtcclxuXHRcdGxldCBtYXRlcmlhbFZhciA9IHN1cGVyLnNldE1hdGVyaWFsKGluZGV4LCBtYXRlcmlhbCk7XHJcblxyXG5cdFx0aWYgKHRoaXMuX3NyY0JsZW5kRmFjdG9yICE9PSBjYy5tYWNyby5CbGVuZEZhY3Rvci5TUkNfQUxQSEEgfHwgXHJcblx0XHRcdHRoaXMuX2RzdEJsZW5kRmFjdG9yICE9PSBjYy5tYWNyby5CbGVuZEZhY3Rvci5PTkVfTUlOVVNfU1JDX0FMUEhBKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3VwZGF0ZU1hdGVyaWFsQmxlbmRGdW5jKG1hdGVyaWFsVmFyKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0dGhpcy5zZXRNYXRzRGlydHkoKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG1hdGVyaWFsVmFyO1xyXG4gICAgfVxyXG5cclxuICAgIF91cGRhdGVCbGVuZEZ1bmMgKGZvcmNlKSB7XHJcbiAgICAgICAgaWYgKCFmb3JjZSkge1xyXG5cdFx0XHRpZiAodGhpcy5fc3JjQmxlbmRGYWN0b3IgPT09IGNjLm1hY3JvLkJsZW5kRmFjdG9yLlNSQ19BTFBIQSAmJiBcclxuXHRcdFx0XHR0aGlzLl9kc3RCbGVuZEZhY3RvciA9PT0gY2MubWFjcm8uQmxlbmRGYWN0b3IuT05FX01JTlVTX1NSQ19BTFBIQSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIGxldCBtYXRlcmlhbHMgPSB0aGlzLmdldE1hdGVyaWFscygpO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbWF0ZXJpYWxzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGxldCBtYXRlcmlhbCA9IG1hdGVyaWFsc1tpXTtcclxuICAgICAgICAgICAgdGhpcy5fdXBkYXRlTWF0ZXJpYWxCbGVuZEZ1bmMobWF0ZXJpYWwpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfdXBkYXRlTWF0ZXJpYWxCbGVuZEZ1bmMgKG1hdGVyaWFsKSB7XHJcbiAgICAgICAgbWF0ZXJpYWwuc2V0QmxlbmQoXHJcbiAgICAgICAgICAgIHRydWUsXHJcbiAgICAgICAgICAgIGdmeC5CTEVORF9GVU5DX0FERCxcclxuICAgICAgICAgICAgdGhpcy5fc3JjQmxlbmRGYWN0b3IsIHRoaXMuX2RzdEJsZW5kRmFjdG9yLFxyXG4gICAgICAgICAgICBnZnguQkxFTkRfRlVOQ19BREQsXHJcbiAgICAgICAgICAgIHRoaXMuX3NyY0JsZW5kRmFjdG9yLCB0aGlzLl9kc3RCbGVuZEZhY3RvclxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG59XHJcbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGggPSBcIi4vcmVuZGVyL3N5c3RlbS9SZW5kZXJTeXN0ZW0udHNcIiAvPlxyXG5cclxud2luZG93LmxjYyRyZW5kZXIgPSBsY2MkcmVuZGVyO1xyXG4iLCJcclxubW9kdWxlIGxjYyRyZW5kZXIge1xyXG5cclxuZXhwb3J0IGNsYXNzIFV0aWxzIHtcclxuXHQvKipcclxuXHQgKiDpgJrov4dVVUlE6I635b6X6LWE5rqQXHJcblx0ICovXHJcblx0c3RhdGljIGdldEFzc2V0QnlVVUlEPFQgZXh0ZW5kcyBjYy5Bc3NldD4odXVpZDpzdHJpbmcpe1xyXG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlPFQ+KChyZXNvbHZlKT0+e1xyXG5cdFx0XHRjYy5hc3NldE1hbmFnZXIubG9hZEFueShbIHV1aWQgXSwgZnVuY3Rpb24gKGVyciwgYXNzZXQ6VCkge1xyXG5cdFx0XHRcdGlmKCFlcnIgJiYgYXNzZXQpe1xyXG5cdFx0XHRcdFx0cmVzb2x2ZShhc3NldCk7XHJcblx0XHRcdFx0fWVsc2V7XHJcblx0XHRcdFx0XHRjYy53YXJuKFwibm90IGZvdW5kIGFzc2V0IDogJXNcIiwgdXVpZCk7XHJcblx0XHRcdFx0XHRyZXNvbHZlKG51bGwpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9KTtcclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiXHJcbm1vZHVsZSBsY2MkcmVuZGVyIHtcclxuXHJcbi8qKlxyXG4gKiDmoYbmnrbkvb/nlKjnmoRVVUlE6KGoXHJcbiAqL1xyXG5leHBvcnQgbGV0IFVVSUQgPSB7XHJcblx0LyoqXHJcblx0ICog5riy5p+T56iL5bqPXHJcblx0ICovXHJcblx0ZWZmZWN0cyA6IHtcclxuXHRcdFtcImxjYy0yZC1mbGFzaF9saWdodFwiXSA6IFwiOTM1MGNkNTMtNzJkMy00NDVlLTliNjQtNjAyNWY5YjAzM2YzXCIsXHJcblx0XHRbXCJsY2MtMmQtZmx1eGF5X3N1cGVyXCJdIDogXCJlMDAzMjM3OC03NWEzLTQwNGEtYWU0ZS1mZTYzYmQ5MWM4NzVcIixcclxuXHRcdFtcImxjYy0yZC1zcHJpdGVfbWFza1wiXSA6IFwiNmY4ZGQ4ZWItZjUwZC00ZDAyLThhZTctNDZkNGJiM2Y3M2ZhXCIsXHJcblx0XHRbXCJsY2MtMmRfZ2xvd19pbm5lclwiXSA6IFwiOGJmNDY0OTgtNjU3Ni00NjJiLTgxNjgtNzY0MGNlNTIxOThjXCIsXHJcblx0XHRbXCJsY2MtMmRfZ2xvd19vdXR0ZXJcIl0gOiBcIjViNDlkMmY0LTkzNDMtNDdhNi04NGY0LTJiZmVlYWYwZWI2M1wiLFxyXG5cdFx0W1wibGNjLTJkX21vc2FpY1wiXSA6IFwiZGY3Y2U1MDUtNjY3Ni00ZjlkLThiYWUtNWY4N2JkNTNmMWMyXCIsXHJcblx0XHRbXCJsY2MtMmRfcGhvdG9cIl0gOiBcImQzNDE0YjMxLWU1NjEtNDBlOC1iNzhlLTBhZDM4ZjhiYzdjOVwiLFxyXG5cdFx0W1wibGNjLTJkX291dGxpbmVcIl0gOiBcImUwYmU4NzQwLTUzNzUtNGFlOS1iMjQ2LTc4NGMxNDdiYmZiOVwiLFxyXG5cdFx0W1wibGNjLTJkX2dhdXNzaWFuX2JsdXJcIl0gOiBcIjQxZjRkNDc0LWQ3MDctNDViYi1hZjkzLTYzNzU3M2Y5MmQ1NFwiLFxyXG5cdFx0W1wibGNjLTJkX3JvdW5kX2Nvcm5lcl9jcm9wXCJdIDogXCI3YzI0YjU3ZS1lODE5LTRmYzktYThkMi1iMDZjZjYxYjc4MmRcIixcclxuXHRcdFtcImxjYy0yZF9kaXNzb2x2ZVwiXSA6IFwiYWIyOTdkOTgtNGMwOS00Y2ExLTk0NWMtYTdmYThlMjkwYmNhXCIsXHJcblx0XHRbXCJsY2MtMmRfdHJhbnNmb3JtXCJdIDogXCIxYmVmMjJmYi04NDMzLTQwNDktYWQzNS0xZmJmOWU3MzQ4YWRcIixcclxuXHRcdFtcImxjYy0yZF9yYWluX2Ryb3BfcmlwcGxlc1wiXSA6IFwiZjM0NWU2NTYtMDZkMi00MmIzLTg5ZWMtOTM4MTgyYjMyMTEzXCIsXHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog5p2Q6LSoXHJcblx0ICovXHJcblx0bWF0ZXJpYWxzIDoge1xyXG5cdFx0W1wibGNjLTJkLWZsYXNoX2xpZ2h0XCJdIDogXCI1OGNlMjY5Ni00YjdiLTQ4MGUtYWY5OC01M2I1YmFlNDhiZTNcIixcclxuXHRcdFtcImxjYy0yZC1mbHV4YXlfc3VwZXJcIl0gOiBcIjQ2YjE1OWY5LTk2NzMtNGMzNi05ODI2LWI5OGU1YWY4NWYxOVwiLFxyXG5cdFx0W1wibGNjLTJkLXNwcml0ZV9tYXNrXCJdIDogXCI1ODMyNDM3YS1mMjVlLTQwOTQtOGMxMS1kMTRlNWJlNWY0ZjlcIixcclxuXHRcdFtcImxjYy0yZF9nbG93X2lubmVyXCJdIDogXCI0MTdmNzVjZi0wOGQ3LTRkMGYtODJmNy0yOTljNzk1MjllYWFcIixcclxuXHRcdFtcImxjYy0yZF9nbG93X291dHRlclwiXSA6IFwiMjM4MGFkY2QtOTMzMS00ZWI4LTliNzMtNWM5YmIyZDcyZDhkXCIsXHJcblx0XHRbXCJsY2MtMmRfbW9zYWljXCJdIDogXCI0YjYxOTVmYi1mMTcyLTRhOTctYjYxOC0xOGFhNTFlYmYzYWFcIixcclxuXHRcdFtcImxjYy0yZF9waG90b1wiXSA6IFwiMDZlNDE4NjQtNGRjNi00ODlhLTg4NjctOTVlODI5ZGIwZTViXCIsXHJcblx0XHRbXCJsY2MtMmRfb3V0bGluZVwiXSA6IFwiZTI0YjNjZmYtYjJlMC00OWFhLWIxMTUtMTM3MDk2NDQzN2RjXCIsXHJcblx0XHRbXCJsY2MtMmRfZ2F1c3NpYW5fYmx1clwiXSA6IFwiZGQzZDhmNzgtOWI3OS00Y2E3LTliZjctN2EwOWY3YjM0MTA4XCIsXHJcblx0XHRbXCJsY2MtMmRfcm91bmRfY29ybmVyX2Nyb3BcIl0gOiBcImE4NmU4ODY0LTUzOTAtNDQzZi1iNDFiLWIzOGU5ZDU4NGM0M1wiLFxyXG5cdFx0W1wibGNjLTJkX2Rpc3NvbHZlXCJdIDogXCI0NjU5ZDgzYi1mMjc0LTQyYzQtODc0ZS02ZDVmM2UyMjBhZGNcIixcclxuXHRcdFtcImxjYy0yZF90cmFuc2Zvcm1cIl0gOiBcIjQ0NWU4NWNlLWRlZmMtNDdjNi04MjFmLWVhYWY4MzE1MzNmYlwiLFxyXG5cdFx0W1wibGNjLTJkX3JhaW5fZHJvcF9yaXBwbGVzXCJdIDogXCI2N2JhY2U0Yi1hNDA2LTQzNzgtOThhOS0zMTllYTE4ZTljNmRcIixcclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi9TaGFkZXJDb21wb25lbnQudHNcIiAvPlxyXG5cclxubW9kdWxlIGxjYyRyZW5kZXIge1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5LCBtZW51IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuLy9AdHMtaWdub3JlXHJcbmxldCBnZnggPSBjYy5nZng7XHJcblxyXG5sZXQgX3RlbXBfY29sb3IgPSBjYy5jb2xvcigpO1xyXG5cclxuQGNjY2xhc3MoXCJsY2MkcmVuZGVyLlNoYWRlckNvbG9yXCIpXHJcbkBtZW51KFwiaTE4bjpsY2MtcmVuZGVyLm1lbnVfY29tcG9uZW50L1NoYWRlckNvbG9yXCIpXHJcbmV4cG9ydCBjbGFzcyBTaGFkZXJDb2xvciBleHRlbmRzIFNoYWRlckNvbXBvbmVudCB7XHJcblxyXG4gICAgX3RhZzpzdHJpbmcgPSBcImNvbG9yXCI7XHJcbiAgICBcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6Y2MuRW51bShWYWx1ZVR5cGUpXHJcblx0fSlcclxuXHRfY29sb3JUeXBlOlZhbHVlVHlwZSA9IFZhbHVlVHlwZS5TSU5HTEU7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOmNjLkVudW0oVmFsdWVUeXBlKSxcclxuXHRcdHRvb2x0aXAgOiBcIuminOiJsuexu+Wei1wiXHJcblx0fSlcclxuXHRnZXQgY29sb3JUeXBlKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fY29sb3JUeXBlO1xyXG5cdH1cclxuXHRzZXQgY29sb3JUeXBlKHZhbHVlOlZhbHVlVHlwZSl7XHJcblx0XHRpZih0aGlzLl9jb2xvclR5cGUgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9jb2xvclR5cGUgPSB2YWx1ZTtcclxuXHRcdFx0aWYodmFsdWUgPT0gVmFsdWVUeXBlLkFSUkFZKXtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2NvbG9yVmFyLnR5cGUgPT0gVmFyaWFibGVUeXBlLlVOSUZPUk07XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jb2xvclZhci5fdHlwZXNlbCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2NvbG9yVmFyLl90eXBlc2VsID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KFZhcmlhYmxlQ29uZmlnKVxyXG5cdF9jb2xvclZhcjpWYXJpYWJsZUNvbmZpZyA9IG5ldyBWYXJpYWJsZUNvbmZpZyhcclxuXHRcdHRydWUsIFZhcmlhYmxlVHlwZS5VTklGT1JNLCBcclxuXHRcdFwiQVRUUl9DT0xPUlwiLCBmYWxzZSwgXCJhX2NvbG9yXCIsIFwiVU5JRl9DT0xPUlwiLCBmYWxzZSwgXCJ1X2NvbG9yXCIpO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogVmFyaWFibGVDb25maWcsXHJcblx0XHR0b29sdGlwIDogXCLpopzoibLlj5jph49cIlxyXG5cdH0pXHJcblx0Z2V0IGNvbG9yVmFyKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fY29sb3JWYXI7XHJcblx0fVxyXG5cdHNldCBjb2xvclZhcih2YWx1ZTpWYXJpYWJsZUNvbmZpZyl7XHJcbiAgICAgICAgdGhpcy5fY29sb3JWYXIgPSB2YWx1ZTtcclxuICAgICAgICB0aGlzLm9uVXBkYXRlQ29sb3JzKCk7XHJcblx0XHR0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuXHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIik7XHJcblx0XHQvL0VkaXRvci5sb2coXCJjb2xvclZhclwiKTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgQHByb3BlcnR5KFtjYy5Db2xvcl0pXHJcblx0X2NvbG9yczpjYy5Db2xvcltdID0gWyBjYy5Db2xvci5XSElURSBdO1xyXG5cdFxyXG5cdEBwcm9wZXJ0eSgpXHJcblx0X3VzZU5vZGVDb2xvcjpib29sZWFuID0gZmFsc2U7XHJcblx0QHByb3BlcnR5KHtcclxuICAgICAgICB0b29sdGlwIDogXCLkvb/nlKjoioLngrnnmoTpopzoibJcIlxyXG5cdH0pXHJcblx0Z2V0IHVzZU5vZGVDb2xvcigpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3VzZU5vZGVDb2xvcjtcclxuXHR9XHJcblx0c2V0IHVzZU5vZGVDb2xvcih2YWx1ZTpib29sZWFuKXtcclxuXHRcdGlmKHRoaXMuX3VzZU5vZGVDb2xvciAhPSB2YWx1ZSl7XHJcbiAgICAgICAgICAgIHRoaXMuX3VzZU5vZGVDb2xvciA9IHZhbHVlO1xyXG4gICAgICAgICAgICBpZih2YWx1ZSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uTm9kZUNvbG9yQ2hhbmdlZCgpO1xyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgIHRoaXMub25VcGRhdGVDb2xvcnMoKTtcclxuICAgICAgICAgICAgfVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHZpc2libGUgKCl7XHJcblx0XHRcdHJldHVybiB0aGlzLl9jb2xvclR5cGUgPT0gVmFsdWVUeXBlLlNJTkdMRSAmJiB0aGlzLl9jb2xvclZhci50eXBlID09IFZhcmlhYmxlVHlwZS5VTklGT1JNO1xyXG5cdFx0fSxcclxuXHRcdHR5cGUgOiBjYy5Db2xvcixcclxuXHRcdHRvb2x0aXAgOiBcIuminOiJsuWAvFwiXHJcblx0fSlcclxuXHRnZXQgY29sb3IoKXtcclxuXHRcdHJldHVybiB0aGlzLl9jb2xvcnNbMF07XHJcblx0fVxyXG5cdHNldCBjb2xvcih2YWx1ZTpjYy5Db2xvcil7XHJcbiAgICAgICAgdGhpcy5fdXNlTm9kZUNvbG9yID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5fY29sb3JzWzBdID0gdmFsdWU7XHJcblx0XHR0aGlzLm9uVXBkYXRlQ29sb3JzKCk7XHJcblx0fVxyXG4gICAgXHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHZpc2libGUgKCl7XHJcblx0XHRcdHJldHVybiB0aGlzLl9jb2xvclR5cGUgPT0gVmFsdWVUeXBlLkFSUkFZIHx8IHRoaXMuX2NvbG9yVmFyLnR5cGUgPT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURTtcclxuXHRcdH0sXHJcblx0XHR0eXBlIDogW2NjLkNvbG9yXSxcclxuXHRcdHRvb2x0aXAgOiBcIuminOiJsuaVsOe7hC/popzoibLpobbngrnmlbDnu4RcIlxyXG5cdH0pXHJcblx0Z2V0IGNvbG9ycygpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2NvbG9ycztcclxuXHR9XHJcblx0c2V0IGNvbG9ycyh2YWx1ZTpjYy5Db2xvcltdKXtcclxuICAgICAgICB0aGlzLl91c2VOb2RlQ29sb3IgPSBmYWxzZTtcclxuXHRcdHRoaXMuX2NvbG9ycyA9IHZhbHVlO1xyXG5cdFx0dGhpcy5vblVwZGF0ZUNvbG9ycygpO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICog6aKc6Imy5YGP56e7XHJcblx0ICovXHJcbiAgICBwcml2YXRlIF9jb2xvck9mZnNldDpudW1iZXIgPSAwO1xyXG5cclxuICAgIG9uTG9hZCgpe1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihcInJlbmRlcl9zaGFwZV9jaGVja2VkXCIsIHRoaXMub25SZW5kZXJTaGFwZUNoZWNrZWQsIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihcInJlbmRlcl91cGRhdGVfbWF0ZXJpYWxcIiwgdGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsLCB0aGlzKTtcclxuXHRcdHRoaXMubm9kZS5vbihcInJlbmRlcl91cGRhdGVfYXR0cmlidXRlXCIsIHRoaXMub25SZW5kZXJVcGRhdGVBdHRyaWJ1dGUsIHRoaXMpO1xyXG5cdFx0dGhpcy5ub2RlLm9uKFwicmVuZGVyX3VwZGF0ZV92ZXJ0ZXhcIiwgdGhpcy5vblJlbmRlclVwZGF0ZVJlbmRlckRhdGEsIHRoaXMpO1xyXG5cdFx0dGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLkNPTE9SX0NIQU5HRUQsIHRoaXMub25Ob2RlQ29sb3JDaGFuZ2VkLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRlc3Ryb3koKXtcclxuXHRcdHRoaXMubm9kZS50YXJnZXRPZmYodGhpcyk7XHJcbiAgICB9XHJcbiAgICBcclxuXHRvbkVuYWJsZSgpe1xyXG5cdFx0dGhpcy5vbk5vZGVDb2xvckNoYW5nZWQoKTtcclxuICAgICAgICB0aGlzLm9uU3RhdGVVcGRhdGUodHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRvbkRpc2FibGUoKXtcclxuICAgICAgICB0aGlzLm9uU3RhdGVVcGRhdGUoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25TdGF0ZVVwZGF0ZShlbmFibGU6Ym9vbGVhbil7XHJcblx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfdGFnXCIpO1xyXG5cdFx0aWYodGhpcy5fdXNlTm9kZUNvbG9yKXtcclxuXHRcdFx0dGhpcy5vbk5vZGVDb2xvckNoYW5nZWQoKTtcclxuXHRcdH1lbHNle1xyXG5cdFx0XHR0aGlzLm9uVXBkYXRlQ29sb3JzKCk7XHJcblx0XHR9XHJcbiAgICB9XHJcbiAgICBcclxuXHRwcml2YXRlIG9uTm9kZUNvbG9yQ2hhbmdlZCgpe1xyXG5cdFx0aWYodGhpcy5fdXNlTm9kZUNvbG9yKXtcclxuICAgICAgICAgICAgdGhpcy5vblVwZGF0ZUNvbG9ycygpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIOajgOafpeW9oueKtlxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIG9uUmVuZGVyU2hhcGVDaGVja2VkKCl7XHJcbiAgICAgICAgaWYodGhpcy5fY29sb3JWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuQVRUUklCVVRFKXtcclxuICAgICAgICAgICAgdGhpcy5vblVwZGF0ZUNvbG9ycygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHRcclxuXHQvKipcclxuXHQgKiDmo4Dmn6XpopzoibJcclxuXHQgKi9cclxuXHRwcml2YXRlIG9uVXBkYXRlQ29sb3JzKCl7XHJcblx0XHRpZih0aGlzLmVuYWJsZWQpe1xyXG4gICAgICAgICAgICBsZXQgZGNvbG9yID0gY2MuQ29sb3IuV0hJVEU7XHJcbiAgICAgICAgICAgIGlmKHRoaXMuX2NvbG9ycy5sZW5ndGggPD0gMCl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jb2xvcnMgPSBbIGRjb2xvci5jbG9uZSgpIF07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYodGhpcy5fY29sb3JWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuQVRUUklCVVRFKXtcclxuICAgICAgICAgICAgICAgIGxldCByc3lzID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKTtcclxuICAgICAgICAgICAgICAgIGlmKHJzeXMpe1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCB2YyA9IHJzeXMuX3ZlcnRpY2VzQ291bnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5fY29sb3JzLmxlbmd0aCA8IHZjKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNvbG9ycyA9IHRoaXMuX2NvbG9ycztcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHZjOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNvbG9yID0gY29sb3JzW2ldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoY29sb3IgPT0gbnVsbCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3JzW2ldID0gZGNvbG9yLmNsb25lKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fY29sb3JzLmxlbmd0aCA9IHZjO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9ZWxzZSBpZih0aGlzLl9jb2xvclR5cGUgPT0gVmFsdWVUeXBlLlNJTkdMRSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jb2xvcnMubGVuZ3RoID0gMTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZih0aGlzLl91c2VOb2RlQ29sb3Ipe1xyXG4gICAgICAgICAgICAgICAgbGV0IG5jb2xvciA9IHRoaXMubm9kZS5jb2xvcjtcclxuICAgICAgICAgICAgICAgIGZvcihsZXQgaSA9IDAsIGwgPSB0aGlzLl9jb2xvcnMubGVuZ3RoOyBpIDwgbDsgaSsrKXtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9jb2xvcnNbaV0gPSBuY29sb3IuY2xvbmUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZih0aGlzLl9jb2xvclZhci50eXBlID09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEUpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX2F0dHJpYnV0ZVwiKTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHR9XHJcbiAgICBcclxuXHRwcml2YXRlIHByZW11bHRpcGx5QWxwaGEoY29sb3I6Y2MuQ29sb3Ipe1xyXG5cdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0Y2MuQ29sb3IucHJlbXVsdGlwbHlBbHBoYShfdGVtcF9jb2xvciwgY29sb3IpO1xyXG5cdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0cmV0dXJuIF90ZW1wX2NvbG9yLl92YWw7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDojrflvpflsZ7mgKfpopzoibJcclxuXHQgKiBAcGFyYW0gY29tcCBcclxuXHQgKi9cclxuXHRwcml2YXRlIGdldEF0dHJpYnV0ZUNvbG9ycyhjb21wOlJlbmRlclN5c3RlbSl7XHJcblx0XHRsZXQgcHJlbXVsdGlwbHkgPSBjb21wLl9zcmNCbGVuZEZhY3RvciA9PT0gY2MubWFjcm8uQmxlbmRGYWN0b3IuT05FO1xyXG5cdFx0bGV0IGNvbG9yczpudW1iZXJbXSA9IFtdO1xyXG5cdFx0aWYodGhpcy5fY29sb3JWYXIudHlwZSA9PT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURSl7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwLCBsID0gdGhpcy5fY29sb3JzLmxlbmd0aDsgaSA8IGw7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICBsZXQgYyA9IHRoaXMuX2NvbG9yc1tpXTtcclxuICAgICAgICAgICAgICAgIC8vIEB0cy1pZ25vcmVcclxuICAgICAgICAgICAgICAgIGNvbG9ycy5wdXNoKHByZW11bHRpcGx5ID8gdGhpcy5wcmVtdWx0aXBseUFscGhhKGMpIDogYy5fdmFsKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHRcdHJldHVybiBjb2xvcnM7XHJcblx0fVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog6I635b6X5bi46YeP6aKc6ImyXHJcbiAgICAgKiBAcGFyYW0gY29tcCBcclxuICAgICAqL1xyXG5cdHByaXZhdGUgZ2V0VW5pZm9ybUNvbG9ycyhjb21wOlJlbmRlclN5c3RlbSl7XHJcblx0XHRsZXQgcHJlbXVsdGlwbHkgPSBjb21wLl9zcmNCbGVuZEZhY3RvciA9PT0gY2MubWFjcm8uQmxlbmRGYWN0b3IuT05FO1xyXG4gICAgICAgIGxldCBjb2xvcnM6Y2MuVmVjNFtdID0gW107XHJcbiAgICAgICAgaWYodGhpcy5fY29sb3JWYXIudHlwZSA9PT0gVmFyaWFibGVUeXBlLlVOSUZPUk0pe1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMCwgbCA9IHRoaXMuX2NvbG9ycy5sZW5ndGg7IGkgPCBsOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgbGV0IGMgPSB0aGlzLl9jb2xvcnNbaV07XHJcbiAgICAgICAgICAgICAgICBpZihwcmVtdWx0aXBseSl7XHJcbiAgICAgICAgICAgICAgICAgICAgLy9AdHMtaWdub3JlXHJcbiAgICAgICAgICAgICAgICAgICAgY2MuQ29sb3IucHJlbXVsdGlwbHlBbHBoYShfdGVtcF9jb2xvciwgYyk7XHJcbiAgICAgICAgICAgICAgICAgICAgYyA9IF90ZW1wX2NvbG9yO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgY29sb3JzLnB1c2gobmV3IGNjLlZlYzQoYy5yIC8gMjU1LCBjLmcgLyAyNTUsIGMuYiAvIDI1NSwgYy5hIC8gMjU1KSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblx0XHRyZXR1cm4gY29sb3JzO1xyXG5cdH1cclxuXHJcbiAgICBwcml2YXRlIG9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoY29tcDpSZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pKSB7XHJcblx0XHRpZihjb21wKXtcclxuXHRcdFx0Ly8gbWFrZSBzdXJlIG1hdGVyaWFsIGlzIGJlbG9uZyB0byBzZWxmLlxyXG5cdFx0XHQvL0B0cy1pZ25vcmVcclxuXHRcdFx0bGV0IG1hdGVyaWFsID0gY29tcC5fbWF0ZXJpYWxzWzBdO1xyXG5cdFx0XHRpZiAobWF0ZXJpYWwpIHtcclxuXHRcdFx0XHRpZiAodGhpcy5jaGVja01hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX2NvbG9yVmFyLnVuaWZNYWNybykpIHtcclxuXHRcdFx0XHRcdGlmKHRoaXMuZW5hYmxlZCAmJiB0aGlzLl9jb2xvclZhci50eXBlID09PSBWYXJpYWJsZVR5cGUuVU5JRk9STSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fY29sb3JWYXIudW5pZk1hY3JvLCB0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNvbG9ycyA9IHRoaXMuZ2V0VW5pZm9ybUNvbG9ycyhjb21wKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5fY29sb3JUeXBlID09IFZhbHVlVHlwZS5TSU5HTEUpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF0ZXJpYWwuc2V0UHJvcGVydHkodGhpcy5fY29sb3JWYXIudW5pZk5hbWUsIGNvbG9yc1swXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNleyAvLyBBUlJBWVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IF9jb2xvcnM6bnVtYmVyW10gPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcihsZXQgaSA9IDAsIGwgPSBjb2xvcnMubGVuZ3RoOyBpIDwgbDsgaSsrKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgYyA9IGNvbG9yc1tpXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfY29sb3JzLnB1c2goYy54LCBjLnksIGMueiwgYy53KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hdGVyaWFsLnNldFByb3BlcnR5KHRoaXMuX2NvbG9yVmFyLnVuaWZOYW1lLCBuZXcgRmxvYXQzMkFycmF5KGNvbG9ycy5sZW5ndGggKiA0KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXRlcmlhbC5zZXRQcm9wZXJ0eSh0aGlzLl9jb2xvclZhci51bmlmTmFtZSwgX2NvbG9ycyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHRcdFx0XHRcdFx0Ly9FZGl0b3IubG9nKFwib25SZW5kZXJVcGRhdGVNYXRlcmlhbFwiLCB0aGlzLmdldFVuaWZvcm1Db2xvcihjb21wKSk7XHJcblx0XHRcdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl9jb2xvclZhci51bmlmTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWxcIiwgdGhpcy5lbmFibGVkKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuICAgIH1cclxuXHRcclxuXHRwcml2YXRlIG9uUmVuZGVyVXBkYXRlQXR0cmlidXRlKGNvbXA6UmVuZGVyU3lzdGVtID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKSl7XHJcblx0XHRpZihjb21wKXtcclxuXHRcdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0XHRsZXQgbWF0ZXJpYWwgPSBjb21wLl9tYXRlcmlhbHNbMF07XHJcblx0XHRcdGlmIChtYXRlcmlhbCkge1xyXG5cdFx0XHRcdGlmICh0aGlzLmNoZWNrTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fY29sb3JWYXIuYXR0ck1hY3JvKSkge1xyXG5cdFx0XHRcdFx0aWYodGhpcy5lbmFibGVkICYmIHRoaXMuX2NvbG9yVmFyLnR5cGUgPT09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEUpe1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX2NvbG9yVmFyLmF0dHJNYWNybywgdHJ1ZSk7XHJcblx0XHRcdFx0XHRcdHRoaXMuX2NvbG9yT2Zmc2V0ID0gY29tcC5hZGRWZXJ0ZXhBdHRyaWJ1dGUoeyBcclxuXHRcdFx0XHRcdFx0XHRuYW1lOiB0aGlzLl9jb2xvclZhci5hdHRyTmFtZSwgXHJcblx0XHRcdFx0XHRcdFx0dHlwZTogZ2Z4LkFUVFJfVFlQRV9VSU5UOCwgXHJcblx0XHRcdFx0XHRcdFx0bnVtOiA0LCBcclxuXHRcdFx0XHRcdFx0XHRub3JtYWxpemU6IHRydWUgXHJcblx0XHRcdFx0XHRcdH0sIDEpO1xyXG5cdFx0XHRcdFx0XHQvL0VkaXRvci5sb2coXCJvblJlbmRlclVwZGF0ZUF0dHJpYnV0ZVwiLCB0aGlzLl9jb2xvck9mZnNldCk7XHJcblx0XHRcdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl9jb2xvclZhci5hdHRyTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5fY29sb3JPZmZzZXQgPSAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0Ly9FZGl0b3IubG9nKFwib25SZW5kZXJVcGRhdGVBdHRyaWJ1dGVcIiwgdGhpcy5lbmFibGVkKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHByaXZhdGUgb25SZW5kZXJVcGRhdGVSZW5kZXJEYXRhKGNvbXA6UmVuZGVyU3lzdGVtID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKSl7XHJcbiAgICAgICAgaWYoY29tcCAmJiB0aGlzLmVuYWJsZWQpe1xyXG5cdFx0XHQvL0VkaXRvci5sb2coXCJvblJlbmRlclVwZGF0ZVJlbmRlckRhdGFcIik7XHJcblx0XHRcdGlmKHRoaXMuX2NvbG9yT2Zmc2V0ID4gMCl7XHJcblx0XHRcdFx0Ly9AdHMtaWdub3JlXHJcblx0XHRcdFx0bGV0IGFzc2VtYmxlciA9IGNvbXAuX2Fzc2VtYmxlcjtcclxuXHRcdFx0XHRsZXQgdWludFZlcnRzID0gYXNzZW1ibGVyLnJlbmRlckRhdGEudWludFZEYXRhc1swXTtcclxuXHRcdFx0XHRpZiAodWludFZlcnRzKSB7XHJcblx0XHRcdFx0XHRsZXQgY29sb3JzID0gdGhpcy5nZXRBdHRyaWJ1dGVDb2xvcnMoY29tcCk7XHJcblx0XHRcdFx0XHRsZXQgY2xlbiA9IGNvbG9ycy5sZW5ndGg7XHJcblx0XHRcdFx0XHRsZXQgY29sb3JPZmZzZXQgPSB0aGlzLl9jb2xvck9mZnNldDtcclxuXHRcdFx0XHRcdGxldCBmbG9hdHNQZXJWZXJ0ID0gYXNzZW1ibGVyLmZsb2F0c1BlclZlcnQ7XHJcblx0XHRcdFx0XHRmb3IgKGxldCBpID0gY29sb3JPZmZzZXQsIGwgPSB1aW50VmVydHMubGVuZ3RoLCBqID0gMDsgaSA8IGw7IGkgKz0gZmxvYXRzUGVyVmVydCwgaisrKSB7XHJcblx0XHRcdFx0XHRcdHVpbnRWZXJ0c1tpXSA9IGogPCBjbGVuID8gY29sb3JzW2pdIDogY29sb3JzWzBdO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG4gICAgICAgIH1cclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi9TaGFkZXJDb21wb25lbnQudHNcIiAvPlxyXG5cclxubW9kdWxlIGxjYyRyZW5kZXIge1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5LCBtZW51IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuLy9AdHMtaWdub3JlXHJcbmxldCBnZnggPSBjYy5nZng7XHJcblxyXG4vKipcclxuICog5YC86YWN572uXHJcbiAqL1xyXG5jb25zdCBWQyA9e1xyXG5cdHRhZyA6IFwidmFsdWVcIixcclxuXHJcblx0dmFyU2VsZWN0IDogdHJ1ZSxcclxuXHR2YXJEZWZ0eXBlIDogVmFyaWFibGVUeXBlLlVOSUZPUk0sXHJcblx0dmFyQXR0ck1hY3JvIDogXCJBVFRSX0ZMT0FUXCIsXHJcblx0dmFyQXR0ck1hY3JvQ2hlY2tPbmx5IDogZmFsc2UsXHJcblx0dmFyQXR0ck5hbWUgOiBcImFfZmxvYXR2XCIsXHJcblx0dmFyVW5pZk1hY3JvIDogXCJVTklGX0ZMT0FUXCIsXHJcblx0dmFyVW5pZk1hY3JvQ2hlY2tPbmx5IDogZmFsc2UsXHJcblx0dmFyVW5pZk5hbWUgOiBcInVfZmxvYXR2XCIsXHJcblx0XHJcblx0dmFsRGVmYXVsdCA6IDAsXHJcblx0dmFsTmV3IDogKCk9PnsgcmV0dXJuIDA7IH0sXHJcblx0dmFsVHlwZSA6IGNjLkZsb2F0LFxyXG5cclxuXHR0eXBlU2l6ZSA6IDEsXHJcblxyXG5cdHRvQXJyYXkgOiAoYXJyOm51bWJlcltdLCB2YWx1ZTpudW1iZXIsIG9mZmVzdDpudW1iZXIpID0+IHtcclxuXHRcdGFycltvZmZlc3RdID0gdmFsdWU7XHJcbiAgICB9LFxyXG59XHJcblxyXG5AY2NjbGFzcyhcImxjYyRyZW5kZXIuU2hhZGVyRmxvYXRcIilcclxuQG1lbnUoXCJpMThuOmxjYy1yZW5kZXIubWVudV9jb21wb25lbnQvU2hhZGVyRmxvYXRcIilcclxuZXhwb3J0IGNsYXNzIFNoYWRlckZsb2F0IGV4dGVuZHMgU2hhZGVyQ29tcG9uZW50IHtcclxuXHJcblx0X3RhZzpzdHJpbmcgPSBWQy50YWc7XHJcblxyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDpjYy5FbnVtKFZhbHVlVHlwZSlcclxuXHR9KVxyXG5cdF92YWx1ZVR5cGU6VmFsdWVUeXBlID0gVmFsdWVUeXBlLlNJTkdMRTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6Y2MuRW51bShWYWx1ZVR5cGUpLFxyXG5cdFx0dG9vbHRpcCA6IFwi5YC857G75Z6LXCJcclxuXHR9KVxyXG5cdGdldCB2YWx1ZVR5cGUoKXtcclxuXHRcdHJldHVybiB0aGlzLl92YWx1ZVR5cGU7XHJcblx0fVxyXG5cdHNldCB2YWx1ZVR5cGUodmFsdWU6VmFsdWVUeXBlKXtcclxuXHRcdGlmKHRoaXMuX3ZhbHVlVHlwZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3ZhbHVlVHlwZSA9IHZhbHVlO1xyXG5cdFx0XHRpZih2YWx1ZSA9PSBWYWx1ZVR5cGUuQVJSQVkpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmFsdWVWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuVU5JRk9STTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3ZhbHVlVmFyLl90eXBlc2VsID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmFsdWVWYXIuX3R5cGVzZWwgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRAcHJvcGVydHkoVmFyaWFibGVDb25maWcpXHJcblx0X3ZhbHVlVmFyOlZhcmlhYmxlQ29uZmlnID0gbmV3IFZhcmlhYmxlQ29uZmlnKFxyXG5cdFx0VkMudmFyU2VsZWN0LCBWQy52YXJEZWZ0eXBlLCBcclxuXHRcdFZDLnZhckF0dHJNYWNybywgVkMudmFyQXR0ck1hY3JvQ2hlY2tPbmx5LCBWQy52YXJBdHRyTmFtZSwgXHJcblx0XHRWQy52YXJVbmlmTWFjcm8sIFZDLnZhclVuaWZNYWNyb0NoZWNrT25seSwgVkMudmFyVW5pZk5hbWUpO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogVmFyaWFibGVDb25maWcsXHJcblx0XHR0b29sdGlwIDogXCLlgLzlj5jph49cIlxyXG5cdH0pXHJcblx0Z2V0IHZhbHVlVmFyKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fdmFsdWVWYXI7XHJcblx0fVxyXG5cdHNldCB2YWx1ZVZhcih2YWx1ZTpWYXJpYWJsZUNvbmZpZyl7XHJcblx0XHR0aGlzLl92YWx1ZVZhciA9IHZhbHVlO1xyXG5cdFx0dGhpcy5vblVwZGF0ZVZhbHVlcygpO1xyXG5cdFx0dGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsKCk7XHJcblx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfYXR0cmlidXRlXCIpO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBAcHJvcGVydHkoW1ZDLnZhbFR5cGVdKVxyXG5cdF92YWx1ZXM6dHlwZW9mIFZDLnZhbERlZmF1bHRbXSA9IFsgVkMudmFsTmV3KCkgXTtcclxuXHRcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dmlzaWJsZSAoKXtcclxuXHRcdFx0cmV0dXJuIHRoaXMuX3ZhbHVlVHlwZSA9PSBWYWx1ZVR5cGUuU0lOR0xFICYmIHRoaXMuX3ZhbHVlVmFyLnR5cGUgPT0gVmFyaWFibGVUeXBlLlVOSUZPUk07XHJcblx0XHR9LFxyXG5cdFx0dHlwZSA6IFZDLnZhbFR5cGUsXHJcblx0XHR0b29sdGlwIDogXCLlgLxcIlxyXG5cdH0pXHJcblx0Z2V0IHZhbHVlKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fdmFsdWVzWzBdO1xyXG5cdH1cclxuXHRzZXQgdmFsdWUodmFsdWVfOnR5cGVvZiBWQy52YWxEZWZhdWx0KXtcclxuXHRcdHRoaXMuX3ZhbHVlc1swXSA9IHZhbHVlXztcclxuXHRcdHRoaXMub25VcGRhdGVWYWx1ZXMoKTtcclxuXHR9XHJcblxyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdmFsdWVUeXBlID09IFZhbHVlVHlwZS5BUlJBWSB8fCB0aGlzLl92YWx1ZVZhci50eXBlID09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEU7XHJcblx0XHR9LFxyXG5cdFx0dHlwZSA6IFtWQy52YWxUeXBlXSxcclxuXHRcdHRvb2x0aXAgOiBcIuWAvOaVsOe7hC/lgLzpobbngrnmlbDnu4RcIlxyXG5cdH0pXHJcblx0Z2V0IHZhbHVlcygpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3ZhbHVlcztcclxuXHR9XHJcblx0c2V0IHZhbHVlcyh2YWx1ZTp0eXBlb2YgVkMudmFsRGVmYXVsdFtdKXtcclxuXHRcdHRoaXMuX3ZhbHVlcyA9IHZhbHVlO1xyXG5cdFx0dGhpcy5vblVwZGF0ZVZhbHVlcygpO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICog5YC85YGP56e7XHJcblx0ICovXHJcbiAgICBwcml2YXRlIF92YWx1ZU9mZnNldDpudW1iZXIgPSAwO1xyXG5cclxuICAgIG9uTG9hZCgpe1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihcInJlbmRlcl9zaGFwZV9jaGVja2VkXCIsIHRoaXMub25SZW5kZXJTaGFwZUNoZWNrZWQsIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihcInJlbmRlcl91cGRhdGVfbWF0ZXJpYWxcIiwgdGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsLCB0aGlzKTtcclxuXHRcdHRoaXMubm9kZS5vbihcInJlbmRlcl91cGRhdGVfYXR0cmlidXRlXCIsIHRoaXMub25SZW5kZXJVcGRhdGVBdHRyaWJ1dGUsIHRoaXMpO1xyXG5cdFx0dGhpcy5ub2RlLm9uKFwicmVuZGVyX3VwZGF0ZV92ZXJ0ZXhcIiwgdGhpcy5vblJlbmRlclVwZGF0ZVJlbmRlckRhdGEsIHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRGVzdHJveSgpe1xyXG5cdFx0dGhpcy5ub2RlLnRhcmdldE9mZih0aGlzKTtcclxuICAgIH1cclxuICAgIFxyXG5cdG9uRW5hYmxlKCl7XHJcbiAgICAgICAgdGhpcy5vblN0YXRlVXBkYXRlKHRydWUpO1xyXG5cdH1cclxuXHJcblx0b25EaXNhYmxlKCl7XHJcbiAgICAgICAgdGhpcy5vblN0YXRlVXBkYXRlKGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uU3RhdGVVcGRhdGUoZW5hYmxlOmJvb2xlYW4pe1xyXG5cdFx0dGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX3RhZ1wiKTtcclxuXHRcdHRoaXMub25VcGRhdGVWYWx1ZXMoKTtcclxuXHR9XHJcblx0XHJcbiAgICAvKipcclxuICAgICAqIOajgOafpeW9oueKtlxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIG9uUmVuZGVyU2hhcGVDaGVja2VkKCl7XHJcbiAgICAgICAgaWYodGhpcy5fdmFsdWVWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuQVRUUklCVVRFKXtcclxuICAgICAgICAgICAgdGhpcy5vblVwZGF0ZVZhbHVlcygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHRcclxuXHQvKipcclxuXHQgKiDmo4Dmn6XlgLxcclxuXHQgKi9cclxuXHRwcml2YXRlIG9uVXBkYXRlVmFsdWVzKCl7XHJcblx0XHRpZih0aGlzLmVuYWJsZWQpe1xyXG4gICAgICAgICAgICBpZih0aGlzLl92YWx1ZXMubGVuZ3RoIDw9IDApe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmFsdWVzID0gWyBWQy52YWxOZXcoKSBdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmKHRoaXMuX3ZhbHVlVmFyLnR5cGUgPT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURSl7XHJcbiAgICAgICAgICAgICAgICBsZXQgcnN5cyA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcbiAgICAgICAgICAgICAgICBpZihyc3lzKXtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgdmMgPSByc3lzLl92ZXJ0aWNlc0NvdW50O1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHRoaXMuX3ZhbHVlcy5sZW5ndGggPCB2Yyl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCB2YWx1ZXMgPSB0aGlzLl92YWx1ZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB2YzsgaSsrKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBjb2xvciA9IHZhbHVlc1tpXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKGNvbG9yID09IG51bGwpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlc1tpXSA9IFZDLnZhbE5ldygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3ZhbHVlcy5sZW5ndGggPSB2YztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfWVsc2UgaWYodGhpcy5fdmFsdWVUeXBlID09IFZhbHVlVHlwZS5TSU5HTEUpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmFsdWVzLmxlbmd0aCA9IDE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYodGhpcy5fdmFsdWVWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuQVRUUklCVVRFKXtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIik7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsKCk7XHJcbiAgICAgICAgICAgIH1cclxuXHRcdH1cclxuXHR9XHJcbiAgICBcclxuICAgIHByaXZhdGUgb25SZW5kZXJVcGRhdGVNYXRlcmlhbCAoY29tcDpSZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pKSB7XHJcblx0XHRpZihjb21wKXtcclxuXHRcdFx0Ly8gbWFrZSBzdXJlIG1hdGVyaWFsIGlzIGJlbG9uZyB0byBzZWxmLlxyXG5cdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdGxldCBtYXRlcmlhbCA9IGNvbXAuX21hdGVyaWFsc1swXTtcclxuXHRcdFx0aWYgKG1hdGVyaWFsKSB7XHJcblx0XHRcdFx0aWYgKHRoaXMuY2hlY2tNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl92YWx1ZVZhci51bmlmTWFjcm8pKSB7XHJcblx0XHRcdFx0XHRpZih0aGlzLmVuYWJsZWQgJiYgdGhpcy5fdmFsdWVWYXIudHlwZSA9PT0gVmFyaWFibGVUeXBlLlVOSUZPUk0pe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3ZhbHVlVmFyLnVuaWZNYWNybywgdHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHRoaXMuX3ZhbHVlVHlwZSA9PSBWYWx1ZVR5cGUuU0lOR0xFKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hdGVyaWFsLnNldFByb3BlcnR5KHRoaXMuX3ZhbHVlVmFyLnVuaWZOYW1lLCB0aGlzLl92YWx1ZXNbMF0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCB2YWx1ZXMgPSB0aGlzLl92YWx1ZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgX3ZhbHVlczpudW1iZXJbXSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGRlZnYgPSBWQy52YWxEZWZhdWx0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yKGxldCBpID0gMCwgbCA9IHZhbHVlcy5sZW5ndGg7IGkgPCBsOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFZDLnRvQXJyYXkoX3ZhbHVlcywgdmFsdWVzW2ldIHx8IGRlZnYsIF92YWx1ZXMubGVuZ3RoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hdGVyaWFsLnNldFByb3BlcnR5KHRoaXMuX3ZhbHVlVmFyLnVuaWZOYW1lLCBuZXcgRmxvYXQzMkFycmF5KHZhbHVlcy5sZW5ndGggKiBWQy50eXBlU2l6ZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF0ZXJpYWwuc2V0UHJvcGVydHkodGhpcy5fdmFsdWVWYXIudW5pZk5hbWUsIF92YWx1ZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblx0XHRcdFx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWxcIiwgdGhpcy5fdmFsdWUpO1xyXG5cdFx0XHRcdFx0fWVsc2V7XHJcblx0XHRcdFx0XHRcdHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fdmFsdWVWYXIudW5pZk1hY3JvLCBmYWxzZSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHQvL0VkaXRvci5sb2coXCJvblJlbmRlclVwZGF0ZU1hdGVyaWFsXCIsIHRoaXMuZW5hYmxlZCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcbiAgICB9XHJcblx0XHJcblx0LyoqXHJcblx0ICog6I635b6X5bGe5oCn5YC8XHJcblx0ICogQHBhcmFtIGNvbXAgXHJcblx0ICovXHJcblx0cHJpdmF0ZSBnZXRBdHRyaWJ1dGVWYWx1ZXMoY29tcDpSZW5kZXJTeXN0ZW0pe1xyXG5cdFx0bGV0IHZhbHVlczpudW1iZXJbXSA9IFtdO1xyXG5cdFx0bGV0IHZjID0gY29tcC5fdmVydGljZXNDb3VudDtcclxuICAgICAgICBsZXQgZGVmdiA9IFZDLnZhbERlZmF1bHQ7XHJcbiAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHZjOyBpKyspe1xyXG4gICAgICAgICAgICBWQy50b0FycmF5KHZhbHVlcywgdGhpcy5fdmFsdWVzW2ldIHx8IGRlZnYsIHZhbHVlcy5sZW5ndGgpO1xyXG4gICAgICAgIH1cclxuXHRcdHJldHVybiB2YWx1ZXM7XHJcblx0fVxyXG5cclxuXHRwcml2YXRlIG9uUmVuZGVyVXBkYXRlQXR0cmlidXRlKGNvbXA6UmVuZGVyU3lzdGVtID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKSl7XHJcblx0XHRpZihjb21wKXtcclxuXHRcdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0XHRsZXQgbWF0ZXJpYWwgPSBjb21wLl9tYXRlcmlhbHNbMF07XHJcblx0XHRcdGlmIChtYXRlcmlhbCkge1xyXG5cdFx0XHRcdGlmICh0aGlzLmNoZWNrTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fdmFsdWVWYXIuYXR0ck1hY3JvKSkge1xyXG5cdFx0XHRcdFx0aWYodGhpcy5lbmFibGVkICYmIHRoaXMuX3ZhbHVlVmFyLnR5cGUgPT09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEUpe1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3ZhbHVlVmFyLmF0dHJNYWNybywgdHJ1ZSk7XHJcblx0XHRcdFx0XHRcdHRoaXMuX3ZhbHVlT2Zmc2V0ID0gY29tcC5hZGRWZXJ0ZXhBdHRyaWJ1dGUoeyBcclxuXHRcdFx0XHRcdFx0XHRuYW1lOiB0aGlzLl92YWx1ZVZhci5hdHRyTmFtZSwgXHJcblx0XHRcdFx0XHRcdFx0dHlwZTogZ2Z4LkFUVFJfVFlQRV9GTE9BVDMyLCBcclxuXHRcdFx0XHRcdFx0XHRudW06IFZDLnR5cGVTaXplLCBcclxuXHRcdFx0XHRcdFx0fSwgVkMudHlwZVNpemUpO1xyXG5cdFx0XHRcdFx0XHQvL0VkaXRvci5sb2coXCJvblJlbmRlclVwZGF0ZUF0dHJpYnV0ZVwiLCB0aGlzLl92YWx1ZU9mZnNldCk7XHJcblx0XHRcdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl92YWx1ZVZhci5hdHRyTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5fdmFsdWVPZmZzZXQgPSAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0Ly9FZGl0b3IubG9nKFwib25SZW5kZXJVcGRhdGVBdHRyaWJ1dGVcIiwgdGhpcy5lbmFibGVkKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHByaXZhdGUgb25SZW5kZXJVcGRhdGVSZW5kZXJEYXRhKGNvbXA6UmVuZGVyU3lzdGVtID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKSl7XHJcbiAgICAgICAgaWYoY29tcCAmJiB0aGlzLmVuYWJsZWQpe1xyXG5cdFx0XHQvL0VkaXRvci5sb2coXCJvblJlbmRlclVwZGF0ZVJlbmRlckRhdGFcIik7XHJcblx0XHRcdGlmKHRoaXMuX3ZhbHVlT2Zmc2V0ID4gMCl7XHJcblx0XHRcdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0XHRcdGxldCBhc3NlbWJsZXIgPSBjb21wLl9hc3NlbWJsZXI7XHJcblx0XHRcdFx0bGV0IHZhbHVlcyA9IHRoaXMuZ2V0QXR0cmlidXRlVmFsdWVzKGNvbXApO1xyXG5cdFx0XHRcdGxldCB2bGVuID0gdmFsdWVzLmxlbmd0aDtcclxuXHRcdFx0XHRsZXQgdmFsdWVPZmZzZXQgPSB0aGlzLl92YWx1ZU9mZnNldDtcclxuXHRcdFx0XHRsZXQgZmxvYXRzUGVyVmVydCA9IGFzc2VtYmxlci5mbG9hdHNQZXJWZXJ0O1xyXG5cdFx0XHRcdGxldCB2ZXJ0cyA9IGFzc2VtYmxlci5yZW5kZXJEYXRhLnZEYXRhc1swXTtcclxuXHRcdFx0XHRmb3IgKGxldCBpID0gMDsgaSA8IDQ7IGkrKykge1xyXG5cdFx0XHRcdFx0bGV0IGRzdE9mZnNldCA9IGZsb2F0c1BlclZlcnQgKiBpICsgdmFsdWVPZmZzZXQ7XHJcblx0XHRcdFx0XHRsZXQgc3JjT2Zmc2V0ID0gVkMudHlwZVNpemUgKiBpO1xyXG5cdFx0XHRcdFx0aWYoc3JjT2Zmc2V0ID49IHZsZW4pe1xyXG5cdFx0XHRcdFx0XHRzcmNPZmZzZXQgPSAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0Zm9yKGxldCBqID0gMDsgaiA8IFZDLnR5cGVTaXplOyBqKyspe1xyXG5cdFx0XHRcdFx0XHR2ZXJ0c1tkc3RPZmZzZXQgKyBqXSA9IHZhbHVlc1tzcmNPZmZzZXQgKyBqXTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuICAgICAgICB9XHJcblx0fVxyXG59XHJcblxyXG59XHJcbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGggPSBcIi4vU2hhZGVyQ29tcG9uZW50LnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2MkcmVuZGVyIHtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eSwgbWVudSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbmVudW0gTWFjcm9WYWx1ZVR5cGUge1xyXG5cdC8qKlxyXG5cdCAqIOW4g+WwlFxyXG5cdCAqL1xyXG5cdEJPT0wsXHJcblxyXG5cdC8qKlxyXG5cdCAqIOaVsOWAvFxyXG5cdCAqL1xyXG5cdFZBTFVFLFxyXG59XHJcblxyXG5AY2NjbGFzcyhcImxjYyRyZW5kZXIuU2hhZGVyTWFjcm9cIilcclxuQG1lbnUoXCJpMThuOmxjYy1yZW5kZXIubWVudV9jb21wb25lbnQvU2hhZGVyTWFjcm9cIilcclxuZXhwb3J0IGNsYXNzIFNoYWRlck1hY3JvIGV4dGVuZHMgU2hhZGVyQ29tcG9uZW50IHtcclxuXHJcblx0X3RhZzpzdHJpbmcgPSBcIm1hY3JvXCI7XHJcblxyXG5cdEBwcm9wZXJ0eSgpXHJcblx0X2NoZWNrTWFjcm86c3RyaW5nID0gXCJcIjtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5qOA5rWL5a6PXCJcclxuXHR9KVxyXG5cdGdldCBjaGVja01hY3JvKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fY2hlY2tNYWNybztcclxuXHR9XHJcblx0c2V0IGNoZWNrTWFjcm8odmFsdWU6c3RyaW5nKXtcclxuXHRcdGlmKHRoaXMuX2NoZWNrTWFjcm8gIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9jaGVja01hY3JvID0gdmFsdWU7XHJcblx0XHRcdHRoaXMub25SZW5kZXJVcGRhdGVNYXRlcmlhbCgpO1xyXG5cdFx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfYXR0cmlidXRlXCIpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHRAcHJvcGVydHkoKVxyXG5cdF9tYWNyb05hbWU6c3RyaW5nID0gXCJcIjtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5a6a5LmJ5a6PXCJcclxuXHR9KVxyXG5cdGdldCBtYWNyb05hbWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl9tYWNyb05hbWU7XHJcblx0fVxyXG5cdHNldCBtYWNyb05hbWUodmFsdWU6c3RyaW5nKXtcclxuXHRcdGlmKHRoaXMuX21hY3JvTmFtZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX21hY3JvTmFtZSA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuXHRcdFx0dGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX2F0dHJpYnV0ZVwiKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuRW51bShNYWNyb1ZhbHVlVHlwZSlcclxuXHR9KVxyXG5cdF92YWx1ZVR5cGU6TWFjcm9WYWx1ZVR5cGUgPSBNYWNyb1ZhbHVlVHlwZS5CT09MO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuRW51bShNYWNyb1ZhbHVlVHlwZSksXHJcblx0XHR0b29sdGlwIDogXCLlgLznsbvlnotcIlxyXG5cdH0pXHJcblx0Z2V0IHZhbHVlVHlwZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3ZhbHVlVHlwZTtcclxuXHR9XHJcblx0c2V0IHZhbHVlVHlwZSh2YWx1ZV86TWFjcm9WYWx1ZVR5cGUpe1xyXG5cdFx0aWYodGhpcy5fdmFsdWVUeXBlICE9IHZhbHVlXyl7XHJcblx0XHRcdHRoaXMuX3ZhbHVlVHlwZSA9IHZhbHVlXztcclxuXHRcdFx0dGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsKCk7XHJcblx0XHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIik7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRAcHJvcGVydHkoKVxyXG5cdF9ib29sOmJvb2xlYW4gPSBmYWxzZTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dmlzaWJsZSAoKXtcclxuXHRcdFx0cmV0dXJuIHRoaXMuX3ZhbHVlVHlwZSA9PSBNYWNyb1ZhbHVlVHlwZS5CT09MO1xyXG5cdFx0fSxcclxuXHRcdHRvb2x0aXAgOiBcIuW4g+WwlOWuj+WAvFwiXHJcblx0fSlcclxuXHRnZXQgYm9vbCgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2Jvb2w7XHJcblx0fVxyXG5cdHNldCBib29sKHZhbHVlOmJvb2xlYW4pe1xyXG5cdFx0aWYodGhpcy5fYm9vbCAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX2Jvb2wgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsKCk7XHJcblx0XHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIik7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRAcHJvcGVydHkoKVxyXG5cdF92YWx1ZTpudW1iZXIgPSAwO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdmFsdWVUeXBlID09IE1hY3JvVmFsdWVUeXBlLlZBTFVFO1xyXG5cdFx0fSxcclxuXHRcdHRvb2x0aXAgOiBcIuaVsOWAvOWuj+WAvFwiXHJcblx0fSlcclxuXHRnZXQgdmFsdWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl92YWx1ZTtcclxuXHR9XHJcblx0c2V0IHZhbHVlKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl92YWx1ZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3ZhbHVlID0gdmFsdWU7XHJcblx0XHRcdHRoaXMub25SZW5kZXJVcGRhdGVNYXRlcmlhbCgpO1xyXG5cdFx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfYXR0cmlidXRlXCIpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuICAgIG9uTG9hZCgpe1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihcInJlbmRlcl91cGRhdGVfbWF0ZXJpYWxcIiwgdGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRlc3Ryb3koKXtcclxuXHRcdHRoaXMubm9kZS50YXJnZXRPZmYodGhpcyk7XHJcbiAgICB9XHJcbiAgICBcclxuXHRvbkVuYWJsZSgpe1xyXG4gICAgICAgIHRoaXMub25TdGF0ZVVwZGF0ZSh0cnVlKTtcclxuXHR9XHJcblxyXG5cdG9uRGlzYWJsZSgpe1xyXG4gICAgICAgIHRoaXMub25TdGF0ZVVwZGF0ZShmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvblN0YXRlVXBkYXRlKGVuYWJsZTpib29sZWFuKXtcclxuXHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV90YWdcIik7XHJcblx0XHR0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuXHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIik7XHJcblx0fVxyXG5cdFxyXG4gICAgcHJpdmF0ZSBvblJlbmRlclVwZGF0ZU1hdGVyaWFsIChjb21wOlJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSkpIHtcclxuXHRcdGlmKGNvbXAgJiYgdGhpcy5fbWFjcm9OYW1lKXtcclxuXHRcdFx0Ly8gbWFrZSBzdXJlIG1hdGVyaWFsIGlzIGJlbG9uZyB0byBzZWxmLlxyXG5cdFx0XHQvL0B0cy1pZ25vcmVcclxuXHRcdFx0bGV0IG1hdGVyaWFsID0gY29tcC5fbWF0ZXJpYWxzWzBdO1xyXG5cdFx0XHRpZiAobWF0ZXJpYWwpIHtcclxuXHRcdFx0XHRpZiAoIXRoaXMuX2NoZWNrTWFjcm8gfHwgbWF0ZXJpYWwuZ2V0RGVmaW5lKHRoaXMuX2NoZWNrTWFjcm8pKSB7XHJcblx0XHRcdFx0XHRpZih0aGlzLmVuYWJsZWQgJiYgbWF0ZXJpYWwuZ2V0RGVmaW5lKHRoaXMuX21hY3JvTmFtZSkgIT09IHVuZGVmaW5lZCl7XHJcblx0XHRcdFx0XHRcdGlmKHRoaXMuX3ZhbHVlVHlwZSA9PSBNYWNyb1ZhbHVlVHlwZS5CT09MKXtcclxuXHRcdFx0XHRcdFx0XHRtYXRlcmlhbC5kZWZpbmUodGhpcy5fbWFjcm9OYW1lLCB0aGlzLl9ib29sKTtcclxuXHRcdFx0XHRcdFx0fWVsc2V7XHJcblx0XHRcdFx0XHRcdFx0bWF0ZXJpYWwuZGVmaW5lKHRoaXMuX21hY3JvTmFtZSwgdGhpcy5fdmFsdWUpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWxcIiwgdGhpcy5fYm9vbCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHQvL0VkaXRvci5sb2coXCJvblJlbmRlclVwZGF0ZU1hdGVyaWFsXCIsIHRoaXMuZW5hYmxlZCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcbiAgICB9XHJcbn1cclxuXHJcbn1cclxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi9TaGFkZXJDb21wb25lbnQudHNcIiAvPlxyXG5cclxubW9kdWxlIGxjYyRyZW5kZXIge1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5LCBtZW51IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuLy9AdHMtaWdub3JlXHJcbmxldCBnZnggPSBjYy5nZng7XHJcblxyXG5lbnVtIFNoYXBlVHlwZSB7XHJcblx0LyoqXHJcblx0ICog6IqC54K55bC65a+4XHJcblx0ICovXHJcblx0Tk9ERSA9IDAsXHJcblxyXG5cdC8qKlxyXG5cdCAqIOiHquWumuS5ieWwuuWvuFxyXG5cdCAqL1xyXG5cdENVU1RPTSxcclxufVxyXG5cclxuZW51bSBTaXplTW9kZSB7XHJcblx0LyoqXHJcblx0ICog6Ieq5a6a5LmJ5bC65a+4XHJcblx0ICovXHJcblx0Q1VTVE9NID0gMCxcclxuXHJcblx0LyoqXHJcblx0ICog6Ieq5Yqo6YCC6YWN5Li657K+54G16KOB5Ymq5ZCO55qE5bC65a+4XHJcblx0ICovXHJcblx0VFJJTU1FRCxcclxuXHJcblx0LyoqXHJcblx0ICog6Ieq5Yqo6YCC6YWN5Li657K+54G15Y6f5Zu+5bC65a+4XHJcblx0ICovXHJcblx0UkFXLFxyXG59XHJcblxyXG5AY2NjbGFzcyhcImxjYyRyZW5kZXIuU2hhZGVyU3ByaXRlRnJhbWVcIilcclxuQG1lbnUoXCJpMThuOmxjYy1yZW5kZXIubWVudV9jb21wb25lbnQvU2hhZGVyU3ByaXRlRnJhbWVcIilcclxuZXhwb3J0IGNsYXNzIFNoYWRlclNwcml0ZUZyYW1lIGV4dGVuZHMgU2hhZGVyQ29tcG9uZW50IHtcclxuXHJcblx0X3RhZzpzdHJpbmcgPSBcInNwcml0ZWZyYW1lXCI7XHJcblxyXG5cdEBwcm9wZXJ0eShWYXJpYWJsZUNvbmZpZylcclxuXHRfdGV4dHVyZVZhcjpWYXJpYWJsZUNvbmZpZyA9IG5ldyBWYXJpYWJsZUNvbmZpZyhcclxuXHRcdGZhbHNlLCBWYXJpYWJsZVR5cGUuVU5JRk9STSwgXHJcblx0XHRcIlwiLCBmYWxzZSwgXCJcIiwgXCJVU0VfVEVYVFVSRVwiLCBmYWxzZSwgXCJ0ZXh0dXJlXCIpO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogVmFyaWFibGVDb25maWcsXHJcblx0XHR0b29sdGlwIDogXCLnurnnkIblj5jph49cIlxyXG5cdH0pXHJcblx0Z2V0IHRleHR1cmVWYXIoKXtcclxuXHRcdHJldHVybiB0aGlzLl90ZXh0dXJlVmFyO1xyXG5cdH1cclxuXHRzZXQgdGV4dHVyZVZhcih2YWx1ZTpWYXJpYWJsZUNvbmZpZyl7XHJcblx0XHR0aGlzLl90ZXh0dXJlVmFyID0gdmFsdWU7XHJcblx0XHR0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuXHR9XHJcblx0XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGU6IGNjLlNwcml0ZUF0bGFzLFxyXG5cdFx0dG9vbHRpcDogQ0NfREVWICYmICdpMThuOkNPTVBPTkVOVC5zcHJpdGUuYXRsYXMnLFxyXG5cdFx0ZWRpdG9yT25seTogdHJ1ZSxcclxuXHRcdHZpc2libGU6IHRydWUsXHJcblx0XHRhbmltYXRhYmxlOiBmYWxzZVxyXG5cdH0pXHJcblx0X2F0bGFzOiBjYy5TcHJpdGVBdGxhcyA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG5cdF9zcHJpdGVGcmFtZTpjYy5TcHJpdGVGcmFtZSA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5TcHJpdGVGcmFtZSxcclxuXHRcdHRvb2x0aXAgOiBcIueyvueBteW4p+WvueixoVwiXHJcblx0fSlcclxuXHRnZXQgc3ByaXRlRnJhbWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl9zcHJpdGVGcmFtZTtcclxuXHR9XHJcblx0c2V0IHNwcml0ZUZyYW1lKHZhbHVlOmNjLlNwcml0ZUZyYW1lKXtcclxuXHRcdGlmKHRoaXMuX3Nwcml0ZUZyYW1lICE9PSB2YWx1ZSl7XHJcblx0XHRcdGxldCBsYXN0U3ByaXRlID0gdGhpcy5fc3ByaXRlRnJhbWU7XHJcblx0XHRcdGlmIChDQ19FRElUT1IpIHtcclxuXHRcdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdFx0aWYgKCgobGFzdFNwcml0ZSAmJiBsYXN0U3ByaXRlLl91dWlkKSA9PT0gKHZhbHVlICYmIHZhbHVlLl91dWlkKSkpIHtcclxuXHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0ZWxzZSB7XHJcblx0XHRcdFx0aWYgKGxhc3RTcHJpdGUgPT09IHZhbHVlKSB7XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHRoaXMuX3Nwcml0ZUZyYW1lID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuX3VzZVNwcml0ZUZyYW1lKGxhc3RTcHJpdGUpO1xyXG5cdFx0XHRpZiAoQ0NfRURJVE9SKSB7XHJcblx0XHRcdFx0dGhpcy5ub2RlLmVtaXQoJ3Nwcml0ZWZyYW1lLWNoYW5nZWQnLCB0aGlzKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcbiAgICBAcHJvcGVydHkoKVxyXG4gICAgX3VzZVNoYXBlOmJvb2xlYW4gPSBmYWxzZTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcDpcIuS9v+eUqOW9oueKtlwiXHJcblx0fSlcclxuXHRnZXQgdXNlU2hhcGUoKXtcclxuXHRcdHJldHVybiB0aGlzLl91c2VTaGFwZTtcclxuXHR9XHJcblx0c2V0IHVzZVNoYXBlKHZhbHVlOmJvb2xlYW4pe1xyXG5cdFx0aWYodGhpcy5fdXNlU2hhcGUgIT09IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fdXNlU2hhcGUgPSB2YWx1ZTtcclxuXHRcdFx0aWYodmFsdWUpe1xyXG5cdFx0XHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VzZXNoYXBlXCIsIHRoaXMpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9zaGFwZVwiKTtcclxuICAgICAgICAgICAgdGhpcy5fdXNlU3ByaXRlU2l6ZSgpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuICAgIEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuRW51bShTaGFwZVR5cGUpXHJcblx0fSlcclxuICAgIF9zaGFwZVR5cGU6U2hhcGVUeXBlID0gU2hhcGVUeXBlLk5PREU7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5FbnVtKFNoYXBlVHlwZSksXHJcblx0XHR0b29sdGlwOlwi5b2i54q257G75Z6LXCIsXHJcblx0XHR2aXNpYmxlICgpIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMuX3VzZVNoYXBlO1xyXG5cdFx0fSxcclxuXHR9KVxyXG5cdGdldCBzaGFwZVR5cGUoKXtcclxuXHRcdHJldHVybiB0aGlzLl9zaGFwZVR5cGU7XHJcblx0fVxyXG5cdHNldCBzaGFwZVR5cGUodmFsdWU6U2hhcGVUeXBlKXtcclxuXHRcdGlmKHRoaXMuX3NoYXBlVHlwZSAhPT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9zaGFwZVR5cGUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX3ZlcnRleFwiKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdEBwcm9wZXJ0eShjYy5TaXplKVxyXG5cdF9zaXplOmNjLlNpemUgPSBjYy5zaXplKDAsMCk7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGU6Y2MuU2l6ZSxcclxuXHRcdHRvb2x0aXAgOiBcIuW9oueKtueahOWwuuWvuFwiLFxyXG5cdFx0dmlzaWJsZSAoKSB7XHJcblx0XHRcdHJldHVybiB0aGlzLl91c2VTaGFwZSAmJiB0aGlzLl9zaGFwZVR5cGUgPT0gU2hhcGVUeXBlLkNVU1RPTTtcclxuXHRcdH1cclxuXHR9KVxyXG5cdGdldCBzaXplKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fc2l6ZTtcclxuXHR9XHJcblx0c2V0IHNpemUodmFsdWU6Y2MuU2l6ZSl7XHJcblx0XHR0aGlzLl9zaXplID0gdmFsdWU7XHJcblx0XHR0aGlzLl9zaXplTW9kZSA9IFNpemVNb2RlLkNVU1RPTTtcclxuXHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV92ZXJ0ZXhcIik7XHJcblx0XHRpZih0aGlzLl9zeW5jU2l6ZVRvTm9kZSl7XHJcblx0XHRcdHRoaXMubm9kZS5zZXRDb250ZW50U2l6ZSh0aGlzLl9zaXplKTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0QHByb3BlcnR5KGNjLlZlYzIpXHJcblx0X2FuY2hvcjpjYy5WZWMyID0gY2MudjIoMC41LCAwLjUpO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlOmNjLlZlYzIsXHJcblx0XHR0b29sdGlwIDogXCLlvaLnirbnmoTplJrngrlcIixcclxuXHRcdHZpc2libGUgKCkge1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdXNlU2hhcGUgJiYgdGhpcy5fc2hhcGVUeXBlID09IFNoYXBlVHlwZS5DVVNUT007XHJcblx0XHR9LFxyXG5cdH0pXHJcblx0Z2V0IGFuY2hvcigpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2FuY2hvcjtcclxuXHR9XHJcblx0c2V0IGFuY2hvcih2YWx1ZTpjYy5WZWMyKXtcclxuXHRcdHRoaXMuX2FuY2hvciA9IHZhbHVlO1xyXG5cdFx0dGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX3ZlcnRleFwiKTtcclxuXHRcdGlmKHRoaXMuX3N5bmNTaXplVG9Ob2RlKXtcclxuXHRcdFx0dGhpcy5ub2RlLnNldEFuY2hvclBvaW50KHRoaXMuX2FuY2hvcik7XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG4gICAgQHByb3BlcnR5KClcclxuICAgIF9zeW5jU2l6ZVRvTm9kZTpib29sZWFuID0gdHJ1ZTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcDpcIuWQjOatpeWwuuWvuOWIsOiKgueCuVwiLFxyXG5cdFx0dmlzaWJsZSAoKSB7XHJcblx0XHRcdHJldHVybiB0aGlzLl91c2VTaGFwZSAmJiB0aGlzLl9zaGFwZVR5cGUgPT0gU2hhcGVUeXBlLkNVU1RPTTtcclxuXHRcdH0sXHJcblx0fSlcclxuXHRnZXQgc3luY1NpemVUb05vZGUoKXtcclxuXHRcdHJldHVybiB0aGlzLl9zeW5jU2l6ZVRvTm9kZTtcclxuXHR9XHJcblx0c2V0IHN5bmNTaXplVG9Ob2RlKHZhbHVlOmJvb2xlYW4pe1xyXG5cdFx0aWYodGhpcy5fc3luY1NpemVUb05vZGUgIT09IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fc3luY1NpemVUb05vZGUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5ub2RlLnNldENvbnRlbnRTaXplKHRoaXMuX3NpemUpO1xyXG5cdFx0XHR0aGlzLm5vZGUuc2V0QW5jaG9yUG9pbnQodGhpcy5fYW5jaG9yKTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcbiAgICBAcHJvcGVydHkoe1xyXG4gICAgICAgIHR5cGUgOiBjYy5FbnVtKFNpemVNb2RlKVxyXG4gICAgfSlcclxuXHRfc2l6ZU1vZGU6U2l6ZU1vZGUgPSBTaXplTW9kZS5UUklNTUVEO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuRW51bShTaXplTW9kZSksXHJcblx0XHR2aXNpYmxlICgpIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMuX3VzZVNoYXBlICYmIHRoaXMuX3NoYXBlVHlwZSA9PSBTaGFwZVR5cGUuQ1VTVE9NO1xyXG5cdFx0fSxcclxuXHRcdGFuaW1hdGFibGU6IGZhbHNlLFxyXG5cdFx0dG9vbHRpcDogQ0NfREVWICYmICdpMThuOkNPTVBPTkVOVC5zcHJpdGUuc2l6ZV9tb2RlJ1xyXG5cdH0pXHJcblx0Z2V0IHNpemVNb2RlKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fc2l6ZU1vZGU7XHJcblx0fVxyXG5cdHNldCBzaXplTW9kZSh2YWx1ZTpTaXplTW9kZSl7XHJcblx0XHRpZih0aGlzLl9zaXplTW9kZSAhPT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9zaXplTW9kZSA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLl91c2VTcHJpdGVTaXplKCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eSgpXHJcblx0X2lzVHJpbW1lZE1vZGU6Ym9vbGVhbiA9IHRydWU7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHZpc2libGUgKCkge1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdXNlU2hhcGU7XHJcblx0XHR9LFxyXG5cdFx0YW5pbWF0YWJsZTogZmFsc2UsXHJcblx0XHR0b29sdGlwOiBDQ19ERVYgJiYgJ2kxOG46Q09NUE9ORU5ULnNwcml0ZS50cmltJyxcclxuXHR9KVxyXG5cdGdldCB0cmltKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5faXNUcmltbWVkTW9kZTtcclxuXHR9XHJcblx0c2V0IHRyaW0odmFsdWU6Ym9vbGVhbil7XHJcblx0XHRpZih0aGlzLl9pc1RyaW1tZWRNb2RlICE9PSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX2lzVHJpbW1lZE1vZGUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX3ZlcnRleFwiKTtcclxuXHRcdH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgQHByb3BlcnR5KClcclxuICAgIF91c2VVVjpib29sZWFuID0gZmFsc2U7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXA6XCLkvb/nlKggVVZcIlxyXG5cdH0pXHJcblx0Z2V0IHVzZVVWKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fdXNlVVY7XHJcblx0fVxyXG5cdHNldCB1c2VVVih2YWx1ZTpib29sZWFuKXtcclxuXHRcdGlmKHRoaXMuX3VzZVVWICE9PSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3VzZVVWID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuY2hlY2tVVkluZGV4ZXMoKTtcclxuXHRcdFx0dGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX2F0dHJpYnV0ZVwiKTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0QHByb3BlcnR5KFZhcmlhYmxlQ29uZmlnKVxyXG5cdF91dlZhcjpWYXJpYWJsZUNvbmZpZyA9IG5ldyBWYXJpYWJsZUNvbmZpZyhcclxuXHRcdGZhbHNlLCBWYXJpYWJsZVR5cGUuQVRUUklCVVRFLCBcclxuXHRcdFwiVVNFX1RFWFRVUkVcIiwgdHJ1ZSwgXCJhX3V2MFwiLCBcIlwiLCBmYWxzZSwgXCJcIik7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBWYXJpYWJsZUNvbmZpZyxcclxuXHRcdHRvb2x0aXAgOiBcIlVW5Y+Y6YePXCIsXHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdXNlVVY7XHJcblx0XHR9LFxyXG5cdH0pXHJcblx0Z2V0IHV2VmFyKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fdXZWYXI7XHJcblx0fVxyXG5cdHNldCB1dlZhcih2YWx1ZTpWYXJpYWJsZUNvbmZpZyl7XHJcblx0XHR0aGlzLl91dlZhciA9IHZhbHVlO1xyXG5cdFx0dGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX2F0dHJpYnV0ZVwiKTtcclxuXHR9XHJcblx0XHJcbiAgICBAcHJvcGVydHkoKVxyXG4gICAgX2N1c3RvbVVWOmJvb2xlYW4gPSBmYWxzZTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcDpcIuiHquWumuS5iSBVVlwiLFxyXG5cdFx0dmlzaWJsZSAoKXtcclxuXHRcdFx0cmV0dXJuIHRoaXMuX3VzZVVWO1xyXG5cdFx0fSxcclxuXHR9KVxyXG5cdGdldCBjdXN0b21VVigpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2N1c3RvbVVWO1xyXG5cdH1cclxuXHRzZXQgY3VzdG9tVVYodmFsdWU6Ym9vbGVhbil7XHJcblx0XHRpZih0aGlzLl9jdXN0b21VViAhPT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9jdXN0b21VViA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLmNoZWNrVVZJbmRleGVzKCk7XHJcblx0XHRcdHRoaXMub25SZW5kZXJVcGRhdGVNYXRlcmlhbCgpO1xyXG5cdFx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfYXR0cmlidXRlXCIpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHRAcHJvcGVydHkoW2NjLkludGVnZXJdKVxyXG5cdF91dkluZGV4ZXM6bnVtYmVyW10gPSBbXTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6IFtjYy5JbnRlZ2VyXSxcclxuXHRcdHRvb2x0aXAgOiBcIlVW57Si5byV6KGoXCIsXHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdXNlVVYgJiYgdGhpcy5fY3VzdG9tVVY7XHJcblx0XHR9LFxyXG5cdH0pXHJcblx0Z2V0IHV2SW5kZXhlcygpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3V2SW5kZXhlcztcclxuXHR9XHJcblx0c2V0IHV2SW5kZXhlcyh2YWx1ZTpudW1iZXJbXSl7XHJcblx0XHR0aGlzLl91dkluZGV4ZXMgPSB2YWx1ZTtcclxuXHRcdHRoaXMuY2hlY2tVVkluZGV4ZXMoKTtcclxuXHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV92ZXJ0ZXhcIik7XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eSgpXHJcbiAgICBfdXNlVVZSZWN0OmJvb2xlYW4gPSBmYWxzZTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcDpcIuS9v+eUqFVW5Yy65Z+fKOWQiOWbvuWQjueahOWMuuWfnylcIlxyXG5cdH0pXHJcblx0Z2V0IHVzZVVWUmVjdCgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3VzZVVWUmVjdDtcclxuXHR9XHJcblx0c2V0IHVzZVVWUmVjdCh2YWx1ZTpib29sZWFuKXtcclxuXHRcdGlmKHRoaXMuX3VzZVVWUmVjdCAhPT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl91c2VVVlJlY3QgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsKCk7XHJcblx0XHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIik7XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdEBwcm9wZXJ0eShWYXJpYWJsZUNvbmZpZylcclxuXHRfdXZSZWN0VmFyOlZhcmlhYmxlQ29uZmlnID0gbmV3IFZhcmlhYmxlQ29uZmlnKFxyXG5cdFx0dHJ1ZSwgVmFyaWFibGVUeXBlLlVOSUZPUk0sIFxyXG5cdFx0XCJBVFRSX1VWUkVDVFwiLCBmYWxzZSwgXCJhX3V2cmVjdFwiLCBcIlVOSUZfVVZSRUNUXCIsIGZhbHNlLCBcInVfdXZyZWN0XCIpO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogVmFyaWFibGVDb25maWcsXHJcblx0XHR0b29sdGlwIDogXCJVVuWMuuWfn+WPmOmHj1wiLFxyXG5cdFx0dmlzaWJsZSAoKXtcclxuXHRcdFx0cmV0dXJuIHRoaXMuX3VzZVVWUmVjdDtcclxuXHRcdH0sXHJcblx0fSlcclxuXHRnZXQgdXZSZWN0VmFyKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fdXZSZWN0VmFyO1xyXG5cdH1cclxuXHRzZXQgdXZSZWN0VmFyKHZhbHVlOlZhcmlhYmxlQ29uZmlnKXtcclxuXHRcdHRoaXMuX3V2UmVjdFZhciA9IHZhbHVlO1xyXG5cdFx0dGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsKCk7XHJcblx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfYXR0cmlidXRlXCIpO1xyXG5cdH1cclxuXHRcclxuICAgIEBwcm9wZXJ0eSgpXHJcbiAgICBfdXNlRnJhbWVTaXplOmJvb2xlYW4gPSBmYWxzZTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcDpcIuS9v+eUqOW4p+WwuuWvuCjlg4/ntKDlpKflsI8pXCJcclxuXHR9KVxyXG5cdGdldCB1c2VGcmFtZVNpemUoKXtcclxuXHRcdHJldHVybiB0aGlzLl91c2VGcmFtZVNpemU7XHJcblx0fVxyXG5cdHNldCB1c2VGcmFtZVNpemUodmFsdWU6Ym9vbGVhbil7XHJcblx0XHRpZih0aGlzLl91c2VGcmFtZVNpemUgIT09IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fdXNlRnJhbWVTaXplID0gdmFsdWU7XHJcblx0XHRcdHRoaXMub25SZW5kZXJVcGRhdGVNYXRlcmlhbCgpO1xyXG5cdFx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfYXR0cmlidXRlXCIpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHRAcHJvcGVydHkoVmFyaWFibGVDb25maWcpXHJcblx0X2ZyYW1lU2l6ZVZhcjpWYXJpYWJsZUNvbmZpZyA9IG5ldyBWYXJpYWJsZUNvbmZpZyhcclxuXHRcdHRydWUsIFZhcmlhYmxlVHlwZS5VTklGT1JNLCBcclxuXHRcdFwiQVRUUl9GUkFNRVNJWkVcIiwgZmFsc2UsIFwiYV9mcmFtZXNpemVcIiwgXCJVTklGX0ZSQU1FU0laRVwiLCBmYWxzZSwgXCJ1X2ZyYW1lc2l6ZVwiKTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6IFZhcmlhYmxlQ29uZmlnLFxyXG5cdFx0dG9vbHRpcCA6IFwi5bin5bC65a+45Y+Y6YePXCIsXHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdXNlRnJhbWVTaXplO1xyXG5cdFx0fSxcclxuXHR9KVxyXG5cdGdldCBmcmFtZVNpemVWYXIoKXtcclxuXHRcdHJldHVybiB0aGlzLl9mcmFtZVNpemVWYXI7XHJcblx0fVxyXG5cdHNldCBmcmFtZVNpemVWYXIodmFsdWU6VmFyaWFibGVDb25maWcpe1xyXG5cdFx0dGhpcy5fZnJhbWVTaXplVmFyID0gdmFsdWU7XHJcblx0XHR0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuXHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIik7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIOW9oueKtuefqeW9ouWMuuWfn1xyXG5cdCAqL1xyXG5cdF9zaGFwZVJlY3Q6IG51bWJlcltdID0gbnVsbDtcclxuXHRcclxuXHQvKipcclxuXHQgKiB1duWBj+enu1xyXG5cdCAqL1xyXG4gICAgcHJpdmF0ZSBfdXZPZmZlc3Q6bnVtYmVyID0gMDtcclxuXHJcblx0LyoqXHJcblx0ICogdXbnn6nlvaLlgY/np7tcclxuXHQgKi9cclxuICAgIHByaXZhdGUgX3V2ck9mZnNldDpudW1iZXIgPSAwO1xyXG5cclxuXHQvKipcclxuXHQgKiDluKflsLrlr7jlgY/np7tcclxuXHQgKi9cclxuICAgIHByaXZhdGUgX2ZzT2Zmc2V0Om51bWJlciA9IDA7XHJcblxyXG4gICAgb25Mb2FkKCl7XHJcbiAgICAgICAgdGhpcy5fc3ByaXRlRnJhbWUgJiYgdGhpcy5fc3ByaXRlRnJhbWUuZW5zdXJlTG9hZFRleHR1cmUoKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oXCJzaGFkZXJfdXNlc2hhcGVcIiwgdGhpcy5vblNoYWRlcnVzZVNoYXBlLCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oXCJyZW5kZXJfY2hlY2tfc2hhcGVcIiwgdGhpcy5vblJlbmRlckNoZWNrU2hhcGUsIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihcInJlbmRlcl9zaGFwZV9jaGVja2VkXCIsIHRoaXMuY2hlY2tVVkluZGV4ZXMsIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihcInJlbmRlcl9wcmVwYXJlXCIsIHRoaXMub25SZW5kZXJQcmVwYXJlLCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oXCJyZW5kZXJfdXBkYXRlX21hdGVyaWFsXCIsIHRoaXMub25SZW5kZXJVcGRhdGVNYXRlcmlhbCwgdGhpcyk7XHJcblx0XHR0aGlzLm5vZGUub24oXCJyZW5kZXJfdXBkYXRlX2F0dHJpYnV0ZVwiLCB0aGlzLm9uUmVuZGVyVXBkYXRlQXR0cmlidXRlLCB0aGlzKTtcclxuXHRcdHRoaXMubm9kZS5vbihcInJlbmRlcl91cGRhdGVfc2hhcGVcIiwgdGhpcy5vblJlbmRlclVwZGF0ZVNoYXBlLCB0aGlzKTtcclxuXHRcdHRoaXMubm9kZS5vbihcInJlbmRlcl91cGRhdGVfdmVydGV4XCIsIHRoaXMub25SZW5kZXJVcGRhdGVSZW5kZXJEYXRhLCB0aGlzKTtcclxuXHRcdHRoaXMubm9kZS5vbihcInJlbmRlcl91cGRhdGVfd29ybGR2ZXJ0ZXhcIiwgdGhpcy51cGRhdGVXb3JsZFZlcnRzLCB0aGlzKTtcclxuXHRcdHRoaXMubm9kZS5vbihcInJlbmRlcl92YWxpZGF0ZV9yZW5kZXJcIiwgdGhpcy5vblJlbmRlclZhbGlkYXRlUmVuZGVyLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRlc3Ryb3koKXtcclxuICAgICAgICB0aGlzLm5vZGUudGFyZ2V0T2ZmKHRoaXMpO1xyXG4gICAgfVxyXG4gICAgXHJcblx0b25FbmFibGUoKXtcclxuICAgICAgICB0aGlzLm9uU3RhdGVVcGRhdGUodHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRvbkRpc2FibGUoKXtcclxuICAgICAgICB0aGlzLm9uU3RhdGVVcGRhdGUoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25TdGF0ZVVwZGF0ZShlbmFibGU6Ym9vbGVhbil7XHJcblx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfdGFnXCIpO1xyXG5cdFx0dGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsKCk7XHJcblx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfYXR0cmlidXRlXCIpO1xyXG5cdFx0aWYoZW5hYmxlKXtcclxuXHRcdFx0dGhpcy5fdXNlU3ByaXRlRnJhbWUobnVsbCk7XHJcblx0XHR9XHJcblx0XHR0aGlzLmNoZWNrVVZJbmRleGVzKCk7XHJcblx0XHQvL3RoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV92ZXJ0ZXhcIik7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIOajgOafpVVW57Si5byVXHJcblx0ICovXHJcblx0cHJpdmF0ZSBjaGVja1VWSW5kZXhlcygpe1xyXG5cdFx0aWYodGhpcy5lbmFibGVkKXtcclxuXHRcdFx0aWYodGhpcy5fdXNlVVYpe1xyXG5cdFx0XHRcdGlmKHRoaXMuX2N1c3RvbVVWKXtcclxuXHRcdFx0XHRcdGxldCByY29tcCA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcblx0XHRcdFx0XHRpZihyY29tcCl7XHJcblx0XHRcdFx0XHRcdGxldCB1dmkgPSB0aGlzLl91dkluZGV4ZXM7XHJcblx0XHRcdFx0XHRcdGxldCB2YyA9IHJjb21wLl92ZXJ0aWNlc0NvdW50O1xyXG5cdFx0XHRcdFx0XHRmb3IobGV0IGkgPSAwOyBpIDwgdmM7IGkrKyl7XHJcblx0XHRcdFx0XHRcdFx0bGV0IGlkeCA9IHV2aVtpXTtcclxuXHRcdFx0XHRcdFx0XHRpZihpZHggPT0gbnVsbCl7XHJcblx0XHRcdFx0XHRcdFx0XHR1dmlbaV0gPSBpICUgNDtcclxuXHRcdFx0XHRcdFx0XHR9ZWxzZSBpZihpZHggPiAzKXtcclxuXHRcdFx0XHRcdFx0XHRcdHV2aVtpXSA9IDM7XHJcblx0XHRcdFx0XHRcdFx0fWVsc2UgaWYoaWR4IDwgMCl7XHJcblx0XHRcdFx0XHRcdFx0XHR1dmlbaV0gPSAwO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRpZih1dmkubGVuZ3RoID4gdmMpe1xyXG5cdFx0XHRcdFx0XHRcdHRoaXMuX3V2SW5kZXhlcyA9IHV2aS5zbGljZSgwLCB2Yyk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRcdHRoaXMuX3V2SW5kZXhlcyA9IFtdO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fWVsc2V7XHJcblx0XHRcdFx0dGhpcy5fdXZJbmRleGVzID0gW107XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbiAgICBcclxuXHRwcml2YXRlIG9uU2hhZGVydXNlU2hhcGUoY29tcDpjYy5Db21wb25lbnQpe1xyXG5cdFx0aWYodGhpcy5lbmFibGVkICYmIHRoaXMgIT09IGNvbXApe1xyXG5cdFx0XHR0aGlzLl91c2VTaGFwZSA9IGZhbHNlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cHJpdmF0ZSBvblJlbmRlckNoZWNrU2hhcGUoY29tcDpSZW5kZXJTeXN0ZW0pe1xyXG5cdFx0aWYodGhpcy5lbmFibGVkICYmIHRoaXMuX3VzZVNoYXBlKXtcclxuXHRcdFx0Y29tcC5zZXRTaGFkZXJTaGFwZSg0LCA2KTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHByaXZhdGUgb25SZW5kZXJQcmVwYXJlKGNvbXA6UmVuZGVyU3lzdGVtKXtcclxuXHRcdGlmKHRoaXMuZW5hYmxlZCl7XHJcblx0XHRcdC8vIOaJk+WMheWKqOaAgee6ueeQhlxyXG5cdFx0XHR0aGlzLnBhY2tUb0R5bmFtaWNBdGxhcyhjb21wLCB0aGlzLl9zcHJpdGVGcmFtZSk7XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdHByaXZhdGUgZ2V0VVZSZWN0KGNvbXA6UmVuZGVyU3lzdGVtKXtcclxuXHRcdGxldCByZWN0ID0gbmV3IGNjLlZlYzQoKTtcclxuXHRcdGlmKHRoaXMuX3Nwcml0ZUZyYW1lKXtcclxuXHRcdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0XHRsZXQgdXYgPSB0aGlzLl9zcHJpdGVGcmFtZS51djtcclxuXHRcdFx0bGV0IGwgPSB1dlswXTtcclxuXHRcdFx0bGV0IGIgPSB1dlsxXTtcclxuXHRcdFx0bGV0IHcgPSB1dls2XSAtIGw7XHJcblx0XHRcdGxldCBoID0gdXZbN10gLSBiO1xyXG5cdFx0XHRyZWN0LnggPSBsO1xyXG5cdFx0XHRyZWN0LnkgPSBiO1xyXG5cdFx0XHRyZWN0LnogPSB3O1xyXG5cdFx0XHRyZWN0LncgPSBoO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHJlY3Q7XHJcblx0fVxyXG5cdFxyXG4gICAgcHJpdmF0ZSBvblJlbmRlclVwZGF0ZU1hdGVyaWFsIChjb21wOlJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSkpIHtcclxuXHRcdGlmKGNvbXApe1xyXG5cdFx0XHRsZXQgdGV4dHVyZSA9IHRoaXMuX3Nwcml0ZUZyYW1lICYmIHRoaXMuX3Nwcml0ZUZyYW1lLmdldFRleHR1cmUoKTtcclxuXHRcdFx0Ly8gbWFrZSBzdXJlIG1hdGVyaWFsIGlzIGJlbG9uZyB0byBzZWxmLlxyXG5cdFx0XHQvL0B0cy1pZ25vcmVcclxuXHRcdFx0bGV0IG1hdGVyaWFsID0gY29tcC5fbWF0ZXJpYWxzWzBdO1xyXG5cdFx0XHRpZiAobWF0ZXJpYWwpIHtcclxuXHRcdFx0XHRpZiAodGhpcy5jaGVja01hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3RleHR1cmVWYXIudW5pZk1hY3JvKSkge1xyXG5cdFx0XHRcdFx0aWYodGhpcy5lbmFibGVkKXtcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl90ZXh0dXJlVmFyLnVuaWZNYWNybywgdHJ1ZSk7XHJcblx0XHRcdFx0XHRcdG1hdGVyaWFsLnNldFByb3BlcnR5KHRoaXMuX3RleHR1cmVWYXIudW5pZk5hbWUsIHRleHR1cmUpO1xyXG5cdFx0XHRcdFx0fWVsc2V7XHJcblx0XHRcdFx0XHRcdHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fdGV4dHVyZVZhci51bmlmTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKHRoaXMuY2hlY2tNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl91dlJlY3RWYXIudW5pZk1hY3JvKSkge1xyXG5cdFx0XHRcdFx0aWYodGhpcy5lbmFibGVkICYmIHRoaXMuX3VzZVVWUmVjdCAmJiB0aGlzLl91dlJlY3RWYXIudHlwZSA9PT0gVmFyaWFibGVUeXBlLlVOSUZPUk0pe1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3V2UmVjdFZhci51bmlmTWFjcm8sIHRydWUpO1xyXG5cdFx0XHRcdFx0XHRtYXRlcmlhbC5zZXRQcm9wZXJ0eSh0aGlzLl91dlJlY3RWYXIudW5pZk5hbWUsIHRoaXMuZ2V0VVZSZWN0KGNvbXApKTtcclxuXHRcdFx0XHRcdFx0Ly9FZGl0b3IubG9nKFwib25SZW5kZXJVcGRhdGVNYXRlcmlhbFwiLCB0aGlzLmdldFVWUmVjdChjb21wKSk7XHJcblx0XHRcdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl91dlJlY3RWYXIudW5pZk1hY3JvLCBmYWxzZSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmICh0aGlzLmNoZWNrTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fZnJhbWVTaXplVmFyLnVuaWZNYWNybykpIHtcclxuXHRcdFx0XHRcdGlmKHRoaXMuZW5hYmxlZCAmJnRoaXMuX3VzZUZyYW1lU2l6ZSAmJiB0aGlzLl9mcmFtZVNpemVWYXIudHlwZSA9PT0gVmFyaWFibGVUeXBlLlVOSUZPUk0pe1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX2ZyYW1lU2l6ZVZhci51bmlmTWFjcm8sIHRydWUpO1xyXG5cdFx0XHRcdFx0XHRpZih0aGlzLl9zcHJpdGVGcmFtZSl7XHJcblx0XHRcdFx0XHRcdFx0Ly9AdHMtaWdub3JlXHJcblx0XHRcdFx0XHRcdFx0bGV0IHJlY3QgPSB0aGlzLl9zcHJpdGVGcmFtZS5fcmVjdDtcclxuXHRcdFx0XHRcdFx0XHRtYXRlcmlhbC5zZXRQcm9wZXJ0eSh0aGlzLl9mcmFtZVNpemVWYXIudW5pZk5hbWUsIGNjLnYyKHJlY3Qud2lkdGgsIHJlY3QuaGVpZ2h0KSk7XHJcblx0XHRcdFx0XHRcdH1lbHNlIHtcclxuXHRcdFx0XHRcdFx0XHRtYXRlcmlhbC5zZXRQcm9wZXJ0eSh0aGlzLl9mcmFtZVNpemVWYXIudW5pZk5hbWUsIGNjLnYyKDEsIDEpKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHQvL0VkaXRvci5sb2coXCJvblJlbmRlclVwZGF0ZU1hdGVyaWFsXCIsIHRoaXMuZ2V0VVZSZWN0KGNvbXApKTtcclxuXHRcdFx0XHRcdH1lbHNle1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX2ZyYW1lU2l6ZVZhci51bmlmTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0cHJpdmF0ZSBvblJlbmRlclVwZGF0ZUF0dHJpYnV0ZShjb21wOlJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSkpe1xyXG5cdFx0aWYoY29tcCl7XHJcblx0XHRcdC8vIEB0cy1pZ25vcmVcclxuXHRcdFx0bGV0IG1hdGVyaWFsID0gY29tcC5fbWF0ZXJpYWxzWzBdO1xyXG5cdFx0XHRpZiAobWF0ZXJpYWwpIHtcclxuXHRcdFx0XHRpZiAodGhpcy5jaGVja01hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3V2VmFyLmF0dHJNYWNybykpIHtcclxuXHRcdFx0XHRcdGlmKHRoaXMuZW5hYmxlZCAmJiB0aGlzLl91c2VVVil7XHJcblx0XHRcdFx0XHRcdHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fdXZWYXIuYXR0ck1hY3JvLCB0cnVlKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5fdXZPZmZlc3QgPSBjb21wLmFkZFZlcnRleEF0dHJpYnV0ZSh7IFxyXG5cdFx0XHRcdFx0XHRcdG5hbWU6IHRoaXMuX3V2VmFyLmF0dHJOYW1lLCBcclxuXHRcdFx0XHRcdFx0XHR0eXBlOiBnZnguQVRUUl9UWVBFX0ZMT0FUMzIsIFxyXG5cdFx0XHRcdFx0XHRcdG51bTogMiBcclxuXHRcdFx0XHRcdFx0fSwgMik7XHJcblx0XHRcdFx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlQXR0cmlidXRlXCIsIHRoaXMuX3V2ck9mZnNldCk7XHJcblx0XHRcdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl91dlZhci5hdHRyTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5fdXZPZmZlc3QgPSAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAodGhpcy5jaGVja01hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3V2UmVjdFZhci5hdHRyTWFjcm8pKSB7XHJcblx0XHRcdFx0XHRpZih0aGlzLmVuYWJsZWQgJiYgdGhpcy5fdXNlVVZSZWN0ICYmIHRoaXMuX3V2UmVjdFZhci50eXBlID09PSBWYXJpYWJsZVR5cGUuQVRUUklCVVRFKXtcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl91dlJlY3RWYXIuYXR0ck1hY3JvLCB0cnVlKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5fdXZyT2Zmc2V0ID0gY29tcC5hZGRWZXJ0ZXhBdHRyaWJ1dGUoeyBcclxuXHRcdFx0XHRcdFx0XHRuYW1lOiB0aGlzLl91dlJlY3RWYXIuYXR0ck5hbWUsIFxyXG5cdFx0XHRcdFx0XHRcdHR5cGU6IGdmeC5BVFRSX1RZUEVfRkxPQVQzMiwgXHJcblx0XHRcdFx0XHRcdFx0bnVtOiA0IFxyXG5cdFx0XHRcdFx0XHR9LCA0KTtcclxuXHRcdFx0XHRcdFx0Ly9FZGl0b3IubG9nKFwib25SZW5kZXJVcGRhdGVBdHRyaWJ1dGVcIiwgdGhpcy5fdXZyT2Zmc2V0KTtcclxuXHRcdFx0XHRcdH1lbHNle1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3V2UmVjdFZhci5hdHRyTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5fdXZyT2Zmc2V0ID0gMDtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKHRoaXMuY2hlY2tNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl9mcmFtZVNpemVWYXIuYXR0ck1hY3JvKSkge1xyXG5cdFx0XHRcdFx0aWYodGhpcy5lbmFibGVkICYmIHRoaXMuX3VzZUZyYW1lU2l6ZSAmJiB0aGlzLl9mcmFtZVNpemVWYXIudHlwZSA9PT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURSl7XHJcblx0XHRcdFx0XHRcdHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fZnJhbWVTaXplVmFyLmF0dHJNYWNybywgdHJ1ZSk7XHJcblx0XHRcdFx0XHRcdHRoaXMuX2ZzT2Zmc2V0ID0gY29tcC5hZGRWZXJ0ZXhBdHRyaWJ1dGUoeyBcclxuXHRcdFx0XHRcdFx0XHRuYW1lOiB0aGlzLl9mcmFtZVNpemVWYXIuYXR0ck5hbWUsIFxyXG5cdFx0XHRcdFx0XHRcdHR5cGU6IGdmeC5BVFRSX1RZUEVfRkxPQVQzMiwgXHJcblx0XHRcdFx0XHRcdFx0bnVtOiAyIFxyXG5cdFx0XHRcdFx0XHR9LCAyKTtcclxuXHRcdFx0XHRcdFx0Ly9FZGl0b3IubG9nKFwib25SZW5kZXJVcGRhdGVBdHRyaWJ1dGVcIiwgdGhpcy5fdXZyT2Zmc2V0KTtcclxuXHRcdFx0XHRcdH1lbHNle1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX2ZyYW1lU2l6ZVZhci5hdHRyTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5fZnNPZmZzZXQgPSAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cHJpdmF0ZSBvblJlbmRlclVwZGF0ZVNoYXBlKGNvbXA6UmVuZGVyU3lzdGVtID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKSl7XHJcblx0XHRpZih0aGlzLmVuYWJsZWQgJiYgdGhpcy5fdXNlU2hhcGUpe1xyXG5cdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdGxldCByZGF0YSA9IGNvbXAuX2Fzc2VtYmxlci5yZW5kZXJEYXRhO1xyXG5cclxuXHRcdFx0cmRhdGEuaW5pdFF1YWRJbmRpY2VzKHJkYXRhLmlEYXRhc1swXSk7XHJcblxyXG5cdFx0XHR0aGlzLl9zaGFwZVJlY3QgPSBbIDAsIDAsIDAsIDAgXTtcclxuXHRcdH1lbHNle1xyXG5cdFx0XHR0aGlzLl9zaGFwZVJlY3QgPSBudWxsO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHRwcml2YXRlIG9uUmVuZGVyVXBkYXRlUmVuZGVyRGF0YShjb21wOlJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSkpe1xyXG4gICAgICAgIGlmKGNvbXAgJiYgdGhpcy5lbmFibGVkKXtcclxuXHRcdFx0Ly9FZGl0b3IubG9nKFwib25SZW5kZXJVcGRhdGVWZXJ0ZXhcIik7XHJcblx0XHRcdC8vIEB0cy1pZ25vcmVcclxuXHRcdFx0bGV0IGFzc2VtYmxlciA9IGNvbXAuX2Fzc2VtYmxlcjtcclxuXHJcblx0XHRcdC8vIOabtOaWsOmhtueCuVxyXG5cdFx0XHRpZih0aGlzLl91c2VTaGFwZSl7XHJcblx0XHRcdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0XHRcdGxldCBjdywgY2gsIGFwcHgsIGFwcHksIGwsIGIsIHIsIHQ7XHJcblx0XHRcdFx0aWYodGhpcy5fc2hhcGVUeXBlID09IFNoYXBlVHlwZS5OT0RFKXtcclxuXHRcdFx0XHRcdGxldCBub2RlID0gdGhpcy5ub2RlO1xyXG5cdFx0XHRcdFx0Y3cgPSBub2RlLndpZHRoO1xyXG5cdFx0XHRcdFx0Y2ggPSBub2RlLmhlaWdodDtcclxuXHRcdFx0XHRcdGFwcHggPSBub2RlLmFuY2hvclggKiBjdztcclxuXHRcdFx0XHRcdGFwcHkgPSBub2RlLmFuY2hvclkgKiBjaDtcclxuXHRcdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRcdGN3ID0gdGhpcy5fc2l6ZS53aWR0aDtcclxuXHRcdFx0XHRcdGNoID0gdGhpcy5fc2l6ZS5oZWlnaHQ7XHJcblx0XHRcdFx0XHRhcHB4ID0gdGhpcy5fYW5jaG9yLnggKiBjdztcclxuXHRcdFx0XHRcdGFwcHkgPSB0aGlzLl9hbmNob3IueSAqIGNoO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAodGhpcy50cmltKSB7XHJcblx0XHRcdFx0XHRsID0gLWFwcHg7XHJcblx0XHRcdFx0XHRiID0gLWFwcHk7XHJcblx0XHRcdFx0XHRyID0gY3cgLSBhcHB4O1xyXG5cdFx0XHRcdFx0dCA9IGNoIC0gYXBweTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0ZWxzZSB7XHJcblx0XHRcdFx0XHRsZXQgZnJhbWUgPSB0aGlzLl9zcHJpdGVGcmFtZSxcclxuXHRcdFx0XHRcdFx0Ly9AdHMtaWdub3JlXHJcblx0XHRcdFx0XHRcdG93ID0gZnJhbWUuX29yaWdpbmFsU2l6ZS53aWR0aCwgb2ggPSBmcmFtZS5fb3JpZ2luYWxTaXplLmhlaWdodCxcclxuXHRcdFx0XHRcdFx0Ly9AdHMtaWdub3JlXHJcblx0XHRcdFx0XHRcdHJ3ID0gZnJhbWUuX3JlY3Qud2lkdGgsIHJoID0gZnJhbWUuX3JlY3QuaGVpZ2h0LFxyXG5cdFx0XHRcdFx0XHQvL0B0cy1pZ25vcmVcclxuXHRcdFx0XHRcdFx0b2Zmc2V0ID0gZnJhbWUuX29mZnNldCxcclxuXHRcdFx0XHRcdFx0c2NhbGVYID0gY3cgLyBvdywgc2NhbGVZID0gY2ggLyBvaDtcclxuXHRcdFx0XHRcdGxldCB0cmltTGVmdCA9IG9mZnNldC54ICsgKG93IC0gcncpIC8gMjtcclxuXHRcdFx0XHRcdGxldCB0cmltUmlnaHQgPSBvZmZzZXQueCAtIChvdyAtIHJ3KSAvIDI7XHJcblx0XHRcdFx0XHRsZXQgdHJpbUJvdHRvbSA9IG9mZnNldC55ICsgKG9oIC0gcmgpIC8gMjtcclxuXHRcdFx0XHRcdGxldCB0cmltVG9wID0gb2Zmc2V0LnkgLSAob2ggLSByaCkgLyAyO1xyXG5cdFx0XHRcdFx0bCA9IHRyaW1MZWZ0ICogc2NhbGVYIC0gYXBweDtcclxuXHRcdFx0XHRcdGIgPSB0cmltQm90dG9tICogc2NhbGVZIC0gYXBweTtcclxuXHRcdFx0XHRcdHIgPSBjdyArIHRyaW1SaWdodCAqIHNjYWxlWCAtIGFwcHg7XHJcblx0XHRcdFx0XHR0ID0gY2ggKyB0cmltVG9wICogc2NhbGVZIC0gYXBweTtcclxuXHRcdFx0XHR9XHJcblx0XHJcblx0XHRcdFx0bGV0IGxvY2FsID0gdGhpcy5fc2hhcGVSZWN0O1xyXG5cdFx0XHRcdGxvY2FsWzBdID0gbDtcclxuXHRcdFx0XHRsb2NhbFsxXSA9IGI7XHJcblx0XHRcdFx0bG9jYWxbMl0gPSByO1xyXG5cdFx0XHRcdGxvY2FsWzNdID0gdDtcclxuXHJcblx0XHRcdFx0dGhpcy51cGRhdGVXb3JsZFZlcnRzKGNvbXApO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQvLyDkvb/nlKhVVlxyXG5cdFx0XHRpZih0aGlzLl91c2VVViAmJiB0aGlzLl91dk9mZmVzdCA+IDApe1xyXG5cdFx0XHRcdC8vIEB0cy1pZ25vcmVcclxuXHRcdFx0XHRsZXQgdXYgPSB0aGlzLl9zcHJpdGVGcmFtZS51djtcclxuXHRcdFx0XHRsZXQgY3V2aSA9IHRoaXMuX3V2SW5kZXhlcztcclxuXHRcdFx0XHRsZXQgdXZPZmZzZXQgPSB0aGlzLl91dk9mZmVzdDtcclxuXHRcdFx0XHRsZXQgZmxvYXRzUGVyVmVydCA9IGFzc2VtYmxlci5mbG9hdHNQZXJWZXJ0O1xyXG5cdFx0XHRcdGxldCB2ZXJ0cyA9IGFzc2VtYmxlci5yZW5kZXJEYXRhLnZEYXRhc1swXTtcclxuXHRcdFx0XHRmb3IgKGxldCBpID0gMDsgaSA8IDQ7IGkrKykge1xyXG5cdFx0XHRcdFx0bGV0IHNyY09mZnNldCA9ICh0aGlzLl9jdXN0b21VViA/IGN1dmlbaV0gOiBpKSAqIDI7XHJcblx0XHRcdFx0XHRsZXQgZHN0T2Zmc2V0ID0gZmxvYXRzUGVyVmVydCAqIGkgKyB1dk9mZnNldDtcclxuXHRcdFx0XHRcdHZlcnRzW2RzdE9mZnNldF0gPSB1dltzcmNPZmZzZXRdO1xyXG5cdFx0XHRcdFx0dmVydHNbZHN0T2Zmc2V0ICsgMV0gPSB1dltzcmNPZmZzZXQgKyAxXTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC8vIOS9v+eUqFVW5Yy65Z+fXHJcblx0XHRcdGlmKHRoaXMuX3VzZVVWUmVjdCAmJiB0aGlzLl91dnJPZmZzZXQgPiAwKXtcclxuXHRcdFx0XHRsZXQgdXZyID0gdGhpcy5nZXRVVlJlY3QoY29tcCk7XHJcblx0XHRcdFx0aWYodXZyKXtcclxuXHRcdFx0XHRcdGxldCB1dnJPZmZzZXQgPSB0aGlzLl91dnJPZmZzZXQ7XHJcblx0XHRcdFx0XHRsZXQgZmxvYXRzUGVyVmVydCA9IGFzc2VtYmxlci5mbG9hdHNQZXJWZXJ0O1xyXG5cdFx0XHRcdFx0bGV0IHZlcnRzID0gYXNzZW1ibGVyLnJlbmRlckRhdGEudkRhdGFzWzBdO1xyXG5cdFx0XHRcdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCA0OyBpKyspIHtcclxuXHRcdFx0XHRcdFx0bGV0IGRzdE9mZnNldCA9IGZsb2F0c1BlclZlcnQgKiBpICsgdXZyT2Zmc2V0O1xyXG5cdFx0XHRcdFx0XHR2ZXJ0c1tkc3RPZmZzZXRdID0gdXZyLng7XHJcblx0XHRcdFx0XHRcdHZlcnRzW2RzdE9mZnNldCArIDFdID0gdXZyLnk7XHJcblx0XHRcdFx0XHRcdHZlcnRzW2RzdE9mZnNldCArIDJdID0gdXZyLno7XHJcblx0XHRcdFx0XHRcdHZlcnRzW2RzdE9mZnNldCArIDNdID0gdXZyLnc7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQvLyDkvb/nlKjluKflsLrlr7jlpKflsI9cclxuXHRcdFx0aWYodGhpcy5fdXNlRnJhbWVTaXplICYmIHRoaXMuX2ZzT2Zmc2V0ID4gMCl7XHJcblx0XHRcdFx0Ly9AdHMtaWdub3JlXHJcblx0XHRcdFx0bGV0IHJlY3QgPSB0aGlzLl9zcHJpdGVGcmFtZS5fcmVjdDtcclxuXHRcdFx0XHRsZXQgZnNPZmZzZXQgPSB0aGlzLl9mc09mZnNldDtcclxuXHRcdFx0XHRsZXQgZmxvYXRzUGVyVmVydCA9IGFzc2VtYmxlci5mbG9hdHNQZXJWZXJ0O1xyXG5cdFx0XHRcdGxldCB2ZXJ0cyA9IGFzc2VtYmxlci5yZW5kZXJEYXRhLnZEYXRhc1swXTtcclxuXHRcdFx0XHRmb3IgKGxldCBpID0gMDsgaSA8IDQ7IGkrKykge1xyXG5cdFx0XHRcdFx0bGV0IGRzdE9mZnNldCA9IGZsb2F0c1BlclZlcnQgKiBpICsgZnNPZmZzZXQ7XHJcblx0XHRcdFx0XHR2ZXJ0c1tkc3RPZmZzZXRdID0gcmVjdC53aWR0aDtcclxuXHRcdFx0XHRcdHZlcnRzW2RzdE9mZnNldCArIDFdID0gcmVjdC5oZWlnaHQ7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcbiAgICAgICAgfVxyXG5cdH1cclxuXHJcbiAgICB1cGRhdGVXb3JsZFZlcnRzKGNvbXA6UmVuZGVyU3lzdGVtKSB7XHJcblx0XHRpZih0aGlzLmVuYWJsZWQgJiYgdGhpcy5fdXNlU2hhcGUpe1xyXG5cdFx0XHRpZiAoQ0NfTkFUSVZFUkVOREVSRVIpIHtcclxuXHRcdFx0XHR0aGlzLnVwZGF0ZVdvcmxkVmVydHNOYXRpdmUoY29tcCk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0dGhpcy51cGRhdGVXb3JsZFZlcnRzV2ViR0woY29tcCk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVXb3JsZFZlcnRzV2ViR0woY29tcDpSZW5kZXJTeXN0ZW0pIHtcclxuXHRcdC8vIEB0cy1pZ25vcmVcclxuXHRcdGxldCBhc3NlbWJsZXIgPSBjb21wLl9hc3NlbWJsZXI7XHJcbiAgICAgICAgbGV0IGxvY2FsID0gdGhpcy5fc2hhcGVSZWN0O1xyXG4gICAgICAgIGxldCB2ZXJ0cyA9IGFzc2VtYmxlci5yZW5kZXJEYXRhLnZEYXRhc1swXTtcclxuXHJcblx0XHQvL0B0cy1pZ25vcmVcclxuICAgICAgICBsZXQgbWF0cml4ID0gY29tcC5ub2RlLl93b3JsZE1hdHJpeDtcclxuICAgICAgICBsZXQgbWF0cml4bSA9IG1hdHJpeC5tLFxyXG4gICAgICAgICAgICBhID0gbWF0cml4bVswXSwgYiA9IG1hdHJpeG1bMV0sIGMgPSBtYXRyaXhtWzRdLCBkID0gbWF0cml4bVs1XSxcclxuICAgICAgICAgICAgdHggPSBtYXRyaXhtWzEyXSwgdHkgPSBtYXRyaXhtWzEzXTtcclxuXHJcbiAgICAgICAgbGV0IHZsID0gbG9jYWxbMF0sIHZyID0gbG9jYWxbMl0sXHJcbiAgICAgICAgICAgIHZiID0gbG9jYWxbMV0sIHZ0ID0gbG9jYWxbM107XHJcbiAgICAgICAgXHJcbiAgICAgICAgLypcclxuICAgICAgICBtMDAgPSAxLCBtMDEgPSAwLCBtMDIgPSAwLCBtMDMgPSAwLFxyXG4gICAgICAgIG0wNCA9IDAsIG0wNSA9IDEsIG0wNiA9IDAsIG0wNyA9IDAsXHJcbiAgICAgICAgbTA4ID0gMCwgbTA5ID0gMCwgbTEwID0gMSwgbTExID0gMCxcclxuICAgICAgICBtMTIgPSAwLCBtMTMgPSAwLCBtMTQgPSAwLCBtMTUgPSAxXHJcbiAgICAgICAgKi9cclxuICAgICAgICBsZXQganVzdFRyYW5zbGF0ZSA9IGEgPT09IDEgJiYgYiA9PT0gMCAmJiBjID09PSAwICYmIGQgPT09IDE7XHJcblxyXG4gICAgICAgIGxldCBpbmRleCA9IDA7XHJcbiAgICAgICAgbGV0IGZsb2F0c1BlclZlcnQgPSBhc3NlbWJsZXIuZmxvYXRzUGVyVmVydDtcclxuICAgICAgICBpZiAoanVzdFRyYW5zbGF0ZSkge1xyXG4gICAgICAgICAgICAvLyBsZWZ0IGJvdHRvbVxyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleF0gPSB2bCArIHR4O1xyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IHZiICsgdHk7XHJcbiAgICAgICAgICAgIGluZGV4ICs9IGZsb2F0c1BlclZlcnQ7XHJcbiAgICAgICAgICAgIC8vIHJpZ2h0IGJvdHRvbVxyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleF0gPSB2ciArIHR4O1xyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IHZiICsgdHk7XHJcbiAgICAgICAgICAgIGluZGV4ICs9IGZsb2F0c1BlclZlcnQ7XHJcbiAgICAgICAgICAgIC8vIGxlZnQgdG9wXHJcbiAgICAgICAgICAgIHZlcnRzW2luZGV4XSA9IHZsICsgdHg7XHJcbiAgICAgICAgICAgIHZlcnRzW2luZGV4KzFdID0gdnQgKyB0eTtcclxuICAgICAgICAgICAgaW5kZXggKz0gZmxvYXRzUGVyVmVydDtcclxuICAgICAgICAgICAgLy8gcmlnaHQgdG9wXHJcbiAgICAgICAgICAgIHZlcnRzW2luZGV4XSA9IHZyICsgdHg7XHJcbiAgICAgICAgICAgIHZlcnRzW2luZGV4KzFdID0gdnQgKyB0eTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBsZXQgYWwgPSBhICogdmwsIGFyID0gYSAqIHZyLFxyXG4gICAgICAgICAgICBibCA9IGIgKiB2bCwgYnIgPSBiICogdnIsXHJcbiAgICAgICAgICAgIGNiID0gYyAqIHZiLCBjdCA9IGMgKiB2dCxcclxuICAgICAgICAgICAgZGIgPSBkICogdmIsIGR0ID0gZCAqIHZ0O1xyXG5cclxuICAgICAgICAgICAgLy8gbGVmdCBib3R0b21cclxuICAgICAgICAgICAgLy8gbmV3eCA9IHZsICogYSArIHZiICogYyArIHR4XHJcbiAgICAgICAgICAgIC8vIG5ld3kgPSB2bCAqIGIgKyB2YiAqIGQgKyB0eVxyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleF0gPSBhbCArIGNiICsgdHg7XHJcbiAgICAgICAgICAgIHZlcnRzW2luZGV4KzFdID0gYmwgKyBkYiArIHR5O1xyXG4gICAgICAgICAgICBpbmRleCArPSBmbG9hdHNQZXJWZXJ0O1xyXG4gICAgICAgICAgICAvLyByaWdodCBib3R0b21cclxuICAgICAgICAgICAgdmVydHNbaW5kZXhdID0gYXIgKyBjYiArIHR4O1xyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IGJyICsgZGIgKyB0eTtcclxuICAgICAgICAgICAgaW5kZXggKz0gZmxvYXRzUGVyVmVydDtcclxuICAgICAgICAgICAgLy8gbGVmdCB0b3BcclxuICAgICAgICAgICAgdmVydHNbaW5kZXhdID0gYWwgKyBjdCArIHR4O1xyXG4gICAgICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IGJsICsgZHQgKyB0eTtcclxuICAgICAgICAgICAgaW5kZXggKz0gZmxvYXRzUGVyVmVydDtcclxuICAgICAgICAgICAgLy8gcmlnaHQgdG9wXHJcbiAgICAgICAgICAgIHZlcnRzW2luZGV4XSA9IGFyICsgY3QgKyB0eDtcclxuICAgICAgICAgICAgdmVydHNbaW5kZXgrMV0gPSBiciArIGR0ICsgdHk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZVdvcmxkVmVydHNOYXRpdmUoY29tcDpSZW5kZXJTeXN0ZW0pIHtcclxuXHRcdC8vIEB0cy1pZ25vcmVcclxuXHRcdGxldCBhc3NlbWJsZXIgPSBjb21wLl9hc3NlbWJsZXI7XHJcbiAgICAgICAgbGV0IGxvY2FsID0gdGhpcy5fc2hhcGVSZWN0O1xyXG4gICAgICAgIGxldCB2ZXJ0cyA9IGFzc2VtYmxlci5yZW5kZXJEYXRhLnZEYXRhc1swXTtcclxuICAgICAgICBsZXQgZmxvYXRzUGVyVmVydCA9IGFzc2VtYmxlci5mbG9hdHNQZXJWZXJ0O1xyXG5cdFx0XHJcbiAgICAgICAgbGV0IHZsID0gbG9jYWxbMF0sXHJcbiAgICAgICAgICAgIHZyID0gbG9jYWxbMl0sXHJcbiAgICAgICAgICAgIHZiID0gbG9jYWxbMV0sXHJcbiAgICAgICAgICAgIHZ0ID0gbG9jYWxbM107XHJcbiAgICAgIFxyXG4gICAgICAgIGxldCBpbmRleDogbnVtYmVyID0gMDtcclxuICAgICAgICAvLyBsZWZ0IGJvdHRvbVxyXG4gICAgICAgIHZlcnRzW2luZGV4XSA9IHZsO1xyXG4gICAgICAgIHZlcnRzW2luZGV4KzFdID0gdmI7XHJcbiAgICAgICAgaW5kZXggKz0gZmxvYXRzUGVyVmVydDtcclxuICAgICAgICAvLyByaWdodCBib3R0b21cclxuICAgICAgICB2ZXJ0c1tpbmRleF0gPSB2cjtcclxuICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IHZiO1xyXG4gICAgICAgIGluZGV4ICs9IGZsb2F0c1BlclZlcnQ7XHJcbiAgICAgICAgLy8gbGVmdCB0b3BcclxuICAgICAgICB2ZXJ0c1tpbmRleF0gPSB2bDtcclxuICAgICAgICB2ZXJ0c1tpbmRleCsxXSA9IHZ0O1xyXG4gICAgICAgIGluZGV4ICs9IGZsb2F0c1BlclZlcnQ7XHJcbiAgICAgICAgLy8gcmlnaHQgdG9wXHJcbiAgICAgICAgdmVydHNbaW5kZXhdID0gdnI7XHJcbiAgICAgICAgdmVydHNbaW5kZXgrMV0gPSB2dDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uUmVuZGVyVmFsaWRhdGVSZW5kZXIgKGNvbXA6UmVuZGVyU3lzdGVtKSB7XHJcbiAgICAgICAgaWYodGhpcy5lbmFibGVkKXtcclxuICAgICAgICAgICAgbGV0IHNwcml0ZUZyYW1lID0gdGhpcy5fc3ByaXRlRnJhbWU7XHJcbiAgICAgICAgICAgIGlmIChzcHJpdGVGcmFtZSAmJlxyXG4gICAgICAgICAgICAgICAgc3ByaXRlRnJhbWUudGV4dHVyZUxvYWRlZCgpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcblx0XHRcdH1cclxuXHRcdFx0Ly9AdHMtaWdub3JlXHJcbiAgICAgICAgICAgIGNvbXAuZGlzYWJsZVJlbmRlcigpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwYWNrVG9EeW5hbWljQXRsYXMoY29tcCwgZnJhbWUpIHtcclxuICAgICAgICBpZiAoQ0NfVEVTVCkgcmV0dXJuO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGlmICghZnJhbWUuX29yaWdpbmFsICYmIGNjLmR5bmFtaWNBdGxhc01hbmFnZXIgJiYgZnJhbWUuX3RleHR1cmUucGFja2FibGUpIHtcclxuICAgICAgICAgICAgbGV0IHBhY2tlZEZyYW1lID0gY2MuZHluYW1pY0F0bGFzTWFuYWdlci5pbnNlcnRTcHJpdGVGcmFtZShmcmFtZSk7XHJcbiAgICAgICAgICAgIC8vQHRzLWlnbm9yZVxyXG4gICAgICAgICAgICBpZiAocGFja2VkRnJhbWUpIHtcclxuICAgICAgICAgICAgICAgIGZyYW1lLl9zZXREeW5hbWljQXRsYXNGcmFtZShwYWNrZWRGcmFtZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IG1hdGVyaWFsID0gY29tcC5fbWF0ZXJpYWxzWzBdO1xyXG4gICAgICAgIGlmICghbWF0ZXJpYWwpIHJldHVybjtcclxuICAgICAgICBcclxuICAgICAgICBpZiAobWF0ZXJpYWwuZ2V0UHJvcGVydHkodGhpcy5fdGV4dHVyZVZhci51bmlmTmFtZSkgIT09IGZyYW1lLl90ZXh0dXJlKSB7XHJcblx0XHRcdHRoaXMub25SZW5kZXJVcGRhdGVNYXRlcmlhbCgpO1xyXG5cdFx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfdmVydGV4XCIpO1xyXG4gICAgICAgIH1cclxuXHR9XHJcblx0XHJcbiAgICBwcml2YXRlIF91c2VTcHJpdGVTaXplICgpIHtcclxuICAgICAgICBpZih0aGlzLmVuYWJsZWQpe1xyXG4gICAgICAgICAgICBpZih0aGlzLl91c2VTaGFwZSl7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuX3Nwcml0ZUZyYW1lIHx8ICF0aGlzLmlzVmFsaWQpICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICBpZiAoU2l6ZU1vZGUuUkFXID09PSB0aGlzLl9zaXplTW9kZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIEB0cy1pZ25vcmVcclxuICAgICAgICAgICAgICAgICAgICB2YXIgc2l6ZSA9IHRoaXMuX3Nwcml0ZUZyYW1lLl9vcmlnaW5hbFNpemU7XHJcblx0XHRcdFx0XHR0aGlzLl9zaXplID0gY2Muc2l6ZShzaXplKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoU2l6ZU1vZGUuVFJJTU1FRCA9PT0gdGhpcy5fc2l6ZU1vZGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBAdHMtaWdub3JlXHJcblx0XHRcdFx0XHR2YXIgcmVjdCA9IHRoaXMuX3Nwcml0ZUZyYW1lLl9yZWN0O1xyXG5cdFx0XHRcdFx0dGhpcy5fc2l6ZSA9IGNjLnNpemUocmVjdC53aWR0aCwgcmVjdC5oZWlnaHQpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZih0aGlzLl9zeW5jU2l6ZVRvTm9kZSl7XHJcblx0XHRcdFx0XHR0aGlzLm5vZGUuc2V0Q29udGVudFNpemUodGhpcy5fc2l6ZSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV92ZXJ0ZXhcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfdXNlU3ByaXRlRnJhbWUgKG9sZEZyYW1lOmNjLlNwcml0ZUZyYW1lKSB7XHJcbiAgICAgICAgaWYodGhpcy5lbmFibGVkKXtcclxuICAgICAgICAgICAgbGV0IG9sZFRleHR1cmUgPSBvbGRGcmFtZSAmJiBvbGRGcmFtZS5nZXRUZXh0dXJlKCk7XHJcbiAgICAgICAgICAgIGlmIChvbGRUZXh0dXJlICYmICFvbGRUZXh0dXJlLmxvYWRlZCkge1xyXG4gICAgICAgICAgICAgICAgb2xkRnJhbWUub2ZmKCdsb2FkJywgdGhpcy5fdXNlU3ByaXRlU2l6ZSwgdGhpcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgICAgICBsZXQgc3ByaXRlRnJhbWUgPSB0aGlzLl9zcHJpdGVGcmFtZTtcclxuICAgICAgICAgICAgaWYgKHNwcml0ZUZyYW1lKSB7XHJcblx0XHRcdFx0Ly90aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfbWF0ZXJpYWxcIik7XHJcblx0XHRcdFx0dGhpcy5vblJlbmRlclVwZGF0ZU1hdGVyaWFsKCk7XHJcbiAgICAgICAgICAgICAgICBsZXQgbmV3VGV4dHVyZSA9IHNwcml0ZUZyYW1lLmdldFRleHR1cmUoKTtcclxuICAgICAgICAgICAgICAgIGlmIChuZXdUZXh0dXJlICYmIG5ld1RleHR1cmUubG9hZGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdXNlU3ByaXRlU2l6ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3ByaXRlRnJhbWUub25jZSgnbG9hZCcsIHRoaXMuX3VzZVNwcml0ZVNpemUsIHRoaXMpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblx0XHR9XHJcblx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfdmVydGV4XCIpO1xyXG4gICAgICAgIGlmIChDQ19FRElUT1IpIHtcclxuICAgICAgICAgICAgdGhpcy5fdXNlQXRsYXModGhpcy5fc3ByaXRlRnJhbWUpO1xyXG4gICAgICAgIH1cclxuXHR9XHJcblxyXG4gICAgcHJpdmF0ZSBfdXNlQXRsYXMoc3ByaXRlRnJhbWU6Y2MuU3ByaXRlRnJhbWUpIHtcclxuXHRcdGlmKENDX0VESVRPUil7XHJcbiAgICAgICAgICAgIC8vRWRpdG9yLmxvZyhcIl91c2VBdGxhc1wiKTtcclxuXHRcdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0XHRpZiAoc3ByaXRlRnJhbWUgJiYgc3ByaXRlRnJhbWUuX2F0bGFzVXVpZCkge1xyXG5cdFx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdFx0Y2MuYXNzZXRNYW5hZ2VyLmxvYWRBbnkoc3ByaXRlRnJhbWUuX2F0bGFzVXVpZCwgZnVuY3Rpb24gKGVyciwgYXNzZXQpIHtcclxuICAgICAgICAgICAgICAgICAgICBzZWxmLl9hdGxhcyA9IGFzc2V0O1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdHRoaXMuX2F0bGFzID0gbnVsbDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG4gICAgfVxyXG59XHJcblxyXG59XHJcbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGggPSBcIi4vU2hhZGVyQ29tcG9uZW50LnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2MkcmVuZGVyIHtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eSwgbWVudSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbi8vQHRzLWlnbm9yZVxyXG5sZXQgZ2Z4ID0gY2MuZ2Z4O1xyXG5cclxuLyoqXHJcbiAqIOWAvOmFjee9rlxyXG4gKi9cclxuY29uc3QgVkMgPXtcclxuXHR0YWcgOiBcInZlYzJcIixcclxuXHJcblx0dmFyU2VsZWN0IDogdHJ1ZSxcclxuXHR2YXJEZWZ0eXBlIDogVmFyaWFibGVUeXBlLlVOSUZPUk0sXHJcblx0dmFyQXR0ck1hY3JvIDogXCJBVFRSX1ZFQzJcIixcclxuXHR2YXJBdHRyTWFjcm9DaGVja09ubHkgOiBmYWxzZSxcclxuXHR2YXJBdHRyTmFtZSA6IFwiYV92ZWMydlwiLFxyXG5cdHZhclVuaWZNYWNybyA6IFwiVU5JRl9WRUMyXCIsXHJcblx0dmFyVW5pZk1hY3JvQ2hlY2tPbmx5IDogZmFsc2UsXHJcblx0dmFyVW5pZk5hbWUgOiBcInVfdmVjMnZcIixcclxuXHRcclxuXHR2YWxEZWZhdWx0IDogY2MudjIoKSxcclxuXHR2YWxOZXcgOiAoKT0+eyByZXR1cm4gY2MudjIoKTsgfSxcclxuXHR2YWxUeXBlIDogY2MuVmVjMixcclxuXHJcblx0dHlwZVNpemUgOiAyLFxyXG5cclxuXHR0b0FycmF5IDogKGFycjpudW1iZXJbXSwgdmFsdWU6Y2MuVmVjMiwgb2ZmZXN0Om51bWJlcikgPT4ge1xyXG5cdFx0YXJyW29mZmVzdF0gPSB2YWx1ZS54O1xyXG5cdFx0YXJyW29mZmVzdCArIDFdID0gdmFsdWUueTtcclxuXHR9XHJcbn1cclxuXHJcbkBjY2NsYXNzKFwibGNjJHJlbmRlci5TaGFkZXJWZWMyXCIpXHJcbkBtZW51KFwiaTE4bjpsY2MtcmVuZGVyLm1lbnVfY29tcG9uZW50L1NoYWRlclZlYzJcIilcclxuZXhwb3J0IGNsYXNzIFNoYWRlclZlYzIgZXh0ZW5kcyBTaGFkZXJDb21wb25lbnQge1xyXG5cclxuXHRfdGFnOnN0cmluZyA9IFZDLnRhZztcclxuXHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOmNjLkVudW0oVmFsdWVUeXBlKVxyXG5cdH0pXHJcblx0X3ZhbHVlVHlwZTpWYWx1ZVR5cGUgPSBWYWx1ZVR5cGUuU0lOR0xFO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDpjYy5FbnVtKFZhbHVlVHlwZSksXHJcblx0XHR0b29sdGlwIDogXCLlgLznsbvlnotcIlxyXG5cdH0pXHJcblx0Z2V0IHZhbHVlVHlwZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3ZhbHVlVHlwZTtcclxuXHR9XHJcblx0c2V0IHZhbHVlVHlwZSh2YWx1ZTpWYWx1ZVR5cGUpe1xyXG5cdFx0aWYodGhpcy5fdmFsdWVUeXBlICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fdmFsdWVUeXBlID0gdmFsdWU7XHJcblx0XHRcdGlmKHZhbHVlID09IFZhbHVlVHlwZS5BUlJBWSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZVZhci50eXBlID09IFZhcmlhYmxlVHlwZS5VTklGT1JNO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmFsdWVWYXIuX3R5cGVzZWwgPSBmYWxzZTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZVZhci5fdHlwZXNlbCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdEBwcm9wZXJ0eShWYXJpYWJsZUNvbmZpZylcclxuXHRfdmFsdWVWYXI6VmFyaWFibGVDb25maWcgPSBuZXcgVmFyaWFibGVDb25maWcoXHJcblx0XHRWQy52YXJTZWxlY3QsIFZDLnZhckRlZnR5cGUsIFxyXG5cdFx0VkMudmFyQXR0ck1hY3JvLCBWQy52YXJBdHRyTWFjcm9DaGVja09ubHksIFZDLnZhckF0dHJOYW1lLCBcclxuXHRcdFZDLnZhclVuaWZNYWNybywgVkMudmFyVW5pZk1hY3JvQ2hlY2tPbmx5LCBWQy52YXJVbmlmTmFtZSk7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBWYXJpYWJsZUNvbmZpZyxcclxuXHRcdHRvb2x0aXAgOiBcIuWAvOWPmOmHj1wiXHJcblx0fSlcclxuXHRnZXQgdmFsdWVWYXIoKXtcclxuXHRcdHJldHVybiB0aGlzLl92YWx1ZVZhcjtcclxuXHR9XHJcblx0c2V0IHZhbHVlVmFyKHZhbHVlOlZhcmlhYmxlQ29uZmlnKXtcclxuXHRcdHRoaXMuX3ZhbHVlVmFyID0gdmFsdWU7XHJcblx0XHR0aGlzLm9uVXBkYXRlVmFsdWVzKCk7XHJcblx0XHR0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuXHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIik7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIEBwcm9wZXJ0eShbVkMudmFsVHlwZV0pXHJcblx0X3ZhbHVlczp0eXBlb2YgVkMudmFsRGVmYXVsdFtdID0gWyBWQy52YWxOZXcoKSBdO1xyXG5cdFxyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdmFsdWVUeXBlID09IFZhbHVlVHlwZS5TSU5HTEUgJiYgdGhpcy5fdmFsdWVWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuVU5JRk9STTtcclxuXHRcdH0sXHJcblx0XHR0eXBlIDogVkMudmFsVHlwZSxcclxuXHRcdHRvb2x0aXAgOiBcIuWAvFwiXHJcblx0fSlcclxuXHRnZXQgdmFsdWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl92YWx1ZXNbMF07XHJcblx0fVxyXG5cdHNldCB2YWx1ZSh2YWx1ZV86dHlwZW9mIFZDLnZhbERlZmF1bHQpe1xyXG5cdFx0dGhpcy5fdmFsdWVzWzBdID0gdmFsdWVfO1xyXG5cdFx0dGhpcy5vblVwZGF0ZVZhbHVlcygpO1xyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHZpc2libGUgKCl7XHJcblx0XHRcdHJldHVybiB0aGlzLl92YWx1ZVR5cGUgPT0gVmFsdWVUeXBlLkFSUkFZIHx8IHRoaXMuX3ZhbHVlVmFyLnR5cGUgPT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURTtcclxuXHRcdH0sXHJcblx0XHR0eXBlIDogW1ZDLnZhbFR5cGVdLFxyXG5cdFx0dG9vbHRpcCA6IFwi5YC85pWw57uEL+WAvOmhtueCueaVsOe7hFwiXHJcblx0fSlcclxuXHRnZXQgdmFsdWVzKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fdmFsdWVzO1xyXG5cdH1cclxuXHRzZXQgdmFsdWVzKHZhbHVlOnR5cGVvZiBWQy52YWxEZWZhdWx0W10pe1xyXG5cdFx0dGhpcy5fdmFsdWVzID0gdmFsdWU7XHJcblx0XHR0aGlzLm9uVXBkYXRlVmFsdWVzKCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDlgLzlgY/np7tcclxuXHQgKi9cclxuICAgIHByaXZhdGUgX3ZhbHVlT2Zmc2V0Om51bWJlciA9IDA7XHJcblxyXG4gICAgb25Mb2FkKCl7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKFwicmVuZGVyX3NoYXBlX2NoZWNrZWRcIiwgdGhpcy5vblJlbmRlclNoYXBlQ2hlY2tlZCwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKFwicmVuZGVyX3VwZGF0ZV9tYXRlcmlhbFwiLCB0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwsIHRoaXMpO1xyXG5cdFx0dGhpcy5ub2RlLm9uKFwicmVuZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIiwgdGhpcy5vblJlbmRlclVwZGF0ZUF0dHJpYnV0ZSwgdGhpcyk7XHJcblx0XHR0aGlzLm5vZGUub24oXCJyZW5kZXJfdXBkYXRlX3ZlcnRleFwiLCB0aGlzLm9uUmVuZGVyVXBkYXRlUmVuZGVyRGF0YSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EZXN0cm95KCl7XHJcblx0XHR0aGlzLm5vZGUudGFyZ2V0T2ZmKHRoaXMpO1xyXG4gICAgfVxyXG4gICAgXHJcblx0b25FbmFibGUoKXtcclxuICAgICAgICB0aGlzLm9uU3RhdGVVcGRhdGUodHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRvbkRpc2FibGUoKXtcclxuICAgICAgICB0aGlzLm9uU3RhdGVVcGRhdGUoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25TdGF0ZVVwZGF0ZShlbmFibGU6Ym9vbGVhbil7XHJcblx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfdGFnXCIpO1xyXG5cdFx0dGhpcy5vblVwZGF0ZVZhbHVlcygpO1xyXG5cdH1cclxuXHRcclxuICAgIC8qKlxyXG4gICAgICog5qOA5p+l5b2i54q2XHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgb25SZW5kZXJTaGFwZUNoZWNrZWQoKXtcclxuICAgICAgICBpZih0aGlzLl92YWx1ZVZhci50eXBlID09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEUpe1xyXG4gICAgICAgICAgICB0aGlzLm9uVXBkYXRlVmFsdWVzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIOajgOafpeWAvFxyXG5cdCAqL1xyXG5cdHByaXZhdGUgb25VcGRhdGVWYWx1ZXMoKXtcclxuXHRcdGlmKHRoaXMuZW5hYmxlZCl7XHJcbiAgICAgICAgICAgIGlmKHRoaXMuX3ZhbHVlcy5sZW5ndGggPD0gMCl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZXMgPSBbIFZDLnZhbE5ldygpIF07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYodGhpcy5fdmFsdWVWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuQVRUUklCVVRFKXtcclxuICAgICAgICAgICAgICAgIGxldCByc3lzID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKTtcclxuICAgICAgICAgICAgICAgIGlmKHJzeXMpe1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCB2YyA9IHJzeXMuX3ZlcnRpY2VzQ291bnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5fdmFsdWVzLmxlbmd0aCA8IHZjKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHZhbHVlcyA9IHRoaXMuX3ZhbHVlcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHZjOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNvbG9yID0gdmFsdWVzW2ldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoY29sb3IgPT0gbnVsbCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVzW2ldID0gVkMudmFsTmV3KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdmFsdWVzLmxlbmd0aCA9IHZjO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9ZWxzZSBpZih0aGlzLl92YWx1ZVR5cGUgPT0gVmFsdWVUeXBlLlNJTkdMRSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZXMubGVuZ3RoID0gMTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZih0aGlzLl92YWx1ZVZhci50eXBlID09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEUpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX2F0dHJpYnV0ZVwiKTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuICAgICAgICAgICAgfVxyXG5cdFx0fVxyXG5cdH1cclxuICAgIFxyXG4gICAgcHJpdmF0ZSBvblJlbmRlclVwZGF0ZU1hdGVyaWFsIChjb21wOlJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSkpIHtcclxuXHRcdGlmKGNvbXApe1xyXG5cdFx0XHQvLyBtYWtlIHN1cmUgbWF0ZXJpYWwgaXMgYmVsb25nIHRvIHNlbGYuXHJcblx0XHRcdC8vIEB0cy1pZ25vcmVcclxuXHRcdFx0bGV0IG1hdGVyaWFsID0gY29tcC5fbWF0ZXJpYWxzWzBdO1xyXG5cdFx0XHRpZiAobWF0ZXJpYWwpIHtcclxuXHRcdFx0XHRpZiAodGhpcy5jaGVja01hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3ZhbHVlVmFyLnVuaWZNYWNybykpIHtcclxuXHRcdFx0XHRcdGlmKHRoaXMuZW5hYmxlZCAmJiB0aGlzLl92YWx1ZVZhci50eXBlID09PSBWYXJpYWJsZVR5cGUuVU5JRk9STSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fdmFsdWVWYXIudW5pZk1hY3JvLCB0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5fdmFsdWVUeXBlID09IFZhbHVlVHlwZS5TSU5HTEUpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF0ZXJpYWwuc2V0UHJvcGVydHkodGhpcy5fdmFsdWVWYXIudW5pZk5hbWUsIHRoaXMuX3ZhbHVlc1swXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHZhbHVlcyA9IHRoaXMuX3ZhbHVlcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBfdmFsdWVzOm51bWJlcltdID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgZGVmdiA9IFZDLnZhbERlZmF1bHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IobGV0IGkgPSAwLCBsID0gdmFsdWVzLmxlbmd0aDsgaSA8IGw7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVkMudG9BcnJheShfdmFsdWVzLCB2YWx1ZXNbaV0gfHwgZGVmdiwgX3ZhbHVlcy5sZW5ndGgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF0ZXJpYWwuc2V0UHJvcGVydHkodGhpcy5fdmFsdWVWYXIudW5pZk5hbWUsIG5ldyBGbG9hdDMyQXJyYXkodmFsdWVzLmxlbmd0aCAqIFZDLnR5cGVTaXplKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXRlcmlhbC5zZXRQcm9wZXJ0eSh0aGlzLl92YWx1ZVZhci51bmlmTmFtZSwgX3ZhbHVlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHRcdFx0XHRcdFx0Ly9FZGl0b3IubG9nKFwib25SZW5kZXJVcGRhdGVNYXRlcmlhbFwiLCB0aGlzLl92YWx1ZSk7XHJcblx0XHRcdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl92YWx1ZVZhci51bmlmTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWxcIiwgdGhpcy5lbmFibGVkKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuICAgIH1cclxuXHRcclxuXHQvKipcclxuXHQgKiDojrflvpflsZ7mgKflgLxcclxuXHQgKiBAcGFyYW0gY29tcCBcclxuXHQgKi9cclxuXHRwcml2YXRlIGdldEF0dHJpYnV0ZVZhbHVlcyhjb21wOlJlbmRlclN5c3RlbSl7XHJcblx0XHRsZXQgdmFsdWVzOm51bWJlcltdID0gW107XHJcblx0XHRsZXQgdmMgPSBjb21wLl92ZXJ0aWNlc0NvdW50O1xyXG4gICAgICAgIGxldCBkZWZ2ID0gVkMudmFsRGVmYXVsdDtcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdmM7IGkrKyl7XHJcbiAgICAgICAgICAgIFZDLnRvQXJyYXkodmFsdWVzLCB0aGlzLl92YWx1ZXNbaV0gfHwgZGVmdiwgdmFsdWVzLmxlbmd0aCk7XHJcbiAgICAgICAgfVxyXG5cdFx0cmV0dXJuIHZhbHVlcztcclxuXHR9XHJcblxyXG5cdHByaXZhdGUgb25SZW5kZXJVcGRhdGVBdHRyaWJ1dGUoY29tcDpSZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pKXtcclxuXHRcdGlmKGNvbXApe1xyXG5cdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdGxldCBtYXRlcmlhbCA9IGNvbXAuX21hdGVyaWFsc1swXTtcclxuXHRcdFx0aWYgKG1hdGVyaWFsKSB7XHJcblx0XHRcdFx0aWYgKHRoaXMuY2hlY2tNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl92YWx1ZVZhci5hdHRyTWFjcm8pKSB7XHJcblx0XHRcdFx0XHRpZih0aGlzLmVuYWJsZWQgJiYgdGhpcy5fdmFsdWVWYXIudHlwZSA9PT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURSl7XHJcblx0XHRcdFx0XHRcdHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fdmFsdWVWYXIuYXR0ck1hY3JvLCB0cnVlKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5fdmFsdWVPZmZzZXQgPSBjb21wLmFkZFZlcnRleEF0dHJpYnV0ZSh7IFxyXG5cdFx0XHRcdFx0XHRcdG5hbWU6IHRoaXMuX3ZhbHVlVmFyLmF0dHJOYW1lLCBcclxuXHRcdFx0XHRcdFx0XHR0eXBlOiBnZnguQVRUUl9UWVBFX0ZMT0FUMzIsIFxyXG5cdFx0XHRcdFx0XHRcdG51bTogVkMudHlwZVNpemUsIFxyXG5cdFx0XHRcdFx0XHR9LCBWQy50eXBlU2l6ZSk7XHJcblx0XHRcdFx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlQXR0cmlidXRlXCIsIHRoaXMuX3ZhbHVlT2Zmc2V0KTtcclxuXHRcdFx0XHRcdH1lbHNle1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3ZhbHVlVmFyLmF0dHJNYWNybywgZmFsc2UpO1xyXG5cdFx0XHRcdFx0XHR0aGlzLl92YWx1ZU9mZnNldCA9IDA7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHQvL0VkaXRvci5sb2coXCJvblJlbmRlclVwZGF0ZUF0dHJpYnV0ZVwiLCB0aGlzLmVuYWJsZWQpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cHJpdmF0ZSBvblJlbmRlclVwZGF0ZVJlbmRlckRhdGEoY29tcDpSZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pKXtcclxuICAgICAgICBpZihjb21wICYmIHRoaXMuZW5hYmxlZCl7XHJcblx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlUmVuZGVyRGF0YVwiKTtcclxuXHRcdFx0aWYodGhpcy5fdmFsdWVPZmZzZXQgPiAwKXtcclxuXHRcdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdFx0bGV0IGFzc2VtYmxlciA9IGNvbXAuX2Fzc2VtYmxlcjtcclxuXHRcdFx0XHRsZXQgdmFsdWVzID0gdGhpcy5nZXRBdHRyaWJ1dGVWYWx1ZXMoY29tcCk7XHJcblx0XHRcdFx0bGV0IHZsZW4gPSB2YWx1ZXMubGVuZ3RoO1xyXG5cdFx0XHRcdGxldCB2YWx1ZU9mZnNldCA9IHRoaXMuX3ZhbHVlT2Zmc2V0O1xyXG5cdFx0XHRcdGxldCBmbG9hdHNQZXJWZXJ0ID0gYXNzZW1ibGVyLmZsb2F0c1BlclZlcnQ7XHJcblx0XHRcdFx0bGV0IHZlcnRzID0gYXNzZW1ibGVyLnJlbmRlckRhdGEudkRhdGFzWzBdO1xyXG5cdFx0XHRcdGZvciAobGV0IGkgPSAwOyBpIDwgNDsgaSsrKSB7XHJcblx0XHRcdFx0XHRsZXQgZHN0T2Zmc2V0ID0gZmxvYXRzUGVyVmVydCAqIGkgKyB2YWx1ZU9mZnNldDtcclxuXHRcdFx0XHRcdGxldCBzcmNPZmZzZXQgPSBWQy50eXBlU2l6ZSAqIGk7XHJcblx0XHRcdFx0XHRpZihzcmNPZmZzZXQgPj0gdmxlbil7XHJcblx0XHRcdFx0XHRcdHNyY09mZnNldCA9IDA7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRmb3IobGV0IGogPSAwOyBqIDwgVkMudHlwZVNpemU7IGorKyl7XHJcblx0XHRcdFx0XHRcdHZlcnRzW2RzdE9mZnNldCArIGpdID0gdmFsdWVzW3NyY09mZnNldCArIGpdO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG4gICAgICAgIH1cclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi9TaGFkZXJDb21wb25lbnQudHNcIiAvPlxyXG5cclxubW9kdWxlIGxjYyRyZW5kZXIge1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5LCBtZW51IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuLy9AdHMtaWdub3JlXHJcbmxldCBnZnggPSBjYy5nZng7XHJcblxyXG4vKipcclxuICog5YC86YWN572uXHJcbiAqL1xyXG5jb25zdCBWQyA9e1xyXG5cdHRhZyA6IFwidmVjM1wiLFxyXG5cclxuXHR2YXJTZWxlY3QgOiBmYWxzZSxcclxuXHR2YXJEZWZ0eXBlIDogVmFyaWFibGVUeXBlLlVOSUZPUk0sXHJcblx0dmFyQXR0ck1hY3JvIDogXCJBVFRSX1ZFQzNcIixcclxuXHR2YXJBdHRyTWFjcm9DaGVja09ubHkgOiBmYWxzZSxcclxuXHR2YXJBdHRyTmFtZSA6IFwiYV92ZWMzdlwiLFxyXG5cdHZhclVuaWZNYWNybyA6IFwiVU5JRl9WRUMzXCIsXHJcblx0dmFyVW5pZk1hY3JvQ2hlY2tPbmx5IDogZmFsc2UsXHJcblx0dmFyVW5pZk5hbWUgOiBcInVfdmVjM3ZcIixcclxuXHRcclxuXHR2YWxEZWZhdWx0IDogY2MudjMoKSxcclxuXHR2YWxOZXcgOiAoKT0+eyByZXR1cm4gY2MudjMoKTsgfSxcclxuXHR2YWxUeXBlIDogY2MuVmVjMyxcclxuXHJcblx0dHlwZVNpemUgOiAzLFxyXG5cclxuXHR0b0FycmF5IDogKGFycjpudW1iZXJbXSwgdmFsdWU6Y2MuVmVjMywgb2ZmZXN0Om51bWJlcikgPT4ge1xyXG5cdFx0YXJyW29mZmVzdF0gPSB2YWx1ZS54O1xyXG5cdFx0YXJyW29mZmVzdCArIDFdID0gdmFsdWUueTtcclxuXHRcdGFycltvZmZlc3QgKyAyXSA9IHZhbHVlLno7XHJcblx0fVxyXG59XHJcblxyXG5AY2NjbGFzcyhcInJlbmRlci5TaGFkZXJWZWMzXCIpXHJcbkBtZW51KFwiaTE4bjpsY2MtcmVuZGVyLm1lbnVfY29tcG9uZW50L1NoYWRlclZlYzNcIilcclxuZXhwb3J0IGNsYXNzIFNoYWRlclZlYzMgZXh0ZW5kcyBTaGFkZXJDb21wb25lbnQge1xyXG5cclxuXHRfdGFnOnN0cmluZyA9IFZDLnRhZztcclxuXHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOmNjLkVudW0oVmFsdWVUeXBlKVxyXG5cdH0pXHJcblx0X3ZhbHVlVHlwZTpWYWx1ZVR5cGUgPSBWYWx1ZVR5cGUuU0lOR0xFO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDpjYy5FbnVtKFZhbHVlVHlwZSksXHJcblx0XHR0b29sdGlwIDogXCLlgLznsbvlnotcIlxyXG5cdH0pXHJcblx0Z2V0IHZhbHVlVHlwZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3ZhbHVlVHlwZTtcclxuXHR9XHJcblx0c2V0IHZhbHVlVHlwZSh2YWx1ZTpWYWx1ZVR5cGUpe1xyXG5cdFx0aWYodGhpcy5fdmFsdWVUeXBlICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fdmFsdWVUeXBlID0gdmFsdWU7XHJcblx0XHRcdGlmKHZhbHVlID09IFZhbHVlVHlwZS5BUlJBWSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZVZhci50eXBlID09IFZhcmlhYmxlVHlwZS5VTklGT1JNO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmFsdWVWYXIuX3R5cGVzZWwgPSBmYWxzZTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZVZhci5fdHlwZXNlbCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdEBwcm9wZXJ0eShWYXJpYWJsZUNvbmZpZylcclxuXHRfdmFsdWVWYXI6VmFyaWFibGVDb25maWcgPSBuZXcgVmFyaWFibGVDb25maWcoXHJcblx0XHRWQy52YXJTZWxlY3QsIFZDLnZhckRlZnR5cGUsIFxyXG5cdFx0VkMudmFyQXR0ck1hY3JvLCBWQy52YXJBdHRyTWFjcm9DaGVja09ubHksIFZDLnZhckF0dHJOYW1lLCBcclxuXHRcdFZDLnZhclVuaWZNYWNybywgVkMudmFyVW5pZk1hY3JvQ2hlY2tPbmx5LCBWQy52YXJVbmlmTmFtZSk7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBWYXJpYWJsZUNvbmZpZyxcclxuXHRcdHRvb2x0aXAgOiBcIuWAvOWPmOmHj1wiXHJcblx0fSlcclxuXHRnZXQgdmFsdWVWYXIoKXtcclxuXHRcdHJldHVybiB0aGlzLl92YWx1ZVZhcjtcclxuXHR9XHJcblx0c2V0IHZhbHVlVmFyKHZhbHVlOlZhcmlhYmxlQ29uZmlnKXtcclxuXHRcdHRoaXMuX3ZhbHVlVmFyID0gdmFsdWU7XHJcblx0XHR0aGlzLm9uVXBkYXRlVmFsdWVzKCk7XHJcblx0XHR0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuXHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIik7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIEBwcm9wZXJ0eShbVkMudmFsVHlwZV0pXHJcblx0X3ZhbHVlczp0eXBlb2YgVkMudmFsRGVmYXVsdFtdID0gWyBWQy52YWxOZXcoKSBdO1xyXG5cdFxyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdmFsdWVUeXBlID09IFZhbHVlVHlwZS5TSU5HTEUgJiYgdGhpcy5fdmFsdWVWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuVU5JRk9STTtcclxuXHRcdH0sXHJcblx0XHR0eXBlIDogVkMudmFsVHlwZSxcclxuXHRcdHRvb2x0aXAgOiBcIuWAvFwiXHJcblx0fSlcclxuXHRnZXQgdmFsdWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl92YWx1ZXNbMF07XHJcblx0fVxyXG5cdHNldCB2YWx1ZSh2YWx1ZV86dHlwZW9mIFZDLnZhbERlZmF1bHQpe1xyXG5cdFx0dGhpcy5fdmFsdWVzWzBdID0gdmFsdWVfO1xyXG5cdFx0dGhpcy5vblVwZGF0ZVZhbHVlcygpO1xyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHZpc2libGUgKCl7XHJcblx0XHRcdHJldHVybiB0aGlzLl92YWx1ZVR5cGUgPT0gVmFsdWVUeXBlLkFSUkFZIHx8IHRoaXMuX3ZhbHVlVmFyLnR5cGUgPT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURTtcclxuXHRcdH0sXHJcblx0XHR0eXBlIDogW1ZDLnZhbFR5cGVdLFxyXG5cdFx0dG9vbHRpcCA6IFwi5YC85pWw57uEL+WAvOmhtueCueaVsOe7hFwiXHJcblx0fSlcclxuXHRnZXQgdmFsdWVzKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fdmFsdWVzO1xyXG5cdH1cclxuXHRzZXQgdmFsdWVzKHZhbHVlOnR5cGVvZiBWQy52YWxEZWZhdWx0W10pe1xyXG5cdFx0dGhpcy5fdmFsdWVzID0gdmFsdWU7XHJcblx0XHR0aGlzLm9uVXBkYXRlVmFsdWVzKCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDlgLzlgY/np7tcclxuXHQgKi9cclxuICAgIHByaXZhdGUgX3ZhbHVlT2Zmc2V0Om51bWJlciA9IDA7XHJcblxyXG4gICAgb25Mb2FkKCl7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKFwicmVuZGVyX3NoYXBlX2NoZWNrZWRcIiwgdGhpcy5vblJlbmRlclNoYXBlQ2hlY2tlZCwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKFwicmVuZGVyX3VwZGF0ZV9tYXRlcmlhbFwiLCB0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwsIHRoaXMpO1xyXG5cdFx0dGhpcy5ub2RlLm9uKFwicmVuZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIiwgdGhpcy5vblJlbmRlclVwZGF0ZUF0dHJpYnV0ZSwgdGhpcyk7XHJcblx0XHR0aGlzLm5vZGUub24oXCJyZW5kZXJfdXBkYXRlX3ZlcnRleFwiLCB0aGlzLm9uUmVuZGVyVXBkYXRlUmVuZGVyRGF0YSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EZXN0cm95KCl7XHJcblx0XHR0aGlzLm5vZGUudGFyZ2V0T2ZmKHRoaXMpO1xyXG4gICAgfVxyXG4gICAgXHJcblx0b25FbmFibGUoKXtcclxuICAgICAgICB0aGlzLm9uU3RhdGVVcGRhdGUodHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRvbkRpc2FibGUoKXtcclxuICAgICAgICB0aGlzLm9uU3RhdGVVcGRhdGUoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25TdGF0ZVVwZGF0ZShlbmFibGU6Ym9vbGVhbil7XHJcblx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfdGFnXCIpO1xyXG5cdFx0dGhpcy5vblVwZGF0ZVZhbHVlcygpO1xyXG5cdH1cclxuXHRcclxuICAgIC8qKlxyXG4gICAgICog5qOA5p+l5b2i54q2XHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgb25SZW5kZXJTaGFwZUNoZWNrZWQoKXtcclxuICAgICAgICBpZih0aGlzLl92YWx1ZVZhci50eXBlID09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEUpe1xyXG4gICAgICAgICAgICB0aGlzLm9uVXBkYXRlVmFsdWVzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIOajgOafpeWAvFxyXG5cdCAqL1xyXG5cdHByaXZhdGUgb25VcGRhdGVWYWx1ZXMoKXtcclxuXHRcdGlmKHRoaXMuZW5hYmxlZCl7XHJcbiAgICAgICAgICAgIGlmKHRoaXMuX3ZhbHVlcy5sZW5ndGggPD0gMCl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZXMgPSBbIFZDLnZhbE5ldygpIF07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYodGhpcy5fdmFsdWVWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuQVRUUklCVVRFKXtcclxuICAgICAgICAgICAgICAgIGxldCByc3lzID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKTtcclxuICAgICAgICAgICAgICAgIGlmKHJzeXMpe1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCB2YyA9IHJzeXMuX3ZlcnRpY2VzQ291bnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5fdmFsdWVzLmxlbmd0aCA8IHZjKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHZhbHVlcyA9IHRoaXMuX3ZhbHVlcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHZjOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNvbG9yID0gdmFsdWVzW2ldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoY29sb3IgPT0gbnVsbCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVzW2ldID0gVkMudmFsTmV3KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdmFsdWVzLmxlbmd0aCA9IHZjO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9ZWxzZSBpZih0aGlzLl92YWx1ZVR5cGUgPT0gVmFsdWVUeXBlLlNJTkdMRSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZXMubGVuZ3RoID0gMTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZih0aGlzLl92YWx1ZVZhci50eXBlID09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEUpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX2F0dHJpYnV0ZVwiKTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuICAgICAgICAgICAgfVxyXG5cdFx0fVxyXG5cdH1cclxuICAgIFxyXG4gICAgcHJpdmF0ZSBvblJlbmRlclVwZGF0ZU1hdGVyaWFsIChjb21wOlJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSkpIHtcclxuXHRcdGlmKGNvbXApe1xyXG5cdFx0XHQvLyBtYWtlIHN1cmUgbWF0ZXJpYWwgaXMgYmVsb25nIHRvIHNlbGYuXHJcblx0XHRcdC8vIEB0cy1pZ25vcmVcclxuXHRcdFx0bGV0IG1hdGVyaWFsID0gY29tcC5fbWF0ZXJpYWxzWzBdO1xyXG5cdFx0XHRpZiAobWF0ZXJpYWwpIHtcclxuXHRcdFx0XHRpZiAodGhpcy5jaGVja01hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3ZhbHVlVmFyLnVuaWZNYWNybykpIHtcclxuXHRcdFx0XHRcdGlmKHRoaXMuZW5hYmxlZCAmJiB0aGlzLl92YWx1ZVZhci50eXBlID09PSBWYXJpYWJsZVR5cGUuVU5JRk9STSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fdmFsdWVWYXIudW5pZk1hY3JvLCB0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5fdmFsdWVUeXBlID09IFZhbHVlVHlwZS5TSU5HTEUpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF0ZXJpYWwuc2V0UHJvcGVydHkodGhpcy5fdmFsdWVWYXIudW5pZk5hbWUsIHRoaXMuX3ZhbHVlc1swXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHZhbHVlcyA9IHRoaXMuX3ZhbHVlcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBfdmFsdWVzOm51bWJlcltdID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgZGVmdiA9IFZDLnZhbERlZmF1bHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IobGV0IGkgPSAwLCBsID0gdmFsdWVzLmxlbmd0aDsgaSA8IGw7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVkMudG9BcnJheShfdmFsdWVzLCB2YWx1ZXNbaV0gfHwgZGVmdiwgX3ZhbHVlcy5sZW5ndGgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF0ZXJpYWwuc2V0UHJvcGVydHkodGhpcy5fdmFsdWVWYXIudW5pZk5hbWUsIG5ldyBGbG9hdDMyQXJyYXkodmFsdWVzLmxlbmd0aCAqIFZDLnR5cGVTaXplKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXRlcmlhbC5zZXRQcm9wZXJ0eSh0aGlzLl92YWx1ZVZhci51bmlmTmFtZSwgX3ZhbHVlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHRcdFx0XHRcdFx0Ly9FZGl0b3IubG9nKFwib25SZW5kZXJVcGRhdGVNYXRlcmlhbFwiLCB0aGlzLl92YWx1ZSk7XHJcblx0XHRcdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl92YWx1ZVZhci51bmlmTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWxcIiwgdGhpcy5lbmFibGVkKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuICAgIH1cclxuXHRcclxuXHQvKipcclxuXHQgKiDojrflvpflsZ7mgKflgLxcclxuXHQgKiBAcGFyYW0gY29tcCBcclxuXHQgKi9cclxuXHRwcml2YXRlIGdldEF0dHJpYnV0ZVZhbHVlcyhjb21wOlJlbmRlclN5c3RlbSl7XHJcblx0XHRsZXQgdmFsdWVzOm51bWJlcltdID0gW107XHJcblx0XHRsZXQgdmMgPSBjb21wLl92ZXJ0aWNlc0NvdW50O1xyXG4gICAgICAgIGxldCBkZWZ2ID0gVkMudmFsRGVmYXVsdDtcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdmM7IGkrKyl7XHJcbiAgICAgICAgICAgIFZDLnRvQXJyYXkodmFsdWVzLCB0aGlzLl92YWx1ZXNbaV0gfHwgZGVmdiwgdmFsdWVzLmxlbmd0aCk7XHJcbiAgICAgICAgfVxyXG5cdFx0cmV0dXJuIHZhbHVlcztcclxuXHR9XHJcblxyXG5cdHByaXZhdGUgb25SZW5kZXJVcGRhdGVBdHRyaWJ1dGUoY29tcDpSZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pKXtcclxuXHRcdGlmKGNvbXApe1xyXG5cdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdGxldCBtYXRlcmlhbCA9IGNvbXAuX21hdGVyaWFsc1swXTtcclxuXHRcdFx0aWYgKG1hdGVyaWFsKSB7XHJcblx0XHRcdFx0aWYgKHRoaXMuY2hlY2tNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl92YWx1ZVZhci5hdHRyTWFjcm8pKSB7XHJcblx0XHRcdFx0XHRpZih0aGlzLmVuYWJsZWQgJiYgdGhpcy5fdmFsdWVWYXIudHlwZSA9PT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURSl7XHJcblx0XHRcdFx0XHRcdHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fdmFsdWVWYXIuYXR0ck1hY3JvLCB0cnVlKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5fdmFsdWVPZmZzZXQgPSBjb21wLmFkZFZlcnRleEF0dHJpYnV0ZSh7IFxyXG5cdFx0XHRcdFx0XHRcdG5hbWU6IHRoaXMuX3ZhbHVlVmFyLmF0dHJOYW1lLCBcclxuXHRcdFx0XHRcdFx0XHR0eXBlOiBnZnguQVRUUl9UWVBFX0ZMT0FUMzIsIFxyXG5cdFx0XHRcdFx0XHRcdG51bTogVkMudHlwZVNpemUsIFxyXG5cdFx0XHRcdFx0XHR9LCBWQy50eXBlU2l6ZSk7XHJcblx0XHRcdFx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlQXR0cmlidXRlXCIsIHRoaXMuX3ZhbHVlT2Zmc2V0KTtcclxuXHRcdFx0XHRcdH1lbHNle1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3ZhbHVlVmFyLmF0dHJNYWNybywgZmFsc2UpO1xyXG5cdFx0XHRcdFx0XHR0aGlzLl92YWx1ZU9mZnNldCA9IDA7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHQvL0VkaXRvci5sb2coXCJvblJlbmRlclVwZGF0ZUF0dHJpYnV0ZVwiLCB0aGlzLmVuYWJsZWQpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cHJpdmF0ZSBvblJlbmRlclVwZGF0ZVJlbmRlckRhdGEoY29tcDpSZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pKXtcclxuICAgICAgICBpZihjb21wICYmIHRoaXMuZW5hYmxlZCl7XHJcblx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlUmVuZGVyRGF0YVwiKTtcclxuXHRcdFx0aWYodGhpcy5fdmFsdWVPZmZzZXQgPiAwKXtcclxuXHRcdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdFx0bGV0IGFzc2VtYmxlciA9IGNvbXAuX2Fzc2VtYmxlcjtcclxuXHRcdFx0XHRsZXQgdmFsdWVzID0gdGhpcy5nZXRBdHRyaWJ1dGVWYWx1ZXMoY29tcCk7XHJcblx0XHRcdFx0bGV0IHZsZW4gPSB2YWx1ZXMubGVuZ3RoO1xyXG5cdFx0XHRcdGxldCB2YWx1ZU9mZnNldCA9IHRoaXMuX3ZhbHVlT2Zmc2V0O1xyXG5cdFx0XHRcdGxldCBmbG9hdHNQZXJWZXJ0ID0gYXNzZW1ibGVyLmZsb2F0c1BlclZlcnQ7XHJcblx0XHRcdFx0bGV0IHZlcnRzID0gYXNzZW1ibGVyLnJlbmRlckRhdGEudkRhdGFzWzBdO1xyXG5cdFx0XHRcdGZvciAobGV0IGkgPSAwOyBpIDwgNDsgaSsrKSB7XHJcblx0XHRcdFx0XHRsZXQgZHN0T2Zmc2V0ID0gZmxvYXRzUGVyVmVydCAqIGkgKyB2YWx1ZU9mZnNldDtcclxuXHRcdFx0XHRcdGxldCBzcmNPZmZzZXQgPSBWQy50eXBlU2l6ZSAqIGk7XHJcblx0XHRcdFx0XHRpZihzcmNPZmZzZXQgPj0gdmxlbil7XHJcblx0XHRcdFx0XHRcdHNyY09mZnNldCA9IDA7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRmb3IobGV0IGogPSAwOyBqIDwgVkMudHlwZVNpemU7IGorKyl7XHJcblx0XHRcdFx0XHRcdHZlcnRzW2RzdE9mZnNldCArIGpdID0gdmFsdWVzW3NyY09mZnNldCArIGpdO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG4gICAgICAgIH1cclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi9TaGFkZXJDb21wb25lbnQudHNcIiAvPlxyXG5cclxubW9kdWxlIGxjYyRyZW5kZXIge1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5LCBtZW51IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuLy9AdHMtaWdub3JlXHJcbmxldCBnZnggPSBjYy5nZng7XHJcblxyXG4vKipcclxuICog5YC86YWN572uXHJcbiAqL1xyXG5jb25zdCBWQyA9e1xyXG5cdHRhZyA6IFwidmVjNFwiLFxyXG5cclxuXHR2YXJTZWxlY3QgOiB0cnVlLFxyXG5cdHZhckRlZnR5cGUgOiBWYXJpYWJsZVR5cGUuVU5JRk9STSxcclxuXHR2YXJBdHRyTWFjcm8gOiBcIkFUVFJfVkVDNFwiLFxyXG5cdHZhckF0dHJNYWNyb0NoZWNrT25seSA6IGZhbHNlLFxyXG5cdHZhckF0dHJOYW1lIDogXCJhX3ZlYzR2XCIsXHJcblx0dmFyVW5pZk1hY3JvIDogXCJVTklGX1ZFQzRcIixcclxuXHR2YXJVbmlmTWFjcm9DaGVja09ubHkgOiBmYWxzZSxcclxuXHR2YXJVbmlmTmFtZSA6IFwidV92ZWM0dlwiLFxyXG5cdFxyXG5cdHZhbERlZmF1bHQgOiBuZXcgY2MuVmVjNCgpLFxyXG5cdHZhbE5ldyA6ICgpPT57IHJldHVybiBuZXcgY2MuVmVjNCgpOyB9LFxyXG5cdHZhbFR5cGUgOiBjYy5WZWM0LFxyXG5cclxuXHR0eXBlU2l6ZSA6IDQsXHJcblxyXG5cdHRvQXJyYXkgOiAoYXJyOm51bWJlcltdLCB2YWx1ZTpjYy5WZWM0LCBvZmZlc3Q6bnVtYmVyKSA9PiB7XHJcblx0XHRhcnJbb2ZmZXN0XSA9IHZhbHVlLng7XHJcblx0XHRhcnJbb2ZmZXN0ICsgMV0gPSB2YWx1ZS55O1xyXG5cdFx0YXJyW29mZmVzdCArIDJdID0gdmFsdWUuejtcclxuXHRcdGFycltvZmZlc3QgKyAzXSA9IHZhbHVlLnc7XHJcblx0fVxyXG59XHJcblxyXG5AY2NjbGFzcyhcInJlbmRlci5TaGFkZXJWZWM0XCIpXHJcbkBtZW51KFwiaTE4bjpsY2MtcmVuZGVyLm1lbnVfY29tcG9uZW50L1NoYWRlclZlYzRcIilcclxuZXhwb3J0IGNsYXNzIFNoYWRlclZlYzQgZXh0ZW5kcyBTaGFkZXJDb21wb25lbnQge1xyXG5cclxuXHRfdGFnOnN0cmluZyA9IFZDLnRhZztcclxuXHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOmNjLkVudW0oVmFsdWVUeXBlKVxyXG5cdH0pXHJcblx0X3ZhbHVlVHlwZTpWYWx1ZVR5cGUgPSBWYWx1ZVR5cGUuU0lOR0xFO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDpjYy5FbnVtKFZhbHVlVHlwZSksXHJcblx0XHR0b29sdGlwIDogXCLlgLznsbvlnotcIlxyXG5cdH0pXHJcblx0Z2V0IHZhbHVlVHlwZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3ZhbHVlVHlwZTtcclxuXHR9XHJcblx0c2V0IHZhbHVlVHlwZSh2YWx1ZTpWYWx1ZVR5cGUpe1xyXG5cdFx0aWYodGhpcy5fdmFsdWVUeXBlICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fdmFsdWVUeXBlID0gdmFsdWU7XHJcblx0XHRcdGlmKHZhbHVlID09IFZhbHVlVHlwZS5BUlJBWSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZVZhci50eXBlID09IFZhcmlhYmxlVHlwZS5VTklGT1JNO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmFsdWVWYXIuX3R5cGVzZWwgPSBmYWxzZTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZVZhci5fdHlwZXNlbCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdEBwcm9wZXJ0eShWYXJpYWJsZUNvbmZpZylcclxuXHRfdmFsdWVWYXI6VmFyaWFibGVDb25maWcgPSBuZXcgVmFyaWFibGVDb25maWcoXHJcblx0XHRWQy52YXJTZWxlY3QsIFZDLnZhckRlZnR5cGUsIFxyXG5cdFx0VkMudmFyQXR0ck1hY3JvLCBWQy52YXJBdHRyTWFjcm9DaGVja09ubHksIFZDLnZhckF0dHJOYW1lLCBcclxuXHRcdFZDLnZhclVuaWZNYWNybywgVkMudmFyVW5pZk1hY3JvQ2hlY2tPbmx5LCBWQy52YXJVbmlmTmFtZSk7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBWYXJpYWJsZUNvbmZpZyxcclxuXHRcdHRvb2x0aXAgOiBcIuWAvOWPmOmHj1wiXHJcblx0fSlcclxuXHRnZXQgdmFsdWVWYXIoKXtcclxuXHRcdHJldHVybiB0aGlzLl92YWx1ZVZhcjtcclxuXHR9XHJcblx0c2V0IHZhbHVlVmFyKHZhbHVlOlZhcmlhYmxlQ29uZmlnKXtcclxuXHRcdHRoaXMuX3ZhbHVlVmFyID0gdmFsdWU7XHJcblx0XHR0aGlzLm9uVXBkYXRlVmFsdWVzKCk7XHJcblx0XHR0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuXHRcdHRoaXMubm9kZS5lbWl0KFwic2hhZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIik7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIEBwcm9wZXJ0eShbVkMudmFsVHlwZV0pXHJcblx0X3ZhbHVlczp0eXBlb2YgVkMudmFsRGVmYXVsdFtdID0gWyBWQy52YWxOZXcoKSBdO1xyXG5cdFxyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR2aXNpYmxlICgpe1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fdmFsdWVUeXBlID09IFZhbHVlVHlwZS5TSU5HTEUgJiYgdGhpcy5fdmFsdWVWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuVU5JRk9STTtcclxuXHRcdH0sXHJcblx0XHR0eXBlIDogVkMudmFsVHlwZSxcclxuXHRcdHRvb2x0aXAgOiBcIuWAvFwiXHJcblx0fSlcclxuXHRnZXQgdmFsdWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl92YWx1ZXNbMF07XHJcblx0fVxyXG5cdHNldCB2YWx1ZSh2YWx1ZV86dHlwZW9mIFZDLnZhbERlZmF1bHQpe1xyXG5cdFx0dGhpcy5fdmFsdWVzWzBdID0gdmFsdWVfO1xyXG5cdFx0dGhpcy5vblVwZGF0ZVZhbHVlcygpO1xyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHZpc2libGUgKCl7XHJcblx0XHRcdHJldHVybiB0aGlzLl92YWx1ZVR5cGUgPT0gVmFsdWVUeXBlLkFSUkFZIHx8IHRoaXMuX3ZhbHVlVmFyLnR5cGUgPT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURTtcclxuXHRcdH0sXHJcblx0XHR0eXBlIDogW1ZDLnZhbFR5cGVdLFxyXG5cdFx0dG9vbHRpcCA6IFwi5YC85pWw57uEL+WAvOmhtueCueaVsOe7hFwiXHJcblx0fSlcclxuXHRnZXQgdmFsdWVzKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fdmFsdWVzO1xyXG5cdH1cclxuXHRzZXQgdmFsdWVzKHZhbHVlOnR5cGVvZiBWQy52YWxEZWZhdWx0W10pe1xyXG5cdFx0dGhpcy5fdmFsdWVzID0gdmFsdWU7XHJcblx0XHR0aGlzLm9uVXBkYXRlVmFsdWVzKCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDlgLzlgY/np7tcclxuXHQgKi9cclxuICAgIHByaXZhdGUgX3ZhbHVlT2Zmc2V0Om51bWJlciA9IDA7XHJcblxyXG4gICAgb25Mb2FkKCl7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKFwicmVuZGVyX3NoYXBlX2NoZWNrZWRcIiwgdGhpcy5vblJlbmRlclNoYXBlQ2hlY2tlZCwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKFwicmVuZGVyX3VwZGF0ZV9tYXRlcmlhbFwiLCB0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwsIHRoaXMpO1xyXG5cdFx0dGhpcy5ub2RlLm9uKFwicmVuZGVyX3VwZGF0ZV9hdHRyaWJ1dGVcIiwgdGhpcy5vblJlbmRlclVwZGF0ZUF0dHJpYnV0ZSwgdGhpcyk7XHJcblx0XHR0aGlzLm5vZGUub24oXCJyZW5kZXJfdXBkYXRlX3ZlcnRleFwiLCB0aGlzLm9uUmVuZGVyVXBkYXRlUmVuZGVyRGF0YSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EZXN0cm95KCl7XHJcblx0XHR0aGlzLm5vZGUudGFyZ2V0T2ZmKHRoaXMpO1xyXG4gICAgfVxyXG4gICAgXHJcblx0b25FbmFibGUoKXtcclxuICAgICAgICB0aGlzLm9uU3RhdGVVcGRhdGUodHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRvbkRpc2FibGUoKXtcclxuICAgICAgICB0aGlzLm9uU3RhdGVVcGRhdGUoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25TdGF0ZVVwZGF0ZShlbmFibGU6Ym9vbGVhbil7XHJcblx0XHR0aGlzLm5vZGUuZW1pdChcInNoYWRlcl91cGRhdGVfdGFnXCIpO1xyXG5cdFx0dGhpcy5vblVwZGF0ZVZhbHVlcygpO1xyXG5cdH1cclxuXHRcclxuICAgIC8qKlxyXG4gICAgICog5qOA5p+l5b2i54q2XHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgb25SZW5kZXJTaGFwZUNoZWNrZWQoKXtcclxuICAgICAgICBpZih0aGlzLl92YWx1ZVZhci50eXBlID09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEUpe1xyXG4gICAgICAgICAgICB0aGlzLm9uVXBkYXRlVmFsdWVzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIOajgOafpeWAvFxyXG5cdCAqL1xyXG5cdHByaXZhdGUgb25VcGRhdGVWYWx1ZXMoKXtcclxuXHRcdGlmKHRoaXMuZW5hYmxlZCl7XHJcbiAgICAgICAgICAgIGlmKHRoaXMuX3ZhbHVlcy5sZW5ndGggPD0gMCl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZXMgPSBbIFZDLnZhbE5ldygpIF07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYodGhpcy5fdmFsdWVWYXIudHlwZSA9PSBWYXJpYWJsZVR5cGUuQVRUUklCVVRFKXtcclxuICAgICAgICAgICAgICAgIGxldCByc3lzID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKTtcclxuICAgICAgICAgICAgICAgIGlmKHJzeXMpe1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCB2YyA9IHJzeXMuX3ZlcnRpY2VzQ291bnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5fdmFsdWVzLmxlbmd0aCA8IHZjKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHZhbHVlcyA9IHRoaXMuX3ZhbHVlcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHZjOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNvbG9yID0gdmFsdWVzW2ldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoY29sb3IgPT0gbnVsbCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVzW2ldID0gVkMudmFsTmV3KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdmFsdWVzLmxlbmd0aCA9IHZjO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9ZWxzZSBpZih0aGlzLl92YWx1ZVR5cGUgPT0gVmFsdWVUeXBlLlNJTkdMRSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZXMubGVuZ3RoID0gMTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZih0aGlzLl92YWx1ZVZhci50eXBlID09IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEUpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLmVtaXQoXCJzaGFkZXJfdXBkYXRlX2F0dHJpYnV0ZVwiKTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWwoKTtcclxuICAgICAgICAgICAgfVxyXG5cdFx0fVxyXG5cdH1cclxuICAgIFxyXG4gICAgcHJpdmF0ZSBvblJlbmRlclVwZGF0ZU1hdGVyaWFsIChjb21wOlJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSkpIHtcclxuXHRcdGlmKGNvbXApe1xyXG5cdFx0XHQvLyBtYWtlIHN1cmUgbWF0ZXJpYWwgaXMgYmVsb25nIHRvIHNlbGYuXHJcblx0XHRcdC8vIEB0cy1pZ25vcmVcclxuXHRcdFx0bGV0IG1hdGVyaWFsID0gY29tcC5fbWF0ZXJpYWxzWzBdO1xyXG5cdFx0XHRpZiAobWF0ZXJpYWwpIHtcclxuXHRcdFx0XHRpZiAodGhpcy5jaGVja01hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3ZhbHVlVmFyLnVuaWZNYWNybykpIHtcclxuXHRcdFx0XHRcdGlmKHRoaXMuZW5hYmxlZCAmJiB0aGlzLl92YWx1ZVZhci50eXBlID09PSBWYXJpYWJsZVR5cGUuVU5JRk9STSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fdmFsdWVWYXIudW5pZk1hY3JvLCB0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5fdmFsdWVUeXBlID09IFZhbHVlVHlwZS5TSU5HTEUpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF0ZXJpYWwuc2V0UHJvcGVydHkodGhpcy5fdmFsdWVWYXIudW5pZk5hbWUsIHRoaXMuX3ZhbHVlc1swXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHZhbHVlcyA9IHRoaXMuX3ZhbHVlcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBfdmFsdWVzOm51bWJlcltdID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgZGVmdiA9IFZDLnZhbERlZmF1bHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IobGV0IGkgPSAwLCBsID0gdmFsdWVzLmxlbmd0aDsgaSA8IGw7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVkMudG9BcnJheShfdmFsdWVzLCB2YWx1ZXNbaV0gfHwgZGVmdiwgX3ZhbHVlcy5sZW5ndGgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF0ZXJpYWwuc2V0UHJvcGVydHkodGhpcy5fdmFsdWVWYXIudW5pZk5hbWUsIG5ldyBGbG9hdDMyQXJyYXkodmFsdWVzLmxlbmd0aCAqIFZDLnR5cGVTaXplKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXRlcmlhbC5zZXRQcm9wZXJ0eSh0aGlzLl92YWx1ZVZhci51bmlmTmFtZSwgX3ZhbHVlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHRcdFx0XHRcdFx0Ly9FZGl0b3IubG9nKFwib25SZW5kZXJVcGRhdGVNYXRlcmlhbFwiLCB0aGlzLl92YWx1ZSk7XHJcblx0XHRcdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl92YWx1ZVZhci51bmlmTWFjcm8sIGZhbHNlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlTWF0ZXJpYWxcIiwgdGhpcy5lbmFibGVkKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuICAgIH1cclxuXHRcclxuXHQvKipcclxuXHQgKiDojrflvpflsZ7mgKflgLxcclxuXHQgKiBAcGFyYW0gY29tcCBcclxuXHQgKi9cclxuXHRwcml2YXRlIGdldEF0dHJpYnV0ZVZhbHVlcyhjb21wOlJlbmRlclN5c3RlbSl7XHJcblx0XHRsZXQgdmFsdWVzOm51bWJlcltdID0gW107XHJcblx0XHRsZXQgdmMgPSBjb21wLl92ZXJ0aWNlc0NvdW50O1xyXG4gICAgICAgIGxldCBkZWZ2ID0gVkMudmFsRGVmYXVsdDtcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdmM7IGkrKyl7XHJcbiAgICAgICAgICAgIFZDLnRvQXJyYXkodmFsdWVzLCB0aGlzLl92YWx1ZXNbaV0gfHwgZGVmdiwgdmFsdWVzLmxlbmd0aCk7XHJcbiAgICAgICAgfVxyXG5cdFx0cmV0dXJuIHZhbHVlcztcclxuXHR9XHJcblxyXG5cdHByaXZhdGUgb25SZW5kZXJVcGRhdGVBdHRyaWJ1dGUoY29tcDpSZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pKXtcclxuXHRcdGlmKGNvbXApe1xyXG5cdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdGxldCBtYXRlcmlhbCA9IGNvbXAuX21hdGVyaWFsc1swXTtcclxuXHRcdFx0aWYgKG1hdGVyaWFsKSB7XHJcblx0XHRcdFx0aWYgKHRoaXMuY2hlY2tNYXRlcmlhbE1hY3JvKG1hdGVyaWFsLCB0aGlzLl92YWx1ZVZhci5hdHRyTWFjcm8pKSB7XHJcblx0XHRcdFx0XHRpZih0aGlzLmVuYWJsZWQgJiYgdGhpcy5fdmFsdWVWYXIudHlwZSA9PT0gVmFyaWFibGVUeXBlLkFUVFJJQlVURSl7XHJcblx0XHRcdFx0XHRcdHRoaXMuZGVmaW5lTWF0ZXJpYWxNYWNybyhtYXRlcmlhbCwgdGhpcy5fdmFsdWVWYXIuYXR0ck1hY3JvLCB0cnVlKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5fdmFsdWVPZmZzZXQgPSBjb21wLmFkZFZlcnRleEF0dHJpYnV0ZSh7IFxyXG5cdFx0XHRcdFx0XHRcdG5hbWU6IHRoaXMuX3ZhbHVlVmFyLmF0dHJOYW1lLCBcclxuXHRcdFx0XHRcdFx0XHR0eXBlOiBnZnguQVRUUl9UWVBFX0ZMT0FUMzIsIFxyXG5cdFx0XHRcdFx0XHRcdG51bTogVkMudHlwZVNpemUsIFxyXG5cdFx0XHRcdFx0XHR9LCBWQy50eXBlU2l6ZSk7XHJcblx0XHRcdFx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlQXR0cmlidXRlXCIsIHRoaXMuX3ZhbHVlT2Zmc2V0KTtcclxuXHRcdFx0XHRcdH1lbHNle1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZU1hdGVyaWFsTWFjcm8obWF0ZXJpYWwsIHRoaXMuX3ZhbHVlVmFyLmF0dHJNYWNybywgZmFsc2UpO1xyXG5cdFx0XHRcdFx0XHR0aGlzLl92YWx1ZU9mZnNldCA9IDA7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHQvL0VkaXRvci5sb2coXCJvblJlbmRlclVwZGF0ZUF0dHJpYnV0ZVwiLCB0aGlzLmVuYWJsZWQpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cHJpdmF0ZSBvblJlbmRlclVwZGF0ZVJlbmRlckRhdGEoY29tcDpSZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pKXtcclxuICAgICAgICBpZihjb21wICYmIHRoaXMuZW5hYmxlZCl7XHJcblx0XHRcdC8vRWRpdG9yLmxvZyhcIm9uUmVuZGVyVXBkYXRlUmVuZGVyRGF0YVwiKTtcclxuXHRcdFx0aWYodGhpcy5fdmFsdWVPZmZzZXQgPiAwKXtcclxuXHRcdFx0XHQvLyBAdHMtaWdub3JlXHJcblx0XHRcdFx0bGV0IGFzc2VtYmxlciA9IGNvbXAuX2Fzc2VtYmxlcjtcclxuXHRcdFx0XHRsZXQgdmFsdWVzID0gdGhpcy5nZXRBdHRyaWJ1dGVWYWx1ZXMoY29tcCk7XHJcblx0XHRcdFx0bGV0IHZsZW4gPSB2YWx1ZXMubGVuZ3RoO1xyXG5cdFx0XHRcdGxldCB2YWx1ZU9mZnNldCA9IHRoaXMuX3ZhbHVlT2Zmc2V0O1xyXG5cdFx0XHRcdGxldCBmbG9hdHNQZXJWZXJ0ID0gYXNzZW1ibGVyLmZsb2F0c1BlclZlcnQ7XHJcblx0XHRcdFx0bGV0IHZlcnRzID0gYXNzZW1ibGVyLnJlbmRlckRhdGEudkRhdGFzWzBdO1xyXG5cdFx0XHRcdGZvciAobGV0IGkgPSAwOyBpIDwgNDsgaSsrKSB7XHJcblx0XHRcdFx0XHRsZXQgZHN0T2Zmc2V0ID0gZmxvYXRzUGVyVmVydCAqIGkgKyB2YWx1ZU9mZnNldDtcclxuXHRcdFx0XHRcdGxldCBzcmNPZmZzZXQgPSBWQy50eXBlU2l6ZSAqIGk7XHJcblx0XHRcdFx0XHRpZihzcmNPZmZzZXQgPj0gdmxlbil7XHJcblx0XHRcdFx0XHRcdHNyY09mZnNldCA9IDA7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRmb3IobGV0IGogPSAwOyBqIDwgVkMudHlwZVNpemU7IGorKyl7XHJcblx0XHRcdFx0XHRcdHZlcnRzW2RzdE9mZnNldCArIGpdID0gdmFsdWVzW3NyY09mZnNldCArIGpdO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG4gICAgICAgIH1cclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi4vc2hhZGVyL1NoYWRlckNvbXBvbmVudC50c1wiIC8+XHJcblxyXG5tb2R1bGUgbGNjJHJlbmRlciB7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHksIG1lbnV9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbi8qKlxyXG4gKiDmurbop6PmlYjmnpxcclxuICovXHJcbkBjY2NsYXNzKFwibGNjJHJlbmRlci5FZmZlY3REaXNzb2x2ZVwiKVxyXG5AbWVudShcImkxOG46bGNjLXJlbmRlci5tZW51X2NvbXBvbmVudC9FZmZlY3REaXNzb2x2ZVwiKVxyXG5leHBvcnQgY2xhc3MgRWZmZWN0RGlzc29sdmUgZXh0ZW5kcyBTaGFkZXJDb21wb25lbnQge1xyXG5cclxuXHRAcHJvcGVydHkoY2MuTWF0ZXJpYWwpXHJcblx0X21hdGVyaWFsOiBjYy5NYXRlcmlhbCA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5NYXRlcmlhbCxcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOadkOi0qFwiXHJcblx0fSlcclxuXHRnZXQgbWF0ZXJpYWwoKXtcclxuXHRcdHJldHVybiB0aGlzLl9tYXRlcmlhbDtcclxuXHR9XHJcblx0c2V0IG1hdGVyaWFsKHZhbHVlOmNjLk1hdGVyaWFsKXtcclxuXHRcdGlmKHRoaXMuX21hdGVyaWFsICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fbWF0ZXJpYWwgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0uc2V0TWF0ZXJpYWwoMCwgdmFsdWUpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG5cdF9zcHJpdGVGcmFtZTogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuU3ByaXRlRnJhbWUsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpznsr7ngbXluKdcIlxyXG5cdH0pXHJcblx0Z2V0IHNwcml0ZUZyYW1lKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fc3ByaXRlRnJhbWU7XHJcblx0fVxyXG5cdHNldCBzcHJpdGVGcmFtZSh2YWx1ZTpjYy5TcHJpdGVGcmFtZSl7XHJcblx0XHRpZih0aGlzLl9zcHJpdGVGcmFtZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3Nwcml0ZUZyYW1lID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUuc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdFx0aWYoIXRoaXMuX25vaXNlRnJhbWUpe1xyXG5cdFx0XHRcdHRoaXMubm9pc2VGcmFtZSA9IHZhbHVlO1xyXG5cdFx0XHR9ZWxzZXtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2hlY2tOb2lzZUZyYW1lKCk7XHJcbiAgICAgICAgICAgIH1cclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0QHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG5cdF9ub2lzZUZyYW1lOiBjYy5TcHJpdGVGcmFtZSA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5TcHJpdGVGcmFtZSxcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOWZquWjsOW4p1wiXHJcblx0fSlcclxuXHRnZXQgbm9pc2VGcmFtZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX25vaXNlRnJhbWU7XHJcblx0fVxyXG5cdHNldCBub2lzZUZyYW1lKHZhbHVlOmNjLlNwcml0ZUZyYW1lKXtcclxuXHRcdGlmKHRoaXMuX25vaXNlRnJhbWUgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9ub2lzZUZyYW1lID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyTm9pc2VGcmFtZS5zcHJpdGVGcmFtZSA9IHZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLmNoZWNrTm9pc2VGcmFtZSgpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHRAcHJvcGVydHkoKVxyXG5cdF9kZWdyZWU6IG51bWJlciA9IDAuNztcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi56iL5bqmXCIsXHJcblx0XHRyYW5nZSA6IFswLCAxLCAwLjAxXSxcclxuXHR9KVxyXG5cdGdldCBkZWdyZWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl9kZWdyZWU7XHJcblx0fVxyXG5cdHNldCBkZWdyZWUodmFsdWU6bnVtYmVyKXtcclxuXHRcdGlmKHRoaXMuX2RlZ3JlZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX2RlZ3JlZSA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnNoYWRlckRlZ3JlZS52YWx1ZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cmVuZGVyU3lzdGVtOlJlbmRlclN5c3RlbSA9IG51bGw7XHJcblx0c2hhZGVyU3ByaXRlRnJhbWU6U2hhZGVyU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdHNoYWRlck5vaXNlRnJhbWU6U2hhZGVyU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdHNoYWRlckRlZ3JlZTpTaGFkZXJGbG9hdCA9IG51bGw7XHJcbiAgICBcclxuXHQvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcblx0b25FbmFibGUgKCkge1xyXG5cdFx0aWYoIXRoaXMuc2hhZGVyU3ByaXRlRnJhbWUpe1xyXG5cdFx0XHR0aGlzLnNoYWRlclNwcml0ZUZyYW1lID0gdGhpcy5nZXRTaGFkZXJDb21wb25lbnQoU2hhZGVyU3ByaXRlRnJhbWUsIFwic3ByaXRlZnJhbWVcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlclNwcml0ZUZyYW1lKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyU3ByaXRlRnJhbWUpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcInNwcml0ZWZyYW1lXCI7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVNoYXBlID0gdHJ1ZTtcclxuXHRcdFx0XHRzaGFkZXIudXNlVVYgPSB0cnVlO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUgPSBzaGFkZXI7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGlmKCF0aGlzLnNoYWRlck5vaXNlRnJhbWUpe1xyXG5cdFx0XHR0aGlzLnNoYWRlck5vaXNlRnJhbWUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSwgXCJub2lzZWZyYW1lXCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJOb2lzZUZyYW1lKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyU3ByaXRlRnJhbWUpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcIm5vaXNlZnJhbWVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3RleHR1cmVWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVTRV9OT0lTRVwiO1xyXG4gICAgICAgICAgICAgICAgc2hhZGVyLl90ZXh0dXJlVmFyLnVuaWZOYW1lID0gXCJub2lzZVwiO1xyXG4gICAgICAgICAgICAgICAgc2hhZGVyLl91dlZhci5hdHRyTWFjcm8uY2hlY2tPbmx5ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBzaGFkZXIuX3V2VmFyLmF0dHJNYWNyby5uYW1lID0gXCJOT0lTRU9USEVSXCI7XHJcbiAgICAgICAgICAgICAgICBzaGFkZXIuX3V2VmFyLmF0dHJOYW1lID0gXCJhX3V2MVwiO1xyXG4gICAgICAgICAgICAgICAgXHJcblx0XHRcdFx0dGhpcy5zaGFkZXJOb2lzZUZyYW1lID0gc2hhZGVyO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5zaGFkZXJEZWdyZWUpe1xyXG5cdFx0XHR0aGlzLnNoYWRlckRlZ3JlZSA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImRlZ3JlZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyRGVncmVlKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcImRlZ3JlZVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfREVHUkVFXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTmFtZSA9IFwiYV9kZWdyZWVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0RFR1JFRVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfZGVncmVlXCI7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJEZWdyZWUgPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLnZhbHVlID0gdGhpcy5fZGVncmVlO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcblx0XHRcdGlmKCF0aGlzLnJlbmRlclN5c3RlbSl7XHJcblx0XHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmFkZENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHR9XHJcbiAgICAgICAgfVxyXG5cdFx0dGhpcy5jaGVja01hdGVyaWFsKCk7XHJcblx0fVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog5qOA5p+l5Zmq5aOw5binXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgY2hlY2tOb2lzZUZyYW1lKCl7XHJcbiAgICAgICAgaWYodGhpcy5fbm9pc2VGcmFtZSAmJiB0aGlzLl9zcHJpdGVGcmFtZSl7XHJcbiAgICAgICAgICAgIC8vIEB0cy1pZ25vcmVcclxuICAgICAgICAgICAgaWYoKHRoaXMuX25vaXNlRnJhbWUgPT0gdGhpcy5fc3ByaXRlRnJhbWUpIHx8ICh0aGlzLl9ub2lzZUZyYW1lLl91dWlkID09IHRoaXMuX3Nwcml0ZUZyYW1lLl91dWlkKSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNoYWRlck5vaXNlRnJhbWUudXNlVVYgPSBmYWxzZTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNoYWRlck5vaXNlRnJhbWUudXNlVVYgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMucmVuZGVyU3lzdGVtLnNldERpcnR5KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcblx0LyoqXHJcblx0ICog5qOA5p+l5p2Q6LSoXHJcblx0ICovXHJcblx0YXN5bmMgY2hlY2tNYXRlcmlhbCgpe1xyXG5cdFx0aWYoQ0NfRURJVE9SKXtcclxuXHRcdFx0aWYoIXRoaXMuX21hdGVyaWFsKXtcclxuXHRcdFx0XHR0aGlzLm1hdGVyaWFsID0gYXdhaXQgVXRpbHMuZ2V0QXNzZXRCeVVVSUQ8Y2MuTWF0ZXJpYWw+KFVVSUQubWF0ZXJpYWxzW1wibGNjLTJkX2Rpc3NvbHZlXCJdKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxufVxyXG5cdCIsIi8vLyA8cmVmZXJlbmNlIHBhdGggPSBcIi4uL3NoYWRlci9TaGFkZXJDb21wb25lbnQudHNcIiAvPlxyXG5cclxubW9kdWxlIGxjYyRyZW5kZXIge1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5LCBtZW51fSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzcyhcImxjYyRyZW5kZXIuRWZmZWN0Rmxhc2hMaWdodFwiKVxyXG5AbWVudShcImkxOG46bGNjLXJlbmRlci5tZW51X2NvbXBvbmVudC9FZmZlY3RGbGFzaExpZ2h0XCIpXHJcbmV4cG9ydCBjbGFzcyBFZmZlY3RGbGFzaExpZ2h0IGV4dGVuZHMgU2hhZGVyQ29tcG9uZW50IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTWF0ZXJpYWwpXHJcblx0X21hdGVyaWFsOiBjYy5NYXRlcmlhbCA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5NYXRlcmlhbCxcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOadkOi0qFwiXHJcblx0fSlcclxuXHRnZXQgbWF0ZXJpYWwoKXtcclxuXHRcdHJldHVybiB0aGlzLl9tYXRlcmlhbDtcclxuXHR9XHJcblx0c2V0IG1hdGVyaWFsKHZhbHVlOmNjLk1hdGVyaWFsKXtcclxuXHRcdGlmKHRoaXMuX21hdGVyaWFsICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fbWF0ZXJpYWwgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0uc2V0TWF0ZXJpYWwoMCwgdmFsdWUpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcbiAgICBAcHJvcGVydHkoY2MuU3ByaXRlRnJhbWUpXHJcblx0X3Nwcml0ZUZyYW1lOiBjYy5TcHJpdGVGcmFtZSA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5TcHJpdGVGcmFtZSxcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOeyvueBteW4p1wiXHJcblx0fSlcclxuXHRnZXQgc3ByaXRlRnJhbWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl9zcHJpdGVGcmFtZTtcclxuXHR9XHJcblx0c2V0IHNwcml0ZUZyYW1lKHZhbHVlOmNjLlNwcml0ZUZyYW1lKXtcclxuXHRcdGlmKHRoaXMuX3Nwcml0ZUZyYW1lICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZS5zcHJpdGVGcmFtZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcbiAgICBAcHJvcGVydHkoY2MuQ29sb3IpXHJcblx0X2xpZ2h0Q29sb3I6IGNjLkNvbG9yID0gY2MuQ29sb3IuV0hJVEU7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5Db2xvcixcclxuXHRcdHRvb2x0aXAgOiBcIuWFiee6v+minOiJslwiXHJcblx0fSlcclxuXHRnZXQgbGlnaHRDb2xvcigpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2xpZ2h0Q29sb3I7XHJcblx0fVxyXG5cdHNldCBsaWdodENvbG9yKHZhbHVlOmNjLkNvbG9yKXtcclxuXHRcdHRoaXMuX2xpZ2h0Q29sb3IgPSB2YWx1ZTtcclxuXHRcdHRoaXMuc2hhZGVyTGlnaHRDb2xvci5jb2xvciA9IHZhbHVlO1xyXG5cdH1cclxuXHJcbiAgICBAcHJvcGVydHkoKVxyXG5cdF9saWdodEFuZ2xlOiBudW1iZXIgPSAxMzU7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIuWFieadn+WAvuaWnOinkuW6plwiLFxyXG5cdFx0cmFuZ2UgOiBbMCwgMzYwLCAxXSxcclxuXHR9KVxyXG5cdGdldCBsaWdodEFuZ2xlKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fbGlnaHRBbmdsZTtcclxuXHR9XHJcblx0c2V0IGxpZ2h0QW5nbGUodmFsdWU6bnVtYmVyKXtcclxuXHRcdGlmKHRoaXMuX2xpZ2h0QW5nbGUgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9saWdodEFuZ2xlID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyTGlnaHRBbmdsZS52YWx1ZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcbiAgICBAcHJvcGVydHkoKVxyXG5cdF9saWdodFdpZHRoOiBudW1iZXIgPSAwLjI7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIuWFieadn+WuveW6plwiXHJcblx0fSlcclxuXHRnZXQgbGlnaHRXaWR0aCgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2xpZ2h0V2lkdGg7XHJcblx0fVxyXG5cdHNldCBsaWdodFdpZHRoKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl9saWdodFdpZHRoICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fbGlnaHRXaWR0aCA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnNoYWRlckxpZ2h0V2lkdGgudmFsdWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KClcclxuXHRfZW5hYmxlR3JhZGllbnQ6IGJvb2xlYW4gPSB0cnVlO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0b29sdGlwIDogXCLlkK/nlKjlhYnmnZ/muJDlj5hcIlxyXG5cdH0pXHJcblx0Z2V0IGVuYWJsZUdyYWRpZW50KCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fZW5hYmxlR3JhZGllbnQ7XHJcblx0fVxyXG5cdHNldCBlbmFibGVHcmFkaWVudCh2YWx1ZTpib29sZWFuKXtcclxuXHRcdGlmKHRoaXMuX2VuYWJsZUdyYWRpZW50ICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fZW5hYmxlR3JhZGllbnQgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJFbmFibGVHcmFkaWVudC52YWx1ZSA9IHZhbHVlID8gMSA6IDA7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eSgpXHJcblx0X2Nyb3BBbHBoYTogYm9vbGVhbiA9IHRydWU7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIuijgeWJquaOiemAj+aYjuWMuuWfn+S4iueahOWFiVwiXHJcblx0fSlcclxuXHRnZXQgY3JvcEFscGhhKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fY3JvcEFscGhhO1xyXG5cdH1cclxuXHRzZXQgY3JvcEFscGhhKHZhbHVlOmJvb2xlYW4pe1xyXG5cdFx0aWYodGhpcy5fY3JvcEFscGhhICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fY3JvcEFscGhhID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyQ3JvcEFscGhhLnZhbHVlID0gdmFsdWUgPyAxIDogMDtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KClcclxuXHRfbW92ZVNwZWVkOiBudW1iZXIgPSAtMTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi56e75Yqo6YCf5bqmXCJcclxuXHR9KVxyXG5cdGdldCBtb3ZlU3BlZWQoKXtcclxuXHRcdHJldHVybiB0aGlzLl9tb3ZlU3BlZWQ7XHJcblx0fVxyXG5cdHNldCBtb3ZlU3BlZWQodmFsdWU6bnVtYmVyKXtcclxuXHRcdGlmKHRoaXMuX21vdmVTcGVlZCAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX21vdmVTcGVlZCA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnNoYWRlck1vdmVTcGVlZC52YWx1ZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcbiAgICBAcHJvcGVydHkoKVxyXG5cdF9tb3ZlV2lkdGg6IG51bWJlciA9IDI7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIuenu+WKqOWuveW6plwiXHJcblx0fSlcclxuXHRnZXQgbW92ZVdpZHRoKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fbW92ZVdpZHRoO1xyXG5cdH1cclxuXHRzZXQgbW92ZVdpZHRoKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl9tb3ZlV2lkdGggIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9tb3ZlV2lkdGggPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJNb3ZlV2lkdGgudmFsdWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KClcclxuXHRfbGlnaHRMaW5lczogbnVtYmVyID0gMTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5YWJ5p2f5pWw6YePXCIsXHJcblx0XHRyYW5nZTpbMSwgMjAsIDFdXHJcblx0fSlcclxuXHRnZXQgbGlnaHRMaW5lcygpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2xpZ2h0TGluZXM7XHJcblx0fVxyXG5cdHNldCBsaWdodExpbmVzKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl9saWdodExpbmVzICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fbGlnaHRMaW5lcyA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnNoYWRlckxpZ2h0TGluZXMudmFsdWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KClcclxuXHRfbGlnaHRTcGFjZTogbnVtYmVyID0gMTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5YWJ5p2f6Ze06LedXCJcclxuXHR9KVxyXG5cdGdldCBsaWdodFNwYWNlKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fbGlnaHRTcGFjZTtcclxuXHR9XHJcblx0c2V0IGxpZ2h0U3BhY2UodmFsdWU6bnVtYmVyKXtcclxuXHRcdGlmKHRoaXMuX2xpZ2h0U3BhY2UgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9saWdodFNwYWNlID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyTGlnaHRTcGFjZS52YWx1ZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cmVuZGVyU3lzdGVtOlJlbmRlclN5c3RlbSA9IG51bGw7XHJcblx0c2hhZGVyU3ByaXRlRnJhbWU6U2hhZGVyU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdHNoYWRlckxpZ2h0Q29sb3I6U2hhZGVyQ29sb3IgPSBudWxsO1xyXG5cdHNoYWRlckxpZ2h0QW5nbGU6U2hhZGVyRmxvYXQgPSBudWxsO1xyXG5cdHNoYWRlckxpZ2h0V2lkdGg6U2hhZGVyRmxvYXQgPSBudWxsO1xyXG5cdHNoYWRlckVuYWJsZUdyYWRpZW50OlNoYWRlckZsb2F0ID0gbnVsbDtcclxuXHRzaGFkZXJDcm9wQWxwaGE6U2hhZGVyRmxvYXQgPSBudWxsO1xyXG5cdHNoYWRlck1vdmVTcGVlZDpTaGFkZXJGbG9hdCA9IG51bGw7XHJcblx0c2hhZGVyTW92ZVdpZHRoOlNoYWRlckZsb2F0ID0gbnVsbDtcclxuXHRzaGFkZXJMaWdodExpbmVzOlNoYWRlckZsb2F0ID0gbnVsbDtcclxuXHRzaGFkZXJMaWdodFNwYWNlOlNoYWRlckZsb2F0ID0gbnVsbDtcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBvbkVuYWJsZSAoKSB7XHJcblx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSwgXCJzcHJpdGVmcmFtZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyU3ByaXRlRnJhbWUpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwic3ByaXRlZnJhbWVcIjtcclxuXHRcdFx0XHRzaGFkZXIudXNlU2hhcGUgPSB0cnVlO1xyXG5cdFx0XHRcdHNoYWRlci51c2VVViA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWUmVjdCA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLl91dlJlY3RWYXIudHlwZSA9IFZhcmlhYmxlVHlwZS5BVFRSSUJVVEU7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHNoYWRlcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyTGlnaHRDb2xvcil7XHJcblx0XHRcdHRoaXMuc2hhZGVyTGlnaHRDb2xvciA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckNvbG9yLCBcImxpZ2h0Y29sb3JcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckxpZ2h0Q29sb3Ipe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJDb2xvcik7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwibGlnaHRjb2xvclwiO1xyXG5cdFx0XHRcdHNoYWRlci51c2VOb2RlQ29sb3IgPSBmYWxzZTtcclxuXHRcdFx0XHRzaGFkZXIuX2NvbG9yVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX0xJR0hUQ09MT1JcIjtcclxuXHRcdFx0XHRzaGFkZXIuX2NvbG9yVmFyLmF0dHJOYW1lID0gXCJhX2xpZ2h0Q29sb3JcIjtcclxuXHRcdFx0XHRzaGFkZXIuX2NvbG9yVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0xJR0hUQ09MT1JcIjtcclxuXHRcdFx0XHRzaGFkZXIuX2NvbG9yVmFyLnVuaWZOYW1lID0gXCJ1X2xpZ2h0Q29sb3JcIjtcclxuXHRcdFx0XHR0aGlzLnNoYWRlckxpZ2h0Q29sb3IgPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLmNvbG9yID0gdGhpcy5fbGlnaHRDb2xvcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyTGlnaHRBbmdsZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyTGlnaHRBbmdsZSA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImxpZ2h0YW5nbGVcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckxpZ2h0QW5nbGUpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJGbG9hdCk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwibGlnaHRhbmdsZVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfTElHSFRBTkdMRVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfbGlnaHRBbmdsZVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVOSUZfTElHSFRBTkdMRVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfbGlnaHRBbmdsZVwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyTGlnaHRBbmdsZSA9IHNoYWRlcjtcclxuXHRcdFx0XHRzaGFkZXIudmFsdWUgPSB0aGlzLl9saWdodEFuZ2xlO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5zaGFkZXJMaWdodFdpZHRoKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJMaWdodFdpZHRoID0gdGhpcy5nZXRTaGFkZXJDb21wb25lbnQoU2hhZGVyRmxvYXQsIFwibGlnaHR3aWR0aFwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyTGlnaHRXaWR0aCl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckZsb2F0KTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJsaWdodHdpZHRoXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTWFjcm8ubmFtZSA9IFwiQVRUUl9MSUdIVFdJRFRIXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTmFtZSA9IFwiYV9saWdodFdpZHRoXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTWFjcm8ubmFtZSA9IFwiVU5JRl9MSUdIVFdJRFRIXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTmFtZSA9IFwidV9saWdodFdpZHRoXCI7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJMaWdodFdpZHRoID0gc2hhZGVyO1xyXG5cdFx0XHRcdHNoYWRlci52YWx1ZSA9IHRoaXMuX2xpZ2h0V2lkdGg7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGlmKCF0aGlzLnNoYWRlckVuYWJsZUdyYWRpZW50KXtcclxuXHRcdFx0dGhpcy5zaGFkZXJFbmFibGVHcmFkaWVudCA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImVuYWJsZWdyYWRpZW50XCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJFbmFibGVHcmFkaWVudCl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckZsb2F0KTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJlbmFibGVncmFkaWVudFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfRU5BQkxFR1JBRElFTlRcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJOYW1lID0gXCJhX2VuYWJsZUdyYWRpZW50XCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTWFjcm8ubmFtZSA9IFwiVU5JRl9FTkFCTEVHUkFESUVOVFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfZW5hYmxlR3JhZGllbnRcIjtcclxuXHRcdFx0XHR0aGlzLnNoYWRlckVuYWJsZUdyYWRpZW50ID0gc2hhZGVyO1xyXG5cdFx0XHRcdHNoYWRlci52YWx1ZSA9IHRoaXMuX2VuYWJsZUdyYWRpZW50ID8gMSA6IDA7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGlmKCF0aGlzLnNoYWRlckNyb3BBbHBoYSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyQ3JvcEFscGhhID0gdGhpcy5nZXRTaGFkZXJDb21wb25lbnQoU2hhZGVyRmxvYXQsIFwiY3JvcGFscGhhXCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJDcm9wQWxwaGEpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJGbG9hdCk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwiY3JvcGFscGhhXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTWFjcm8ubmFtZSA9IFwiQVRUUl9DUk9QQUxQSEFcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJOYW1lID0gXCJhX2Nyb3BBbHBoYVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVOSUZfQ1JPUEFMUEhBXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTmFtZSA9IFwidV9jcm9wQWxwaGFcIjtcclxuXHRcdFx0XHR0aGlzLnNoYWRlckNyb3BBbHBoYSA9IHNoYWRlcjtcclxuXHRcdFx0XHRzaGFkZXIudmFsdWUgPSB0aGlzLl9jcm9wQWxwaGEgPyAxIDogMDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyTW92ZVNwZWVkKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJNb3ZlU3BlZWQgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJGbG9hdCwgXCJtb3Zlc3BlZWRcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlck1vdmVTcGVlZCl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckZsb2F0KTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJtb3Zlc3BlZWRcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX01PVkVTUEVFRFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfbW92ZVNwZWVkXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTWFjcm8ubmFtZSA9IFwiVU5JRl9NT1ZFU1BFRURcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZOYW1lID0gXCJ1X21vdmVTcGVlZFwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyTW92ZVNwZWVkID0gc2hhZGVyO1xyXG5cdFx0XHRcdHNoYWRlci52YWx1ZSA9IHRoaXMuX21vdmVTcGVlZDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyTW92ZVdpZHRoKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJNb3ZlV2lkdGggPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJGbG9hdCwgXCJtb3Zld2lkdGhcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlck1vdmVXaWR0aCl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckZsb2F0KTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJtb3Zld2lkdGhcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX01PVkVXSURUSFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfbW92ZVdpZHRoXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTWFjcm8ubmFtZSA9IFwiVU5JRl9NT1ZFV0lEVEhcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZOYW1lID0gXCJ1X21vdmVXaWR0aFwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyTW92ZVdpZHRoID0gc2hhZGVyO1xyXG5cdFx0XHRcdHNoYWRlci52YWx1ZSA9IHRoaXMuX21vdmVXaWR0aDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyTGlnaHRMaW5lcyl7XHJcblx0XHRcdHRoaXMuc2hhZGVyTGlnaHRMaW5lcyA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImxpZ2h0bGluZXNcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckxpZ2h0TGluZXMpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJGbG9hdCk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwibGlnaHRsaW5lc1wiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfTElHSFRMSU5FU1wiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfbGlnaHRMaW5lc1wiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVOSUZfTElHSFRMSU5FU1wiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfbGlnaHRMaW5lc1wiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyTGlnaHRMaW5lcyA9IHNoYWRlcjtcclxuXHRcdFx0XHRzaGFkZXIudmFsdWUgPSB0aGlzLl9saWdodExpbmVzO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5zaGFkZXJMaWdodFNwYWNlKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJMaWdodFNwYWNlID0gdGhpcy5nZXRTaGFkZXJDb21wb25lbnQoU2hhZGVyRmxvYXQsIFwibGlnaHRzcGFjZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyTGlnaHRTcGFjZSl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckZsb2F0KTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJsaWdodHNwYWNlXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTWFjcm8ubmFtZSA9IFwiQVRUUl9MSUdIVFNQQUNFXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTmFtZSA9IFwiYV9saWdodFNwYWNlXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTWFjcm8ubmFtZSA9IFwiVU5JRl9MSUdIVFNQQUNFXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTmFtZSA9IFwidV9saWdodFNwYWNlXCI7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJMaWdodFNwYWNlID0gc2hhZGVyO1xyXG5cdFx0XHRcdHNoYWRlci52YWx1ZSA9IHRoaXMuX2xpZ2h0U3BhY2U7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGlmKCF0aGlzLnJlbmRlclN5c3RlbSl7XHJcblx0XHRcdHRoaXMucmVuZGVyU3lzdGVtID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKTtcclxuXHRcdFx0aWYoIXRoaXMucmVuZGVyU3lzdGVtKXtcclxuXHRcdFx0XHR0aGlzLnJlbmRlclN5c3RlbSA9IHRoaXMuYWRkQ29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGlmKENDX0VESVRPUil7XHJcblx0XHRcdHRoaXMuY2hlY2tNYXRlcmlhbCgpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICog5qOA5p+l5p2Q6LSoXHJcblx0ICovXHJcblx0YXN5bmMgY2hlY2tNYXRlcmlhbCgpe1xyXG5cdFx0aWYoQ0NfRURJVE9SKXtcclxuXHRcdFx0aWYoIXRoaXMuX21hdGVyaWFsKXtcclxuXHRcdFx0XHR0aGlzLm1hdGVyaWFsID0gYXdhaXQgVXRpbHMuZ2V0QXNzZXRCeVVVSUQ8Y2MuTWF0ZXJpYWw+KFVVSUQubWF0ZXJpYWxzW1wibGNjLTJkLWZsYXNoX2xpZ2h0XCJdKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxufVxyXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID0gXCIuLi9zaGFkZXIvU2hhZGVyQ29tcG9uZW50LnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2MkcmVuZGVyIHtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eSwgbWVudX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3MoXCJsY2MkcmVuZGVyLkVmZmVjdEZsdXhheVN1cGVyXCIpXHJcbkBtZW51KFwiaTE4bjpsY2MtcmVuZGVyLm1lbnVfY29tcG9uZW50L0VmZmVjdEZsdXhheVN1cGVyXCIpXHJcbmV4cG9ydCBjbGFzcyBFZmZlY3RGbHV4YXlTdXBlciBleHRlbmRzIFNoYWRlckNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk1hdGVyaWFsKVxyXG5cdF9tYXRlcmlhbDogY2MuTWF0ZXJpYWwgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuTWF0ZXJpYWwsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpzmnZDotKhcIlxyXG5cdH0pXHJcblx0Z2V0IG1hdGVyaWFsKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fbWF0ZXJpYWw7XHJcblx0fVxyXG5cdHNldCBtYXRlcmlhbCh2YWx1ZTpjYy5NYXRlcmlhbCl7XHJcblx0XHRpZih0aGlzLl9tYXRlcmlhbCAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX21hdGVyaWFsID0gdmFsdWU7XHJcblx0XHRcdHRoaXMucmVuZGVyU3lzdGVtLnNldE1hdGVyaWFsKDAsIHZhbHVlKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG5cdF9zcHJpdGVGcmFtZTogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuU3ByaXRlRnJhbWUsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpznsr7ngbXluKdcIlxyXG5cdH0pXHJcblx0Z2V0IHNwcml0ZUZyYW1lKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fc3ByaXRlRnJhbWU7XHJcblx0fVxyXG5cdHNldCBzcHJpdGVGcmFtZSh2YWx1ZTpjYy5TcHJpdGVGcmFtZSl7XHJcblx0XHRpZih0aGlzLl9zcHJpdGVGcmFtZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3Nwcml0ZUZyYW1lID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUuc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KClcclxuXHRfc3BlZWQ6IG51bWJlciA9IDE7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOmAn+W6plwiXHJcblx0fSlcclxuXHRnZXQgc3BlZWQoKXtcclxuXHRcdHJldHVybiB0aGlzLl9zcGVlZDtcclxuXHR9XHJcblx0c2V0IHNwZWVkKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl9zcGVlZCAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3NwZWVkID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3BlZWQudmFsdWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0cmVuZGVyU3lzdGVtOlJlbmRlclN5c3RlbSA9IG51bGw7XHJcblx0c2hhZGVyU3ByaXRlRnJhbWU6U2hhZGVyU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdHNoYWRlclNwZWVkOlNoYWRlckZsb2F0ID0gbnVsbDtcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBvbkVuYWJsZSAoKSB7XHJcblx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSwgXCJzcHJpdGVmcmFtZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyU3ByaXRlRnJhbWUpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwic3ByaXRlZnJhbWVcIjtcclxuXHRcdFx0XHRzaGFkZXIudXNlVVYgPSB0cnVlO1xyXG5cdFx0XHRcdHNoYWRlci51c2VTaGFwZSA9IHRydWU7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHNoYWRlcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyU3BlZWQpe1xyXG5cdFx0XHR0aGlzLnNoYWRlclNwZWVkID0gdGhpcy5nZXRTaGFkZXJDb21wb25lbnQoU2hhZGVyRmxvYXQsIFwic3BlZWRcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlclNwZWVkKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcInNwZWVkXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTWFjcm8ubmFtZSA9IFwiQVRUUl9TUEVFRFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfc3BlZWRcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX1NQRUVEXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTmFtZSA9IFwidV9zcGVlZFwiO1xyXG5cdFx0XHRcdHNoYWRlci52YWx1ZSA9IDE7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJTcGVlZCA9IHNoYWRlcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMucmVuZGVyU3lzdGVtKXtcclxuXHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHRcdHRoaXMucmVuZGVyU3lzdGVtID0gdGhpcy5hZGRDb21wb25lbnQoUmVuZGVyU3lzdGVtKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoQ0NfRURJVE9SKXtcclxuXHRcdFx0dGhpcy5jaGVja01hdGVyaWFsKCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDmo4Dmn6XmnZDotKhcclxuXHQgKi9cclxuXHRhc3luYyBjaGVja01hdGVyaWFsKCl7XHJcblx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHRpZighdGhpcy5fbWF0ZXJpYWwpe1xyXG5cdFx0XHRcdHRoaXMubWF0ZXJpYWwgPSBhd2FpdCBVdGlscy5nZXRBc3NldEJ5VVVJRDxjYy5NYXRlcmlhbD4oVVVJRC5tYXRlcmlhbHNbXCJsY2MtMmQtZmx1eGF5X3N1cGVyXCJdKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxufVxyXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID0gXCIuLi9zaGFkZXIvU2hhZGVyQ29tcG9uZW50LnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2MkcmVuZGVyIHtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eSwgbWVudX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3MoXCJsY2MkcmVuZGVyLkVmZmVjdEdhdXNzaWFuQmx1clwiKVxyXG5AbWVudShcImkxOG46bGNjLXJlbmRlci5tZW51X2NvbXBvbmVudC9FZmZlY3RHYXVzc2lhbkJsdXJcIilcclxuZXhwb3J0IGNsYXNzIEVmZmVjdEdhdXNzaWFuQmx1ciBleHRlbmRzIFNoYWRlckNvbXBvbmVudCB7XHJcblxyXG5cdEBwcm9wZXJ0eShjYy5NYXRlcmlhbClcclxuXHRfbWF0ZXJpYWw6IGNjLk1hdGVyaWFsID0gbnVsbDtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6IGNjLk1hdGVyaWFsLFxyXG5cdFx0dG9vbHRpcCA6IFwi5pWI5p6c5p2Q6LSoXCJcclxuXHR9KVxyXG5cdGdldCBtYXRlcmlhbCgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX21hdGVyaWFsO1xyXG5cdH1cclxuXHRzZXQgbWF0ZXJpYWwodmFsdWU6Y2MuTWF0ZXJpYWwpe1xyXG5cdFx0aWYodGhpcy5fbWF0ZXJpYWwgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9tYXRlcmlhbCA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbS5zZXRNYXRlcmlhbCgwLCB2YWx1ZSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRAcHJvcGVydHkoY2MuU3ByaXRlRnJhbWUpXHJcblx0X3Nwcml0ZUZyYW1lOiBjYy5TcHJpdGVGcmFtZSA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5TcHJpdGVGcmFtZSxcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOeyvueBteW4p1wiXHJcblx0fSlcclxuXHRnZXQgc3ByaXRlRnJhbWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl9zcHJpdGVGcmFtZTtcclxuXHR9XHJcblx0c2V0IHNwcml0ZUZyYW1lKHZhbHVlOmNjLlNwcml0ZUZyYW1lKXtcclxuXHRcdGlmKHRoaXMuX3Nwcml0ZUZyYW1lICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZS5zcHJpdGVGcmFtZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cmVuZGVyU3lzdGVtOlJlbmRlclN5c3RlbSA9IG51bGw7XHJcblx0c2hhZGVyU3ByaXRlRnJhbWU6U2hhZGVyU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdFxyXG5cdC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuXHRvbkVuYWJsZSAoKSB7XHJcblx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSwgXCJzcHJpdGVmcmFtZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyU3ByaXRlRnJhbWUpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwic3ByaXRlZnJhbWVcIjtcclxuXHRcdFx0XHRzaGFkZXIudXNlU2hhcGUgPSB0cnVlO1xyXG5cdFx0XHRcdHNoYWRlci51c2VVViA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWUmVjdCA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZUZyYW1lU2l6ZSA9IHRydWU7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHNoYWRlcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMucmVuZGVyU3lzdGVtKXtcclxuXHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHRcdHRoaXMucmVuZGVyU3lzdGVtID0gdGhpcy5hZGRDb21wb25lbnQoUmVuZGVyU3lzdGVtKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoQ0NfRURJVE9SKXtcclxuXHRcdFx0dGhpcy5jaGVja01hdGVyaWFsKCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDmo4Dmn6XmnZDotKhcclxuXHQgKi9cclxuXHRhc3luYyBjaGVja01hdGVyaWFsKCl7XHJcblx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHRpZighdGhpcy5fbWF0ZXJpYWwpe1xyXG5cdFx0XHRcdHRoaXMubWF0ZXJpYWwgPSBhd2FpdCBVdGlscy5nZXRBc3NldEJ5VVVJRDxjYy5NYXRlcmlhbD4oVVVJRC5tYXRlcmlhbHNbXCJsY2MtMmRfZ2F1c3NpYW5fYmx1clwiXSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbn1cclxuXHQiLCIvLy8gPHJlZmVyZW5jZSBwYXRoID0gXCIuLi9zaGFkZXIvU2hhZGVyQ29tcG9uZW50LnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2MkcmVuZGVyIHtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eSwgbWVudX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3MoXCJsY2MkcmVuZGVyLkVmZmVjdEdsb3dJbm5lclwiKVxyXG5AbWVudShcImkxOG46bGNjLXJlbmRlci5tZW51X2NvbXBvbmVudC9FZmZlY3RHbG93SW5uZXJcIilcclxuZXhwb3J0IGNsYXNzIEVmZmVjdEdsb3dJbm5lciBleHRlbmRzIFNoYWRlckNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk1hdGVyaWFsKVxyXG5cdF9tYXRlcmlhbDogY2MuTWF0ZXJpYWwgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuTWF0ZXJpYWwsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpzmnZDotKhcIlxyXG5cdH0pXHJcblx0Z2V0IG1hdGVyaWFsKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fbWF0ZXJpYWw7XHJcblx0fVxyXG5cdHNldCBtYXRlcmlhbCh2YWx1ZTpjYy5NYXRlcmlhbCl7XHJcblx0XHRpZih0aGlzLl9tYXRlcmlhbCAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX21hdGVyaWFsID0gdmFsdWU7XHJcblx0XHRcdHRoaXMucmVuZGVyU3lzdGVtLnNldE1hdGVyaWFsKDAsIHZhbHVlKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG5cdF9zcHJpdGVGcmFtZTogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuU3ByaXRlRnJhbWUsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpznsr7ngbXluKdcIlxyXG5cdH0pXHJcblx0Z2V0IHNwcml0ZUZyYW1lKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fc3ByaXRlRnJhbWU7XHJcblx0fVxyXG5cdHNldCBzcHJpdGVGcmFtZSh2YWx1ZTpjYy5TcHJpdGVGcmFtZSl7XHJcblx0XHRpZih0aGlzLl9zcHJpdGVGcmFtZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3Nwcml0ZUZyYW1lID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUuc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkNvbG9yKVxyXG5cdF9nbG93Q29sb3I6IGNjLkNvbG9yID0gY2MuQ29sb3IuWUVMTE9XO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuQ29sb3IsXHJcblx0XHR0b29sdGlwIDogXCLlj5HlhYnpopzoibJcIlxyXG5cdH0pXHJcblx0Z2V0IGdsb3dDb2xvcigpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2dsb3dDb2xvcjtcclxuXHR9XHJcblx0c2V0IGdsb3dDb2xvcih2YWx1ZTpjYy5Db2xvcil7XHJcblx0XHR0aGlzLl9nbG93Q29sb3IgPSB2YWx1ZTtcclxuXHRcdHRoaXMuc2hhZGVyR2xvd0NvbG9yLmNvbG9yID0gdmFsdWU7XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eSgpXHJcblx0X2dsb3dDb2xvclNpemU6IG51bWJlciA9IDAuMjtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5Y+R5YWJ5a695bqmXCIsXHJcblx0XHRyYW5nZSA6IFswLCAxLCAwLjFdLFxyXG5cdH0pXHJcblx0Z2V0IGdsb3dDb2xvclNpemUoKXtcclxuXHRcdHJldHVybiB0aGlzLl9nbG93Q29sb3JTaXplO1xyXG5cdH1cclxuXHRzZXQgZ2xvd0NvbG9yU2l6ZSh2YWx1ZTpudW1iZXIpe1xyXG5cdFx0aWYodGhpcy5fZ2xvd0NvbG9yU2l6ZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX2dsb3dDb2xvclNpemUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJHbG93Q29sb3JTaXplLnZhbHVlID0gdmFsdWU7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eSgpXHJcblx0X2dsb3dUaHJlc2hvbGQ6IG51bWJlciA9IDAuMTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5Y+R5YWJ6ZiI5YC8XCIsXHJcblx0XHRyYW5nZSA6IFswLCAxLCAwLjFdLFxyXG5cdH0pXHJcblx0Z2V0IGdsb3dUaHJlc2hvbGQoKXtcclxuXHRcdHJldHVybiB0aGlzLl9nbG93VGhyZXNob2xkO1xyXG5cdH1cclxuXHRzZXQgZ2xvd1RocmVzaG9sZCh2YWx1ZTpudW1iZXIpe1xyXG5cdFx0aWYodGhpcy5fZ2xvd1RocmVzaG9sZCAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX2dsb3dUaHJlc2hvbGQgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJHbG93VGhyZXNob2xkLnZhbHVlID0gdmFsdWU7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eSgpXHJcblx0X2dsb3dGbGFzaDogbnVtYmVyID0gMC4wO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0b29sdGlwIDogXCLlj5HlhYnpl6rng4HpgJ/luqZcIixcclxuXHR9KVxyXG5cdGdldCBnbG93Rmxhc2goKXtcclxuXHRcdHJldHVybiB0aGlzLl9nbG93Rmxhc2g7XHJcblx0fVxyXG5cdHNldCBnbG93Rmxhc2godmFsdWU6bnVtYmVyKXtcclxuXHRcdGlmKHRoaXMuX2dsb3dGbGFzaCAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX2dsb3dGbGFzaCA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnNoYWRlckdsb3dGbGFzaC52YWx1ZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cmVuZGVyU3lzdGVtOlJlbmRlclN5c3RlbSA9IG51bGw7XHJcblx0c2hhZGVyU3ByaXRlRnJhbWU6U2hhZGVyU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdHNoYWRlckdsb3dDb2xvcjpTaGFkZXJDb2xvciA9IG51bGw7XHJcblx0c2hhZGVyR2xvd0NvbG9yU2l6ZTpTaGFkZXJGbG9hdCA9IG51bGw7XHJcblx0c2hhZGVyR2xvd1RocmVzaG9sZDpTaGFkZXJGbG9hdCA9IG51bGw7XHJcblx0c2hhZGVyR2xvd0ZsYXNoOlNoYWRlckZsb2F0ID0gbnVsbDtcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBvbkVuYWJsZSAoKSB7XHJcblx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSwgXCJzcHJpdGVmcmFtZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyU3ByaXRlRnJhbWUpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwic3ByaXRlZnJhbWVcIjtcclxuXHRcdFx0XHRzaGFkZXIudXNlU2hhcGUgPSB0cnVlO1xyXG5cdFx0XHRcdHNoYWRlci51c2VVViA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWUmVjdCA9IHRydWU7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHNoYWRlcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyR2xvd0NvbG9yKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJHbG93Q29sb3IgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJDb2xvciwgXCJnbG93Q29sb3JcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckdsb3dDb2xvcil7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckNvbG9yKTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJnbG93Q29sb3JcIjtcclxuXHRcdFx0XHRzaGFkZXIudXNlTm9kZUNvbG9yID0gZmFsc2U7XHJcblx0XHRcdFx0c2hhZGVyLl9jb2xvclZhci5hdHRyTWFjcm8ubmFtZSA9IFwiQVRUUl9HTE9XQ09MT1JcIjtcclxuXHRcdFx0XHRzaGFkZXIuX2NvbG9yVmFyLmF0dHJOYW1lID0gXCJhX2dsb3dDb2xvclwiO1xyXG5cdFx0XHRcdHNoYWRlci5fY29sb3JWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVOSUZfR0xPV0NPTE9SXCI7XHJcblx0XHRcdFx0c2hhZGVyLl9jb2xvclZhci51bmlmTmFtZSA9IFwidV9nbG93Q29sb3JcIjtcclxuXHRcdFx0XHR0aGlzLnNoYWRlckdsb3dDb2xvciA9IHNoYWRlcjtcclxuXHRcdFx0XHRzaGFkZXIuY29sb3IgPSB0aGlzLl9nbG93Q29sb3I7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGlmKCF0aGlzLnNoYWRlckdsb3dDb2xvclNpemUpe1xyXG5cdFx0XHR0aGlzLnNoYWRlckdsb3dDb2xvclNpemUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJGbG9hdCwgXCJnbG93Q29sb3JTaXplXCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJHbG93Q29sb3JTaXplKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcImdsb3dDb2xvclNpemVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX0dMT1dDT0xPUlNJWkVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJOYW1lID0gXCJhX2dsb3dDb2xvclNpemVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0dMT1dDT0xPUlNJWkVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZOYW1lID0gXCJ1X2dsb3dDb2xvclNpemVcIjtcclxuXHRcdFx0XHR0aGlzLnNoYWRlckdsb3dDb2xvclNpemUgPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLnZhbHVlID0gdGhpcy5fZ2xvd0NvbG9yU2l6ZTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyR2xvd1RocmVzaG9sZCl7XHJcblx0XHRcdHRoaXMuc2hhZGVyR2xvd1RocmVzaG9sZCA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImdsb3dUaHJlc2hvbGRcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckdsb3dUaHJlc2hvbGQpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJGbG9hdCk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwiZ2xvd1RocmVzaG9sZFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfR0xPV1RIUkVTSE9MRFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfZ2xvd1RocmVzaG9sZFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVOSUZfR0xPV1RIUkVTSE9MRFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfZ2xvd1RocmVzaG9sZFwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyR2xvd1RocmVzaG9sZCA9IHNoYWRlcjtcclxuXHRcdFx0XHRzaGFkZXIudmFsdWUgPSB0aGlzLl9nbG93VGhyZXNob2xkO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5zaGFkZXJHbG93Rmxhc2gpe1xyXG5cdFx0XHR0aGlzLnNoYWRlckdsb3dGbGFzaCA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImdsb3dGbGFzaFwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyR2xvd0ZsYXNoKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcImdsb3dGbGFzaFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfR0xPV0ZMQVNIXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTmFtZSA9IFwiYV9nbG93Rmxhc2hcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0dMT1dGTEFTSFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfZ2xvd0ZsYXNoXCI7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJHbG93Rmxhc2ggPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLnZhbHVlID0gdGhpcy5fZ2xvd0ZsYXNoO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcblx0XHRcdGlmKCF0aGlzLnJlbmRlclN5c3RlbSl7XHJcblx0XHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmFkZENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHR0aGlzLmNoZWNrTWF0ZXJpYWwoKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIOajgOafpeadkOi0qFxyXG5cdCAqL1xyXG5cdGFzeW5jIGNoZWNrTWF0ZXJpYWwoKXtcclxuXHRcdGlmKENDX0VESVRPUil7XHJcblx0XHRcdGlmKCF0aGlzLl9tYXRlcmlhbCl7XHJcblx0XHRcdFx0dGhpcy5tYXRlcmlhbCA9IGF3YWl0IFV0aWxzLmdldEFzc2V0QnlVVUlEPGNjLk1hdGVyaWFsPihVVUlELm1hdGVyaWFsc1tcImxjYy0yZF9nbG93X2lubmVyXCJdKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxufVxyXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID0gXCIuLi9zaGFkZXIvU2hhZGVyQ29tcG9uZW50LnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2MkcmVuZGVyIHtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eSwgbWVudX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3MoXCJsY2MkcmVuZGVyLkVmZmVjdEdsb3dPdXR0ZXJcIilcclxuQG1lbnUoXCJpMThuOmxjYy1yZW5kZXIubWVudV9jb21wb25lbnQvRWZmZWN0R2xvd091dHRlclwiKVxyXG5leHBvcnQgY2xhc3MgRWZmZWN0R2xvd091dHRlciBleHRlbmRzIFNoYWRlckNvbXBvbmVudCB7XHJcblxyXG5cdEBwcm9wZXJ0eShjYy5NYXRlcmlhbClcclxuXHRfbWF0ZXJpYWw6IGNjLk1hdGVyaWFsID0gbnVsbDtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6IGNjLk1hdGVyaWFsLFxyXG5cdFx0dG9vbHRpcCA6IFwi5pWI5p6c5p2Q6LSoXCJcclxuXHR9KVxyXG5cdGdldCBtYXRlcmlhbCgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX21hdGVyaWFsO1xyXG5cdH1cclxuXHRzZXQgbWF0ZXJpYWwodmFsdWU6Y2MuTWF0ZXJpYWwpe1xyXG5cdFx0aWYodGhpcy5fbWF0ZXJpYWwgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9tYXRlcmlhbCA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbS5zZXRNYXRlcmlhbCgwLCB2YWx1ZSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRAcHJvcGVydHkoY2MuU3ByaXRlRnJhbWUpXHJcblx0X3Nwcml0ZUZyYW1lOiBjYy5TcHJpdGVGcmFtZSA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5TcHJpdGVGcmFtZSxcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOeyvueBteW4p1wiXHJcblx0fSlcclxuXHRnZXQgc3ByaXRlRnJhbWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl9zcHJpdGVGcmFtZTtcclxuXHR9XHJcblx0c2V0IHNwcml0ZUZyYW1lKHZhbHVlOmNjLlNwcml0ZUZyYW1lKXtcclxuXHRcdGlmKHRoaXMuX3Nwcml0ZUZyYW1lICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZS5zcHJpdGVGcmFtZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KGNjLkNvbG9yKVxyXG5cdF9nbG93Q29sb3I6IGNjLkNvbG9yID0gY2MuQ29sb3IuWUVMTE9XO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuQ29sb3IsXHJcblx0XHR0b29sdGlwIDogXCLlj5HlhYnpopzoibJcIlxyXG5cdH0pXHJcblx0Z2V0IGdsb3dDb2xvcigpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2dsb3dDb2xvcjtcclxuXHR9XHJcblx0c2V0IGdsb3dDb2xvcih2YWx1ZTpjYy5Db2xvcil7XHJcblx0XHR0aGlzLl9nbG93Q29sb3IgPSB2YWx1ZTtcclxuXHRcdHRoaXMuc2hhZGVyR2xvd0NvbG9yLmNvbG9yID0gdmFsdWU7XHJcblx0fVxyXG5cclxuXHRAcHJvcGVydHkoKVxyXG5cdF9nbG93Q29sb3JTaXplOiBudW1iZXIgPSAwLjE7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIuWPkeWFieWuveW6plwiLFxyXG5cdFx0cmFuZ2UgOiBbMCwgMSwgMC4xXSxcclxuXHR9KVxyXG5cdGdldCBnbG93Q29sb3JTaXplKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fZ2xvd0NvbG9yU2l6ZTtcclxuXHR9XHJcblx0c2V0IGdsb3dDb2xvclNpemUodmFsdWU6bnVtYmVyKXtcclxuXHRcdGlmKHRoaXMuX2dsb3dDb2xvclNpemUgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9nbG93Q29sb3JTaXplID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyR2xvd0NvbG9yU2l6ZS52YWx1ZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KClcclxuXHRfZ2xvd1RocmVzaG9sZDogbnVtYmVyID0gMTtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5Y+R5YWJ6ZiI5YC8XCIsXHJcblx0XHRyYW5nZSA6IFswLCAxLCAwLjFdLFxyXG5cdH0pXHJcblx0Z2V0IGdsb3dUaHJlc2hvbGQoKXtcclxuXHRcdHJldHVybiB0aGlzLl9nbG93VGhyZXNob2xkO1xyXG5cdH1cclxuXHRzZXQgZ2xvd1RocmVzaG9sZCh2YWx1ZTpudW1iZXIpe1xyXG5cdFx0aWYodGhpcy5fZ2xvd1RocmVzaG9sZCAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX2dsb3dUaHJlc2hvbGQgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJHbG93VGhyZXNob2xkLnZhbHVlID0gdmFsdWU7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRAcHJvcGVydHkoKVxyXG5cdF9nbG93Rmxhc2g6IG51bWJlciA9IDAuMDtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi5Y+R5YWJ6Zeq54OB6YCf5bqmXCIsXHJcblx0fSlcclxuXHRnZXQgZ2xvd0ZsYXNoKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fZ2xvd0ZsYXNoO1xyXG5cdH1cclxuXHRzZXQgZ2xvd0ZsYXNoKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl9nbG93Rmxhc2ggIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9nbG93Rmxhc2ggPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJHbG93Rmxhc2gudmFsdWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHJlbmRlclN5c3RlbTpSZW5kZXJTeXN0ZW0gPSBudWxsO1xyXG5cdHNoYWRlclNwcml0ZUZyYW1lOlNoYWRlclNwcml0ZUZyYW1lID0gbnVsbDtcclxuXHRzaGFkZXJHbG93Q29sb3I6U2hhZGVyQ29sb3IgPSBudWxsO1xyXG5cdHNoYWRlckdsb3dDb2xvclNpemU6U2hhZGVyRmxvYXQgPSBudWxsO1xyXG5cdHNoYWRlckdsb3dUaHJlc2hvbGQ6U2hhZGVyRmxvYXQgPSBudWxsO1xyXG5cdHNoYWRlckdsb3dGbGFzaDpTaGFkZXJGbG9hdCA9IG51bGw7XHJcblxyXG5cdC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuXHRvbkVuYWJsZSAoKSB7XHJcblx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSwgXCJzcHJpdGVmcmFtZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyU3ByaXRlRnJhbWUpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwic3ByaXRlZnJhbWVcIjtcclxuXHRcdFx0XHRzaGFkZXIudXNlU2hhcGUgPSB0cnVlO1xyXG5cdFx0XHRcdHNoYWRlci51c2VVViA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWUmVjdCA9IHRydWU7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHNoYWRlcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyR2xvd0NvbG9yKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJHbG93Q29sb3IgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJDb2xvciwgXCJnbG93Q29sb3JcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckdsb3dDb2xvcil7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckNvbG9yKTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJnbG93Q29sb3JcIjtcclxuXHRcdFx0XHRzaGFkZXIudXNlTm9kZUNvbG9yID0gZmFsc2U7XHJcblx0XHRcdFx0c2hhZGVyLl9jb2xvclZhci5hdHRyTWFjcm8ubmFtZSA9IFwiQVRUUl9HTE9XQ09MT1JcIjtcclxuXHRcdFx0XHRzaGFkZXIuX2NvbG9yVmFyLmF0dHJOYW1lID0gXCJhX2dsb3dDb2xvclwiO1xyXG5cdFx0XHRcdHNoYWRlci5fY29sb3JWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVOSUZfR0xPV0NPTE9SXCI7XHJcblx0XHRcdFx0c2hhZGVyLl9jb2xvclZhci51bmlmTmFtZSA9IFwidV9nbG93Q29sb3JcIjtcclxuXHRcdFx0XHR0aGlzLnNoYWRlckdsb3dDb2xvciA9IHNoYWRlcjtcclxuXHRcdFx0XHRzaGFkZXIuY29sb3IgPSB0aGlzLl9nbG93Q29sb3I7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGlmKCF0aGlzLnNoYWRlckdsb3dDb2xvclNpemUpe1xyXG5cdFx0XHR0aGlzLnNoYWRlckdsb3dDb2xvclNpemUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJGbG9hdCwgXCJnbG93Q29sb3JTaXplXCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJHbG93Q29sb3JTaXplKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcImdsb3dDb2xvclNpemVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX0dMT1dDT0xPUlNJWkVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJOYW1lID0gXCJhX2dsb3dDb2xvclNpemVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0dMT1dDT0xPUlNJWkVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZOYW1lID0gXCJ1X2dsb3dDb2xvclNpemVcIjtcclxuXHRcdFx0XHR0aGlzLnNoYWRlckdsb3dDb2xvclNpemUgPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLnZhbHVlID0gdGhpcy5fZ2xvd0NvbG9yU2l6ZTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyR2xvd1RocmVzaG9sZCl7XHJcblx0XHRcdHRoaXMuc2hhZGVyR2xvd1RocmVzaG9sZCA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImdsb3dUaHJlc2hvbGRcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckdsb3dUaHJlc2hvbGQpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJGbG9hdCk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwiZ2xvd1RocmVzaG9sZFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfR0xPV1RIUkVTSE9MRFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfZ2xvd1RocmVzaG9sZFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVOSUZfR0xPV1RIUkVTSE9MRFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfZ2xvd1RocmVzaG9sZFwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyR2xvd1RocmVzaG9sZCA9IHNoYWRlcjtcclxuXHRcdFx0XHRzaGFkZXIudmFsdWUgPSB0aGlzLl9nbG93VGhyZXNob2xkO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5zaGFkZXJHbG93Rmxhc2gpe1xyXG5cdFx0XHR0aGlzLnNoYWRlckdsb3dGbGFzaCA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImdsb3dGbGFzaFwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyR2xvd0ZsYXNoKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcImdsb3dGbGFzaFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfR0xPV0ZMQVNIXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTmFtZSA9IFwiYV9nbG93Rmxhc2hcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0dMT1dGTEFTSFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfZ2xvd0ZsYXNoXCI7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJHbG93Rmxhc2ggPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLnZhbHVlID0gdGhpcy5fZ2xvd0ZsYXNoO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcblx0XHRcdGlmKCF0aGlzLnJlbmRlclN5c3RlbSl7XHJcblx0XHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmFkZENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHR0aGlzLmNoZWNrTWF0ZXJpYWwoKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIOajgOafpeadkOi0qFxyXG5cdCAqL1xyXG5cdGFzeW5jIGNoZWNrTWF0ZXJpYWwoKXtcclxuXHRcdGlmKENDX0VESVRPUil7XHJcblx0XHRcdGlmKCF0aGlzLl9tYXRlcmlhbCl7XHJcblx0XHRcdFx0dGhpcy5tYXRlcmlhbCA9IGF3YWl0IFV0aWxzLmdldEFzc2V0QnlVVUlEPGNjLk1hdGVyaWFsPihVVUlELm1hdGVyaWFsc1tcImxjYy0yZF9nbG93X291dHRlclwiXSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbn1cclxuXHQiLCIvLy8gPHJlZmVyZW5jZSBwYXRoID0gXCIuLi9zaGFkZXIvU2hhZGVyQ29tcG9uZW50LnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2MkcmVuZGVyIHtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eSwgbWVudX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3MoXCJsY2MkcmVuZGVyLkVmZmVjdE1vc2FpY1wiKVxyXG5AbWVudShcImkxOG46bGNjLXJlbmRlci5tZW51X2NvbXBvbmVudC9FZmZlY3RNb3NhaWNcIilcclxuZXhwb3J0IGNsYXNzIEVmZmVjdE1vc2FpYyBleHRlbmRzIFNoYWRlckNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk1hdGVyaWFsKVxyXG5cdF9tYXRlcmlhbDogY2MuTWF0ZXJpYWwgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuTWF0ZXJpYWwsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpzmnZDotKhcIlxyXG5cdH0pXHJcblx0Z2V0IG1hdGVyaWFsKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fbWF0ZXJpYWw7XHJcblx0fVxyXG5cdHNldCBtYXRlcmlhbCh2YWx1ZTpjYy5NYXRlcmlhbCl7XHJcblx0XHRpZih0aGlzLl9tYXRlcmlhbCAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX21hdGVyaWFsID0gdmFsdWU7XHJcblx0XHRcdHRoaXMucmVuZGVyU3lzdGVtLnNldE1hdGVyaWFsKDAsIHZhbHVlKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG5cdF9zcHJpdGVGcmFtZTogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuU3ByaXRlRnJhbWUsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpznsr7ngbXluKdcIlxyXG5cdH0pXHJcblx0Z2V0IHNwcml0ZUZyYW1lKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fc3ByaXRlRnJhbWU7XHJcblx0fVxyXG5cdHNldCBzcHJpdGVGcmFtZSh2YWx1ZTpjYy5TcHJpdGVGcmFtZSl7XHJcblx0XHRpZih0aGlzLl9zcHJpdGVGcmFtZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3Nwcml0ZUZyYW1lID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUuc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KClcclxuXHRfZGVncmVlOiBudW1iZXIgPSAwLjA7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIumprOi1m+WFi+eoi+W6plwiLFxyXG5cdFx0cmFuZ2UgOiBbMCwgMSwgMC4wMV0sXHJcblx0fSlcclxuXHRnZXQgZGVncmVlKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fZGVncmVlO1xyXG5cdH1cclxuXHRzZXQgZGVncmVlKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl9kZWdyZWUgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9kZWdyZWUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJEZWdyZWUudmFsdWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHJlbmRlclN5c3RlbTpSZW5kZXJTeXN0ZW0gPSBudWxsO1xyXG5cdHNoYWRlclNwcml0ZUZyYW1lOlNoYWRlclNwcml0ZUZyYW1lID0gbnVsbDtcclxuXHRzaGFkZXJEZWdyZWU6U2hhZGVyRmxvYXQgPSBudWxsO1xyXG5cclxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuICAgIG9uRW5hYmxlICgpIHtcclxuXHRcdGlmKCF0aGlzLnNoYWRlclNwcml0ZUZyYW1lKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlclNwcml0ZUZyYW1lLCBcInNwcml0ZWZyYW1lXCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlclNwcml0ZUZyYW1lKTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJzcHJpdGVmcmFtZVwiO1xyXG5cdFx0XHRcdHNoYWRlci51c2VTaGFwZSA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWID0gdHJ1ZTtcclxuXHRcdFx0XHRzaGFkZXIudXNlVVZSZWN0ID0gdHJ1ZTtcclxuXHRcdFx0XHR0aGlzLnNoYWRlclNwcml0ZUZyYW1lID0gc2hhZGVyO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5zaGFkZXJEZWdyZWUpe1xyXG5cdFx0XHR0aGlzLnNoYWRlckRlZ3JlZSA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImRlZ3JlZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyRGVncmVlKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcImRlZ3JlZVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfREVHUkVFXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTmFtZSA9IFwiYV9kZWdyZWVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0RFR1JFRVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfZGVncmVlXCI7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJEZWdyZWUgPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLnZhbHVlID0gdGhpcy5fZGVncmVlO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcblx0XHRcdGlmKCF0aGlzLnJlbmRlclN5c3RlbSl7XHJcblx0XHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmFkZENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHR0aGlzLmNoZWNrTWF0ZXJpYWwoKTtcclxuXHRcdH1cclxuXHR9XHJcbiAgICBcclxuXHQvKipcclxuXHQgKiDmo4Dmn6XmnZDotKhcclxuXHQgKi9cclxuXHRhc3luYyBjaGVja01hdGVyaWFsKCl7XHJcblx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHRpZighdGhpcy5fbWF0ZXJpYWwpe1xyXG5cdFx0XHRcdHRoaXMubWF0ZXJpYWwgPSBhd2FpdCBVdGlscy5nZXRBc3NldEJ5VVVJRDxjYy5NYXRlcmlhbD4oVVVJRC5tYXRlcmlhbHNbXCJsY2MtMmRfbW9zYWljXCJdKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxufVxyXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID0gXCIuLi9zaGFkZXIvU2hhZGVyQ29tcG9uZW50LnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2MkcmVuZGVyIHtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eSwgbWVudX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3MoXCJsY2MkcmVuZGVyLkVmZmVjdE91dGxpbmVcIilcclxuQG1lbnUoXCJpMThuOmxjYy1yZW5kZXIubWVudV9jb21wb25lbnQvRWZmZWN0T3V0bGluZVwiKVxyXG5leHBvcnQgY2xhc3MgRWZmZWN0T3V0bGluZSBleHRlbmRzIFNoYWRlckNvbXBvbmVudCB7XHJcblxyXG5cdEBwcm9wZXJ0eShjYy5NYXRlcmlhbClcclxuXHRfbWF0ZXJpYWw6IGNjLk1hdGVyaWFsID0gbnVsbDtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6IGNjLk1hdGVyaWFsLFxyXG5cdFx0dG9vbHRpcCA6IFwi5pWI5p6c5p2Q6LSoXCJcclxuXHR9KVxyXG5cdGdldCBtYXRlcmlhbCgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX21hdGVyaWFsO1xyXG5cdH1cclxuXHRzZXQgbWF0ZXJpYWwodmFsdWU6Y2MuTWF0ZXJpYWwpe1xyXG5cdFx0aWYodGhpcy5fbWF0ZXJpYWwgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9tYXRlcmlhbCA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbS5zZXRNYXRlcmlhbCgwLCB2YWx1ZSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRAcHJvcGVydHkoY2MuU3ByaXRlRnJhbWUpXHJcblx0X3Nwcml0ZUZyYW1lOiBjYy5TcHJpdGVGcmFtZSA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5TcHJpdGVGcmFtZSxcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOeyvueBteW4p1wiXHJcblx0fSlcclxuXHRnZXQgc3ByaXRlRnJhbWUoKXtcclxuXHRcdHJldHVybiB0aGlzLl9zcHJpdGVGcmFtZTtcclxuXHR9XHJcblx0c2V0IHNwcml0ZUZyYW1lKHZhbHVlOmNjLlNwcml0ZUZyYW1lKXtcclxuXHRcdGlmKHRoaXMuX3Nwcml0ZUZyYW1lICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZS5zcHJpdGVGcmFtZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KGNjLkNvbG9yKVxyXG5cdF9vdXRsaW5lQ29sb3I6IGNjLkNvbG9yID0gY2MuQ29sb3IuUkVEO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuQ29sb3IsXHJcblx0XHR0b29sdGlwIDogXCLmj4/ovrnpopzoibJcIlxyXG5cdH0pXHJcblx0Z2V0IG91dGxpbmVDb2xvcigpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX291dGxpbmVDb2xvcjtcclxuXHR9XHJcblx0c2V0IG91dGxpbmVDb2xvcih2YWx1ZTpjYy5Db2xvcil7XHJcblx0XHR0aGlzLl9vdXRsaW5lQ29sb3IgPSB2YWx1ZTtcclxuXHRcdHRoaXMuc2hhZGVyT3V0bGluZUNvbG9yLmNvbG9yID0gdmFsdWU7XHJcblx0fVxyXG5cclxuXHRAcHJvcGVydHkoKVxyXG5cdF9vdXRsaW5lV2lkdGg6IG51bWJlciA9IDAuMDAyO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0b29sdGlwIDogXCLmj4/ovrnlrr3luqZcIixcclxuXHRcdHJhbmdlIDogWzAsIDEsIDAuMDFdLFxyXG5cdH0pXHJcblx0Z2V0IG91dGxpbmVXaWR0aCgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX291dGxpbmVXaWR0aDtcclxuXHR9XHJcblx0c2V0IG91dGxpbmVXaWR0aCh2YWx1ZTpudW1iZXIpe1xyXG5cdFx0aWYodGhpcy5fb3V0bGluZVdpZHRoICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fb3V0bGluZVdpZHRoID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyT3V0bGluZVdpZHRoLnZhbHVlID0gdmFsdWU7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRAcHJvcGVydHkoKVxyXG5cdF9nbG93Rmxhc2g6IG51bWJlciA9IDAuMDtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi6Zeq54OB6YCf5bqmXCIsXHJcblx0fSlcclxuXHRnZXQgZ2xvd0ZsYXNoKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fZ2xvd0ZsYXNoO1xyXG5cdH1cclxuXHRzZXQgZ2xvd0ZsYXNoKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl9nbG93Rmxhc2ggIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9nbG93Rmxhc2ggPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJHbG93Rmxhc2gudmFsdWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHJlbmRlclN5c3RlbTpSZW5kZXJTeXN0ZW0gPSBudWxsO1xyXG5cdHNoYWRlclNwcml0ZUZyYW1lOlNoYWRlclNwcml0ZUZyYW1lID0gbnVsbDtcclxuXHRzaGFkZXJPdXRsaW5lQ29sb3I6U2hhZGVyQ29sb3IgPSBudWxsO1xyXG5cdHNoYWRlck91dGxpbmVXaWR0aDpTaGFkZXJGbG9hdCA9IG51bGw7XHJcblx0c2hhZGVyR2xvd0ZsYXNoOlNoYWRlckZsb2F0ID0gbnVsbDtcclxuXHJcblx0Ly8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XHJcblxyXG5cdG9uRW5hYmxlICgpIHtcclxuXHRcdGlmKCF0aGlzLnNoYWRlclNwcml0ZUZyYW1lKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlclNwcml0ZUZyYW1lLCBcInNwcml0ZWZyYW1lXCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlclNwcml0ZUZyYW1lKTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJzcHJpdGVmcmFtZVwiO1xyXG5cdFx0XHRcdHNoYWRlci51c2VTaGFwZSA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWID0gdHJ1ZTtcclxuXHRcdFx0XHRzaGFkZXIudXNlVVZSZWN0ID0gdHJ1ZTtcclxuXHRcdFx0XHR0aGlzLnNoYWRlclNwcml0ZUZyYW1lID0gc2hhZGVyO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5zaGFkZXJPdXRsaW5lQ29sb3Ipe1xyXG5cdFx0XHR0aGlzLnNoYWRlck91dGxpbmVDb2xvciA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckNvbG9yLCBcIm91dGxpbmVDb2xvclwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyT3V0bGluZUNvbG9yKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyQ29sb3IpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcIm91dGxpbmVDb2xvclwiO1xyXG5cdFx0XHRcdHNoYWRlci51c2VOb2RlQ29sb3IgPSBmYWxzZTtcclxuXHRcdFx0XHRzaGFkZXIuX2NvbG9yVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX09VVExJTkVDT0xPUlwiO1xyXG5cdFx0XHRcdHNoYWRlci5fY29sb3JWYXIuYXR0ck5hbWUgPSBcImFfb3V0bGluZUNvbG9yXCI7XHJcblx0XHRcdFx0c2hhZGVyLl9jb2xvclZhci51bmlmTWFjcm8ubmFtZSA9IFwiVU5JRl9PVVRMSU5FQ09MT1JcIjtcclxuXHRcdFx0XHRzaGFkZXIuX2NvbG9yVmFyLnVuaWZOYW1lID0gXCJ1X291dGxpbmVDb2xvclwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyT3V0bGluZUNvbG9yID0gc2hhZGVyO1xyXG5cdFx0XHRcdHNoYWRlci5jb2xvciA9IHRoaXMuX291dGxpbmVDb2xvcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyT3V0bGluZVdpZHRoKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJPdXRsaW5lV2lkdGggPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJGbG9hdCwgXCJvdXRsaW5lV2lkdGhcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlck91dGxpbmVXaWR0aCl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckZsb2F0KTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJvdXRsaW5lV2lkdGhcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX09VVExJTkVXSURUSFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfb3V0bGluZVdpZHRoXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTWFjcm8ubmFtZSA9IFwiVU5JRl9PVVRMSU5FV0lEVEhcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZOYW1lID0gXCJ1X291dGxpbmVXaWR0aFwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyT3V0bGluZVdpZHRoID0gc2hhZGVyO1xyXG5cdFx0XHRcdHNoYWRlci52YWx1ZSA9IHRoaXMuX291dGxpbmVXaWR0aDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyR2xvd0ZsYXNoKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJHbG93Rmxhc2ggPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJGbG9hdCwgXCJnbG93Rmxhc2hcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckdsb3dGbGFzaCl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckZsb2F0KTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJnbG93Rmxhc2hcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX0dMT1dGTEFTSFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfZ2xvd0ZsYXNoXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTWFjcm8ubmFtZSA9IFwiVU5JRl9HTE9XRkxBU0hcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZOYW1lID0gXCJ1X2dsb3dGbGFzaFwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyR2xvd0ZsYXNoID0gc2hhZGVyO1xyXG5cdFx0XHRcdHNoYWRlci52YWx1ZSA9IHRoaXMuX2dsb3dGbGFzaDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMucmVuZGVyU3lzdGVtKXtcclxuXHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHRcdHRoaXMucmVuZGVyU3lzdGVtID0gdGhpcy5hZGRDb21wb25lbnQoUmVuZGVyU3lzdGVtKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoQ0NfRURJVE9SKXtcclxuXHRcdFx0dGhpcy5jaGVja01hdGVyaWFsKCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDmo4Dmn6XmnZDotKhcclxuXHQgKi9cclxuXHRhc3luYyBjaGVja01hdGVyaWFsKCl7XHJcblx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHRpZighdGhpcy5fbWF0ZXJpYWwpe1xyXG5cdFx0XHRcdHRoaXMubWF0ZXJpYWwgPSBhd2FpdCBVdGlscy5nZXRBc3NldEJ5VVVJRDxjYy5NYXRlcmlhbD4oVVVJRC5tYXRlcmlhbHNbXCJsY2MtMmRfb3V0bGluZVwiXSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi4vc2hhZGVyL1NoYWRlckNvbXBvbmVudC50c1wiIC8+XHJcblxyXG5tb2R1bGUgbGNjJHJlbmRlciB7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHksIG1lbnV9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbi8qKlxyXG4gKiDpopzoibLnsbvlnotcclxuICovXHJcbmVudW0gQ29sb3JUeXBlIHtcclxuXHRPTEQgPSAwLFx0Ly8g6ICB54Wn54mHXHJcblx0R1JBWSxcdFx0Ly8g5Y+Y54GwXHJcblx0UkVWRVJTQUwsXHQvLyDlj43nm7hcclxuXHRGUk9aRU4sXHRcdC8vIOWGsOWGu1xyXG5cdENBUlRPT04sXHQvLyDljaHpgJpcclxufVxyXG5cclxuQGNjY2xhc3MoXCJsY2MkcmVuZGVyLkVmZmVjdFBob3RvXCIpXHJcbkBtZW51KFwiaTE4bjpsY2MtcmVuZGVyLm1lbnVfY29tcG9uZW50L0VmZmVjdFBob3RvXCIpXHJcbmV4cG9ydCBjbGFzcyBFZmZlY3RQaG90byBleHRlbmRzIFNoYWRlckNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk1hdGVyaWFsKVxyXG5cdF9tYXRlcmlhbDogY2MuTWF0ZXJpYWwgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuTWF0ZXJpYWwsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpzmnZDotKhcIlxyXG5cdH0pXHJcblx0Z2V0IG1hdGVyaWFsKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fbWF0ZXJpYWw7XHJcblx0fVxyXG5cdHNldCBtYXRlcmlhbCh2YWx1ZTpjYy5NYXRlcmlhbCl7XHJcblx0XHRpZih0aGlzLl9tYXRlcmlhbCAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX21hdGVyaWFsID0gdmFsdWU7XHJcblx0XHRcdHRoaXMucmVuZGVyU3lzdGVtLnNldE1hdGVyaWFsKDAsIHZhbHVlKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG5cdF9zcHJpdGVGcmFtZTogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuU3ByaXRlRnJhbWUsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpznsr7ngbXluKdcIlxyXG5cdH0pXHJcblx0Z2V0IHNwcml0ZUZyYW1lKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fc3ByaXRlRnJhbWU7XHJcblx0fVxyXG5cdHNldCBzcHJpdGVGcmFtZSh2YWx1ZTpjYy5TcHJpdGVGcmFtZSl7XHJcblx0XHRpZih0aGlzLl9zcHJpdGVGcmFtZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3Nwcml0ZUZyYW1lID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUuc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5FbnVtKENvbG9yVHlwZSlcclxuXHR9KVxyXG5cdF9jb2xvclR5cGU6IENvbG9yVHlwZSA9IENvbG9yVHlwZS5PTEQ7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIuminOiJsuexu+Wei1wiLFxyXG5cdFx0dHlwZSA6IGNjLkVudW0oQ29sb3JUeXBlKVxyXG5cdH0pXHJcblx0Z2V0IGNvbG9yVHlwZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2NvbG9yVHlwZTtcclxuXHR9XHJcblx0c2V0IGNvbG9yVHlwZSh2YWx1ZTpDb2xvclR5cGUpe1xyXG5cdFx0aWYodGhpcy5fY29sb3JUeXBlICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fY29sb3JUeXBlID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyQ29sb3JUeXBlLnZhbHVlID0gdmFsdWU7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eSgpXHJcblx0X2RlZ3JlZTogbnVtYmVyID0gMS4wO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0b29sdGlwIDogXCLnqIvluqZcIixcclxuXHRcdHJhbmdlIDogWzAsIDEsIDAuMDFdLFxyXG5cdH0pXHJcblx0Z2V0IGRlZ3JlZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2RlZ3JlZTtcclxuXHR9XHJcblx0c2V0IGRlZ3JlZSh2YWx1ZTpudW1iZXIpe1xyXG5cdFx0aWYodGhpcy5fZGVncmVlICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fZGVncmVlID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyRGVncmVlLnZhbHVlID0gdmFsdWU7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRyZW5kZXJTeXN0ZW06UmVuZGVyU3lzdGVtID0gbnVsbDtcclxuXHRzaGFkZXJTcHJpdGVGcmFtZTpTaGFkZXJTcHJpdGVGcmFtZSA9IG51bGw7XHJcblx0c2hhZGVyQ29sb3JUeXBlOlNoYWRlckZsb2F0ID0gbnVsbDtcclxuXHRzaGFkZXJEZWdyZWU6U2hhZGVyRmxvYXQgPSBudWxsO1xyXG5cclxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuICAgIG9uRW5hYmxlICgpIHtcclxuXHRcdGlmKCF0aGlzLnNoYWRlclNwcml0ZUZyYW1lKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlclNwcml0ZUZyYW1lLCBcInNwcml0ZWZyYW1lXCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlclNwcml0ZUZyYW1lKTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJzcHJpdGVmcmFtZVwiO1xyXG5cdFx0XHRcdHNoYWRlci51c2VTaGFwZSA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWID0gdHJ1ZTtcclxuXHRcdFx0XHR0aGlzLnNoYWRlclNwcml0ZUZyYW1lID0gc2hhZGVyO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5zaGFkZXJDb2xvclR5cGUpe1xyXG5cdFx0XHR0aGlzLnNoYWRlckNvbG9yVHlwZSA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImNvbG9yVHlwZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyQ29sb3JUeXBlKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcImNvbG9yVHlwZVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfQ09MT1JUWVBFXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTmFtZSA9IFwiYV9jb2xvclR5cGVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0NPTE9SVFlQRVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfY29sb3JUeXBlXCI7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJDb2xvclR5cGUgPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLnZhbHVlID0gdGhpcy5fY29sb3JUeXBlO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5zaGFkZXJEZWdyZWUpe1xyXG5cdFx0XHR0aGlzLnNoYWRlckRlZ3JlZSA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImRlZ3JlZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyRGVncmVlKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcImRlZ3JlZVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfREVHUkVFXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTmFtZSA9IFwiYV9kZWdyZWVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0RFR1JFRVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfZGVncmVlXCI7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJEZWdyZWUgPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLnZhbHVlID0gdGhpcy5fZGVncmVlO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcblx0XHRcdGlmKCF0aGlzLnJlbmRlclN5c3RlbSl7XHJcblx0XHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmFkZENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHR0aGlzLmNoZWNrTWF0ZXJpYWwoKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIOajgOafpeadkOi0qFxyXG5cdCAqL1xyXG5cdGFzeW5jIGNoZWNrTWF0ZXJpYWwoKXtcclxuXHRcdGlmKENDX0VESVRPUil7XHJcblx0XHRcdGlmKCF0aGlzLl9tYXRlcmlhbCl7XHJcblx0XHRcdFx0dGhpcy5tYXRlcmlhbCA9IGF3YWl0IFV0aWxzLmdldEFzc2V0QnlVVUlEPGNjLk1hdGVyaWFsPihVVUlELm1hdGVyaWFsc1tcImxjYy0yZF9waG90b1wiXSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi4vc2hhZGVyL1NoYWRlckNvbXBvbmVudC50c1wiIC8+XHJcblxyXG5tb2R1bGUgbGNjJHJlbmRlciB7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHksIG1lbnV9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzKFwibGNjJHJlbmRlci5FZmZlY3RSYWluRHJvcFJpcHBsZXNcIilcclxuQG1lbnUoXCJpMThuOmxjYy1yZW5kZXIubWVudV9jb21wb25lbnQvRWZmZWN0UmFpbkRyb3BSaXBwbGVzXCIpXHJcbmV4cG9ydCBjbGFzcyBFZmZlY3RSYWluRHJvcFJpcHBsZXMgZXh0ZW5kcyBTaGFkZXJDb21wb25lbnQge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5NYXRlcmlhbClcclxuXHRfbWF0ZXJpYWw6IGNjLk1hdGVyaWFsID0gbnVsbDtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6IGNjLk1hdGVyaWFsLFxyXG5cdFx0dG9vbHRpcCA6IFwi5pWI5p6c5p2Q6LSoXCJcclxuXHR9KVxyXG5cdGdldCBtYXRlcmlhbCgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX21hdGVyaWFsO1xyXG5cdH1cclxuXHRzZXQgbWF0ZXJpYWwodmFsdWU6Y2MuTWF0ZXJpYWwpe1xyXG5cdFx0aWYodGhpcy5fbWF0ZXJpYWwgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9tYXRlcmlhbCA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbS5zZXRNYXRlcmlhbCgwLCB2YWx1ZSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcclxuXHRfc3ByaXRlRnJhbWU6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6IGNjLlNwcml0ZUZyYW1lLFxyXG5cdFx0dG9vbHRpcCA6IFwi5pWI5p6c57K+54G15binXCJcclxuXHR9KVxyXG5cdGdldCBzcHJpdGVGcmFtZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3Nwcml0ZUZyYW1lO1xyXG5cdH1cclxuXHRzZXQgc3ByaXRlRnJhbWUodmFsdWU6Y2MuU3ByaXRlRnJhbWUpe1xyXG5cdFx0aWYodGhpcy5fc3ByaXRlRnJhbWUgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9zcHJpdGVGcmFtZSA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnNoYWRlclNwcml0ZUZyYW1lLnNwcml0ZUZyYW1lID0gdmFsdWU7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eSgpXHJcblx0X2RlbnNpdHk6IG51bWJlciA9IDE1LjA7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIuWvhuW6plwiLFxyXG5cdH0pXHJcblx0Z2V0IGRlbnNpdHkoKXtcclxuXHRcdHJldHVybiB0aGlzLl9kZW5zaXR5O1xyXG5cdH1cclxuXHRzZXQgZGVuc2l0eSh2YWx1ZTpudW1iZXIpe1xyXG5cdFx0aWYodGhpcy5fZGVuc2l0eSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX2RlbnNpdHkgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJEZW5zaXR5LnZhbHVlID0gdmFsdWU7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eSgpXHJcblx0X2NoYW5nZTogYm9vbGVhbiA9IHRydWU7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIuWKqOaAgeaUueWPmFwiLFxyXG5cdH0pXHJcblx0Z2V0IGNoYW5nZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX2NoYW5nZTtcclxuXHR9XHJcblx0c2V0IGNoYW5nZSh2YWx1ZTpib29sZWFuKXtcclxuXHRcdGlmKHRoaXMuX2NoYW5nZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX2NoYW5nZSA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnNoYWRlckNoYW5nZS52YWx1ZSA9IHZhbHVlID8gMSA6IDA7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRyZW5kZXJTeXN0ZW06UmVuZGVyU3lzdGVtID0gbnVsbDtcclxuXHRzaGFkZXJTcHJpdGVGcmFtZTpTaGFkZXJTcHJpdGVGcmFtZSA9IG51bGw7XHJcblx0c2hhZGVyRGVuc2l0eTpTaGFkZXJGbG9hdCA9IG51bGw7XHJcblx0c2hhZGVyQ2hhbmdlOlNoYWRlckZsb2F0ID0gbnVsbDtcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBvbkVuYWJsZSAoKSB7XHJcblx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSwgXCJzcHJpdGVmcmFtZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyU3ByaXRlRnJhbWUpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwic3ByaXRlZnJhbWVcIjtcclxuXHRcdFx0XHRzaGFkZXIudXNlU2hhcGUgPSB0cnVlO1xyXG5cdFx0XHRcdHNoYWRlci51c2VVViA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWUmVjdCA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZUZyYW1lU2l6ZSA9IHRydWU7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHNoYWRlcjtcclxuXHRcdFx0fVxyXG4gICAgICAgIH1cclxuXHRcdGlmKCF0aGlzLnNoYWRlckRlbnNpdHkpe1xyXG5cdFx0XHR0aGlzLnNoYWRlckRlbnNpdHkgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJGbG9hdCwgXCJkZW5zaXR5XCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJEZW5zaXR5KXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcImRlbnNpdHlcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX0RFTlNJVFlcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJOYW1lID0gXCJhX2RlbnNpdHlcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0RFTlNJVFlcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZOYW1lID0gXCJ1X2RlbnNpdHlcIjtcclxuXHRcdFx0XHR0aGlzLnNoYWRlckRlbnNpdHkgPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLnZhbHVlID0gdGhpcy5fZGVuc2l0eTtcclxuXHRcdFx0fVxyXG4gICAgICAgIH1cclxuXHRcdGlmKCF0aGlzLnNoYWRlckNoYW5nZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyQ2hhbmdlID0gdGhpcy5nZXRTaGFkZXJDb21wb25lbnQoU2hhZGVyRmxvYXQsIFwiY2hhbmdlXCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJDaGFuZ2Upe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJGbG9hdCk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwiY2hhbmdlXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTWFjcm8ubmFtZSA9IFwiQVRUUl9DSEFOR0VcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJOYW1lID0gXCJhX2NoYW5nZVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVOSUZfQ0hBTkdFXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTmFtZSA9IFwidV9jaGFuZ2VcIjtcclxuXHRcdFx0XHR0aGlzLnNoYWRlckNoYW5nZSA9IHNoYWRlcjtcclxuXHRcdFx0XHRzaGFkZXIudmFsdWUgPSB0aGlzLl9jaGFuZ2UgPyAxIDogMDtcclxuXHRcdFx0fVxyXG4gICAgICAgIH1cclxuXHRcdGlmKCF0aGlzLnJlbmRlclN5c3RlbSl7XHJcblx0XHRcdHRoaXMucmVuZGVyU3lzdGVtID0gdGhpcy5nZXRDb21wb25lbnQoUmVuZGVyU3lzdGVtKTtcclxuXHRcdFx0aWYoIXRoaXMucmVuZGVyU3lzdGVtKXtcclxuXHRcdFx0XHR0aGlzLnJlbmRlclN5c3RlbSA9IHRoaXMuYWRkQ29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGlmKENDX0VESVRPUil7XHJcblx0XHRcdHRoaXMuY2hlY2tNYXRlcmlhbCgpO1xyXG5cdFx0fVxyXG5cdH1cclxuICAgIFxyXG5cdC8qKlxyXG5cdCAqIOajgOafpeadkOi0qFxyXG5cdCAqL1xyXG5cdGFzeW5jIGNoZWNrTWF0ZXJpYWwoKXtcclxuXHRcdGlmKENDX0VESVRPUil7XHJcblx0XHRcdGlmKCF0aGlzLl9tYXRlcmlhbCl7XHJcblx0XHRcdFx0dGhpcy5tYXRlcmlhbCA9IGF3YWl0IFV0aWxzLmdldEFzc2V0QnlVVUlEPGNjLk1hdGVyaWFsPihVVUlELm1hdGVyaWFsc1tcImxjYy0yZF9yYWluX2Ryb3BfcmlwcGxlc1wiXSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi4vc2hhZGVyL1NoYWRlckNvbXBvbmVudC50c1wiIC8+XHJcblxyXG5tb2R1bGUgbGNjJHJlbmRlciB7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHksIG1lbnV9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzKFwibGNjJHJlbmRlci5FZmZlY3RSb3VuZENvcm5lckNyb3BcIilcclxuQG1lbnUoXCJpMThuOmxjYy1yZW5kZXIubWVudV9jb21wb25lbnQvRWZmZWN0Um91bmRDb3JuZXJDcm9wXCIpXHJcbmV4cG9ydCBjbGFzcyBFZmZlY3RSb3VuZENvcm5lckNyb3AgZXh0ZW5kcyBTaGFkZXJDb21wb25lbnQge1xyXG5cclxuXHRAcHJvcGVydHkoY2MuTWF0ZXJpYWwpXHJcblx0X21hdGVyaWFsOiBjYy5NYXRlcmlhbCA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5NYXRlcmlhbCxcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOadkOi0qFwiXHJcblx0fSlcclxuXHRnZXQgbWF0ZXJpYWwoKXtcclxuXHRcdHJldHVybiB0aGlzLl9tYXRlcmlhbDtcclxuXHR9XHJcblx0c2V0IG1hdGVyaWFsKHZhbHVlOmNjLk1hdGVyaWFsKXtcclxuXHRcdGlmKHRoaXMuX21hdGVyaWFsICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fbWF0ZXJpYWwgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0uc2V0TWF0ZXJpYWwoMCwgdmFsdWUpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG5cdF9zcHJpdGVGcmFtZTogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuU3ByaXRlRnJhbWUsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpznsr7ngbXluKdcIlxyXG5cdH0pXHJcblx0Z2V0IHNwcml0ZUZyYW1lKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fc3ByaXRlRnJhbWU7XHJcblx0fVxyXG5cdHNldCBzcHJpdGVGcmFtZSh2YWx1ZTpjYy5TcHJpdGVGcmFtZSl7XHJcblx0XHRpZih0aGlzLl9zcHJpdGVGcmFtZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3Nwcml0ZUZyYW1lID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUuc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KClcclxuXHRfZGVncmVlOiBudW1iZXIgPSAwLjU7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIueoi+W6plwiLFxyXG5cdFx0cmFuZ2UgOiBbMCwgMSwgMC4wMV0sXHJcblx0fSlcclxuXHRnZXQgZGVncmVlKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fZGVncmVlO1xyXG5cdH1cclxuXHRzZXQgZGVncmVlKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl9kZWdyZWUgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9kZWdyZWUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJEZWdyZWUudmFsdWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHJlbmRlclN5c3RlbTpSZW5kZXJTeXN0ZW0gPSBudWxsO1xyXG5cdHNoYWRlclNwcml0ZUZyYW1lOlNoYWRlclNwcml0ZUZyYW1lID0gbnVsbDtcclxuXHRzaGFkZXJEZWdyZWU6U2hhZGVyRmxvYXQgPSBudWxsO1xyXG5cdFxyXG5cdC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuXHRvbkVuYWJsZSAoKSB7XHJcblx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSwgXCJzcHJpdGVmcmFtZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyU3ByaXRlRnJhbWUpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwic3ByaXRlZnJhbWVcIjtcclxuXHRcdFx0XHRzaGFkZXIudXNlU2hhcGUgPSB0cnVlO1xyXG5cdFx0XHRcdHNoYWRlci51c2VVViA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWUmVjdCA9IHRydWU7XHJcblx0XHRcdFx0c2hhZGVyLnVzZUZyYW1lU2l6ZSA9IHRydWU7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHNoYWRlcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyRGVncmVlKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJEZWdyZWUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJGbG9hdCwgXCJkZWdyZWVcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckRlZ3JlZSl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckZsb2F0KTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJkZWdyZWVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX0RFR1JFRVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfZGVncmVlXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTWFjcm8ubmFtZSA9IFwiVU5JRl9ERUdSRUVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZOYW1lID0gXCJ1X2RlZ3JlZVwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyRGVncmVlID0gc2hhZGVyO1xyXG5cdFx0XHRcdHNoYWRlci52YWx1ZSA9IHRoaXMuX2RlZ3JlZTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMucmVuZGVyU3lzdGVtKXtcclxuXHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmdldENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHRcdHRoaXMucmVuZGVyU3lzdGVtID0gdGhpcy5hZGRDb21wb25lbnQoUmVuZGVyU3lzdGVtKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoQ0NfRURJVE9SKXtcclxuXHRcdFx0dGhpcy5jaGVja01hdGVyaWFsKCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDmo4Dmn6XmnZDotKhcclxuXHQgKi9cclxuXHRhc3luYyBjaGVja01hdGVyaWFsKCl7XHJcblx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHRpZighdGhpcy5fbWF0ZXJpYWwpe1xyXG5cdFx0XHRcdHRoaXMubWF0ZXJpYWwgPSBhd2FpdCBVdGlscy5nZXRBc3NldEJ5VVVJRDxjYy5NYXRlcmlhbD4oVVVJRC5tYXRlcmlhbHNbXCJsY2MtMmRfcm91bmRfY29ybmVyX2Nyb3BcIl0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG59XHJcbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGggPSBcIi4uL3NoYWRlci9TaGFkZXJDb21wb25lbnQudHNcIiAvPlxyXG5cclxubW9kdWxlIGxjYyRyZW5kZXIge1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5LCBtZW51fSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG4vKipcclxuICog6L+H5rih5pa55ZCRXHJcbiAqL1xyXG5lbnVtIEZhZGVEaXJlY3Qge1xyXG5cdExFRlRfVE9fUklHSFQgPSAwLFx0Ly8g5bem5Yiw5Y+zXHJcblx0UklHSFRfVE9fTEVGVCxcdFx0Ly8g5Y+z5Yiw5bemXHJcblx0VE9QX1RPX0JPVFRPTSxcdFx0Ly8g5LiK5Yiw5LiLXHJcblx0Qk9UVE9NX1RPX1RPUCxcdFx0Ly8g5LiL5Yiw5LiKXHJcbn1cclxuXHJcbi8qKlxyXG4gKiDlubPmu5Hov4fmuKHmlYjmnpxcclxuICovXHJcbkBjY2NsYXNzKFwibGNjJHJlbmRlci5FZmZlY3RTbW9vdGhUcmFuc2Zvcm1cIilcclxuQG1lbnUoXCJpMThuOmxjYy1yZW5kZXIubWVudV9jb21wb25lbnQvRWZmZWN0U21vb3RoVHJhbnNmb3JtXCIpXHJcbmV4cG9ydCBjbGFzcyBFZmZlY3RTbW9vdGhUcmFuc2Zvcm0gZXh0ZW5kcyBTaGFkZXJDb21wb25lbnQge1xyXG5cclxuXHRAcHJvcGVydHkoY2MuTWF0ZXJpYWwpXHJcblx0X21hdGVyaWFsOiBjYy5NYXRlcmlhbCA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5NYXRlcmlhbCxcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOadkOi0qFwiXHJcblx0fSlcclxuXHRnZXQgbWF0ZXJpYWwoKXtcclxuXHRcdHJldHVybiB0aGlzLl9tYXRlcmlhbDtcclxuXHR9XHJcblx0c2V0IG1hdGVyaWFsKHZhbHVlOmNjLk1hdGVyaWFsKXtcclxuXHRcdGlmKHRoaXMuX21hdGVyaWFsICE9IHZhbHVlKXtcclxuXHRcdFx0dGhpcy5fbWF0ZXJpYWwgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0uc2V0TWF0ZXJpYWwoMCwgdmFsdWUpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG5cdF9zcHJpdGVGcmFtZTogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdEBwcm9wZXJ0eSh7XHJcblx0XHR0eXBlIDogY2MuU3ByaXRlRnJhbWUsXHJcblx0XHR0b29sdGlwIDogXCLmlYjmnpznsr7ngbXluKdcIlxyXG5cdH0pXHJcblx0Z2V0IHNwcml0ZUZyYW1lKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fc3ByaXRlRnJhbWU7XHJcblx0fVxyXG5cdHNldCBzcHJpdGVGcmFtZSh2YWx1ZTpjYy5TcHJpdGVGcmFtZSl7XHJcblx0XHRpZih0aGlzLl9zcHJpdGVGcmFtZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX3Nwcml0ZUZyYW1lID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUuc3ByaXRlRnJhbWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0QHByb3BlcnR5KClcclxuXHRfZmFkZVdpZHRoOiBudW1iZXIgPSAwLjI7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIui/h+a4oeWuveW6plwiLFxyXG5cdFx0cmFuZ2UgOiBbMCwgMSwgMC4wMV0sXHJcblx0fSlcclxuXHRnZXQgZmFkZVdpZHRoKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fZmFkZVdpZHRoO1xyXG5cdH1cclxuXHRzZXQgZmFkZVdpZHRoKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl9mYWRlV2lkdGggIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9mYWRlV2lkdGggPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJGYWRlV2lkdGgudmFsdWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gICAgQHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5FbnVtKEZhZGVEaXJlY3QpXHJcblx0fSlcclxuXHRfZmFkZURpcmVjdDogRmFkZURpcmVjdCA9IEZhZGVEaXJlY3QuTEVGVF9UT19SSUdIVDtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dG9vbHRpcCA6IFwi6L+H5rih5pa55ZCRXCIsXHJcblx0XHR0eXBlIDogY2MuRW51bShGYWRlRGlyZWN0KVxyXG5cdH0pXHJcblx0Z2V0IGZhZGVEaXJlY3QoKXtcclxuXHRcdHJldHVybiB0aGlzLl9mYWRlRGlyZWN0O1xyXG5cdH1cclxuXHRzZXQgZmFkZURpcmVjdCh2YWx1ZTpGYWRlRGlyZWN0KXtcclxuXHRcdGlmKHRoaXMuX2ZhZGVEaXJlY3QgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9mYWRlRGlyZWN0ID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2hhZGVyRmFkZURpcmVjdC52YWx1ZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0QHByb3BlcnR5KClcclxuXHRfZGVncmVlOiBudW1iZXIgPSAwLjU7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHRvb2x0aXAgOiBcIueoi+W6plwiLFxyXG5cdFx0cmFuZ2UgOiBbMCwgMSwgMC4wMV0sXHJcblx0fSlcclxuXHRnZXQgZGVncmVlKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fZGVncmVlO1xyXG5cdH1cclxuXHRzZXQgZGVncmVlKHZhbHVlOm51bWJlcil7XHJcblx0XHRpZih0aGlzLl9kZWdyZWUgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9kZWdyZWUgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zaGFkZXJEZWdyZWUudmFsdWUgPSB2YWx1ZTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHJlbmRlclN5c3RlbTpSZW5kZXJTeXN0ZW0gPSBudWxsO1xyXG5cdHNoYWRlclNwcml0ZUZyYW1lOlNoYWRlclNwcml0ZUZyYW1lID0gbnVsbDtcclxuXHRzaGFkZXJGYWRlV2lkdGg6U2hhZGVyRmxvYXQgPSBudWxsO1xyXG5cdHNoYWRlckZhZGVEaXJlY3Q6U2hhZGVyRmxvYXQgPSBudWxsO1xyXG5cdHNoYWRlckRlZ3JlZTpTaGFkZXJGbG9hdCA9IG51bGw7XHJcblxyXG5cdC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuXHRvbkVuYWJsZSAoKSB7XHJcblx0XHRpZighdGhpcy5zaGFkZXJTcHJpdGVGcmFtZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUgPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSwgXCJzcHJpdGVmcmFtZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyU3ByaXRlRnJhbWUpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwic3ByaXRlZnJhbWVcIjtcclxuXHRcdFx0XHRzaGFkZXIudXNlU2hhcGUgPSB0cnVlO1xyXG5cdFx0XHRcdHNoYWRlci51c2VVViA9IHRydWU7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJTcHJpdGVGcmFtZSA9IHNoYWRlcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyRmFkZVdpZHRoKXtcclxuXHRcdFx0dGhpcy5zaGFkZXJGYWRlV2lkdGggPSB0aGlzLmdldFNoYWRlckNvbXBvbmVudChTaGFkZXJGbG9hdCwgXCJmYWRlV2lkdGhcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckZhZGVXaWR0aCl7XHJcblx0XHRcdFx0bGV0IHNoYWRlciA9IHRoaXMuYWRkQ29tcG9uZW50KFNoYWRlckZsb2F0KTtcclxuXHRcdFx0XHRzaGFkZXIudGFnID0gXCJmYWRlV2lkdGhcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLmF0dHJNYWNyby5uYW1lID0gXCJBVFRSX0ZBREVXSURUSFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfZmFkZVdpZHRoXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci51bmlmTWFjcm8ubmFtZSA9IFwiVU5JRl9GQURFV0lEVEhcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZOYW1lID0gXCJ1X2ZhZGVXaWR0aFwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyRmFkZVdpZHRoID0gc2hhZGVyO1xyXG5cdFx0XHRcdHNoYWRlci52YWx1ZSA9IHRoaXMuX2ZhZGVXaWR0aDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0aWYoIXRoaXMuc2hhZGVyRmFkZURpcmVjdCl7XHJcblx0XHRcdHRoaXMuc2hhZGVyRmFkZURpcmVjdCA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImZhZGVEaXJlY3RcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlckZhZGVEaXJlY3Qpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJGbG9hdCk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwiZmFkZURpcmVjdFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfRkFERURJUkVDVFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck5hbWUgPSBcImFfZmFkZURpcmVjdFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVOSUZfRkFERURJUkVDVFwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfZmFkZURpcmVjdFwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyRmFkZURpcmVjdCA9IHNoYWRlcjtcclxuXHRcdFx0XHRzaGFkZXIudmFsdWUgPSB0aGlzLl9mYWRlRGlyZWN0O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5zaGFkZXJEZWdyZWUpe1xyXG5cdFx0XHR0aGlzLnNoYWRlckRlZ3JlZSA9IHRoaXMuZ2V0U2hhZGVyQ29tcG9uZW50KFNoYWRlckZsb2F0LCBcImRlZ3JlZVwiKTtcclxuXHRcdFx0aWYoIXRoaXMuc2hhZGVyRGVncmVlKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyRmxvYXQpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcImRlZ3JlZVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIuYXR0ck1hY3JvLm5hbWUgPSBcIkFUVFJfREVHUkVFXCI7XHJcblx0XHRcdFx0c2hhZGVyLl92YWx1ZVZhci5hdHRyTmFtZSA9IFwiYV9kZWdyZWVcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3ZhbHVlVmFyLnVuaWZNYWNyby5uYW1lID0gXCJVTklGX0RFR1JFRVwiO1xyXG5cdFx0XHRcdHNoYWRlci5fdmFsdWVWYXIudW5pZk5hbWUgPSBcInVfZGVncmVlXCI7XHJcblx0XHRcdFx0dGhpcy5zaGFkZXJEZWdyZWUgPSBzaGFkZXI7XHJcblx0XHRcdFx0c2hhZGVyLnZhbHVlID0gdGhpcy5fZGVncmVlO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcblx0XHRcdGlmKCF0aGlzLnJlbmRlclN5c3RlbSl7XHJcblx0XHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmFkZENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHR0aGlzLmNoZWNrTWF0ZXJpYWwoKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIOajgOafpeadkOi0qFxyXG5cdCAqL1xyXG5cdGFzeW5jIGNoZWNrTWF0ZXJpYWwoKXtcclxuXHRcdGlmKENDX0VESVRPUil7XHJcblx0XHRcdGlmKCF0aGlzLl9tYXRlcmlhbCl7XHJcblx0XHRcdFx0dGhpcy5tYXRlcmlhbCA9IGF3YWl0IFV0aWxzLmdldEFzc2V0QnlVVUlEPGNjLk1hdGVyaWFsPihVVUlELm1hdGVyaWFsc1tcImxjYy0yZF90cmFuc2Zvcm1cIl0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG59XHJcblx0IiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi4vc2hhZGVyL1NoYWRlckNvbXBvbmVudC50c1wiIC8+XHJcblxyXG5tb2R1bGUgbGNjJHJlbmRlciB7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHksIG1lbnV9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzKFwibGNjJHJlbmRlci5FZmZlY3RTcHJpdGVNYXNrXCIpXHJcbkBtZW51KFwiaTE4bjpsY2MtcmVuZGVyLm1lbnVfY29tcG9uZW50L0VmZmVjdFNwcml0ZU1hc2tcIilcclxuZXhwb3J0IGNsYXNzIEVmZmVjdFNwcml0ZU1hc2sgZXh0ZW5kcyBTaGFkZXJDb21wb25lbnQge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5NYXRlcmlhbClcclxuXHRfbWF0ZXJpYWw6IGNjLk1hdGVyaWFsID0gbnVsbDtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6IGNjLk1hdGVyaWFsLFxyXG5cdFx0dG9vbHRpcCA6IFwi5pWI5p6c5p2Q6LSoXCJcclxuXHR9KVxyXG5cdGdldCBtYXRlcmlhbCgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX21hdGVyaWFsO1xyXG5cdH1cclxuXHRzZXQgbWF0ZXJpYWwodmFsdWU6Y2MuTWF0ZXJpYWwpe1xyXG5cdFx0aWYodGhpcy5fbWF0ZXJpYWwgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9tYXRlcmlhbCA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbS5zZXRNYXRlcmlhbCgwLCB2YWx1ZSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcclxuXHRfc3ByaXRlRnJhbWU6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcclxuXHRAcHJvcGVydHkoe1xyXG5cdFx0dHlwZSA6IGNjLlNwcml0ZUZyYW1lLFxyXG5cdFx0dG9vbHRpcCA6IFwi5pWI5p6c57K+54G15binXCJcclxuXHR9KVxyXG5cdGdldCBzcHJpdGVGcmFtZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3Nwcml0ZUZyYW1lO1xyXG5cdH1cclxuXHRzZXQgc3ByaXRlRnJhbWUodmFsdWU6Y2MuU3ByaXRlRnJhbWUpe1xyXG5cdFx0aWYodGhpcy5fc3ByaXRlRnJhbWUgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9zcHJpdGVGcmFtZSA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnNoYWRlclNwcml0ZUZyYW1lLnNwcml0ZUZyYW1lID0gdmFsdWU7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcclxuXHRfbWFza0ZyYW1lOiBjYy5TcHJpdGVGcmFtZSA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5TcHJpdGVGcmFtZSxcclxuXHRcdHRvb2x0aXAgOiBcIuaViOaenOmBrue9qeW4p1wiXHJcblx0fSlcclxuXHRnZXQgbWFza0ZyYW1lKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fbWFza0ZyYW1lO1xyXG5cdH1cclxuXHRzZXQgbWFza0ZyYW1lKHZhbHVlOmNjLlNwcml0ZUZyYW1lKXtcclxuXHRcdGlmKHRoaXMuX21hc2tGcmFtZSAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX21hc2tGcmFtZSA9IHZhbHVlO1xyXG5cdFx0XHR0aGlzLnNoYWRlck1hc2tGcmFtZS5zcHJpdGVGcmFtZSA9IHZhbHVlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cmVuZGVyU3lzdGVtOlJlbmRlclN5c3RlbSA9IG51bGw7XHJcblx0c2hhZGVyU3ByaXRlRnJhbWU6U2hhZGVyU3ByaXRlRnJhbWUgPSBudWxsO1xyXG5cdHNoYWRlck1hc2tGcmFtZTpTaGFkZXJTcHJpdGVGcmFtZSA9IG51bGw7XHJcblxyXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XHJcblxyXG4gICAgb25FbmFibGUgKCkge1xyXG5cdFx0aWYoIXRoaXMuc2hhZGVyU3ByaXRlRnJhbWUpe1xyXG5cdFx0XHR0aGlzLnNoYWRlclNwcml0ZUZyYW1lID0gdGhpcy5nZXRTaGFkZXJDb21wb25lbnQoU2hhZGVyU3ByaXRlRnJhbWUsIFwic3ByaXRlZnJhbWVcIik7XHJcblx0XHRcdGlmKCF0aGlzLnNoYWRlclNwcml0ZUZyYW1lKXtcclxuXHRcdFx0XHRsZXQgc2hhZGVyID0gdGhpcy5hZGRDb21wb25lbnQoU2hhZGVyU3ByaXRlRnJhbWUpO1xyXG5cdFx0XHRcdHNoYWRlci50YWcgPSBcInNwcml0ZWZyYW1lXCI7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWID0gdHJ1ZTtcclxuXHRcdFx0XHRzaGFkZXIudXNlU2hhcGUgPSB0cnVlO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyU3ByaXRlRnJhbWUgPSBzaGFkZXI7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGlmKCF0aGlzLnNoYWRlck1hc2tGcmFtZSl7XHJcblx0XHRcdHRoaXMuc2hhZGVyTWFza0ZyYW1lID0gdGhpcy5nZXRTaGFkZXJDb21wb25lbnQoU2hhZGVyU3ByaXRlRnJhbWUsIFwibWFza2ZyYW1lXCIpO1xyXG5cdFx0XHRpZighdGhpcy5zaGFkZXJNYXNrRnJhbWUpe1xyXG5cdFx0XHRcdGxldCBzaGFkZXIgPSB0aGlzLmFkZENvbXBvbmVudChTaGFkZXJTcHJpdGVGcmFtZSk7XHJcblx0XHRcdFx0c2hhZGVyLnRhZyA9IFwibWFza2ZyYW1lXCI7XHJcblx0XHRcdFx0c2hhZGVyLnVzZVVWID0gdHJ1ZTtcclxuXHRcdFx0XHRzaGFkZXIuX3RleHR1cmVWYXIudW5pZk1hY3JvLm5hbWUgPSBcIlVTRV9NQVNLXCI7XHJcblx0XHRcdFx0c2hhZGVyLl90ZXh0dXJlVmFyLnVuaWZOYW1lID0gXCJtYXNrXCI7XHJcblx0XHRcdFx0c2hhZGVyLl91dlZhci5hdHRyTWFjcm8ubmFtZSA9IFwiVVNFX01BU0tcIjtcclxuXHRcdFx0XHRzaGFkZXIuX3V2VmFyLmF0dHJOYW1lID0gXCJhX3V2MVwiO1xyXG5cdFx0XHRcdHRoaXMuc2hhZGVyTWFza0ZyYW1lID0gc2hhZGVyO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZighdGhpcy5yZW5kZXJTeXN0ZW0pe1xyXG5cdFx0XHR0aGlzLnJlbmRlclN5c3RlbSA9IHRoaXMuZ2V0Q29tcG9uZW50KFJlbmRlclN5c3RlbSk7XHJcblx0XHRcdGlmKCF0aGlzLnJlbmRlclN5c3RlbSl7XHJcblx0XHRcdFx0dGhpcy5yZW5kZXJTeXN0ZW0gPSB0aGlzLmFkZENvbXBvbmVudChSZW5kZXJTeXN0ZW0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZihDQ19FRElUT1Ipe1xyXG5cdFx0XHR0aGlzLmNoZWNrTWF0ZXJpYWwoKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIOajgOafpeadkOi0qFxyXG5cdCAqL1xyXG5cdGFzeW5jIGNoZWNrTWF0ZXJpYWwoKXtcclxuXHRcdGlmKENDX0VESVRPUil7XHJcblx0XHRcdGlmKCF0aGlzLl9tYXRlcmlhbCl7XHJcblx0XHRcdFx0dGhpcy5tYXRlcmlhbCA9IGF3YWl0IFV0aWxzLmdldEFzc2V0QnlVVUlEPGNjLk1hdGVyaWFsPihVVUlELm1hdGVyaWFsc1tcImxjYy0yZC1zcHJpdGVfbWFza1wiXSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9IFwiLi9SZW5kZXJTeXN0ZW0udHNcIiAvPlxyXG5cclxubW9kdWxlIGxjYyRyZW5kZXIge1xyXG5cclxuLyoqXHJcbiAqIOa4suafk+ijhemFjeWZqFxyXG4gKi9cclxuLy9AdHMtaWdub3JlXHJcbmNsYXNzIFJlbmRlckFzc2VtYmxlciBleHRlbmRzIGNjLkFzc2VtYmxlciB7XHJcbiAgICB2ZXJ0aWNlc0NvdW50ID0gNDtcclxuICAgIGluZGljZXNDb3VudCA9IDY7XHJcbiAgICBmbG9hdHNQZXJWZXJ0ID0gMDtcclxuXHJcbiAgICAvKipcclxuXHQgKiDmuLLmn5PmlbDmja5cclxuXHQgKi9cclxuXHQvLyBAdHMtaWdub3JlXHJcblx0cmVuZGVyRGF0YTogY2MuUmVuZGVyRGF0YSA9IG51bGw7XHJcblx0XHJcblx0LyoqXHJcblx0ICog5Yid5aeL5YyWXHJcblx0ICovXHJcblx0aW5pdChyY29tcDpSZW5kZXJTeXN0ZW0pIHtcclxuXHRcdHN1cGVyLmluaXQocmNvbXApO1xyXG5cdFx0cmNvbXAuc2V0RGlydHkoKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICog5omA5pyJ6aG254K55pWw5o2u5aSn5bCPXHJcblx0ICovXHJcbiAgICBnZXQgdmVydGljZXNGbG9hdHMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmVydGljZXNDb3VudCAqIHRoaXMuZmxvYXRzUGVyVmVydDtcclxuICAgIH1cclxuXHRcclxuXHQvKipcclxuXHQgKiDojrflvpfmuLLmn5PmlbDmja7nvJPlhrJcclxuXHQgKi9cclxuICAgIGdldEJ1ZmZlcigpIHtcclxuICAgICAgICAvL0B0cy1pZ25vcmVcclxuICAgICAgICByZXR1cm4gY2MucmVuZGVyZXIuX2hhbmRsZS5nZXRCdWZmZXIoXCJtZXNoXCIsIHRoaXMuX3JlbmRlckNvbXAuZ2V0VmZtdCgpKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICog6YeN572u5riy5p+T5pWw5o2uXHJcblx0ICovXHJcbiAgICByZXNldFJlbmRlckRhdGEoKSB7XHJcbiAgICAgICAgLy8gQHRzLWlnbm9yZVxyXG4gICAgICAgIGxldCBkYXRhID0gbmV3IGNjLlJlbmRlckRhdGEoKTtcclxuICAgICAgICBkYXRhLmluaXQodGhpcyk7XHJcbiAgICAgICAgLy8gQHRzLWlnbm9yZVxyXG5cdFx0ZGF0YS5jcmVhdGVGbGV4RGF0YSgwLCB0aGlzLnZlcnRpY2VzQ291bnQsIHRoaXMuaW5kaWNlc0NvdW50LCB0aGlzLl9yZW5kZXJDb21wLmdldFZmbXQoKSk7XHJcblx0XHR0aGlzLnJlbmRlckRhdGEgPSBkYXRhO1xyXG5cclxuXHRcdHJldHVybiBkYXRhO1xyXG4gICAgfVxyXG5cdFxyXG4gICAgZmlsbEJ1ZmZlcnMoY29tcDpSZW5kZXJTeXN0ZW0sIHJlbmRlcmVyKSB7XHJcbiAgICAgICAgaWYgKHJlbmRlcmVyLndvcmxkTWF0RGlydHkpIHtcclxuICAgICAgICAgICAgY29tcC51cGRhdGVXb3JsZFZlcnRzKCk7XHJcbiAgICAgICAgfVxyXG5cdFx0XHJcbiAgICAgICAgbGV0IHJlbmRlckRhdGEgPSB0aGlzLnJlbmRlckRhdGE7XHJcbiAgICAgICAgbGV0IHZEYXRhID0gcmVuZGVyRGF0YS52RGF0YXNbMF07XHJcbiAgICAgICAgbGV0IGlEYXRhID0gcmVuZGVyRGF0YS5pRGF0YXNbMF07XHJcblxyXG4gICAgICAgIGxldCBidWZmZXIgPSB0aGlzLmdldEJ1ZmZlcigvKnJlbmRlcmVyKi8pO1xyXG4gICAgICAgIGxldCBvZmZzZXRJbmZvID0gYnVmZmVyLnJlcXVlc3QodGhpcy52ZXJ0aWNlc0NvdW50LCB0aGlzLmluZGljZXNDb3VudCk7XHJcblxyXG4gICAgICAgIC8vIGJ1ZmZlciBkYXRhIG1heSBiZSByZWFsbG9jLCBuZWVkIGdldCByZWZlcmVuY2UgYWZ0ZXIgcmVxdWVzdC5cclxuXHJcbiAgICAgICAgLy8gZmlsbCB2ZXJ0aWNlc1xyXG4gICAgICAgIGxldCB2ZXJ0ZXhPZmZzZXQgPSBvZmZzZXRJbmZvLmJ5dGVPZmZzZXQgPj4gMixcclxuICAgICAgICAgICAgdmJ1ZiA9IGJ1ZmZlci5fdkRhdGE7XHJcblxyXG4gICAgICAgIGlmICh2RGF0YS5sZW5ndGggKyB2ZXJ0ZXhPZmZzZXQgPiB2YnVmLmxlbmd0aCkge1xyXG4gICAgICAgICAgICB2YnVmLnNldCh2RGF0YS5zdWJhcnJheSgwLCB2YnVmLmxlbmd0aCAtIHZlcnRleE9mZnNldCksIHZlcnRleE9mZnNldCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdmJ1Zi5zZXQodkRhdGEsIHZlcnRleE9mZnNldCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBmaWxsIGluZGljZXNcclxuICAgICAgICBsZXQgaWJ1ZiA9IGJ1ZmZlci5faURhdGEsXHJcbiAgICAgICAgICAgIGluZGljZU9mZnNldCA9IG9mZnNldEluZm8uaW5kaWNlT2Zmc2V0LFxyXG4gICAgICAgICAgICB2ZXJ0ZXhJZCA9IG9mZnNldEluZm8udmVydGV4T2Zmc2V0O1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwLCBsID0gaURhdGEubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGlidWZbaW5kaWNlT2Zmc2V0KytdID0gdmVydGV4SWQgKyBpRGF0YVtpXTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIHVwZGF0ZVJlbmRlckRhdGEoc3ByaXRlOlJlbmRlclN5c3RlbSkge1xyXG4gICAgICAgIHNwcml0ZS51cGRhdGVSZW5kZXJEYXRhKCk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vQHRzLWlnbm9yZVxyXG5jYy5Bc3NlbWJsZXIucmVnaXN0ZXIoUmVuZGVyU3lzdGVtLCBSZW5kZXJBc3NlbWJsZXIpO1xyXG5cclxufVxyXG4iXX0=
